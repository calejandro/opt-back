﻿using System;
using System.Collections.Generic;
using Optimus.Models.Views;

namespace TestFechas
{
    class Program
    {
        static void Main(string[] args)
        {
            var turnos = GetShifts(new DateTime(2021, 03, 26, 01, 10, 0));
            var day = GetDay(new DateTime(2021, 03, 26, 01, 10, 0));

            turnos.Add(day);

            foreach (var item in turnos)
            {
                Console.WriteLine($"Turno {item.Turno}: {item.Inicio.ToString("dd/MM/yyyy HH:mm")} - {item.Fin.ToString("dd/MM/yyyy HH:mm")}");
            }
        }

        public static List<Shift> GetShifts(DateTime fecha)
        {
            // horas turnos 
            TimeSpan tn_i = new TimeSpan(22, 00, 0);
            TimeSpan tn_f = new TimeSpan(5, 59, 0);
            TimeSpan tm_i = new TimeSpan(6, 00, 0);
            TimeSpan tm_f = new TimeSpan(13, 59, 0);
            TimeSpan tt_i = new TimeSpan(14, 00, 0);
            TimeSpan tt_f = new TimeSpan(21, 59, 0);

            List<Shift> shifts = new List<Shift>();
            var plusDay = 0;
            var subtractDay = 0;
            var today = DateTime.Now;

            if (fecha.Hour >= 22)
                plusDay = 1;
            else
                subtractDay = -1;
            Shift turnoN = new Shift();
            turnoN.Turno = "Noche";
            turnoN.Fecha = fecha;
            turnoN.Inicio = fecha.AddDays(subtractDay).Date + tn_i;
            turnoN.Fin = fecha.AddDays(plusDay).Date + tn_f;
            turnoN.Paso = turnoN.Inicio <= today;
            shifts.Add(turnoN);


            Shift turnoM = new Shift();
            turnoM.Turno = "Mañana";
            turnoM.Fecha = fecha;
            turnoM.Inicio = fecha.AddDays(plusDay).Date + tm_i;
            turnoM.Fin = fecha.AddDays(plusDay).Date + tm_f;
            turnoM.Paso = turnoM.Inicio <= today;

            shifts.Add(turnoM);

            Shift turnoT = new Shift();
            turnoT.Turno = "Tarde";
            turnoT.Fecha = fecha;
            turnoT.Inicio = fecha.AddDays(plusDay).Date + tt_i;
            turnoT.Fin = fecha.AddDays(plusDay).Date + tt_f;
            turnoT.Paso = turnoT.Inicio <= today;

            shifts.Add(turnoT);


            return shifts;
        }

        public static Shift GetDay(DateTime fecha)
        {
            // horas turnos 

            TimeSpan te_i = new TimeSpan(22, 00, 0);
            TimeSpan te_f = new TimeSpan(21, 59, 0);

            Shift turnoTday = new Shift();
            turnoTday.Turno = "DiaEntero";
            turnoTday.Fecha = fecha;
            turnoTday.Inicio = fecha.AddDays(-1).Date + te_i;
            turnoTday.Fin = fecha.Date + te_f;
            turnoTday.Paso = true;


            return turnoTday;
        }
    }
}
