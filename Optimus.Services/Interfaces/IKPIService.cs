﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Services.Interfaces
{
    public interface IKpiService : IGenericServiceMapper<KpiViewModel, KpiViewModel, Guid, BaseFilter>
    {
    }
}
