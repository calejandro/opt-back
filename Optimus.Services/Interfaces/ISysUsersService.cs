﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using System;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ISysUsersService : IGenericServiceMapper<SysUsersViewModel, SysUsersViewModel, int, UserFilter>
    {
        Task AddSysUser(Guid idUserIdentity);
        Task<SysUsersViewModel> AssignUserToPlants(SysUsersViewModel usersView);
        Task RemoveGroup(int sysUserId, int groupId);
        Task AddGroup(int sysUserId, int groupId);
        Task<SysUsersViewModel> GetSysUserByUserCognito(Guid userId, string[] includes);
    }
}
