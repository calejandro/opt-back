﻿using Optimus.Models.Enum;
using Optimus.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface IStoreProceduresMongoService
    {
        Task<List<ProduccionEficiencia>> GetEfficiencyProductionMongo(int idLinea, List<int> idLineas, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShiftMongo(int idLinea, List<int> idLineas, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<KpiDepletion>> GetKpiDepletionForBottlesMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, GroupingTypeEnum tipoAgrupacion);
        Task<List<MachineActualStatus>> GetMachineActualStatusMongo(int idLinea);
        Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropiaHora(int idLinea, int idEquipo);
        Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrdersMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<TimeStops>> GetTimeStopsMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, int idEstado);
        Task<List<AverageTimeFailuresProduction>> AverageTimeFailuresProductionMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
    }
}
