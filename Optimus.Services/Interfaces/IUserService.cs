﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface IUserService : IGenericServiceMapper<UserViewModel, UserCreateViewModel, Guid, UserFilter>
    {
        Task<UserViewModel> Register(UserCreateViewModel userViewModel);
        Task ConfirmEmail(UserEmailConfirmation userEmailConfirmation);
        Task SendResetPassword(UserSendResetPassword userSendResetPassword);
        Task ResetPassword(UserResetPassword userResetPassword);
        Task<UserViewModel> UpdateProfile(UserCreateViewModel userViewModel);
        Task<UserViewModel> Undelete(UserViewModel userViewModel);
        Task<UserIdentity> RegisterUserIdentity(UserCreateViewModel userViewModel);
        Task<bool> ExistsByEmail(string email, Guid userId);
    }
}
