﻿using System;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Interfaces
{
    public interface IUserGroupService : IGenericServiceMapper<UserGroupViewModel, UserGroupViewModel, int, BaseFilter>
    {
    }
}
