﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ILineasProduccionService : IGenericServiceMapper<LineasProduccionViewModel, LineasProduccionCreateViewModel, int, LineaFilters>
    {
        Task<bool> HasProductionOrders(int lineId);
        Task<List<SapOrdenesProduccionViewModel>> ListOrders(int id, List<DateTime> from, List<DateTime> to, string[] includes);
    }
}
