﻿using Microsoft.AspNetCore.Http;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface IFileUrlService : IGenericServiceMapper<FileUrlViewModel, FileUrlViewModel, Guid, FileUrlFilter>
    {
        Task InsertFiles(List<IFormFile> files, FileUrlToBelongEnum toBelong, Guid idEntity);
        Task<(Stream, string,string)> DownloadFile(Guid idFile);
    }
}
