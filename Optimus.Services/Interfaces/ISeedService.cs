﻿using System;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ISeedService
    {
        Task Seed();
    }
}
