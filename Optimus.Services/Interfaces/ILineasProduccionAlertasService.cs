﻿using Microsoft.AspNetCore.Http;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ILineasProduccionAlertasService : IGenericServiceMapper<LineasProduccionAlertasViewModel, LineasProduccionAlertasCreateViewModel, int, LineaProduccionAlertaFilter>
    {
        Task InsertImagLineasProduccionAlertas(List<IFormFile> files, int idAlerta);
        Task<(Stream, string)> GetImagLineasProduccionAlertas(Guid idImage);

    }
}
