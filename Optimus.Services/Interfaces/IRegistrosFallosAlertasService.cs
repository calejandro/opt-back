﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Services.Interfaces
{
    public interface IRegistrosFallosAlertasService : IGenericServiceMapper<RegistrosFallosAlertasViewModel, RegistrosFallosAlertasCreateViewModel, int, RegistrosFallosAlertasFilter>
    {
    }
}
