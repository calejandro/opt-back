﻿using System;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Interfaces
{
    public interface ILineaProduccionObjetivosConfigService: IGenericServiceMapper<LineasProduccionObjetivosConfigViewModel, LineasProduccionObjetivosConfigViewModel, Guid, LineasProduccionObjetivosConfigFilter>
    {
    }
}
