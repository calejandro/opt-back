﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ISapOrdenesProduccionService : IGenericServiceMapper<SapOrdenesProduccionViewModel, SapOrdenesProduccionCreateViewModel, int, SapOrdenesProduccionFilter>
    {
        Task<SapOrdenesProduccionViewModel> LinkTemporalOrder(LinkTemporalOrder linkTemporalOrder);
    }
}
