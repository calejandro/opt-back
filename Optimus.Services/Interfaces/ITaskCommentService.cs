﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ITaskCommentService : IGenericServiceMapper<TaskCommentViewModel, TaskCommentCreateViewModel, Guid, TaskCommentFilter>
    {
        Task<TaskCommentViewModel>  InsertAsync(TaskCommentCreateViewModel entity, Guid idCognito);
    }
}
