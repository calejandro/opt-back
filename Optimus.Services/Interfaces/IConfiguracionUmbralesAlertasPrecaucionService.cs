﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Interfaces
{
    public interface IConfiguracionUmbralesAlertasPrecaucionService : IGenericServiceMapper<ConfiguracionUmbralesAlertasPrecaucionViewModel, ConfiguracionUmbralesAlertasPrecaucionCreateViewModel, int, CommonFilter>
    {
    }
}
