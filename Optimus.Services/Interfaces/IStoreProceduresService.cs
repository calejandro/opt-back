﻿using Optimus.Models.Enum;
using Optimus.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface IStoreProceduresService
    {
        // *EFICIENCIA*
        Task<List<ProduccionEficiencia>> GetEfficiencyProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShift(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<ProduccionEficienciaReal> GetEfficiencyTrueProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        // *PERFORMANCE DE EQUIPOS*
        Task<List<MachineActualStatus>> GetMachineActualStatus(int idLinea);
        Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrders(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<TimeStops>> GetTimeStops(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, int idEstado);
        Task<List<AverageTimeFailuresProduction>> GetAverageTimeFailuresProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin);
        List<Shift> GetShifts(DateTime fecha);
        Shift GetDay(DateTime fecha);
        Shift GetCurrentShift(DateTime dateTime);
        string GetNameShift(DateTime fechaHoraInicio, DateTime fechaHoraFin);
        Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropiaHora(int idLinea, int idEquipo);
        // *MERMAS*
        Task<List<KpiDepletion>> GetKpiDepletionForBottles(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, GroupingTypeEnum tipoAgrupacion);
    }
}
