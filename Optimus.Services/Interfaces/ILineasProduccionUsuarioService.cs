﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Services.Interfaces
{
    public interface ILineasProduccionUsuarioService : IGenericServiceMapper<LineasProduccionUsuarioViewModel, LineasProduccionUsuarioCreateViewModel, int, LineasProduccionUsuarioFilter>
    {
        Task<List<LineasProduccionUsuarioTakeViewModel>> Take(List<LineasProduccionUsuarioTakeViewModel> lineasProduccionUsuarios, Guid userId);
        Task Clear(Guid userId);
        Task<bool> IsValidStatusTake(int id, DateTime date);
    }
}
