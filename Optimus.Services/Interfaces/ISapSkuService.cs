﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface ISapSkuService : IGenericServiceMapper<SapSkuViewModel, SapSkuCreateViewModel, int, SapSkuFilter>
    {
        Task<List<MarcaViewModel>> MarcaSaborFormato();
        Task<List<Tuple<int, string>>> ListSelectSKU(SkuByLineViewFilter filter);
        Task<List<string>> ListBrand(SkuByLineViewFilter filter);
        Task<List<string>> ListFlavor(SkuByLineViewFilter filter);
        Task<List<string>> ListFormat(SkuByLineViewFilter filter);

    }
}
