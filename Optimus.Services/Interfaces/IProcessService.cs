﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Services.Interfaces
{
    public interface IProcessService : IGenericServiceMapper<ProcessViewModel, ProcessViewModel, Guid, BaseFilter>
    {
    }
}
