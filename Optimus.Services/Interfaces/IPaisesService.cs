﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.Interfaces
{
    public interface IPaisesService : IGenericServiceMapper<PaisesViewModel, PaisesCreateViewModel, int, BaseFilter>
    {
    }
}
