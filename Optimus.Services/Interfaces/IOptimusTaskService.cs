﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Interfaces
{
    public interface IOptimusTaskService : IGenericServiceMapper<OptimusTaskViewModel, OptimusTaskCreateViewModel, Guid, OptimusTaskFilter>
    {
        Task<List<OptimusTaskViewModel>> MyOptimusTaskListAsync(Guid idCognito, TypeUserToOptimusTaskEnum typeUser, string[] includes= null, OptimusTaskFilter filter = null, PaginatorRequestModel paginator = null, List<Order> orders = null);
        Task<OptimusTaskViewModel> ArchiveUnarchiveAsync(Guid id);
        Task<OptimusTaskViewModel> InsertAsync(OptimusTaskCreateViewModel entity, Guid idCognito);
        Task<int> MyOptimusTaskCountAsync(Guid idCognito, TypeUserToOptimusTaskEnum typeUser, OptimusTaskFilter filter = null, string[] includes = null);
        Task<OptimusTaskViewModel> UpdateAsync(Guid idCognito, OptimusTaskCreateViewModel entity);
        Task DeleteAsync(Guid idCognito, Guid id);
    }
}
