﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.Interfaces
{
    public interface ILineasProduccionEficienciaMesService : IGenericServiceMapper<LineasProduccionEficienciaMesViewModel, LineasProduccionEficienciaMesCreateViewModel, int, LineaFilters>
    {
    }
}
