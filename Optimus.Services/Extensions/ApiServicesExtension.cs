﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Optimus.Commons.Services;
using Optimus.Services.Implementations;
using Optimus.Services.Implementations.Mongo;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.Jobs;

namespace Optimus.Services.Extensions
{
    public static class ApiServicesExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IAlertaMermaConfiguracionesService, AlertaMermaConfiguracionesService>();
            services.AddScoped<IConfiguracionAlertasParadaMaquinaService, ConfiguracionAlertasParadaMaquinaService>();
            services.AddScoped<IConfiguracionUmbralesTmefTmprService, ConfiguracionUmbralesTmefTmprService>();
            services.AddScoped<IEquiposEstadosService, EquiposEstadosService>();
            services.AddScoped<IEquiposService, EquiposService>();
            services.AddScoped<ILineasProduccionAlertasService, LineasProduccionAlertasService>();
            services.AddScoped<ILineasProduccionService, LineasProduccionService>();
            services.AddScoped<ILineasProduccionUsuarioService, LineasProduccionUsuarioService>();
            services.AddScoped<IPaisesService, PaisesService>();
            services.AddScoped<IPlantasService, PlantasService>();
            services.AddScoped<ITiposFallosService, TiposFallosService>();
            services.AddScoped<ISapSkuService, SapSkuService>();
            services.AddScoped<ICausasFallosService, CausasFallosService>();
            services.AddScoped<ILineasProduccionEficienciaMesService, LineasProduccionEficienciaMesService>();
            services.AddScoped<ISysUserReportConfigurationService, SysUserReportConfigurationService>();
            services.AddScoped<ISapOrdenesProduccionService, SapOrdenesProduccionService>();
            services.AddScoped<ISysUsersService, SysUsersService>();
            services.AddScoped<IStoreProceduresService, StoreProceduresService>();
            services.AddScoped<ISeedService, SeedService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IStoreProceduresMongoService, StoreProceduresMongoService>();
            services.AddScoped<IMongoService, MongoService>();
            services.AddScoped<IUserGroupService, UserGroupService>();
            services.AddScoped<IConfiguracionUmbralesAlertasPrecaucionService, ConfiguracionUmbralesAlertasPrecaucionService>();
            services.AddScoped<IOptimusTaskService, OptimusTaskService>();
            services.AddScoped<ITaskCommentService, TaskCommentService>();
            services.AddScoped<IFileUrlService, FileUrlService>();
            services.AddScoped<IKpiService, KpiService>();
            services.AddScoped<ISectorService, SectorService>();
            services.AddScoped<IProcessService, ProcessService>();
            services.AddScoped<IRegistrosFallosAlertasService, RegistrosFallosAlertasService>();




            services.AddScoped<IAverageTimeFailuresProductionService, AverageTimeFailuresProductionService>();
            services.AddScoped<IKpiDepletionService, KpiDepletionService>();
            services.AddScoped<IMachineStatusProductionOrderService, MachineStatusProductionOrderService>();
            services.AddScoped<IProduccionEficienciaByShiftService, ProduccionEficienciaByShiftService>();
            services.AddScoped<IProduccionEficienciaService, ProduccionEficienciaService>();
            services.AddScoped<ITimeStopsService, TimeStopsService>();
            services.AddScoped<ILineaProduccionObjetivosConfigService, LineaProduccionObjetivosConfigService>();
            services.AddScoped<IAcumEficienciasService, AcumEficienciasService>();


            return services;
        }
        public static IServiceCollection AddJobsServices(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddSingleton<ProduccionEficienciaByShiftJobs>();
            services.AddSingleton<KpiDepletionJobs>();
            services.AddSingleton<MachineActualStatusJobs>();
            services.AddSingleton<MachineStatusProductionOrderJobs>();
            services.AddSingleton<TimeStopsJobs>();
            services.AddSingleton<AverageTimeFailuresProductionJobs>();

            services.AddSingleton(new JobSchedule(
             jobKey: "ProduccionEficienciaJobs",
             jobType: typeof(ProduccionEficienciaJobs),
             cronExpression: "50 0/1 * ? * * *",
             description: "",
             runStart: true)
          );

            services.AddSingleton(new JobSchedule(
            jobKey: "ProduccionEficienciaByShiftJobs",
            jobType: typeof(ProduccionEficienciaByShiftJobs),
            cronExpression: "50 0/1 * ? * * *",
            description: "",
            runStart: true)
         );
            services.AddSingleton(new JobSchedule(
            jobKey: "KpiDepletionJobs",
            jobType: typeof(KpiDepletionJobs),
            cronExpression: "0 0/30 * ? * * *",
            description: "",
            runStart: true)
          );
            services.AddSingleton(new JobSchedule(
            jobKey: "MachineActualStatusJobs",
            jobType: typeof(MachineActualStatusJobs),
            cronExpression: "0 0/30 * ? * * *",
            description: "",
            runStart: true)
         );
            services.AddSingleton(new JobSchedule(
            jobKey: "MachineStatusProductionOrderJobs",
            jobType: typeof(MachineStatusProductionOrderJobs),
            cronExpression: "50 0/1 * ? * * *",
            description: "",
            runStart: true)
         );
            services.AddSingleton(new JobSchedule(
            jobKey: "TimeStopsJobs",
            jobType: typeof(TimeStopsJobs),
            cronExpression: "50 0/1 * ? * * *",
            description: "",
            runStart: true)
         );
            services.AddSingleton(new JobSchedule(
            jobKey: "AverageTimeFailuresProductionJobs",
            jobType: typeof(AverageTimeFailuresProductionJobs),
            cronExpression: "50 0/1 * ? * * *",
            description: "",
            runStart: true)
         );

            return services;
        }

    }
}
