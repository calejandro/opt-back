﻿using Optimus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Models.Views;
using System.Collections.Generic;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;
using MongoDB.Driver;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using Optimus.Services.SignalR;
using Optimus.Services.Interfaces.Mongo;

namespace Optimus.Services.Jobs
{

    public class MachineStatusProductionOrderJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        public MachineStatusProductionOrderJobs(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresService>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var machineStatusProductionOrderService = scope.ServiceProvider.GetService<IMachineStatusProductionOrderService>();


                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);


                    foreach (var linea in lineas)
                    {
                        await machineStatusProductionOrderService.InsertMachineStatusProductionOrder(linea.IdLinea, turnoDia);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

        }
    }
}
