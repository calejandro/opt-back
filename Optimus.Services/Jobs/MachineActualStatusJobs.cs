﻿using Optimus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Models.Views;
using System.Collections.Generic;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;
using MongoDB.Driver;
using System.Linq;
using Optimus.Models.Enum;
using Optimus.Services.SignalR;
using Microsoft.AspNetCore.SignalR;

namespace Optimus.Services.Jobs
{

    public class MachineActualStatusJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IHubContext<OptimusHub> hubContext;

        public MachineActualStatusJobs(IServiceProvider serviceProvider, IHubContext<OptimusHub> hubContext)
        {
            this.hubContext = hubContext;
            this.serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresService>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var machineActualStatusRepository = scope.ServiceProvider.GetService<IMachineActualStatusRepository>();

                    var turno = storeProcedures.GetDay(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turno.Inicio, turno.Fin);


                    foreach (var linea in lineas)
                    {
                        var machineActualStatus = await storeProcedures.GetMachineActualStatus(linea.IdLinea);

                        // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                        //filtros
                        var builder = Builders<MachineActualStatusMongo>.Filter;
                        var filter = builder.Eq(widget => widget.IdLinea, linea.IdLinea);

                        MachineActualStatusMongo machineActualStatusMongo = await machineActualStatusRepository.GetAsync(filter);

                        if (machineActualStatusMongo != null)
                        {
                            machineActualStatusMongo.MachineActualStatus = machineActualStatus;
                            machineActualStatusMongo.Updated = DateTime.Now;
                            machineActualStatusMongo.UpdatedBy = "job";
                            await machineActualStatusRepository.UpdateAsync(machineActualStatusMongo);
                        }
                        else
                        {

                            await machineActualStatusRepository.InsertAsync(new MachineActualStatusMongo()
                            {
                                IdLinea = linea.IdLinea,
                                MachineActualStatus = machineActualStatus,
                                Created = DateTime.Now,
                                CreatedBy = "job"
                            });
                            machineActualStatusMongo = await machineActualStatusRepository.GetAsync(filter);
                        }
                        await hubContext.Clients.Group(linea.IdLinea.ToString()).SendAsync("MachineActualStatus", machineActualStatusMongo);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
