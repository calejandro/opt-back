﻿using Optimus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Models.Views;
using System.Collections.Generic;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;
using MongoDB.Driver;
using System.Linq;
using Microsoft.Extensions.Logging;
using Optimus.Services.SignalR;
using Microsoft.AspNetCore.SignalR;
using Optimus.Services.Interfaces.Mongo;

namespace Optimus.Services.Jobs
{

    public class ProduccionEficienciaJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        public ProduccionEficienciaJobs(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context )
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresService>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var produccionEficienciaService = scope.ServiceProvider.GetService<IProduccionEficienciaService>();

                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var turnos = storeProcedures.GetShifts(DateTime.Now);
                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    turnos.Add(turnoDia);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);


                    foreach (var linea in lineas)
                    {
                        await produccionEficienciaService.InsertProduccionEficiencia(linea.IdLinea, turnos);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
           
        }
    }
}
