﻿using Optimus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Models.Views;
using System.Collections.Generic;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;
using MongoDB.Driver;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using Optimus.Services.SignalR;
using Optimus.Services.Interfaces.Mongo;

namespace Optimus.Services.Jobs
{

    public class AverageTimeFailuresProductionJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IHubContext<OptimusHub> hubContext;
        public AverageTimeFailuresProductionJobs(IServiceProvider serviceProvider, IHubContext<OptimusHub> hubContext)
        {
            this.serviceProvider = serviceProvider;
            this.hubContext = hubContext;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var averageTimeFailuresProductionService = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionService>();
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresService>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var averageTimeFailuresProductionMongo = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionRepository>();


                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);


                    foreach (var linea in lineas)
                    {
                        await averageTimeFailuresProductionService.InsertAverageTimeFailuresProduction(linea.IdLinea, turnoDia);
                    }
                }
            }
            catch (Exception e )
            {
                throw e;
            }

        }
    }
}
