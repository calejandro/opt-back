﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.Implementations
{
    public class EquiposEstadosService : GenericServiceMapper<EquiposEstados, EquiposEstadosCreateViewModel, int, EquiposEstadosFilter, EquiposEstadosViewModel, IEquiposEstadosRepository>, IEquiposEstadosService
    {
        public EquiposEstadosService(IEquiposEstadosRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
