﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class CausasFallosService : GenericServiceMapper<TiposFallos, CausasFallosCreateViewModel, int, TipoFallosFilter, CausasFallosViewModel, ICausasFallosRepository>, ICausasFallosService
    {
        public CausasFallosService(ICausasFallosRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
        public async override Task<CausasFallosViewModel> InsertAsync(CausasFallosCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<TiposFallos>(entity)))
                throw new BussinessException(entity.Tipo == TipoModoFalloEnum.Causa? ExceptionConstants.ERROR_CAUSA_DUPLICADO: ExceptionConstants.ERROR_FALLOS_DUPLICADO, 
                    BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.InsertAsync(entity);
        }
        public async override Task<CausasFallosViewModel> UpdateAsync(CausasFallosCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<TiposFallos>(entity)))
                throw new BussinessException(entity.Tipo == TipoModoFalloEnum.Causa ? ExceptionConstants.ERROR_CAUSA_DUPLICADO : ExceptionConstants.ERROR_FALLOS_DUPLICADO,
                    BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.UpdateAsync(entity);
        }
    }
}
