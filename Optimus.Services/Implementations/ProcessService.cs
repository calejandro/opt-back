﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Services.Implementations
{
    public class ProcessService : GenericServiceMapper<Process, ProcessViewModel, Guid, BaseFilter, ProcessViewModel, IProcessRepository>, IProcessService
    {
        public ProcessService(IProcessRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
