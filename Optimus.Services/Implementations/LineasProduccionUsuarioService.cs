﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Optimus.Commons.Services.Quartz;
using System.Collections.Generic;
using System;
using System.Linq;
using Optimus.Models.Views;
using Optimus.Services.Interfaces.Mongo;
using Microsoft.Extensions.DependencyInjection;

namespace Optimus.Services.Implementations
{
    public class LineasProduccionUsuarioService : GenericServiceMapper<LineasProduccionUsuario, LineasProduccionUsuarioCreateViewModel, int, LineasProduccionUsuarioFilter, LineasProduccionUsuarioViewModel, ILineasProduccionUsuarioRepository>, ILineasProduccionUsuarioService
    {
        private readonly ISapOrdenesProduccionRepository sapOrdenesProduccionRepository;
        private readonly IMongoService mongoService;
        private readonly IQuartzService quartzService;
        private readonly ISysUsersService sysUsersService;
        private readonly IStoreProceduresService storeProceduresService;
        private readonly IEquiposEstadosRepository equiposEstadosRepository;

        private readonly IAverageTimeFailuresProductionService averageTimeFailuresProductionService;
        private readonly IKpiDepletionService kpiDepletionService;
        private readonly IMachineStatusProductionOrderService machineStatusProductionOrderService;
       private readonly IProduccionEficienciaService produccionEficienciaService;
        private readonly IProduccionEficienciaByShiftService produccionEficienciaByShiftService;
        private readonly ITimeStopsService timeStopsService;
        private readonly IServiceProvider serviceProvider;
        private readonly IAcumEficienciaRepository acumEficienciaRepository;

        public LineasProduccionUsuarioService(ISapOrdenesProduccionRepository sapOrdenesProduccionRepository,
        ILineasProduccionUsuarioRepository lineasProduccionUsuarioRepository, 
        IMapper mapper,
        IMongoService mongoService,
        IQuartzService quartzService,
        ISysUsersService sysUsersService,
        IStoreProceduresService storeProceduresService,
        IEquiposEstadosRepository equiposEstadosRepository,
        IAverageTimeFailuresProductionService averageTimeFailuresProductionService,
        IKpiDepletionService kpiDepletionService,
        IMachineStatusProductionOrderService machineStatusProductionOrderService,
        IProduccionEficienciaService produccionEficienciaService,
        IProduccionEficienciaByShiftService produccionEficienciaByShiftService,
        ITimeStopsService timeStopsService,
        IServiceProvider serviceProvider,
        IAcumEficienciaRepository acumEficienciaRepository) : base(lineasProduccionUsuarioRepository, mapper)
        {
            this.sapOrdenesProduccionRepository = sapOrdenesProduccionRepository;
            this.mongoService = mongoService;
            this.quartzService = quartzService;
            this.sysUsersService = sysUsersService;
            this.storeProceduresService = storeProceduresService;
            this.averageTimeFailuresProductionService = averageTimeFailuresProductionService;
            this.kpiDepletionService = kpiDepletionService;
            this.machineStatusProductionOrderService = machineStatusProductionOrderService;
            this.produccionEficienciaService = produccionEficienciaService;
            this.produccionEficienciaByShiftService = produccionEficienciaByShiftService;
            this.timeStopsService = timeStopsService;
            this.equiposEstadosRepository = equiposEstadosRepository;
            this.serviceProvider = serviceProvider;
            this.acumEficienciaRepository = acumEficienciaRepository;
        }

        private async  Task RunCache(int idLinea, DateTime fecha)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var averageTimeFailuresProductionService = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionService>();
                    var kpiDepletionService = scope.ServiceProvider.GetService<IKpiDepletionService>();
                    var machineStatusProductionOrderService = scope.ServiceProvider.GetService<IMachineStatusProductionOrderService>();
                    var produccionEficienciaByShiftService = scope.ServiceProvider.GetService<IProduccionEficienciaByShiftService>();
                    var produccionEficienciaService = scope.ServiceProvider.GetService<IProduccionEficienciaService>();
                    var timeStopsService = scope.ServiceProvider.GetService<ITimeStopsService>();
                    var acumEficienciaRepository = scope.ServiceProvider.GetService<IAcumEficienciaRepository>();
                    var lineasProduccionRepository = scope.ServiceProvider.GetService<ILineasProduccionRepository>();
                    var storeProceduresRepository = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var equiposEstadosRepository = scope.ServiceProvider.GetService<IEquiposEstadosRepository>();



                    var linea = await lineasProduccionRepository.GetAsync(idLinea);
                    var estados = await equiposEstadosRepository.ListAsync(null, null, new EquiposEstadosFilter() { TipoEquipo = 1 });

                    var turnos = storeProceduresRepository.GetShifts(fecha);
                    var turnoDia = storeProceduresRepository.GetDay(fecha);
                    turnos.Add(turnoDia);

                    await averageTimeFailuresProductionService.InsertAverageTimeFailuresProduction(idLinea, turnoDia);
                    await kpiDepletionService.InsertKpiDepletion(idLinea, turnos);
                    await machineStatusProductionOrderService.InsertMachineStatusProductionOrder(idLinea, turnoDia);
                    await produccionEficienciaByShiftService.InsertProduccionEficienciaByShift(idLinea, turnos);
                    await produccionEficienciaService.InsertProduccionEficiencia(idLinea, turnos);
                    await timeStopsService.InsertTimeStops(idLinea, turnoDia, estados);

                    await acumEficienciaRepository.CreateEficienciaDia(linea, turnoDia.Fin, turnoDia);
                    await acumEficienciaRepository.CreateEficienciaMes(linea, turnoDia.Fin);
                    await acumEficienciaRepository.CreateEficienciaAño(linea, turnoDia.Fin);

                }
            }
            catch (Exception e)
            {
                throw;
            }

        }
        private async Task DeleteCache(int id)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();

                    //var lineasProduccionUsuario = (await lineasProduccionUsuarioRepository.ListAsync(null, null, new LineasProduccionUsuarioFilters { IdLinea = idLinea }, null, true)).FirstOrDefault();
                    var lineasProduccionUsuario = await lineasProduccionUsuarioRepository.GetAsync(id);
                    var previousFechaToma = lineasProduccionUsuario.FechaToma.Value;
                    await acumEficienciaRepository.DeleteEficienciaDia(lineasProduccionUsuario.IdLinea, previousFechaToma);
                    await this.mongoService.DeleteMongo(lineasProduccionUsuario.IdLinea, previousFechaToma);
                }
            }
            catch (Exception e )
            {

                throw;
            }
        }
        public async override Task<LineasProduccionUsuarioViewModel> InsertAsync(LineasProduccionUsuarioCreateViewModel entity)
        {
            try
            {
                if (entity.Id == 0)
                {
                    if (entity.SapOrdenesProduccion == null)
                    {
                        return await base.InsertAsync(entity);
                    }
                    else
                    {
                        if (entity.IdOrdenSap == 0 || entity.SapOrdenesProduccion == null || entity.IdOrdenSap != entity.SapOrdenesProduccion.IdOrdenSap)
                            throw new BussinessException(ExceptionConstants.ERROR_LPU_ID_ORDEN_SAP_NULL, BusinessExceptionCodeEnumeration.LPU_IdOrdenSap_Null);
                        var lineasProduccionUsuario = await this.MapSapOrdenes(entity);
                        if (lineasProduccionUsuario.FechaLiberacion != null)
                        {
                            lineasProduccionUsuario.Borrado = true; lineasProduccionUsuario.Borrado = true;
                        }                            
                        var result = this.mapper.Map<LineasProduccionUsuarioViewModel>(await base.genericRepository.InsertAsync(lineasProduccionUsuario));

                        this.RunCache(result.IdLinea, result.FechaToma.Value);
                        return result;
                    }
                }

                return this.mapper.Map<LineasProduccionUsuarioViewModel>(entity);
            }
            catch (Exception e)
            {
                throw;
            }

            
        }
        public async override Task<LineasProduccionUsuarioViewModel> UpdateAsync(LineasProduccionUsuarioCreateViewModel entity)
        {
            try
            {
                if (entity.IdOrdenSap == 0 || entity.SapOrdenesProduccion == null || entity.IdOrdenSap != entity.SapOrdenesProduccion.IdOrdenSap)
                    throw new BussinessException(ExceptionConstants.ERROR_LPU_ID_ORDEN_SAP_NULL, BusinessExceptionCodeEnumeration.LPU_IdOrdenSap_Null);

              
                 var lineasProduccionUsuario = await this.MapSapOrdenes(entity);

                if (lineasProduccionUsuario.FechaLiberacion != null)
                    lineasProduccionUsuario.Borrado = true;
                else
                    lineasProduccionUsuario.Borrado = false;
               await this.DeleteCache(entity.Id);
                var resul = this.mapper.Map<LineasProduccionUsuarioViewModel>(await base.genericRepository.UpdateAsync(lineasProduccionUsuario));

              
                this.RunCache(resul.IdLinea, resul.FechaToma.Value);
                return resul;
            }
            catch (Exception e)
            {

                throw;
            }
           
        }
        private async Task<LineasProduccionUsuario> MapSapOrdenes(LineasProduccionUsuarioCreateViewModel entity)
        {
            LineasProduccionUsuario lineasProduccionUsuario = this.mapper.Map<LineasProduccionUsuario>(entity);
            var sapOrden = await this.sapOrdenesProduccionRepository.GetAsync(entity.IdOrdenSap);
            sapOrden.FhFinProduccion = lineasProduccionUsuario.SapOrdenesProduccion.FhFinProduccion;
            sapOrden.FhIncioProduccion = lineasProduccionUsuario.SapOrdenesProduccion.FhIncioProduccion;
            sapOrden.FechaSupervision = lineasProduccionUsuario.SapOrdenesProduccion.FechaSupervision;
            lineasProduccionUsuario.SapOrdenesProduccion = sapOrden;
            return lineasProduccionUsuario;
        }
        public async override Task DeleteAsync(int id)
        {
             await this.genericRepository.CreateExecutionStrategy().ExecuteAsync(async () =>
            {
                using (var transaction = this.genericRepository.BeginTransaction())
                {
                    var lineasProduccionUsuario = await base.genericRepository.GetAsync(id);
                    var sapOrden = await this.sapOrdenesProduccionRepository.GetAsync(lineasProduccionUsuario.IdOrdenSap);
                    if (lineasProduccionUsuario.FechaToma.HasValue)
                    {
                        await this.mongoService.DeleteMongo(lineasProduccionUsuario.IdLinea, lineasProduccionUsuario.FechaToma.Value);
                        await acumEficienciaRepository.DeleteEficienciaDia(lineasProduccionUsuario.IdLinea, lineasProduccionUsuario.FechaToma.Value);
                    }
                    sapOrden.FhFinProduccion = null;
                    sapOrden.FhIncioProduccion = null;
                    sapOrden.FechaSupervision = null;
                    await this.sapOrdenesProduccionRepository.UpdateAsync(sapOrden);
                    await base.DeleteAsync(id);
                    transaction.Commit();
                }

            });
           
        }


        public async Task Clear(Guid userId)
        {
            await this.genericRepository.CreateExecutionStrategy().ExecuteAsync(async () =>
            {

                using (var transaction = this.genericRepository.BeginTransaction())
                {
                    SysUsersViewModel sysUsersViewModel = await this.sysUsersService.GetSysUserByUserCognito(userId, null);
                    List<LineasProduccionUsuario> lineasTaken =
                        await this.genericRepository.ListAsync(null, new string[] { "SapOrdenesProduccion" }, new LineasProduccionUsuarioFilter
                        {
                            IsTake = true,
                            UserId = sysUsersViewModel.UserId,
                        });

                    foreach (var item in lineasTaken)
                    {
                        item.FechaLiberacion = DateTime.Now;
                        item.Borrado = true;
                        if(item.Supervisada != null && item.Supervisada.Value)
                        {
                            item.SapOrdenesProduccion.FhFinProduccion = DateTime.Now;
                        }
                                                
                        await this.genericRepository.UpdateAsync(item);
                    }

                    transaction.Commit();
                }

            });
              
        }


        public async Task<List<LineasProduccionUsuarioTakeViewModel>> Take(List<LineasProduccionUsuarioTakeViewModel> lineasProduccionUsuarios, Guid userId)
        {
            
            await this.genericRepository.CreateExecutionStrategy().ExecuteAsync(async () =>
            {

                using(var transaction = this.genericRepository.BeginTransaction())
                {
                    SysUsersViewModel sysUsersViewModel = await this.sysUsersService.GetSysUserByUserCognito(userId, null);
                    List<LineasProduccionUsuario> lineasTaken =
                        await this.genericRepository.ListAsync(null, new string[] { "SapOrdenesProduccion" }, new LineasProduccionUsuarioFilter
                        {
                            IsTake = true,
                            UserId = sysUsersViewModel.UserId,
                        });
                    List<LineasProduccionUsuario> takeOffLines = lineasTaken.Where(w => lineasProduccionUsuarios.Any(a => a.IdLinea != w.IdLinea)).ToList();
                    foreach (var item in takeOffLines)
                    {
                        if(item.Supervisada == true)
                        {
                            item.FechaLiberacion = DateTime.Now;
                            item.Borrado = true;
                            item.SapOrdenesProduccion.FhFinProduccion = DateTime.Now;
                            await this.genericRepository.UpdateAsync(item);
                        }
                        
                    }

                    foreach (var item in lineasProduccionUsuarios)
                    {
                        if (item.Id == 0)
                        {
                            var r = await this.InsertAsync(item);
                            var order = await this.sapOrdenesProduccionRepository.GetAsync(item.IdOrdenSap);
                            if (item.Supervisada == true)
                            {
                                
                                if (order.FhIncioProduccion == null)
                                {
                                    order.FhIncioProduccion = item.FechaToma;
                                }
                                order.FhFinProduccion = this.storeProceduresService.GetCurrentShift(DateTime.Now).Fin;
                                await this.sapOrdenesProduccionRepository.UpdateAsync(order);
                            }
                            
                            item.Id = r.Id;
                        }
                        else if(item.Supervisada == true)
                        {
                            var order = await this.sapOrdenesProduccionRepository.GetAsync(item.IdOrdenSap);
                            order.FhFinProduccion = this.storeProceduresService.GetCurrentShift(DateTime.Now).Fin;
                            await this.sapOrdenesProduccionRepository.UpdateAsync(order);
                        }                       
                        
                    }

                    transaction.Commit();
                }                
            });
            
            

            return lineasProduccionUsuarios;
        }
        public async Task<bool> IsValidStatusTake(int id, DateTime date)
        {
            var to = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
            var from = to.AddHours(-1);

            if (date.Date != DateTime.Now.Date)
            {
                Shift day = this.storeProceduresService.GetDay(date);
                from = day.Inicio;
                to = day.Fin;
            }

            if (await this.genericRepository.IsLineaConToma(id, from, to))
            {
                return true;
            }


            var efficiencia = await this.storeProceduresService.GetEfficiencyTrueProduction(id, from, to);
            if (efficiencia != null &&
                efficiencia.BotellasProducidas > 500 &&
                efficiencia.IdOrdenSAP == 0)
            {
                return false;
            }

            return true;
        }

    }
}
