﻿using AutoMapper;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;


namespace Optimus.Services.Implementations
{
    public class ConfiguracionUmbralesAlertasPrecaucionService : GenericServiceMapper<ConfiguracionUmbralesAlertasPrecaucion, ConfiguracionUmbralesAlertasPrecaucionCreateViewModel, int, CommonFilter, ConfiguracionUmbralesAlertasPrecaucionViewModel, IConfiguracionUmbralesAlertasPrecaucionRepository>, IConfiguracionUmbralesAlertasPrecaucionService
    {
        public ConfiguracionUmbralesAlertasPrecaucionService(IConfiguracionUmbralesAlertasPrecaucionRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
