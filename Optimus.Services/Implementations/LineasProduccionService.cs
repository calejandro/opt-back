﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class LineasProduccionService : GenericServiceMapper<LineasProduccion, LineasProduccionCreateViewModel, int, LineaFilters, LineasProduccionViewModel, ILineasProduccionRepository>, ILineasProduccionService
    {
        private readonly ILineasProdSapToLineasProdElpRepository lineasProdSapToLineasProdElpRepository;
        private readonly ISapOrdenesProduccionRepository sapOrdenesProduccionRepository;

        public LineasProduccionService(ILineasProduccionRepository systemModuleRepository, IMapper mapper,
            ILineasProdSapToLineasProdElpRepository lineasProdSapToLineasProdElpRepository,
            ISapOrdenesProduccionRepository sapOrdenesProduccionRepository
          ) : base(systemModuleRepository, mapper)
        {
            this.lineasProdSapToLineasProdElpRepository = lineasProdSapToLineasProdElpRepository;
            this.sapOrdenesProduccionRepository = sapOrdenesProduccionRepository;
        }

        /*public async Task<List<LineasProduccionViewModel>> ListProdSapAsync()
        {
            var lineasProdSapToLineasProdElp = await this.lineasProdSapToLineasProdElpRepository.ListAsync(null,includes: new string[] { "LineasProduccionSap" });

            var lineasProdSap = this.mapper.Map<List<LineasProduccionViewModel>>(lineasProdSapToLineasProdElp.Select(x => x.LineasProduccionSap));
            return lineasProdSap;
        }*/


        public async Task<bool> HasProductionOrders(int lineId)
        {
            LineasProdSapToLineasProdElp lineasProdSapToLineasProdElp = await this.lineasProdSapToLineasProdElpRepository.GetAsync(lineId);
            return this.sapOrdenesProduccionRepository.HasProductOrder(lineasProdSapToLineasProdElp.IdLineaSap);
        }

        public override async Task<List<LineasProduccionViewModel>> ListAsync(string[] includes = null, LineaFilters filter = null, PaginatorRequestModel requestModel = null, List<Order> orders = null)
        {
            var list = await base.ListAsync(includes, filter, requestModel, orders);
            foreach (var item in list)
            {
                LineasProdSapToLineasProdElp lineasProdSapToLineasProdElp = await this.lineasProdSapToLineasProdElpRepository.GetAsync(item.IdLinea);
                if(lineasProdSapToLineasProdElp != null)
                {
                    item.HasProductionOrder = this.sapOrdenesProduccionRepository.HasProductOrder(lineasProdSapToLineasProdElp.IdLineaSap);
                }                
            }

            return list;

        }

        public async Task<List<SapOrdenesProduccionViewModel>> ListOrders(int id, List<DateTime> from, List<DateTime> to, string[] includes)
        {
            LineasProdSapToLineasProdElp lineasProdSapToLineasProdElp = await this.lineasProdSapToLineasProdElpRepository.GetAsync(id);
            var list = await this.sapOrdenesProduccionRepository.ListAsync(null, includes,
                new SapOrdenesProduccionFilter
                {
                    IdLinea = lineasProdSapToLineasProdElp.IdLineaSap,
                    FhInicio = from,
                    FhFin = to
                });

            return this.mapper.Map<List<SapOrdenesProduccionViewModel>>(list);
        } 
    
    }
}
