﻿using System;
using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Implementations
{
    public class UserGroupService : GenericServiceMapper<SysUsersGroups, UserGroupViewModel, int, BaseFilter, UserGroupViewModel, IUserGroupRepository>,
        IUserGroupService
    {
        public UserGroupService(IUserGroupRepository userGroupRepository, IMapper mapper): base(userGroupRepository, mapper)
        {
        }
    }
}
