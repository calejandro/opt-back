﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Services.Implementations
{
    public class SectorService : GenericServiceMapper<Sector, SectorViewModel, Guid, BaseFilter, SectorViewModel, ISectorRepository>, ISectorService
    {
        public SectorService(ISectorRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
