﻿using AutoMapper;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Services.Implementations
{
    public class RegistrosFallosAlertasService : GenericServiceMapper<RegistrosFallosAlertas, RegistrosFallosAlertasCreateViewModel, int, RegistrosFallosAlertasFilter, RegistrosFallosAlertasViewModel, IRegistrosFallosAlertasRepository>, IRegistrosFallosAlertasService
    {

        public RegistrosFallosAlertasService(IRegistrosFallosAlertasRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}