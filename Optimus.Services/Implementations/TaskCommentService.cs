﻿using AutoMapper;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class TaskCommentService : GenericServiceMapper<TaskComment, TaskCommentCreateViewModel, Guid, TaskCommentFilter, TaskCommentViewModel, ITaskCommentRepository>, ITaskCommentService
    {
        private readonly IUserRepository userRepository;

        public TaskCommentService(ITaskCommentRepository systemModuleRepository, IMapper mapper, IUserRepository userRepository) : base(systemModuleRepository, mapper)
        {
            this.userRepository = userRepository;
        }
        public async Task<TaskCommentViewModel> InsertAsync(TaskCommentCreateViewModel entity, Guid idCognito)
        {
            var user = await this.userRepository.GetByCognitoId(idCognito);
            var taskComment = this.mapper.Map<TaskComment>(entity);
            taskComment.IdCreatorUser = user.Id;
            return this.mapper.Map<TaskCommentViewModel>(await this.genericRepository.InsertAsync(taskComment));
        }
    }
}
