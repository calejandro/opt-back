﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.Implementations
{
    public class EquiposService : GenericServiceMapper<Equipos, EquiposCreateViewModel, int, CommonFilter, EquiposViewModel, IEquiposRepository>, IEquiposService
    {
        public EquiposService(IEquiposRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
