﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class SapSkuService : GenericServiceMapper<SapSku, SapSkuCreateViewModel, int, SapSkuFilter, SapSkuViewModel, ISapSkuRepository>, ISapSkuService
    {
        public readonly ISkuByLineViewRepository skuByLineViewRepository;
        public SapSkuService(ISapSkuRepository systemModuleRepository, IMapper mapper,
            ISkuByLineViewRepository skuByLineViewRepository) : base(systemModuleRepository, mapper)
        {
            this.skuByLineViewRepository = skuByLineViewRepository;
        }
        public async Task<List<MarcaViewModel>> MarcaSaborFormato()
        {
            var sku = await genericRepository.ListAsync();
            var listMarcaSaborFormato = new List<MarcaViewModel>();
            var marcas = sku.GroupBy(x => x.Marca);
            foreach (var marca in marcas)
            {
                var marcaSaborFormato = new MarcaViewModel();
                marcaSaborFormato.Sabores = new List<SaborViewModel>();
                marcaSaborFormato.Marca = marca.Key;
               var sabores = sku.Where(x => x.Marca == marca.Key).GroupBy(x => x.Sabor);
                foreach (var sabor in sabores)
                {
                    var saborFormato = new SaborViewModel();
                    saborFormato.Sabor = sabor.Key;
                    saborFormato.Formatos = sku.Where(x => x.Marca == marca.Key && x.Sabor == sabor.Key).Select(x => new { x.Marca, x.Sabor,x.Formato }).Distinct().Select(x => x.Formato).ToArray();

                    marcaSaborFormato.Sabores.Add(saborFormato);
                }
                listMarcaSaborFormato.Add(marcaSaborFormato);
            }
            return listMarcaSaborFormato;
        }

        public async Task<List<Tuple<int, string>>> ListSelectSKU(SkuByLineViewFilter filter)
        {
            var listMarcaSaborFormato = new List<MarcaViewModel>();
            var marc = await this.skuByLineViewRepository.ListAsync(null, null, filter);

            return marc.Select(x => new Tuple<int, string>(x.IdSku, x.DescSku)).Distinct().ToList();
        }
        public async Task<List<string>> ListBrand(SkuByLineViewFilter filter)
        {
            var listMarcaSaborFormato = new List<MarcaViewModel>();
            var marc = await this.skuByLineViewRepository.ListAsync(null, null, filter);
             
            return marc.Select(x => x.Marca).Distinct().ToList(); 
        }
        public async Task<List<string>> ListFlavor(SkuByLineViewFilter filter)
        {
            var listMarcaSaborFormato = new List<MarcaViewModel>();
            var marc = await this.skuByLineViewRepository.ListAsync(null, null, filter);

            return  marc.Select(x => x.Sabor).Distinct().ToList();
        }
        public async Task<List<string>> ListFormat(SkuByLineViewFilter filter)
        {
            var listMarcaSaborFormato = new List<MarcaViewModel>();
            var marc = await this.skuByLineViewRepository.ListAsync(null, null, filter);
            return marc.Select(x => x.Formato).Distinct().ToList();
        }

    }
}
