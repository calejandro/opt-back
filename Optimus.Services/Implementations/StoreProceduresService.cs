﻿using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class StoreProceduresService : IStoreProceduresService
    {
        protected IStoreProceduresRepository storeProceduresRepository;
        public StoreProceduresService(IStoreProceduresRepository storeProceduresRepository)
        {
            this.storeProceduresRepository = storeProceduresRepository;
        }

        // *EFICIENCIA*
        public Task<List<ProduccionEficiencia>> GetEfficiencyProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetEfficiencyProduction(idLinea, fechaHoraInicio, fechaHoraFin);
        }
        public Task<ProduccionEficienciaReal> GetEfficiencyTrueProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetEfficiencyTrueProduction(idLinea, fechaHoraInicio, fechaHoraFin);
        }
        public Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShift(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetEfficiencyProductionByShift(idLinea, fechaHoraInicio, fechaHoraFin);
        }
        public List<Shift> GetShifts(DateTime fecha)
        {
            return this.storeProceduresRepository.GetShifts(fecha);
        }

        public Shift GetCurrentShift(DateTime dateTime)
        {
            return this.storeProceduresRepository.GetCurrentShift(dateTime);
        }
        public string GetNameShift(DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetNameShift( fechaHoraInicio,  fechaHoraFin);
        }

        public Shift GetDay(DateTime fecha)
        {
            return this.storeProceduresRepository.GetDay(fecha);
        }
        // *PERFORMANCE DE EQUIPOS*
        public Task<List<MachineActualStatus>> GetMachineActualStatus(int idLinea)
        {
            return this.storeProceduresRepository.GetMachineActualStatus(idLinea);
        }
        public Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrders(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetMachineStatusProductionOrders(idLinea, fechaHoraInicio, fechaHoraFin);
        }
        public Task<List<TimeStops>> GetTimeStops(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, int idEstado)
        {
            return this.storeProceduresRepository.GetTimeStops(idLinea, fechaHoraInicio, fechaHoraFin, idEstado);
        }
        public Task<List<AverageTimeFailuresProduction>> GetAverageTimeFailuresProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            return this.storeProceduresRepository.GetAverageTimeFailuresProduction(idLinea, fechaHoraInicio, fechaHoraFin);
        }

        public Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropiaHora(int idLinea, int idEquipo)
        {
            return this.storeProceduresRepository.GetEvolucionParadaPropiaHora(idLinea, idEquipo);
        }

        // *MERMAS*
        public Task<List<KpiDepletion>> GetKpiDepletionForBottles(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, GroupingTypeEnum tipoAgrupacion)
        {
            return this.storeProceduresRepository.GetKpiDepletionForBottles(idLinea, fechaHoraInicio, fechaHoraFin, tipoAgrupacion);
        }
    }
}
