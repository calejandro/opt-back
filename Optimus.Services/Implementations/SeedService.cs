﻿using Optimus.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using Optimus.Models.Identity;
using System.Collections.Generic;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Implementations
{
    class SeedService : ISeedService
    {
        private readonly UserManager<UserIdentity> userManager;
        private readonly RoleManager<RoleIdentity> roleManager;
        private readonly IKpiService kPIService;
        private readonly ISectorService sectorService;
        private readonly IProcessService processService;

        public SeedService(
            UserManager<UserIdentity> userManager,
            RoleManager<RoleIdentity> roleManager,
            IKpiService kPIService,
            ISectorService sectorService,
            IProcessService processService
            )
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.kPIService = kPIService;
            this.sectorService = sectorService;
            this.processService = processService;
        }
        public async Task Seed()
        {

            if (false)
            {
                try
                {
                    //await this.AddRoles();
                    //await this.AddUsers();
                    await this.KPI();
                    await this.Process();
                    await this.Sector();

                }
                catch (Exception e)
                {
                    throw;
                    //TODO:
                    //log error
                }
            }
        }
        private async Task KPI()
        {
            var exist = await kPIService.ListAsync();
            if (exist?.Count == 0)
            {
                await kPIService.InsertAllAsync(new List<KpiViewModel>() { 
                      new KpiViewModel() {Name="P-Eficiencia" },
                      new KpiViewModel() {Name="C-Mermas" },
                      new KpiViewModel() {Name="S-Incidente" },
                      new KpiViewModel() {Name="S-Accidente" },
                      new KpiViewModel() {Name="Quality" },
                      new KpiViewModel() {Name="Moral" },
                      new KpiViewModel() {Name="Delivery" },
                }
                    );
               
            }
          
        }
        private async Task Process()
        {
            var exist = await processService.ListAsync();
            if (exist?.Count == 0)
            {
                await processService.InsertAllAsync(new List<ProcessViewModel>() {
                      new ProcessViewModel() {Name="Carbonatado" },
                      new ProcessViewModel() {Name="No Carbonatados" },
                      new ProcessViewModel() {Name="Suministro" },
                      new ProcessViewModel() {Name="Latas" },
                      new ProcessViewModel() {Name="Env. RefPet" },
                      new ProcessViewModel() {Name="Env. Latas" },
                      new ProcessViewModel() {Name="Env. Tetra" },
                      new ProcessViewModel() {Name="Preformas" },
                      new ProcessViewModel() {Name="CO2" },
                }
                    );

            }

        }
        private async Task Sector()
        {
            var exist = await sectorService.ListAsync();
            if (exist?.Count == 0)
            {
                await sectorService.InsertAllAsync(new List<SectorViewModel>() {
                      new SectorViewModel() {Name="Línea 1" },
                      new SectorViewModel() {Name="Línea 2" },
                      new SectorViewModel() {Name="Línea 3" },
                      new SectorViewModel() {Name="Línea 4" },
                      new SectorViewModel() {Name="Línea 5" },
                      new SectorViewModel() {Name="Línea 6" },
                      new SectorViewModel() {Name="Línea 7" },
                      new SectorViewModel() {Name="Línea 8" },
                      new SectorViewModel() {Name="Línea 9" },
                      new SectorViewModel() {Name="Línea 10" },
                      new SectorViewModel() {Name="Línea 11" },
                      new SectorViewModel() {Name="Línea 20" },
                      new SectorViewModel() {Name="Línea 21" },
                      new SectorViewModel() {Name="Línea 23" },
                      new SectorViewModel() {Name="Línea 30" },
                      new SectorViewModel() {Name="Unidad 1" },
                      new SectorViewModel() {Name="Unidad 2" },
                      new SectorViewModel() {Name="Unidad 3" },
                      new SectorViewModel() {Name="Bag in Box" },
                      new SectorViewModel() {Name="Jugos" },
                      new SectorViewModel() {Name="Sala de Bebidas" },
                      new SectorViewModel() {Name="Planta de Azúcar" },
                      new SectorViewModel() {Name="Alm. Minorista" },

                }
                    );

            }

        }
        private async Task AddRoles()
        {
            var rootExists = await roleManager.RoleExistsAsync("root");
            if (!rootExists)
            {
                var rootRole = new RoleIdentity { Name = "root", Description = "root", IsSystemRole = true };
                await roleManager.CreateAsync(rootRole);
                //await roleManager.AddClaimAsync(rootRole, new System.Security.Claims.Claim("cliente", "cliente"));
                //await roleManager.AddClaimAsync(rootRole, new System.Security.Claims.Claim("ropa", "clothes"));
                //await roleManager.AddClaimAsync(rootRole, new System.Security.Claims.Claim("reserva", "reserve"));
            }
            //var admExists = await roleManager.RoleExistsAsync("administrador");
            //if (!admExists)
            //{
            //    var admRole = new RoleIdentity { Name = "administrador", Description = "administrador", IsSystemRole = true };
            //    await roleManager.CreateAsync(admRole);
            //    await roleManager.AddClaimAsync(admRole, new System.Security.Claims.Claim("cliente", "cliente"));
            //    await roleManager.AddClaimAsync(admRole, new System.Security.Claims.Claim("ropa", "clothes"));
            //    await roleManager.AddClaimAsync(admRole, new System.Security.Claims.Claim("reserva", "reserve"));
            //}
        }

        private async Task AddUsers()
        {
            var userRoot = await userManager.FindByNameAsync("root");
            var rootRole = await roleManager.FindByNameAsync("root");
            if (userRoot == null && rootRole != null)
            {
                var rootUser = new UserIdentity
                {
                    UserName = "root",
                    EmailConfirmed = true,
                    LockoutEnabled = true,
                    Name = "Super",
                    LastName = "Admin",
                    Email = "root@root.com"
                };
                await userManager.CreateAsync(rootUser, "Superadm12!");
            }

        }

    }
}
