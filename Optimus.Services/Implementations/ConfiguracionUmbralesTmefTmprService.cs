﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class ConfiguracionUmbralesTmefTmprService : GenericServiceMapper<ConfiguracionUmbralesTmefTmpr, ConfiguracionUmbralesTmefTmprCreateViewModel, int, CommonFilter, ConfiguracionUmbralesTmefTmprViewModel, IConfiguracionUmbralesTmefTmprRepository>, IConfiguracionUmbralesTmefTmprService
    {
        public ConfiguracionUmbralesTmefTmprService(IConfiguracionUmbralesTmefTmprRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
        public async override Task<ConfiguracionUmbralesTmefTmprViewModel> InsertAsync(ConfiguracionUmbralesTmefTmprCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<ConfiguracionUmbralesTmefTmpr>(entity), false))
                throw new BussinessException(ExceptionConstants.ERROR_UMBRAL_DUPLICADO, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.InsertAsync(entity);
        }
        public async override Task<ConfiguracionUmbralesTmefTmprViewModel> UpdateAsync(ConfiguracionUmbralesTmefTmprCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<ConfiguracionUmbralesTmefTmpr>(entity), true))
                throw new BussinessException(ExceptionConstants.ERROR_UMBRAL_DUPLICADO, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryUpdate);
            return await base.UpdateAsync(entity);
        }
    }
}
