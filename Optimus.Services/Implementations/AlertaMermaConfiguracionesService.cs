﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class AlertaMermaConfiguracionesService : GenericServiceMapper<AlertaMermaConfiguraciones, AlertaMermaConfiguracionesCreateViewModel, int, AlertaMermaFilter, AlertaMermaConfiguracionesViewModel, IAlertaMermaConfiguracionesRepository>, IAlertaMermaConfiguracionesService
    {
        public AlertaMermaConfiguracionesService(IAlertaMermaConfiguracionesRepository alertaMermaConfiguracionesRepository, IMapper mapper) : base(alertaMermaConfiguracionesRepository, mapper)
        {
        }

        public override async Task<AlertaMermaConfiguracionesViewModel> InsertAsync(AlertaMermaConfiguracionesCreateViewModel entity)
        {
            var e = this.mapper.Map<AlertaMermaConfiguraciones>(entity);
            if(await this.genericRepository.ExistsEntityAsync(e, false))
            {
                throw new BussinessException(ExceptionConstants.ERROR_CONFIG_MERMA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);
            }
            return this.mapper.Map<AlertaMermaConfiguracionesViewModel>(await this.genericRepository.InsertAsync(e));
        }

        public override async Task<AlertaMermaConfiguracionesViewModel> UpdateAsync(AlertaMermaConfiguracionesCreateViewModel entity)
        {
            var e = this.mapper.Map<AlertaMermaConfiguraciones>(entity);
            if (await this.genericRepository.ExistsEntityAsync(e, true))
            {
                throw new BussinessException(ExceptionConstants.ERROR_CONFIG_MERMA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);
            }
            return this.mapper.Map<AlertaMermaConfiguracionesViewModel>(await this.genericRepository.UpdateAsync(e));
        }
    }
}
