﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class SysUsersService : GenericServiceMapper<SysUsers, SysUsersViewModel, int, UserFilter, SysUsersViewModel, ISysUsersRepository>, ISysUsersService
    {
        private readonly IUserRepository userRepository;
        private readonly ISysUserPlantsRepository sysUserPlantsRepository;
        private readonly IMultipleGroupRepository multipleGroupRepository;
        public SysUsersService(
            IUserRepository userRepository,
            ISysUserPlantsRepository sysUserPlantsRepository,
            ISysUsersRepository systemModuleRepository,
            IMultipleGroupRepository multipleGroupRepository,
            IMapper mapper) : base(systemModuleRepository, mapper)
        {
            this.userRepository = userRepository;
            this.sysUserPlantsRepository = sysUserPlantsRepository;
            this.multipleGroupRepository = multipleGroupRepository;
        }        

        public async Task AddSysUser(Guid idUserIdentity)
        {
            var user = await userRepository.GetAsync(idUserIdentity);
            var sysUser = await this.genericRepository.GetAsync(null, new UserFilter() { IdUserIdentity = user.Id });
            
            if(sysUser == null) {
                var newSysUser = new SysUsers()
                {
                    //TODO: lenguaje y esAdministrador
                    UserName = user.UserName,
                    FullName = user.Name,
                    LastName = user.LastName,
                    Email = user.Email,
                    IsActive = user.IsEnabled ? "y" : "n",
                    LanguageId = 1,
                    Telefono = user.PhoneNumber,
                    EsAdministrador = true,
                    IdUserIdentity = user.Id
                };
                await this.genericRepository.InsertAsync(newSysUser);
            }
            else
            {
                if(sysUser.IdUserIdentity == null)
                {
                    sysUser.IdUserIdentity = user.Id;
                    await this.genericRepository.UpdateAsync(sysUser);
                }
            }
        }

        public async Task<SysUsersViewModel> AssignUserToPlants(SysUsersViewModel usersView)
        {
            var user = this.mapper.Map<SysUsers>(usersView);
            var userPlans =  await this.sysUserPlantsRepository.ListAsync(null, filter: new SysUserPlantsFilter() { UserId = usersView.UserId });

            foreach (var userPlan in userPlans)
            {
                await this.sysUserPlantsRepository.DeleteAsync(userPlan.UserPlantId);
            }
            foreach (var userPlan in user.SysUsersPlantas)
            {
                userPlan.UserPlantId = 0;
                await this.sysUserPlantsRepository.InsertAsync(userPlan);
            }
            return  this.mapper.Map<SysUsersViewModel>(user);
        }

        public async Task AddGroup(int sysUserId, int groupId)
        {
            var sysUser = await this.genericRepository.GetAsync(sysUserId, new string[] { "groups" });
            if(sysUser.Groups.Where(w => w.UserGroupId == groupId).Count() == 0)
            {
                await this.multipleGroupRepository.InsertAsync(new SysMultipleGroups { UserGroupId = groupId, UserId = sysUserId });
            }
        }

        public async Task RemoveGroup(int sysUserId, int groupId)
        {
            var sysUser = await this.genericRepository.GetAsync(sysUserId, new string[] { "groups" });
            if (sysUser.Groups.Where(w => w.UserGroupId == groupId).Count() > 0)
            {
                await this.multipleGroupRepository.DeleteAllAsync(sysUser.Groups.Where(w => w.UserGroupId == groupId).Select(s => s.MultipleGroupId).ToArray());
            }
        }

        public async Task<SysUsersViewModel> GetSysUserByUserCognito(Guid userId, string[] includes)
        {            
            var user = await this.userRepository.GetByCognitoId(userId);
            await this.AddSysUser(user.Id);
            return this.mapper.Map<SysUsersViewModel>(await this.genericRepository.GetAsync(includes, new UserFilter() { IdUserIdentity = user.Id }));
        }
    }
}
