﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class SapOrdenesProduccionService : GenericServiceMapper<SapOrdenesProduccion, SapOrdenesProduccionCreateViewModel, int, SapOrdenesProduccionFilter, SapOrdenesProduccionViewModel, ISapOrdenesProduccionRepository>, ISapOrdenesProduccionService
    {
        private readonly ILineasProdSapToLineasProdElpRepository lineasProdSapToLineasProdElpRepository;
        private readonly ILineasProduccionUsuarioRepository lineasProduccionUsuarioRepository;
        public SapOrdenesProduccionService(ISapOrdenesProduccionRepository systemModuleRepository, IMapper mapper,
            ILineasProdSapToLineasProdElpRepository lineasProdSapToLineasProdElpRepository,
            ILineasProduccionUsuarioRepository lineasProduccionUsuarioRepository) : base(systemModuleRepository, mapper)
        {
            this.lineasProdSapToLineasProdElpRepository = lineasProdSapToLineasProdElpRepository;
            this.lineasProduccionUsuarioRepository = lineasProduccionUsuarioRepository;
        }

        public async override Task<SapOrdenesProduccionViewModel> InsertAsync(SapOrdenesProduccionCreateViewModel entity)
        {
            var ultimoSapOrdenes = ( await base.ListAsync(null, filter: new SapOrdenesProduccionFilter() {EsTemporal = "true" }, 
                requestModel: new PaginatorRequestModel() { Page = 0, RowsPage = 1 }, 
                orders: new List<Order> { new Order() { Property = "IdOrdenSap", Descending = true } })).FirstOrDefault();

            if (ultimoSapOrdenes == null)
                entity.IdOrdenSap = 100;
            else
                entity.IdOrdenSap = ultimoSapOrdenes.IdOrdenSap + 1;

            entity.EsTemporal = true;

            // cambio el id de elp por el id de sap
            entity.IdLinea = await this.ChangeIdAsync(entity.IdLinea);
            return await base.InsertAsync(entity);
        }
        public async override Task<SapOrdenesProduccionViewModel> UpdateAsync(SapOrdenesProduccionCreateViewModel entity)
        {
            // se usa listAsync por el traking
            var sapOrden = (await base.ListAsync(null,filter: new SapOrdenesProduccionFilter() { IdOrdenSap= entity.IdOrdenSap })).FirstOrDefault();
            if(sapOrden.EsTemporal != true)
              throw new BussinessException(ExceptionConstants.ERROR_SAP_ORDEN_EDIT_TEMPORAL, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            // cambio el id de elp por el id de sap
            entity.IdLinea = await this.ChangeIdAsync(entity.IdLinea);
            return await base.UpdateAsync(entity);
        }
        private async  Task<int> ChangeIdAsync(int IdLinea)
        {
            var lineasProdSapToLineasProdElp = (await this.lineasProdSapToLineasProdElpRepository.ListAsync(null, null
             , filter: new LineasProdSapToLineasProdElpFilter() { IdLineaElp = IdLinea })).FirstOrDefault();
            if (lineasProdSapToLineasProdElp != null)
                return lineasProdSapToLineasProdElp.IdLineaSap;
            else
                return IdLinea;
        }
        public async override Task DeleteAsync(int id)
        {
            var sapOrden = await base.GetAsync(id);
            if (sapOrden.EsTemporal != true)
                throw new BussinessException(ExceptionConstants.ERROR_SAP_ORDEN_DELETE_TEMPORAL, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);

            await base.DeleteAsync(id);
        }
        public async Task<SapOrdenesProduccionViewModel> LinkTemporalOrder(LinkTemporalOrder linkTemporalOrder)
        {
            return await this.genericRepository.CreateExecutionStrategy().ExecuteAsync(async () =>
            {
                using (var transaction = this.genericRepository.BeginTransaction())
                {
                    var sapOrden = (await base.ListAsync(null, filter: new SapOrdenesProduccionFilter() { IdOrdenSap = linkTemporalOrder.IdOrdenSap })).FirstOrDefault();
                    var sapOrdenTemporal = (await base.ListAsync(null, filter: new SapOrdenesProduccionFilter() { IdOrdenSap = linkTemporalOrder.IdOrdenSapTemporal })).FirstOrDefault();
                    await Validation(sapOrden, sapOrdenTemporal);


                    

                    // -1 - verificar que no la orden temporal no tenga una orden vinculada 
                    if (sapOrdenTemporal.IdOrdenVinculada != null)
                    {
                        var sapOrdenvinculada = (await base.ListAsync(null, filter: new SapOrdenesProduccionFilter() { IdOrdenSap = sapOrdenTemporal.IdOrdenVinculada })).FirstOrDefault();
                        sapOrden.IdOrdenVinculada = linkTemporalOrder.IdOrdenSapTemporal;
                        sapOrdenTemporal.IdOrdenVinculada = linkTemporalOrder.IdOrdenSap;
                        await base.UpdateAsync(this.mapper.Map<SapOrdenesProduccionCreateViewModel>(sapOrdenTemporal));
                        // ahora la variable sapOrdenTemporal es la orden sap anteriormente vinculada 
                        sapOrdenTemporal = sapOrdenvinculada;
                        sapOrdenTemporal.IdOrdenVinculada = null;
                    }
                    else
                    {
                        // 3 - seteo cruzado de  idOrdenVinculada
                        sapOrden.IdOrdenVinculada = linkTemporalOrder.IdOrdenSapTemporal;
                        sapOrdenTemporal.IdOrdenVinculada = linkTemporalOrder.IdOrdenSap;
                    }



                    //1 -  cambiar el ID_ORDEN_SAP en LineasProdUsuario
                    var lineasProdUs = await this.lineasProduccionUsuarioRepository.ListAsync(null, null, 
                        filter: new LineasProduccionUsuarioFilter() { IdOrdenSap = sapOrdenTemporal.IdOrdenSap });
                   
                    foreach (var lineaProdUs in lineasProdUs)
                    {
                        lineaProdUs.IdOrdenSap = linkTemporalOrder.IdOrdenSap;
                        await this.lineasProduccionUsuarioRepository.UpdateAsync(lineaProdUs);
                    }

                    // 2 - setiar las fechaInicioProd, fechafinProd y FechaSupervision orden uno A la orden dos
                    sapOrden.FhFinProduccion = sapOrdenTemporal.FhFinProduccion;
                    sapOrden.FhIncioProduccion = sapOrdenTemporal.FhIncioProduccion;
                    sapOrden.FechaSupervision = sapOrdenTemporal.FechaSupervision;

                   

                    //4 - hacer nulas las fecha inicio y fecha fin el la orden temporal para que no salga en los reportes
                    sapOrdenTemporal.FhFinProduccion = null;
                    sapOrdenTemporal.FhIncioProduccion = null;

                    await base.UpdateAsync(this.mapper.Map<SapOrdenesProduccionCreateViewModel>(sapOrdenTemporal));
                    var resul = await base.UpdateAsync(this.mapper.Map<SapOrdenesProduccionCreateViewModel>(sapOrden));
                    transaction.Commit();

                    return resul;

                }

            });
          
        }

        private async Task Validation(SapOrdenesProduccionViewModel sapOrden, SapOrdenesProduccionViewModel sapOrdenTemporal)
        {
            if (sapOrden.EsTemporal == true)
                throw new BussinessException(ExceptionConstants.ERROR_SAP_ORDEN_LINK_TEMPORAL, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            if (sapOrdenTemporal.EsTemporal == false)
                throw new BussinessException(ExceptionConstants.ERROR_SAP_ORDEN_LINK_NO_TEMPORAL, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            if (sapOrden.IdLinea != sapOrdenTemporal.IdLinea)
                throw new BussinessException(ExceptionConstants.ERROR__SAP_ORDEN_NO_MACH_LINEA, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            if (sapOrden.IdSku != sapOrdenTemporal.IdSku)
                throw new BussinessException(ExceptionConstants.ERROR__SAP_ORDEN_NO_MACH_SKU, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            if (sapOrdenTemporal.IdOrdenVinculada == sapOrden.IdOrdenSap)
                throw new BussinessException(ExceptionConstants.ERROR__SAP_ORDEN_IS_LIKE, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
            // validamos que la orden no esté tomada ya que se sitiaran sus fechas de producción
            if (sapOrden.FhFinProduccion != null || sapOrden.FhIncioProduccion != null) 
                throw new BussinessException(ExceptionConstants.ERROR__SAP_ORDEN_IS_TAKEN, BusinessExceptionCodeEnumeration.SAP_OrdenTemporal);
           


        }
    }
}
