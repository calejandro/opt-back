﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations.Mongo
{
    public class TimeStopsService : ITimeStopsService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly ITimeStopsRepository timeStopsRepository;
        private readonly IStoreProceduresService storeProcedures;
        public TimeStopsService(ITimeStopsRepository timeStopsRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresService storeProcedures, IEquiposEstadosRepository equiposEstadosRepository)
        {
            this.timeStopsRepository = timeStopsRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertTimeStops(int IdLinea, Shift turno, List<EquiposEstados> estados)
        {
            try
            {
                List<TimeStopsMongo> timesStopsMongo = new List<TimeStopsMongo>();
                foreach (var estado in estados)
                {
                    TimeStopsMongo timeStopsMongo = new TimeStopsMongo();
                    List<TimeStops> timeStops = await storeProcedures.GetTimeStops(IdLinea, turno.Inicio, turno.Fin, estado.Id);

                    // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                    //filtros
                    var builder = Builders<TimeStopsMongo>.Filter;
                    var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                        builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                        builder.Eq(widget => widget.FechaHoraFin, turno.Fin) &
                        builder.Eq(widget => widget.IdEstado, estado.Id);

                    timeStopsMongo = await timeStopsRepository.GetAsync(filter);

                    if (timeStopsMongo != null)
                    {
                        timeStopsMongo.TimeStops = timeStops;
                        timeStopsMongo.Updated = DateTime.Now;
                        timeStopsMongo.UpdatedBy = "job";
                        await timeStopsRepository.UpdateAsync(timeStopsMongo);
                    }
                    else
                    {

                        await timeStopsRepository.InsertAsync(new TimeStopsMongo()
                        {
                            IdLinea = IdLinea,
                            FechaHoraInicio = turno.Inicio,
                            FechaHoraFin = turno.Fin,
                            Turno = turno.Turno,
                            IdEstado = estado.Id,
                            TimeStops = timeStops,
                            Created = DateTime.Now,
                            CreatedBy = "job"
                        });
                        timeStopsMongo = await timeStopsRepository.GetAsync(filter);
                    }
                    timesStopsMongo.Add(timeStopsMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("TimeStops", timesStopsMongo);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
