﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations.Mongo
{
    public class ProduccionEficienciaByShiftService : IProduccionEficienciaByShiftService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository;
        private readonly IStoreProceduresService storeProcedures;
        public ProduccionEficienciaByShiftService(IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresService storeProcedures)
        {
            this.produccionEficienciaByShiftRepository = produccionEficienciaByShiftRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertProduccionEficienciaByShift(int IdLinea, List<Shift> turnos)
        {
            try
            {
                List<ProduccionEficienciaByShiftMongo> eficienciasMongo = new List<ProduccionEficienciaByShiftMongo>();
                foreach (var turno in turnos)
                {
                    ProduccionEficienciaByShiftMongo eficienciaMongo = new ProduccionEficienciaByShiftMongo();
                    if (turno.Paso)
                    {
                        List<ProduccionEficienciaByShift> eficiencia = await storeProcedures.GetEfficiencyProductionByShift(IdLinea, turno.Inicio, turno.Fin);
                        // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                        //filtros
                        var builder = Builders<ProduccionEficienciaByShiftMongo>.Filter;
                        var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                            builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                            builder.Eq(widget => widget.FechaHoraFin, turno.Fin);
                        eficienciaMongo = await produccionEficienciaByShiftRepository.GetAsync(filter);

                        if (eficienciaMongo != null)
                        {
                            eficienciaMongo.ProduccionEficienciaByShift = eficiencia;
                            eficienciaMongo.Updated = DateTime.Now;
                            eficienciaMongo.UpdatedBy = "job";
                            eficienciaMongo.Turno = turno.Turno;
                            await produccionEficienciaByShiftRepository.UpdateAsync(eficienciaMongo);
                        }
                        else
                        {

                            await produccionEficienciaByShiftRepository.InsertAsync(new ProduccionEficienciaByShiftMongo()
                            {
                                IdLinea = IdLinea,
                                FechaHoraInicio = turno.Inicio,
                                FechaHoraFin = turno.Fin,
                                Turno = turno.Turno,
                                ProduccionEficienciaByShift = eficiencia,
                                Created = DateTime.Now,
                                CreatedBy = "job"
                            });
                            eficienciaMongo = await produccionEficienciaByShiftRepository.GetAsync(filter);
                        }
                    }
                    eficienciasMongo.Add(eficienciaMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("EficienciaByShift", eficienciasMongo);
            }
            catch (Exception e)
            {

                throw;
            }


        }
    }
}
