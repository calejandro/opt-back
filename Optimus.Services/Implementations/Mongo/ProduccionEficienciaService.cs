﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations.Mongo
{
    public class ProduccionEficienciaService : IProduccionEficienciaService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IProduccionEficienciaRepository produccionEficienciaRepository;
        private readonly IStoreProceduresService storeProcedures;
        public ProduccionEficienciaService(IProduccionEficienciaRepository produccionEficienciaRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresService storeProcedures)
        {
            this.produccionEficienciaRepository = produccionEficienciaRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertProduccionEficiencia(int IdLinea, List<Shift> turnos)
        {
            try
            {
                List<ProduccionEficienciaMongo> eficienciasMongo = new List<ProduccionEficienciaMongo>();

                foreach (var turno in turnos)
                {
                    ProduccionEficienciaMongo eficienciaMongo = new ProduccionEficienciaMongo();
                    if (turno.Paso)
                    {
                        List<ProduccionEficiencia> eficiencia = await storeProcedures.GetEfficiencyProduction(IdLinea, turno.Inicio, turno.Fin);

                        // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                        //filtros
                        var builder = Builders<ProduccionEficienciaMongo>.Filter;
                        var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                            builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                            builder.Eq(widget => widget.FechaHoraFin, turno.Fin);

                        eficienciaMongo = await produccionEficienciaRepository.GetAsync(filter);

                        if (eficienciaMongo != null)
                        {
                            eficienciaMongo.ProduccionEficiencia = eficiencia;
                            eficienciaMongo.Updated = DateTime.Now;
                            eficienciaMongo.UpdatedBy = "job";
                            eficienciaMongo.Turno = turno.Turno;
                            await produccionEficienciaRepository.UpdateAsync(eficienciaMongo);
                        }
                        else
                        {
                            await produccionEficienciaRepository.InsertAsync(new ProduccionEficienciaMongo()
                            {
                                IdLinea = IdLinea,
                                FechaHoraInicio = turno.Inicio,
                                FechaHoraFin = turno.Fin,
                                Turno = turno.Turno,
                                ProduccionEficiencia = eficiencia,
                                Created = DateTime.Now,
                                CreatedBy = "job"
                            });
                            eficienciaMongo = await produccionEficienciaRepository.GetAsync(filter);
                        }
                    }
                    eficienciasMongo.Add(eficienciaMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("Eficiencia", eficienciasMongo);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
