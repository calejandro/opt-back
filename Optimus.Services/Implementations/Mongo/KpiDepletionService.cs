﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations.Mongo
{
    public class KpiDepletionService : IKpiDepletionService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IKpiDepletionRepository kpiDepletionRepository;
        private readonly IStoreProceduresService storeProcedures;
        public KpiDepletionService(IKpiDepletionRepository kpiDepletionRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresService storeProcedures)
        {
            this.kpiDepletionRepository = kpiDepletionRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertKpiDepletion(int IdLinea, List<Shift> turnos)
        {
            try
            {
                List<KpiDepletionMongo> kpiDepletionsMongo = new List<KpiDepletionMongo>();
                foreach (int groupingTypeEnum in Enum.GetValues(typeof(GroupingTypeEnum)))
                {
                    KpiDepletionMongo kpiDepletionMongo = new KpiDepletionMongo();
                    var turno = turnos.Find(x => x.Turno == "DiaEntero");
                    var kpiDepletion = await storeProcedures.GetKpiDepletionForBottles(IdLinea, turno.Inicio, turno.Fin, (GroupingTypeEnum)groupingTypeEnum);

                    // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                    //filtros
                    var builder = Builders<KpiDepletionMongo>.Filter;
                    var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                        builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                        builder.Eq(widget => widget.FechaHoraFin, turno.Fin) &
                        builder.Eq(widget => widget.tipoAgrupacion, (GroupingTypeEnum)groupingTypeEnum);

                    kpiDepletionMongo = await kpiDepletionRepository.GetAsync(filter);

                    if (kpiDepletionMongo != null)
                    {
                        kpiDepletionMongo.KpiDepletion = kpiDepletion;
                        kpiDepletionMongo.Updated = DateTime.Now;
                        kpiDepletionMongo.UpdatedBy = "job";
                        await kpiDepletionRepository.UpdateAsync(kpiDepletionMongo);
                    }
                    else
                    {

                        await kpiDepletionRepository.InsertAsync(new KpiDepletionMongo()
                        {
                            IdLinea = IdLinea,
                            FechaHoraInicio = turno.Inicio,
                            FechaHoraFin = turno.Fin,
                            Turno = turno.Turno,
                            tipoAgrupacion = (GroupingTypeEnum)groupingTypeEnum,
                            KpiDepletion = kpiDepletion,
                            Created = DateTime.Now,
                            CreatedBy = "job"
                        });
                        kpiDepletionMongo = await kpiDepletionRepository.GetAsync(filter);
                    }
                    kpiDepletionsMongo.Add(kpiDepletionMongo);
                }
                foreach (var turno in turnos)
                {
                    KpiDepletionMongo kpiDepletionMongo = new KpiDepletionMongo();
                    var kpiDepletion = await storeProcedures.GetKpiDepletionForBottles(IdLinea, turno.Inicio, turno.Fin, GroupingTypeEnum.PerLineEquipment);

                    // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                    //filtros
                    var builder = Builders<KpiDepletionMongo>.Filter;
                    var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                        builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                        builder.Eq(widget => widget.FechaHoraFin, turno.Fin) &
                        builder.Eq(widget => widget.tipoAgrupacion, GroupingTypeEnum.PerLineEquipment);

                    kpiDepletionMongo = await kpiDepletionRepository.GetAsync(filter);

                    if (kpiDepletionMongo != null)
                    {
                        kpiDepletionMongo.KpiDepletion = kpiDepletion;
                        kpiDepletionMongo.Updated = DateTime.Now;
                        kpiDepletionMongo.UpdatedBy = "job";
                        await kpiDepletionRepository.UpdateAsync(kpiDepletionMongo);
                    }
                    else
                    {

                        await kpiDepletionRepository.InsertAsync(new KpiDepletionMongo()
                        {
                            IdLinea = IdLinea,
                            FechaHoraInicio = turno.Inicio,
                            FechaHoraFin = turno.Fin,
                            Turno = turno.Turno,
                            tipoAgrupacion = GroupingTypeEnum.PerLineEquipment,
                            KpiDepletion = kpiDepletion,
                            Created = DateTime.Now,
                            CreatedBy = "job"
                        });
                        kpiDepletionMongo = await kpiDepletionRepository.GetAsync(filter);
                    }
                    kpiDepletionsMongo.Add(kpiDepletionMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("KpiDepletion", kpiDepletionsMongo);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
