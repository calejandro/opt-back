﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations.Mongo
{
    public class MachineStatusProductionOrderService : IMachineStatusProductionOrderService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IMachineStatusProductionOrderRepository machineStatusProductionOrderMongo;
        private readonly IStoreProceduresService storeProcedures;
        public MachineStatusProductionOrderService(IMachineStatusProductionOrderRepository machineStatusProductionOrderMongo, IHubContext<OptimusHub> hubContext, IStoreProceduresService storeProcedures)
        {
            this.machineStatusProductionOrderMongo = machineStatusProductionOrderMongo;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertMachineStatusProductionOrder(int IdLinea, Shift turno)
        {
            try
            {
                List<MachineStatusProductionOrder> eficiencia = await storeProcedures.GetMachineStatusProductionOrders(IdLinea, turno.Inicio, turno.Fin);

                // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                //filtros
                var builder = Builders<MachineStatusProductionOrderMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin);

                MachineStatusProductionOrderMongo eficienciaMongo = await machineStatusProductionOrderMongo.GetAsync(filter);

                if (eficienciaMongo != null)
                {
                    eficienciaMongo.MachineStatusProductionOrder = eficiencia;
                    eficienciaMongo.Updated = DateTime.Now;
                    eficienciaMongo.UpdatedBy = "job";
                    await machineStatusProductionOrderMongo.UpdateAsync(eficienciaMongo);
                }
                else
                {

                    await machineStatusProductionOrderMongo.InsertAsync(new MachineStatusProductionOrderMongo()
                    {
                        IdLinea = IdLinea,
                        FechaHoraInicio = turno.Inicio,
                        FechaHoraFin = turno.Fin,
                        Turno = turno.Turno,
                        MachineStatusProductionOrder = eficiencia,
                        Created = DateTime.Now,
                        CreatedBy = "job"
                    });
                    eficienciaMongo = await machineStatusProductionOrderMongo.GetAsync(filter);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("MachineStatusProductionOrder", eficienciaMongo);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
