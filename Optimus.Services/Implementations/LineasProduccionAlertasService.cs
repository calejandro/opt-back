﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Optimus.Commons.Entities;
using Optimus.Commons.FileWriter.Implementation;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Configuration;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class LineasProduccionAlertasService : GenericServiceMapper<LineasProduccionAlertas, LineasProduccionAlertasCreateViewModel, int, LineaProduccionAlertaFilter, LineasProduccionAlertasViewModel, ILineasProduccionAlertasRepository>, ILineasProduccionAlertasService
    {
        private readonly IFileWriter fileWriter;
        private readonly IUrlImageRepository urlImageRepository;
        private readonly AppConfig appConfig;

        public LineasProduccionAlertasService(IOptions<AppConfig> appConfig,IUrlImageRepository urlImageRepository, IFileWriter fileWriter, ILineasProduccionAlertasRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
            this.urlImageRepository = urlImageRepository;
            this.fileWriter = fileWriter;
            this.appConfig = appConfig.Value;
        }
        public async Task InsertImagLineasProduccionAlertas(List<IFormFile> files, int idAlerta)
        {
            try
            {
                List<UrlImage> urlImages = new List<UrlImage>();
                foreach (var file in files)
                {
                    UrlImage urlImage = new UrlImage();
                    urlImage.Name = await fileWriter.UploadImage(file, this.appConfig.FilePath);
                    urlImage.Id = Guid.Parse(urlImage.Name);
                    urlImage.IdEntity = idAlerta;
                    urlImage.Entity = "LineasProduccionAlertas";
                    urlImages.Add(urlImage);
                }
              

                await urlImageRepository.InsertAllAsync(urlImages);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<(Stream, string)> GetImagLineasProduccionAlertas(Guid idImage)
        {
            try
            {
                return (new MemoryStream(File.ReadAllBytes(Path.Combine(this.appConfig.FilePath, idImage.ToString()))), idImage.ToString());

            }
            catch (Exception e)
            {

                throw;
            }
        }

       
    }
}
