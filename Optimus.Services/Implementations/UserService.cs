﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Optimus.Commons.Configurations;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Commons.Services.Email;
using Optimus.Commons.ViewModels;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Configuration;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class UserService : GenericServiceMapper<UserIdentity, UserCreateViewModel, Guid, UserFilter, UserViewModel, IUserRepository>, IUserService
    {
        private readonly UserManager<UserIdentity> userManager;
        private readonly AppConfig appConfig;
        private readonly IEmailSender emailSender;
        private readonly ISysUsersService sysUsersService;
        private readonly AuthorizatorConfig authorizatorConfig;
        public UserService(UserManager<UserIdentity> userManager,
            IOptions<AppConfig> appConfig,
            IEmailSender emailSender,
            IUserRepository systemModuleRepository, 
            IMapper mapper,
           ISysUsersService sysUsersService,
           IOptions<AuthorizatorConfig> authorizatorConfig) : base(systemModuleRepository, mapper)
        {
            this.userManager = userManager;
            this.appConfig = appConfig.Value;
            this.emailSender = emailSender;
            this.sysUsersService = sysUsersService;
            this.authorizatorConfig = authorizatorConfig.Value;
        }
        public async Task<UserIdentity> RegisterUserIdentity(UserCreateViewModel userViewModel)
        {
            var user = new UserIdentity
            {
                UserName = userViewModel.UserName,
                Email = userViewModel.Email,
                Name = userViewModel.Name,
                LastName = userViewModel.LastName,
                PhoneNumber = userViewModel.PhoneNumber,
                IsEnabled = userViewModel.IsEnabled,
                TwoFactorEnabled = false,
                EmailConfirmed = false,
                LockoutEnabled = true,
            };
            var result = new IdentityResult();
            try
            {
                result = await this.userManager.CreateAsync(user);
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains("Duplicate entry") || ex.InnerException.Message.Contains("duplicate key"))
                    throw new BussinessException(ExceptionConstants.ERROR_GENERAL_DUPLICATE_ENTRY, BusinessExceptionCodeEnumeration.USER_DUPLICATEENTRYCREATE);
                throw ex;
            }
            this.ValidateErrors(result);
            return user;
        }
        public async Task<UserViewModel> Register(UserCreateViewModel userViewModel)
        {
            var user = await this.RegisterUserIdentity(userViewModel);

            // envio de e-mail
            var tokenConofirmEmail = await this.userManager.GenerateEmailConfirmationTokenAsync(await this.userManager.FindByNameAsync(userViewModel.UserName));
            if (!emailSender.UseIntenalEmailService)
            {
                var  UrlConfirm = $"{this.appConfig.BaseUrl}{this.appConfig.ConfirmUrlSegment}/?token={EncodeToken(tokenConofirmEmail)}&user={userViewModel.UserName}";
                var messageTemplate = $@" 
                   <div> 
                   <div>Bienvenido {user.Name} {user.LastName}  a Optimus.</div> 
                   <div> Usted fue dado de alta en el sistema con el nombre de usuario: {user.UserName} </div>
                   <div> Para confirmar su cuenta haga click <a href=""{UrlConfirm}"" >aqui </a> </div>
                   </div>";
                //TODO: generar template
                if (authorizatorConfig.AuthType.Equals("Cognito"))
                {
                    messageTemplate = $@" 
                   <div> 
                   <div>Bienvenido {user.Name} {user.LastName} a Optimus.</div>                    
                   <div> Para acceder haga click <a href=""{UrlConfirm}"" >aqui </a> </div>
                   </div>";
                }
                
                
                var recipients = new List<EmailRecipientsViewModel>();
                recipients.Add(new EmailRecipientsViewModel
                {
                    Email = user.Email,
                    Name = user.Name +" "+ user.LastName
                });
                await emailSender.SendEmailAsync(recipients, authorizatorConfig.AuthType.Equals("Cognito") ? "Bienvenido a Optimus!!" : "Confirme su cuenta", messageTemplate, new List<EmailAttachmentViewModel>());
            }

            // asignamos el System user (también se está convocando este método cuando el usuario identity inicia sesión)
            await this.sysUsersService.AddSysUser(user.Id);
            var userAddedViewModel = this.mapper.Map<UserViewModel>(user);

            return userAddedViewModel;
        }
        public async Task ConfirmEmail(UserEmailConfirmation userEmailConfirmation)
        {
            var user = await this.userManager.FindByNameAsync(userEmailConfirmation.UserName);
            if (user == null)
            {
                throw new BussinessException(ExceptionConstants.ERROR_USER_NOT_EXISTS, BusinessExceptionCodeEnumeration.SEC_UserNotExists);
            }
            var result = await this.userManager.ConfirmEmailAsync(user, DecodeToken(userEmailConfirmation.Token));
            this.ValidateErrors(result);
            var passwordToken = await this.userManager.GeneratePasswordResetTokenAsync(user);
            result = await this.userManager.ResetPasswordAsync(user, passwordToken, userEmailConfirmation.Password);
            this.ValidateErrors(result);
        }
        public async Task SendResetPassword(UserSendResetPassword userSendResetPassword)
        {
            var user = await this.userManager.FindByNameAsync(userSendResetPassword.UserName.Trim());
            if (user == null)
            {
                var sysUser = await this.sysUsersService.GetAsync(null, filter: new UserFilter() { UserName = userSendResetPassword.UserName });
                if (sysUser == null)
                    throw new BussinessException(ExceptionConstants.ERROR_USER_NOT_EXISTS, BusinessExceptionCodeEnumeration.SEC_UserNotExists);

                await this.RegisterUserIdentity(new UserCreateViewModel()
                {
                    UserName = sysUser.UserName,
                    Name = sysUser.FullName,
                    LastName = sysUser.LastName,
                    Email = sysUser.Email,
                    PhoneNumber = sysUser.Telefono,
                    IsEnabled = true
                });
                user = await this.userManager.FindByNameAsync(userSendResetPassword.UserName.Trim());           
            }
            var token = await this.userManager.GeneratePasswordResetTokenAsync(user);
            if (!emailSender.UseIntenalEmailService)
            {
                var UrlConfirm = $"{this.appConfig.BaseUrl}{this.appConfig.ResetPasswordUrlSegment}/?token={EncodeToken(token)}&user={userSendResetPassword.UserName}";

                //TODO: generar template
                
                var messageTemplate = $@"
                   <div> 
                   <div>Hola {user.Name} {user.LastName}.</div> 
                   <div> Para cambiar su contraseña haga click <a href=""{UrlConfirm}"" >aqui </a> </div>
                   <div> Recuerde que su nombre de usuario es: {user.UserName} </div>
                  </div>";

                var recipients = new List<EmailRecipientsViewModel>();
                recipients.Add(new EmailRecipientsViewModel
                {
                    Email = user.Email,
                    Name = user.Name + " " + user.LastName
                });
                await emailSender.SendEmailAsync(recipients, "Restablecer contraseña", messageTemplate, new List<EmailAttachmentViewModel>());
            }
        }

        public async Task ResetPassword(UserResetPassword userResetPassword)
        {
            var user = await this.userManager.FindByNameAsync(userResetPassword.UserName.Trim());
            if (user == null)
            {
                    throw new BussinessException(ExceptionConstants.ERROR_USER_NOT_EXISTS, BusinessExceptionCodeEnumeration.SEC_UserNotExists);
            }
            var result = await this.userManager.ResetPasswordAsync(user, DecodeToken(userResetPassword.Token), userResetPassword.Password);
            this.ValidateErrors(result);
        }
        public async Task<UserViewModel> UpdateProfile(UserCreateViewModel userViewModel)
        {
            var result = new IdentityResult();
            var user = await this.userManager.FindByIdAsync(userViewModel.Id.ToString());
            try
            {
                mapper.Map<UserCreateViewModel, UserIdentity>(userViewModel, user);
                result = await this.userManager.UpdateAsync(user);

            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw ex;

                if (ex.InnerException.Message.Contains("Duplicate entry"))
                    throw new BussinessException(ExceptionConstants.ERROR_USER_DUPLICATE_ENTRY_UPDATE, BusinessExceptionCodeEnumeration.USER_DUPLICATEENTRYUPDATE);
                throw ex;
            }
            this.ValidateErrors(result);

            var userUpdatedViewModel = this.mapper.Map<UserViewModel>(user);

            return userUpdatedViewModel;
        }

        public async Task<UserViewModel> Undelete(UserViewModel userViewModel)
        {
            UserIdentity user = await this.genericRepository.GetAsync(null, new UserFilter() { UserName = userViewModel.UserName }, true);                        
            user.DeletedAt = null;
            user.DeletedBy = null;
            await this.userManager.UpdateAsync(user);
            var userUpdatedViewModel = this.mapper.Map<UserViewModel>(user);

            return userUpdatedViewModel;
        }

        public async Task<bool> ExistsByEmail(string email, Guid userId)
        {
            var user = await this.userManager.FindByNameAsync(email);
            if(user != null && user.IdCognito == null)
            {
                user.IdCognito = userId;
                await this.genericRepository.UpdateAsync(user);
            }
            return user != null;
        }

        #region support
        private void ValidateErrors(IdentityResult result)
        {
            if (result != null && !result.Succeeded)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var item in result.Errors)
                {
                    stringBuilder.AppendLine(item.Description);
                }
                throw new BussinessException(stringBuilder.ToString(), BusinessExceptionCodeEnumeration.Unknown);
            }
        }

        private string EncodeToken(string token)
        {
            byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(token);
            var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);
            return codeEncoded;
        }
        private string DecodeToken(string token)
        {
            var codeDecodedBytes = WebEncoders.Base64UrlDecode(token);
            var codeDecoded = Encoding.UTF8.GetString(codeDecodedBytes);
            return codeDecoded;
        }
        #endregion
    }
}
