﻿using MongoDB.Driver;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class MongoService : IMongoService
    {
        private readonly IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository;
        private readonly IKpiDepletionRepository kpiDepletionRepository;
        private readonly IMachineStatusProductionOrderRepository machineStatusProductionOrderRepository;
        private readonly IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository;
        private readonly IProduccionEficienciaRepository produccionEficienciaRepository;
        private readonly ITimeStopsRepository timeStopsRepository;
        private readonly IStoreProceduresService storeProcedures;
        private readonly IEquiposEstadosRepository equiposEstadosRepository;
        public MongoService(IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository,
         IKpiDepletionRepository kpiDepletionRepository,
         IMachineStatusProductionOrderRepository machineStatusProductionOrderRepository,
         IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository,
         IProduccionEficienciaRepository produccionEficienciaRepository,
        ITimeStopsRepository timeStopsRepository,
        IStoreProceduresService storeProcedures,
        IEquiposEstadosRepository equiposEstadosRepository)
        {
            this.averageTimeFailuresProductionRepository = averageTimeFailuresProductionRepository;
            this.kpiDepletionRepository = kpiDepletionRepository;
            this.machineStatusProductionOrderRepository = machineStatusProductionOrderRepository;
            this.produccionEficienciaByShiftRepository = produccionEficienciaByShiftRepository;
            this.produccionEficienciaRepository = produccionEficienciaRepository;
            this.timeStopsRepository = timeStopsRepository;
            this.storeProcedures = storeProcedures;
            this.equiposEstadosRepository = equiposEstadosRepository;
        }
        public async Task DeleteAverageTimeFailuresProductionMongo(int linea, DateTime fecha)
        {
            var turno = storeProcedures.GetDay(fecha);
            var builder = Builders<AverageTimeFailuresProductionMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, linea) &
                builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                builder.Eq(widget => widget.FechaHoraFin, turno.Fin);

            AverageTimeFailuresProductionMongo averageTimeFailuresProductionMongo = await averageTimeFailuresProductionRepository.GetAsync(filter);

            if (averageTimeFailuresProductionMongo != null)
                await averageTimeFailuresProductionRepository.DeleteAsync(averageTimeFailuresProductionMongo._Id);
        }
        public async Task DeleteKpiDepletionMongo(int linea, DateTime fecha)
        {
            foreach (int groupingTypeEnum in Enum.GetValues(typeof(GroupingTypeEnum)))
            {
                var turno = storeProcedures.GetDay(fecha);

                var builder = Builders<KpiDepletionMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, linea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin) &
                    builder.Eq(widget => widget.tipoAgrupacion, (GroupingTypeEnum)groupingTypeEnum);

                var kpiDepletionMongo = await kpiDepletionRepository.GetAsync(filter);


                if (kpiDepletionMongo != null)
                    await kpiDepletionRepository.DeleteAsync(kpiDepletionMongo._Id);
            }
        }
        public async Task DeleteMachineStatusProductionMongo(int linea, DateTime fecha)
        {
            var turno = storeProcedures.GetDay(fecha);

            var builder = Builders<MachineStatusProductionOrderMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, linea) &
                builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                builder.Eq(widget => widget.FechaHoraFin, turno.Fin);

            MachineStatusProductionOrderMongo machineStatusProductionMongo = await machineStatusProductionOrderRepository.GetAsync(filter);

            if (machineStatusProductionMongo != null)
                await machineStatusProductionOrderRepository.DeleteAsync(machineStatusProductionMongo._Id);

        }
        public async Task DeleteProduccionEficienciaByShiftMongo(int linea, DateTime fecha)
        {
            var turnos = storeProcedures.GetShifts(fecha);
            turnos.Add(storeProcedures.GetDay(fecha));
            foreach (var turno in turnos)
            {
                ProduccionEficienciaByShiftMongo eficienciaMongo = new ProduccionEficienciaByShiftMongo();


                var builder = Builders<ProduccionEficienciaByShiftMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, linea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin);
                eficienciaMongo = await produccionEficienciaByShiftRepository.GetAsync(filter);

                if (eficienciaMongo != null)
                    await produccionEficienciaByShiftRepository.DeleteAsync(eficienciaMongo._Id);
            }
        }
        public async Task DeleteProduccionEficienciaMongo(int linea, DateTime fecha)
        {
            var turnos = storeProcedures.GetShifts(fecha);
            foreach (var turno in turnos)
            {
                ProduccionEficienciaMongo eficienciaMongo = new ProduccionEficienciaMongo();
                List<ProduccionEficiencia> eficiencia = await storeProcedures.GetEfficiencyProduction(linea, turno.Inicio, turno.Fin);

                // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                //filtros
                var builder = Builders<ProduccionEficienciaMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, linea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin);

                eficienciaMongo = await produccionEficienciaRepository.GetAsync(filter);
                if (eficienciaMongo != null)
                    await produccionEficienciaRepository.DeleteAsync(eficienciaMongo._Id);
            }
        }
        public async Task DeleteTimeStopsMongo(int linea, DateTime fecha)
        {
            var turno = storeProcedures.GetDay(fecha);
            var estados = await equiposEstadosRepository.ListAsync(null, null, new EquiposEstadosFilter() { TipoEquipo = 1 });

            foreach (var estado in estados)
            {
                var builder = Builders<TimeStopsMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, linea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin) &
                    builder.Eq(widget => widget.IdEstado, estado.Id);

                var timeStopsMongo = await timeStopsRepository.GetAsync(filter);
                if (timeStopsMongo != null)
                    await timeStopsRepository.DeleteAsync(timeStopsMongo._Id);
            }
        }

        public async Task DeleteMongo(int linea, DateTime fecha)
        {
            await DeleteAverageTimeFailuresProductionMongo(linea, fecha);
            await DeleteKpiDepletionMongo(linea, fecha);
            await DeleteMachineStatusProductionMongo(linea, fecha);
            await DeleteProduccionEficienciaByShiftMongo(linea, fecha);
            await DeleteProduccionEficienciaMongo(linea, fecha);
            await DeleteTimeStopsMongo(linea, fecha);
        }
    }
}
