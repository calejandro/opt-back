﻿using AutoMapper;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Services.Implementations
{
    public class SysUserReportConfigurationService : GenericServiceMapper<SysUserReportConfiguration, SysUserReportConfigurationCreateViewModel, int, SysUserReportConfigurationFilter, SysUserReportConfigurationViewModel, ISysUserReportConfigurationRepository>, ISysUserReportConfigurationService
    {

        public SysUserReportConfigurationService(ISysUserReportConfigurationRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}