﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class LineasProduccionEficienciaMesService : GenericServiceMapper<LineasProduccionEficienciaMes, LineasProduccionEficienciaMesCreateViewModel, int, LineaFilters, LineasProduccionEficienciaMesViewModel, ILineasProduccionEficienciaMesRepository>, ILineasProduccionEficienciaMesService
    {
        public LineasProduccionEficienciaMesService(ILineasProduccionEficienciaMesRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
        public async override Task<LineasProduccionEficienciaMesViewModel> InsertAsync(LineasProduccionEficienciaMesCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<LineasProduccionEficienciaMes>(entity)))
                throw new BussinessException(ExceptionConstants.ERROR_EFICIENCIA_MENSUAL_DUPLICADO, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.InsertAsync(entity);
        }
        public async override Task<LineasProduccionEficienciaMesViewModel> UpdateAsync(LineasProduccionEficienciaMesCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<LineasProduccionEficienciaMes>(entity)))
                throw new BussinessException(ExceptionConstants.ERROR_EFICIENCIA_MENSUAL_DUPLICADO, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryUpdate);
            return await base.UpdateAsync(entity);
        }
    }
}
