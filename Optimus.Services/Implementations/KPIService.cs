﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Services.Implementations
{
    public class KpiService : GenericServiceMapper<Kpi, KpiViewModel, Guid, BaseFilter, KpiViewModel, IKpiRepository>, IKpiService
    {
        public KpiService(IKpiRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
