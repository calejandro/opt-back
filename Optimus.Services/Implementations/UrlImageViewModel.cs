﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;

namespace Optimus.Services.ViewModels
{
    public partial class UrlImageViewModel 
    {
        public  Guid Id { get; set; }
        public int IdEntity { get; set; }
        public string Entity { get; set; }
        public string Name { get; set; }
        public int? IdPlanta { get; set; }

        public LineasProduccionAlertasViewModel LineasProduccionAlertas { get; set; }

}
}
