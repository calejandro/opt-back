﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;


namespace Optimus.Services.Implementations
{
    public class ProfileService : IProfileService
    {

        private readonly IUserClaimsPrincipalFactory<UserIdentity> claimsFactory;
        private readonly UserManager<UserIdentity> userManager;
        private readonly IdentityContext apiContext;

        public ProfileService(UserManager<UserIdentity> userManager,
            IUserClaimsPrincipalFactory<UserIdentity> claimsFactory,
            IdentityContext apiContext
          )
        {
            this.userManager = userManager;
            this.claimsFactory = claimsFactory;
            this.apiContext = apiContext;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {            
            var sub = context.Subject.GetSubjectId();
            var user = await this.userManager.FindByIdAsync(sub);
            var rol = await this.userManager.GetRolesAsync(user);

            var claims = new List<Claim>();

            //claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();
            claims.Add(new Claim("sub", sub));
            claims.Add(new Claim("name", user.UserName));
            claims.Add(new Claim("giveName", user.Name));
            claims.Add(new Claim("surName", user.LastName));
            //claims.Add(new Claim("roleName", rol.First()));
            // Add custom claims in token here based on user properties or any other source


            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await this.userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}
