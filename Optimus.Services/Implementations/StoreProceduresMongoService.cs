﻿using MongoDB.Driver;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class StoreProceduresMongoService : IStoreProceduresMongoService
    {
        protected IProduccionEficienciaRepository produccionEficienciaRepository;
        protected IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository;
        protected IKpiDepletionRepository kpiDepletionRepository;
        protected IMachineActualStatusRepository machineActualStatusRepository;
        protected IMachineStatusProductionOrderRepository machineStatusProductionOrderRepository;
        protected ITimeStopsRepository timeStopsRepository;
        protected IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository;
        protected ILineasProduccionUsuarioRepository lineasProduccionUsuarioRepository;
        private readonly IStoreProceduresService storeProceduresService;

        public StoreProceduresMongoService(
        IProduccionEficienciaRepository produccionEficienciaRepository,
        IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository,
        IKpiDepletionRepository kpiDepletionRepository,
        IMachineActualStatusRepository machineActualStatusRepository,
        IMachineStatusProductionOrderRepository machineStatusProductionOrderRepository,
        ITimeStopsRepository timeStopsRepository,
        IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository,
         ILineasProduccionUsuarioRepository lineasProduccionUsuarioRepository,
        IStoreProceduresService storeProceduresService)
        {
            this.produccionEficienciaRepository = produccionEficienciaRepository;
            this.produccionEficienciaByShiftRepository = produccionEficienciaByShiftRepository;
            this.kpiDepletionRepository = kpiDepletionRepository;
            this.machineActualStatusRepository = machineActualStatusRepository;
            this.machineStatusProductionOrderRepository = machineStatusProductionOrderRepository;
            this.timeStopsRepository = timeStopsRepository;
            this.averageTimeFailuresProductionRepository = averageTimeFailuresProductionRepository;
            this.lineasProduccionUsuarioRepository = lineasProduccionUsuarioRepository;
            this.storeProceduresService = storeProceduresService;
        }
        public async Task<List<ProduccionEficiencia>> GetEfficiencyProductionMongo(int idLinea, List<int> idLineas, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            if(idLinea != 0 && !idLineas.Contains(idLinea))
            {
                idLineas.Add(idLinea);
            }
            List<ProduccionEficiencia> result = new List<ProduccionEficiencia>();

            foreach (var item in idLineas)
            {
                // verificamos que hay o hubo una línea tomada para ese rango de fechas
                if (!await lineasProduccionUsuarioRepository.IsLineaConToma(item, fechaHoraInicio, fechaHoraFin))
                    return result;
                var builder = Builders<ProduccionEficienciaMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, item) &
                    builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                    builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin);

                ProduccionEficienciaMongo produccionEficiencia = await this.produccionEficienciaRepository.GetAsync(filter);
                List<ProduccionEficiencia> produccionEficiencias;
                if (produccionEficiencia?.ProduccionEficiencia == null) // si no esta en la base mongo buscamos en el SP
                {
                    var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                    var produccionEficienciaSP = await this.storeProceduresService.GetEfficiencyProduction(idLinea, fechaHoraInicio, fechaHoraFin);
                    if (produccionEficienciaSP != null)
                    {
                        // si esta en el SP guardamos en mongo 
                        await this.produccionEficienciaRepository.InsertAsync(new ProduccionEficienciaMongo()
                        {
                            IdLinea = item,
                            FechaHoraInicio = fechaHoraInicio,
                            FechaHoraFin = fechaHoraFin,
                            Turno = turno,
                            ProduccionEficiencia = produccionEficienciaSP,
                            Created = DateTime.Now,
                            CreatedBy = "api"
                        });
                    }
                    produccionEficiencias = produccionEficienciaSP;
                }
                else
                {
                    produccionEficiencias = produccionEficiencia?.ProduccionEficiencia;
                }

                if(produccionEficiencias != null)
                {
                    result.AddRange(produccionEficiencias);
                }
                
            }

            return result;
            
        }
        public async Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShiftMongo(int idLinea, List<int> idLineas, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            if(idLinea != 0 && !idLineas.Contains(idLinea))
            {
                idLineas.Add(idLinea);
            }
            List<ProduccionEficienciaByShift> result = new List<ProduccionEficienciaByShift>();

            foreach (var item in idLineas)
            {
                // verificamos que hay o hubo una línea tomada para ese rango de fechas
                if (!await lineasProduccionUsuarioRepository.IsLineaConToma(item, fechaHoraInicio, fechaHoraFin))
                    return result;

                var builder = Builders<ProduccionEficienciaByShiftMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, item) &
                    builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                    builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin);
                var produccionEficiencia = await this.produccionEficienciaByShiftRepository.GetAsync(filter);
                List<ProduccionEficienciaByShift> produccionEficienciaByShifts = new List<ProduccionEficienciaByShift>();
                if (produccionEficiencia?.ProduccionEficienciaByShift == null)
                {
                    var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                    var produccionEficienciaSP = await this.storeProceduresService.GetEfficiencyProductionByShift(idLinea, fechaHoraInicio, fechaHoraFin);
                    if (produccionEficienciaSP != null)
                    {
                        await this.produccionEficienciaByShiftRepository.InsertAsync(new ProduccionEficienciaByShiftMongo()
                        {
                            IdLinea = item,
                            FechaHoraInicio = fechaHoraInicio,
                            FechaHoraFin = fechaHoraFin,
                            Turno = turno,
                            ProduccionEficienciaByShift = produccionEficienciaSP,
                            Created = DateTime.Now,
                            CreatedBy = "api"
                        });
                    }
                    produccionEficienciaByShifts = produccionEficienciaSP;
                }
                else
                {
                    produccionEficienciaByShifts = produccionEficiencia?.ProduccionEficienciaByShift;
                }
                if(produccionEficienciaByShifts != null)
                {
                    result.AddRange(produccionEficienciaByShifts);
                }
                
            }

            return result;
            
        }
        public async Task<List<KpiDepletion>> GetKpiDepletionForBottlesMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, GroupingTypeEnum tipoAgrupacion)
        {
            // verificamos que hay o hubo una línea tomada para ese rango de fechas
            if (!await lineasProduccionUsuarioRepository.IsLineaConToma(idLinea, fechaHoraInicio, fechaHoraFin))
                return new List<KpiDepletion>();
            var builder = Builders<KpiDepletionMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, idLinea) & 
                builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin) &
                builder.Eq(widget => widget.tipoAgrupacion, tipoAgrupacion);
            var kpiDepletionMongo = await this.kpiDepletionRepository.GetAsync(filter);

            
            if (kpiDepletionMongo?.KpiDepletion == null)
            {
                var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                var kpiDepletionMongoSP = await this.storeProceduresService.GetKpiDepletionForBottles(idLinea, fechaHoraInicio, fechaHoraFin, tipoAgrupacion);
                if (kpiDepletionMongoSP != null)
                {
                    await this.kpiDepletionRepository.InsertAsync(new KpiDepletionMongo()
                    {
                        IdLinea = idLinea,
                        FechaHoraInicio = fechaHoraInicio,
                        FechaHoraFin = fechaHoraFin,
                        Turno = turno,
                        KpiDepletion = kpiDepletionMongoSP,
                        tipoAgrupacion = tipoAgrupacion,
                        Created = DateTime.Now,
                        CreatedBy = "api"
                    });
                }
                return kpiDepletionMongoSP;
            }
            return kpiDepletionMongo?.KpiDepletion;
        }

        public async Task<List<MachineActualStatus>> GetMachineActualStatusMongo(int idLinea)
        {

            var builder = Builders<MachineActualStatusMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, idLinea);

            MachineActualStatusMongo machineActualStatusMongo = await this.machineActualStatusRepository.GetAsync(filter);
            if (machineActualStatusMongo?.MachineActualStatus == null)
            {

                var kpiDepletionMongoSP = await this.storeProceduresService.GetMachineActualStatus(idLinea);
                if (kpiDepletionMongoSP != null)
                {
                    await this.machineActualStatusRepository.InsertAsync(new MachineActualStatusMongo()
                    {
                        IdLinea = idLinea,
                        MachineActualStatus = kpiDepletionMongoSP,
                        Created = DateTime.Now,
                        CreatedBy = "api"
                    });
                }
                return kpiDepletionMongoSP;
            }
            return machineActualStatusMongo?.MachineActualStatus;
        }
        public async Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrdersMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            // verificamos que hay o hubo una línea tomada para ese rango de fechas
            if (!await lineasProduccionUsuarioRepository.IsLineaConToma(idLinea, fechaHoraInicio, fechaHoraFin))
                return null;
            var builder = Builders<MachineStatusProductionOrderMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, idLinea) &
                builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin);
            var machineStatusProductionOrderMongo = await this.machineStatusProductionOrderRepository.GetAsync(filter);

            if (machineStatusProductionOrderMongo?.MachineStatusProductionOrder == null)
            {
                var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                var machineStatusProductionOrderSP = await this.storeProceduresService.GetMachineStatusProductionOrders(idLinea, fechaHoraInicio, fechaHoraFin);
                if (machineStatusProductionOrderSP != null)
                {
                    await this.machineStatusProductionOrderRepository.InsertAsync(new MachineStatusProductionOrderMongo()
                    {
                        IdLinea = idLinea,
                        FechaHoraInicio = fechaHoraInicio,
                        FechaHoraFin = fechaHoraFin,
                        Turno = turno,
                        MachineStatusProductionOrder = machineStatusProductionOrderSP,
                        Created = DateTime.Now,
                        CreatedBy = "api"
                    });
                }
                return machineStatusProductionOrderSP;
            }
            return machineStatusProductionOrderMongo?.MachineStatusProductionOrder;
        }
        public async Task<List<TimeStops>> GetTimeStopsMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, int idEstado)
        {
            if (!await lineasProduccionUsuarioRepository.IsLineaConToma(idLinea, fechaHoraInicio, fechaHoraFin))
                return null;
            var builder = Builders<TimeStopsMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, idLinea) &
                builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin) &
                builder.Eq(widget => widget.IdEstado, idEstado);
            var timeStopsMongo = await this.timeStopsRepository.GetAsync(filter);

            if (timeStopsMongo?.TimeStops == null)
            {
                var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                var timeStopsMongoSP = await this.storeProceduresService.GetTimeStops(idLinea, fechaHoraInicio, fechaHoraFin, idEstado);
                if (timeStopsMongoSP != null)
                {
                    await this.timeStopsRepository.InsertAsync(new TimeStopsMongo()
                    {
                        IdLinea = idLinea,
                        FechaHoraInicio = fechaHoraInicio,
                        FechaHoraFin = fechaHoraFin,
                        Turno = turno,
                        TimeStops = timeStopsMongoSP,
                        Created = DateTime.Now,
                        CreatedBy = "api",
                        IdEstado = idEstado
                    });
                }
                return timeStopsMongoSP;
            }
            return timeStopsMongo.TimeStops;
        }
        public async Task<List<AverageTimeFailuresProduction>> AverageTimeFailuresProductionMongo(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            if (!await lineasProduccionUsuarioRepository.IsLineaConToma(idLinea, fechaHoraInicio, fechaHoraFin))
                return null;
            var builder = Builders<AverageTimeFailuresProductionMongo>.Filter;
            var filter = builder.Eq(widget => widget.IdLinea, idLinea) &
                builder.Eq(widget => widget.FechaHoraInicio, fechaHoraInicio) &
                builder.Eq(widget => widget.FechaHoraFin, fechaHoraFin);
            var averageTimeFailuresProduction = await this.averageTimeFailuresProductionRepository.GetAsync(filter);

            if (averageTimeFailuresProduction?.AverageTimeFailuresProduction == null)
            {
                var turno = this.storeProceduresService.GetNameShift(fechaHoraInicio, fechaHoraFin);
                var averageTimeFailuresProductionSP = await this.storeProceduresService.GetAverageTimeFailuresProduction(idLinea, fechaHoraInicio, fechaHoraFin);
                if (averageTimeFailuresProductionSP != null)
                {
                    await this.averageTimeFailuresProductionRepository.InsertAsync(new AverageTimeFailuresProductionMongo()
                    {
                        IdLinea = idLinea,
                        FechaHoraInicio = fechaHoraInicio,
                        FechaHoraFin = fechaHoraFin,
                        Turno = turno,
                        AverageTimeFailuresProduction = averageTimeFailuresProductionSP,
                        Created = DateTime.Now,
                        CreatedBy = "api"
                    });
                }
                return averageTimeFailuresProductionSP;
            }
            return averageTimeFailuresProduction?.AverageTimeFailuresProduction;
        }

        public Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropiaHora(int idLinea, int idEquipo)
        {
            return this.storeProceduresService.GetEvolucionParadaPropiaHora(idLinea, idEquipo);
        }
    }
}
