﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class OptimusTaskService : GenericServiceMapper<OptimusTask, OptimusTaskCreateViewModel, Guid, OptimusTaskFilter, OptimusTaskViewModel, IOptimusTaskRepository>, IOptimusTaskService
    {
        private readonly IUserRepository userRepository;
        public OptimusTaskService(IUserRepository userRepository, IOptimusTaskRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
            this.userRepository = userRepository;
        }

        public async  Task<OptimusTaskViewModel> InsertAsync(OptimusTaskCreateViewModel entity, Guid idCognito)
        {
            var user = await this.userRepository.GetByCognitoId(idCognito);
            var optimusTask = this.mapper.Map<OptimusTask>(entity);
            optimusTask.IdCreatorUser = user.Id;
            optimusTask.Identifier = await LastIdentifier(entity.IdParentOptimusTask);
            return this.mapper.Map<OptimusTaskViewModel>(await genericRepository.InsertAsync(optimusTask));
        }
        public async Task<OptimusTaskViewModel> ArchiveUnarchiveAsync(Guid id)
        {
            var task = await this.genericRepository.GetAsync(id);
            task.Archived = !task.Archived;
            return this.mapper.Map<OptimusTaskViewModel>(await this.genericRepository.UpdateAsync(task));    
        }
        public async Task<List<OptimusTaskViewModel>> MyOptimusTaskListAsync(Guid idCognito, TypeUserToOptimusTaskEnum typeUser, string[] includes=null, OptimusTaskFilter filter = null, PaginatorRequestModel paginator = null, List<Order> orders = null)
        {
            var user = await this.userRepository.GetByCognitoId(idCognito);
            switch (typeUser)
            {
                case TypeUserToOptimusTaskEnum.Creator:
                    filter.IdCreatorUser = user.Id;
                    break;
                case TypeUserToOptimusTaskEnum.Validator:
                    filter.IdVerifierUser = user.Id;
                    break;
                case TypeUserToOptimusTaskEnum.Assigned:
                    filter.IdAssignedUser = user.Id;
                    break;
                default:
                    filter.CreOrAssiOrVerf = user.Id;
                    break;
            }

            return await base.ListAsync(includes, filter, paginator,orders);
        }
        public override Task<int> CountAsync(OptimusTaskFilter filter, string[] includes = null)
        {
            return base.CountAsync(filter, includes);
        }
        public async Task<int> MyOptimusTaskCountAsync(Guid idCognito, TypeUserToOptimusTaskEnum typeUser, OptimusTaskFilter filter=null, string[] includes = null)
        {

            var user = await this.userRepository.GetByCognitoId(idCognito);
            switch (typeUser)
            {
                case TypeUserToOptimusTaskEnum.Creator:
                    filter.IdCreatorUser = user.Id;
                    break;
                case TypeUserToOptimusTaskEnum.Validator:
                    filter.IdVerifierUser = user.Id;
                    break;
                case TypeUserToOptimusTaskEnum.Assigned:
                    filter.IdAssignedUser = user.Id;
                    break;
                default:
                    filter.CreOrAssiOrVerf = user.Id;
                    break;
            }

            return await base.CountAsync(filter,includes);
        }
        public async Task<OptimusTaskViewModel> UpdateAsync(Guid idCognito, OptimusTaskCreateViewModel entity)
        {
            //await ValidIdCreate(idCognito, entity.Id);
            return await base.UpdateAsync(entity);
        }
        public async Task DeleteAsync(Guid idCognito, Guid id)
        {
            //await ValidIdCreate(idCognito, id);
            await base.DeleteAsync(id);
        }


        private async Task ValidIdCreate(Guid idCognito, Guid id)
        {
            var optimusTaskBD = await genericRepository.GetAsync(id);
            var user = await this.userRepository.GetByCognitoId(idCognito);
           // var optimusTaskBD = (await genericRepository.ListAsync(null,null,new OptimusTaskFilter { Id = id },null,true)).FirstOrDefault();
            if (user.Id != optimusTaskBD.IdCreatorUser)
            {
                throw new BussinessException(ExceptionConstants.ERROR_OPTIMUS_TASK_IS_CREADOR, BusinessExceptionCodeEnumeration.OPTTASK_idCreador);
            }
        }

        private async Task<string> LastIdentifier(Guid? id)
        {
            var firstPart = "MT";
            if (id.HasValue)
            {
                firstPart = "ST";
            }
            var lastOptimusTask = (await base.ListAsync(null, new OptimusTaskFilter { Identifier = firstPart },
                new PaginatorRequestModel { Page = 0, RowsPage = 1 },
                new List<Order> { new Order { Descending = true, Property = "CreatedAT" } }
                )).FirstOrDefault();

            if (lastOptimusTask != null)
            {
                var number = Convert.ToInt32(lastOptimusTask.Identifier.Substring(2)) + 1;
                return firstPart + number;
            }
            else
            {
                return firstPart + 1;
            }
        }

    }
}
