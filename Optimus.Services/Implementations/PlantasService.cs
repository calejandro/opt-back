﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.Implementations
{
    public class PlantasService : GenericServiceMapper<Plantas, PlantasCreateViewModel, int, BaseFilter, PlantasViewModel, IPlantasRepository>, IPlantasService
    {
        public PlantasService(IPlantasRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
    }
}
