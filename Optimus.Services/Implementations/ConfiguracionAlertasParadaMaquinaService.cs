﻿using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Services.Implementations
{
    public class ConfiguracionAlertasParadaMaquinaService : GenericServiceMapper<ConfiguracionAlertasParadaMaquina, ConfiguracionAlertasParadaMaquinaCreateViewModel, int, ConfigAlertasParadaMaqFilter, ConfiguracionAlertasParadaMaquinaViewModel, IConfiguracionAlertasParadaMaquinaRepository>, IConfiguracionAlertasParadaMaquinaService
    {
        public ConfiguracionAlertasParadaMaquinaService(IConfiguracionAlertasParadaMaquinaRepository systemModuleRepository, IMapper mapper) : base(systemModuleRepository, mapper)
        {
        }
        public async override Task<ConfiguracionAlertasParadaMaquinaViewModel> InsertAsync(ConfiguracionAlertasParadaMaquinaCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<ConfiguracionAlertasParadaMaquina>(entity), false))
                throw new BussinessException(ExceptionConstants.ERROR_ALERTA_PARADA_PROPIA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.InsertAsync(entity);
        }
        public async override Task<ConfiguracionAlertasParadaMaquinaViewModel> UpdateAsync(ConfiguracionAlertasParadaMaquinaCreateViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<ConfiguracionAlertasParadaMaquina>(entity), true))
                throw new BussinessException(ExceptionConstants.ERROR_ALERTA_PARADA_PROPIA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryUpdate);
            return await base.UpdateAsync(entity);
        }
    }
}
