﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Optimus.Commons.Exceptions;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.Support.Constants;
using Optimus.Services.ViewModels;

namespace Optimus.Services.Implementations
{
    public class LineaProduccionObjetivosConfigService :
        GenericServiceMapper<LineasProduccionObjetivosConfig, LineasProduccionObjetivosConfigViewModel, Guid, LineasProduccionObjetivosConfigFilter, LineasProduccionObjetivosConfigViewModel, ILineaProduccionObjetivosConfigRepository>, ILineaProduccionObjetivosConfigService
    {
        public LineaProduccionObjetivosConfigService(ILineaProduccionObjetivosConfigRepository repository, IMapper mapper): base(repository, mapper)
        {
        }
        public async override Task<LineasProduccionObjetivosConfigViewModel> InsertAsync(LineasProduccionObjetivosConfigViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<LineasProduccionObjetivosConfig>(entity), false))
                throw new BussinessException(ExceptionConstants.ERROR_OBJETIVO_LINIA_PROPIA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryCreate);

            return await base.InsertAsync(entity);
        }
        public async override Task<LineasProduccionObjetivosConfigViewModel> UpdateAsync(LineasProduccionObjetivosConfigViewModel entity)
        {
            if (await this.genericRepository.ExistsEntityAsync(this.mapper.Map<LineasProduccionObjetivosConfig>(entity), true))
                throw new BussinessException(ExceptionConstants.ERROR_OBJETIVO_LINIA_PROPIA, BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryUpdate);
            return await base.UpdateAsync(entity);
        }
    }
}
