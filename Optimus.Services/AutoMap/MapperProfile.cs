﻿using AutoMapper;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using Optimus.Services.ViewModels.Identity;

namespace Optimus.Services.AutoMap
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<LineasProdSapToLineasProdElp, LineasProdSapToLineasProdElpViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<AlertaMermaConfiguraciones, AlertaMermaConfiguracionesViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionAlertasParadaMaquina, ConfiguracionAlertasParadaMaquinaViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionUmbralesTmefTmpr, ConfiguracionUmbralesTmefTmprViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<EquiposEstados, EquiposEstadosViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Equipos, EquiposViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionAlertas, LineasProduccionAlertasViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccion, LineasProduccionViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionUsuario, LineasProduccionUsuarioViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Paises, PaisesViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Plantas, PlantasViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TiposFallos, TiposFallosViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TmefTmpr, TmefTmprViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapSku, SapSkuViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TiposFallos, CausasFallosViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionEficienciaMes, LineasProduccionEficienciaMesViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUserReportConfiguration, SysUserReportConfigurationViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUsersGroups, SysUsersGroupsViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapOrdenesProduccion, SapOrdenesProduccionViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUsers, SysUsersViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<UserIdentity, UserViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUsersPlants, SysUsersPlantsViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<RegistrosFallosAlertas, RegistrosFallosAlertasViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<UrlImage, UrlImageViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionUmbralesAlertasPrecaucion, ConfiguracionUmbralesAlertasPrecaucionViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUsersGroups, UserGroupViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysMultipleGroups, MultipleGroupsViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionObjetivosConfig, LineasProduccionObjetivosConfigViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<OptimusTask, OptimusTaskViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TaskComment, TaskCommentViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<FileUrl, FileUrlViewModel>()
          .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
           CreateMap<Kpi, KpiViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
           CreateMap<Sector, SectorViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
           CreateMap<Process, ProcessViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();


            // Creates
            CreateMap<AlertaMermaConfiguraciones, AlertaMermaConfiguracionesCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionAlertasParadaMaquina, ConfiguracionAlertasParadaMaquinaCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionUmbralesTmefTmpr, ConfiguracionUmbralesTmefTmprCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<EquiposEstados, EquiposEstadosCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Equipos, EquiposCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionAlertas, LineasProduccionAlertasCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccion, LineasProduccionCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionUsuario, LineasProduccionUsuarioCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Paises, PaisesCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<Plantas, PlantasCreateViewModel>()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TiposFallos, TiposFallosCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TmefTmpr, TmefTmprCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapSku, SapSkuCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TiposFallos, CausasFallosCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<LineasProduccionEficienciaMes, LineasProduccionEficienciaMesCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SysUserReportConfiguration, SysUserReportConfigurationCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapOrdenesProduccion, SapOrdenesProduccionCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapOrdenesProduccion, SapOrdProdLPUCreateViewModel>()
          .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<UserIdentity, UserCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
           .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<SapOrdenesProduccionViewModel, SapOrdenesProduccionCreateViewModel>()
          .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<ConfiguracionUmbralesAlertasPrecaucion, ConfiguracionUmbralesAlertasPrecaucionCreateViewModel>()
             .IgnoreAllPropertiesWithAnInaccessibleSetter()
             .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<OptimusTask, OptimusTaskCreateViewModel>()
          .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<TaskComment, TaskCommentCreateViewModel>()
           .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
            CreateMap<RegistrosFallosAlertas, RegistrosFallosAlertasCreateViewModel>()
          .IgnoreAllPropertiesWithAnInaccessibleSetter()
          .IgnoreAllSourcePropertiesWithAnInaccessibleSetter().ReverseMap();
        }
    }
}
