﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class RegistrosFallosAlertasViewModel
    {
        public int Id { get; set; }
        public int IdAlerta { get; set; }
        public int IdModoFallo { get; set; }
        public int IdFallo { get; set; }
        public int IdCausa { get; set; }
        public string AccionTomada { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public bool? Borrado { get; set; }
        public TiposFallosViewModel ModoFallo { get; set; }
        public TiposFallosViewModel Fallo { get; set; }
        public TiposFallosViewModel Causa { get; set; }
    }
}
