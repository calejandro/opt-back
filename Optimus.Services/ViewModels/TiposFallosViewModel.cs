﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class TiposFallosViewModel
    {
        public int IdTipo { get; set; }
        public string Nombre { get; set; }
        public TipoModoFalloEnum Tipo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public bool? Borrado { get; set; }
        public bool? Activo { get; set; }
        public int? IdEquipo { get; set; }
        public int? IdLinea { get; set; }
        public TipoAlertEnum? TipoAlerta { get; set; }
        public int? IdPlanta { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public EquiposViewModel Equipo { get; set; }
        public PlantasViewModel Planta { get; set; }
    }
}
