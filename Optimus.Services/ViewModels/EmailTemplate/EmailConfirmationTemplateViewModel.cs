﻿using Newtonsoft.Json;

namespace Optimus.Services.ViewModels.EmailTemplate
{
    public class EmailConfirmationTemplateViewModel
    {
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("urlConfirm")]
        public string UrlConfirm { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrlApp { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }
        public EmailConfirmationTemplateViewModel()
        {
        }
    }
}
