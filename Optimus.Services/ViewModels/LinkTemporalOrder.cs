﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class LinkTemporalOrder
    {
        public int IdOrdenSap { get; set; }
        public int IdOrdenSapTemporal { get; set; }
    }
}
