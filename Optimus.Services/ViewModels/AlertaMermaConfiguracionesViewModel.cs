﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public partial class AlertaMermaConfiguracionesViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public decimal? MinimoPrimerAlerta { get; set; }
        public decimal? MinimoSegundaAlerta { get; set; }
        public TipoMerma? TipoMerma { get; set; }
        public int IdSku { get; set; }
        public int IdPlanta { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public EquiposViewModel Equipo { get; set; }
        public PlantasViewModel Planta { get; set; }

        public SapSkuViewModel SapSku { get; set; }
    }
}
