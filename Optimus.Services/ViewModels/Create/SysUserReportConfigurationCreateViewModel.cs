﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels.Create
{
    public class SysUserReportConfigurationCreateViewModel
    {
        public int Id { get; set; }
        public int UserGroupId { get; set; }
        public ReportTypeEnum ReportType { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
