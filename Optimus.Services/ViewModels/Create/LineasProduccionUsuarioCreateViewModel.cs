﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels.Create
{
    public  class LineasProduccionUsuarioCreateViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int UserId { get; set; }
        public DateTime? FechaToma { get; set; }
        public bool? Supervisada { get; set; }
        public int IdOrdenSap { get; set; }
        public bool? OrdenCumplimentada { get; set; }
        public bool? Borrado { get; set; }
        public bool? ExtiendeTurno { get; set; }
        public DateTime? FechaLiberacion { get; set; }
        public bool? TurnoFinalizado { get; set; }

        public SapOrdProdLPUCreateViewModel SapOrdenesProduccion { get; set; }

    }
}
