﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels.Create
{
    public class SapOrdProdLPUCreateViewModel
    {
        public int IdOrdenSap { get; set; }
        public DateTime? FhIncioProduccion { get; set; }
        public DateTime? FhFinProduccion { get; set; }
    }
}
