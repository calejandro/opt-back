﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class PlantasCreateViewModel
    {
        //public PlantasViewModel()
        //{
        //    SysUsersPlants = new HashSet<SysUsersPlants>();
        //}
        public int IdPlanta { get; set; }
        public string DescPlanta { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
        public int? IdPais { get; set; }


        // public virtual ICollection<SysUsersPlants> SysUsersPlants { get; set; }
    }
}
