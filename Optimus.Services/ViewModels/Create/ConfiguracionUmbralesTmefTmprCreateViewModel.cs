﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class ConfiguracionUmbralesTmefTmprCreateViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public TiempoPromTipoReparacionEnum Tipo { get; set; }
        public double ValorMinimo { get; set; }
        public double ValorMaximo { get; set; }
        public int IdPlanta { get; set; }

     
    }
}
