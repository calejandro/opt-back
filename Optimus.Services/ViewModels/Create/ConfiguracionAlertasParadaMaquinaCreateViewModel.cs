﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class ConfiguracionAlertasParadaMaquinaCreateViewModel
    {
        public int Id { get; set; }
        public int IdPlanta { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public int ValorMinimo { get; set; }
        public int? ValorMaximo { get; set; }
        public TipoAlertaParadaEnum TipoAlerta { get; set; }
        public int? PorcMinimo { get; set; }
        public int? PorcMaximo { get; set; }

      
    }
}
