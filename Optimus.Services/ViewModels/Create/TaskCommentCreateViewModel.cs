﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class TaskCommentCreateViewModel
    {
        public new Guid Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? DeliverDate { get; set; }
        public Guid? IdAnswer { get; set; }
        public Guid? IdOptimusTask { get; set; }
    }
}
