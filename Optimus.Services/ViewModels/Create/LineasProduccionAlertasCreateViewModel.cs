﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class LineasProduccionAlertasCreateViewModel
    {
        public int IdAlerta { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public EstadosLPAEnum Estado { get; set; }
        public double ValorAcumulado { get; set; }
        public bool EnvioDesvio { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool? Borrado { get; set; }
        public TipoAlertaParadaEnum? TipoAlerta { get; set; }
        public ClasificacionAlertaEnum? Clasificacion { get; set; }
        public string Descripcion { get; set; }
        public TipoMerma? TipoMerma { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Umbral { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public bool? NotificacionEnviada { get; set; }

     
    }
}
