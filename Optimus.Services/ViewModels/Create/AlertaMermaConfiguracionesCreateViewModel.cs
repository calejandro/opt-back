﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public partial class AlertaMermaConfiguracionesCreateViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public decimal? MinimoPrimerAlerta { get; set; }
        public decimal? MinimoSegundaAlerta { get; set; }
        public TipoModoFalloEnum? TipoMerma { get; set; }
        public int IdSku { get; set; }
        public int IdPlanta { get; set; }

    }
}
