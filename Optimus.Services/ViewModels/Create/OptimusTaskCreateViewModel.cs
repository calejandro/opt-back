﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class OptimusTaskCreateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? DeliverDate { get; set; }
        public DateTime? EndDate { get; set; }
        public StateOptimusTaskEnum IdState { get; set; }
        public bool Archived { get; set; }
        public Guid? IdAssignedUser { get; set; }
        public Guid? IdVerifierUser { get; set; }
        public Guid? IdKpi { get; set; }
        public Guid? IdSector { get; set; }
        public Guid? IdProcess { get; set; }
        public OptimusTaskTypeEnum? Type { get; set; }
        public OptimusTaskPriorityEnum? Priority { get; set; }
        public Guid? IdParentOptimusTask { get; set; }


    }
}
