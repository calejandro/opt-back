﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class LineasProduccionCreateViewModel
    {
        public int IdLinea { get; set; }
        public string DescLinea { get; set; }
        public int IdPlanta { get; set; }
        public int TipoLinea { get; set; }
        public int? PuedeRecuperarHistoricos { get; set; }
        public string TagOpcDiaTurnoRecuperar { get; set; }
        public string TagOpcFlagActivarRecuperacion { get; set; }
        public string TagOpcAnioPlc { get; set; }
        public string TagOpcMesPlc { get; set; }
        public string TagOpcDiaPlc { get; set; }
        public string TagOpcHoraPlc { get; set; }
        public string TagOpcMinutosPlc { get; set; }
        public string TagOpcSegundosPlc { get; set; }
        public string TagOpcProduccionH0 { get; set; }
        public string TagOpcProduccionH1 { get; set; }
        public string TagOpcProduccionH2 { get; set; }
        public string TagOpcProduccionH3 { get; set; }
        public string TagOpcProduccionH4 { get; set; }
        public string TagOpcProduccionH5 { get; set; }
        public string TagOpcProduccionH6 { get; set; }
        public string TagOpcProduccionH7 { get; set; }
        public string TagOpcProduccionH8 { get; set; }
        public string TagOpcProduccionH9 { get; set; }
        public string TagOpcProduccionH10 { get; set; }
        public string TagOpcProduccionH11 { get; set; }
        public string TagOpcProduccionH12 { get; set; }
        public string TagOpcProduccionH13 { get; set; }
        public string TagOpcProduccionH14 { get; set; }
        public string TagOpcProduccionH15 { get; set; }
        public string TagOpcProduccionH16 { get; set; }
        public string TagOpcProduccionH17 { get; set; }
        public string TagOpcProduccionH18 { get; set; }
        public string TagOpcProduccionH19 { get; set; }
        public string TagOpcProduccionH21 { get; set; }
        public string TagOpcProduccionH20 { get; set; }
        public string TagOpcProduccionH22 { get; set; }
        public string TagOpcProduccionH23 { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }

    }
}
