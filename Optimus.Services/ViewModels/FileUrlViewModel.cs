﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
   public class FileUrlViewModel
    {
        public new Guid Id { get; set; }
        public Guid? IdTaskComment { get; set; }
        public Guid? IdOptimusTask { get; set; }
        public string Name { get; set; }

    }
}
