﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class ImageViewModel
    {
        public string Name { get; set; }
        public string Data { get; set; }
    }
}
