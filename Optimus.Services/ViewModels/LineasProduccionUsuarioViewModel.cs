﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public  class LineasProduccionUsuarioViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int UserId { get; set; }
        public DateTime? FechaToma { get; set; }
        public bool? Supervisada { get; set; }
        public int IdOrdenSap { get; set; }
        public bool? OrdenCumplimentada { get; set; }
        public bool? Borrado { get; set; }
        public bool? ExtiendeTurno { get; set; }
        public DateTime? FechaLiberacion { get; set; }
        public bool? TurnoFinalizado { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public SapOrdenesProduccionViewModel SapOrdenesProduccion { get; set; }     
        public SysUsersViewModel SysUsers { get; set; }
    }
}
