﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class AcumEficienciasViewModels
    {
        public int IdLinea { get; set; }
        public int IdPlanta { get; set; }
        public double? EficienciaDiaria { get; set; }
        public double? EficienciaMensual { get; set; }
        public double? EficienciaAnual { get; set; }

    }
}
