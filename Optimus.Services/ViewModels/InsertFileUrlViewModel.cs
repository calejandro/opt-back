﻿using Microsoft.AspNetCore.Http;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class InsertFileUrlViewModel
    {
        public List<IFormFile> Files { get; set; }
        public FileUrlToBelongEnum ToBelong { get; set; }
        public InsertFileUrlViewModel()
        {
        }
    }
}
