﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class SysUsersGroupsViewModel
    {
        public int UserGroupId { get; set; }
        public string Description { get; set; }
        public int? Level { get; set; }
        public string InitUrl { get; set; }
        public int? SubsistemaId { get; set; }
        public string IsActive { get; set; }
    }
}
