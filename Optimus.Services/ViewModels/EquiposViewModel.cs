﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class EquiposViewModel
    {
        public int IdEquipo { get; set; }
        public string DescEquipo { get; set; }
        public int IdLinea { get; set; }
        public int? IdPlanta { get; set; }
        public byte? OrdenVisualizacion { get; set; }
        public int? TipoEquipo { get; set; }
        public int? PuedeRecuperarHistoricos { get; set; }
        public int? IntervaloMuestreoMinutos { get; set; }
        public int? CantidadDecimales { get; set; }
        public double? AlLeerMultiplicarPor { get; set; }
        public int? AlLeerMultiplicarPorPotenciaDiezALa { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
        public bool? ProrratearLecturasPorMinuto { get; set; }
        public decimal? IgnorarValorMenorA { get; set; }
        public decimal? IgnorarValorMayorA { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public PlantasViewModel Planta { get; set; }

        public List<AlertaMermaConfiguracionesViewModel> AlertaMermaConfiguraciones { get; set; }
    }
}
