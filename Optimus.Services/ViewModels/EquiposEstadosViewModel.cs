﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class EquiposEstadosViewModel
    {
        public int IdRegistro { get; set; }
        public int TipoEquipo { get; set; }
        public byte Estado { get; set; }
        public string DescEstado { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
        public string Color { get; set; }
    }
}
