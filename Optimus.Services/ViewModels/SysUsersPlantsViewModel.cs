﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class SysUsersPlantsViewModel
    {
        public int UserPlantId { get; set; }
        public int UserId { get; set; }
        public int PlantId { get; set; }
    }
}
