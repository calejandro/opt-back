﻿
namespace Optimus.Services.ViewModels
{
    public class SapSkuViewModel
    {
        public int IdSku { get; set; }
        public string DescSku { get; set; }
        public string Formato { get; set; }
        public string Marca { get; set; }
        public string Sabor { get; set; }
        public string IndicadorCarbono { get; set; }
        public string MaterialEnvase { get; set; }
        public string Retornable { get; set; }
        public string UnidadMedida { get; set; }
        public int ConversionBotellas { get; set; }
        public double ConversionLitros { get; set; }
        public int IdCentro { get; set; }
    }
}
