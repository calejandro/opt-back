﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Services.ViewModels
{
    public class UserGroupViewModel
    {       
                    
        public int UserGroupId { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        public int? Level { get; set; }
        [StringLength(100)]
        public string InitUrl { get; set; }
        public int? SubsistemaId { get; set; }
        [StringLength(1)]
        public string IsActive { get; set; }


        public UserGroupViewModel()
        {
        }
    }
}
