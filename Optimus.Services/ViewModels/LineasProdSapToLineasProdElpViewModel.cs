﻿using System;
using System.Collections.Generic;

namespace Optimus.Services.ViewModels
{
    public class LineasProdSapToLineasProdElpViewModel
    {        
        public int IdLineaElp { get; set; }
        
        public int IdLineaSap { get; set; }

        public LineasProduccionViewModel LineasProduccionElp { get; set; }

        public LineasProduccionViewModel LineasProduccionSap { get; set; }


        public LineasProdSapToLineasProdElpViewModel()
        {
        }
    }
}
