﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class SapOrdenesProduccionViewModel
    {
        public int IdOrdenSap { get; set; }
        public int IdSku { get; set; }
        public int CantidadTotalCajones { get; set; }
        public string FhInicio { get; set; }
        public string FhFin { get; set; }
        public string IdCentro { get; set; }
        public int IdPlanta { get; set; }
        public int IdLinea { get; set; }
        public string GrupoRecetas { get; set; }
        public int CantidadEntregada { get; set; }
        public int VelocidadNominalLlenadora { get; set; }
        public DateTime? FhIncioProduccion { get; set; }
        public DateTime? FhFinProduccion { get; set; }
        public DateTime? FechaSupervision { get; set; }
        public bool? EsTemporal { get; set; }
        public int? IdOrdenVinculada { get; set; }

        public SapSkuViewModel SapSku { get; set; }
        public LineasProduccionViewModel LineasProduccion { get; set; }
        public SapOrdenesProduccionViewModel SapOrdenesProdVinculada { get; set; }
    }
}
