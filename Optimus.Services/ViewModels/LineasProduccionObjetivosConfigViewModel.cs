﻿using System;
using Optimus.Models.Enum;

namespace Optimus.Services.ViewModels
{
    public class LineasProduccionObjetivosConfigViewModel
    {
        public Guid Id { get; set; }
        public int IdLinea { get; set; }
        public int IdPlanta { get; set; }
        public TipoObjetivoEnum Tipo { get; set; }
        public double Valor { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public LineasProduccionViewModel Linea { get; set; }

        public LineasProduccionObjetivosConfigViewModel()
        {
        }
    }
}
