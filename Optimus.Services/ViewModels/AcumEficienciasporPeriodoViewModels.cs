﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class AcumEficienciasPorPeriodoViewModels
    {
        public int IdLinea { get; set; }
        public int IdPlanta { get; set; }
        public double? Eficiencia { get; set; }
        public PeriodEfficiencyEnum PeriodoEficiencia { get; set; }
        public DateTime Fecha { get; set; }

    }
}
