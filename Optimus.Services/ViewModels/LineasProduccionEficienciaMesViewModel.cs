﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class LineasProduccionEficienciaMesViewModel
    {
        public int Id { get; set; }
        public int? IdLinea { get; set; }
        public double EficienciaMensual { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool? Borrado { get; set; }
        public int IdPlanta { get; set; }

        public PlantasViewModel Planta { get; set; }
        public LineasProduccionViewModel LineaProduccion { get; set; }

    }
}
