﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class LineasProduccionViewModel
    {
        public int IdLinea { get; set; }
        public string DescLinea { get; set; }
        public int IdPlanta { get; set; }
        public int TipoLinea { get; set; }
        public int? PuedeRecuperarHistoricos { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
        public PlantasViewModel Planta { get; set; }

        public LineasProdSapToLineasProdElpViewModel LineasProdSapToLineasProdElp { get; set; }

        public bool HasProductionOrder { get; set; }

    }
}
