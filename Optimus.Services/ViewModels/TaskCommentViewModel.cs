﻿using Optimus.Services.ViewModels.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class TaskCommentViewModel
    {
        public new Guid Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? DeliverDate { get; set; }
        public Guid? IdAnswer { get; set; }
        public Guid? IdOptimusTask { get; set; }
        public Guid IdCreatorUser { get; set; }


        public TaskCommentViewModel Answer { get; set; }
        public OptimusTaskViewModel OptimusTask { get; set; }
        public UserViewModel CreatorUser { get; set; }
        public List<FileUrlViewModel> FilesUrl { get; set; }

    }
}
