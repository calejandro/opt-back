﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class PlantasViewModel
    {
        //public PlantasViewModel()
        //{
        //    SysUsersPlants = new HashSet<SysUsersPlants>();
        //}

        public int IdPlanta { get; set; }
        public string DescPlanta { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
        public int? IdPais { get; set; }

        public PaisesViewModel Pais { get; set; }
        public List<SysUsersPlantsViewModel>? SysUsersPlantas { get; set; }
    }
}
