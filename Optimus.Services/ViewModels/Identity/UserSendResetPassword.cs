﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Services.ViewModels.Identity
{
    public class UserSendResetPassword
    {
        [Required]
        public string UserName { get; set; }

        public UserSendResetPassword()
        {
        }
    }
}
