﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Optimus.Services.ViewModels.Identity
{
    public class UserCreateViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(2)]
        [MaxLength(100)]
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }


        [Required]
        public bool IsEnabled { get; set; }


    }
}
