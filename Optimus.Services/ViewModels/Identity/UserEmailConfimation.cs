﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Services.ViewModels
{
    public class UserEmailConfirmation
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string RepeatPassword { get; set; }

        public UserEmailConfirmation()
        {
        }
    }
}
