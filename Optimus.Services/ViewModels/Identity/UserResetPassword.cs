﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Services.ViewModels.Identity
{
    public class UserResetPassword
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string RepeatPassword { get; set; }

        public UserResetPassword()
        {
        }
    }
}
