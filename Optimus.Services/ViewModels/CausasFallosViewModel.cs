﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class CausasFallosViewModel
    {
        public int IdTipo { get; set; }
        public string Nombre { get; set; }
        public TipoModoFalloEnum Tipo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public bool? Borrado { get; set; }
        public bool? Activo { get; set; }

    }
}
