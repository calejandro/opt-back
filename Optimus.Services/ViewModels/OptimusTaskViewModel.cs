﻿
using Optimus.Models.Enum;
using Optimus.Services.ViewModels.Identity;
using System;
using System.Collections.Generic;


namespace Optimus.Services.ViewModels
{

    public class OptimusTaskViewModel 
    {
        public Guid Id { get; set; }
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? DeliverDate { get; set; }
        public DateTime? EndDate { get; set; }
        public StateOptimusTaskEnum IdState { get; set; }
        public bool Archived { get; set; }
        public Guid IdCreatorUser { get; set; }
        public Guid? IdAssignedUser { get; set; }
        public Guid? IdVerifierUser { get; set; }
        public Guid? IdKpi { get; set; }
        public Guid? IdSector { get; set; }
        public Guid? IdProcess { get; set; }
        public OptimusTaskTypeEnum? Type { get; set; }
        public OptimusTaskPriorityEnum? Priority { get; set; }
        public Guid? IdParentOptimusTask { get; set; }


        public UserViewModel CreatorUser { get; set; }
        public UserViewModel? AssignedUser { get; set; }
        public UserViewModel? VerifierUser { get; set; }
        public KpiViewModel Kpi { get; set; }
        public SectorViewModel Sector { get; set; }
        public ProcessViewModel Process { get; set; }
        public OptimusTaskViewModel ParentOptimusTask { get; set; }

        public List<TaskCommentViewModel> TaskComments { get; set; }
        public List<FileUrlViewModel> FilesUrl { get; set; }
        public List<OptimusTaskViewModel> SubOptimusTask { get; set; }

    }
}
