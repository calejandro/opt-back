﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class InsertImagLineasProduccionAlertasViewModel
    {
        public List<IFormFile> Images { get; set; }

        public InsertImagLineasProduccionAlertasViewModel()
        {
        }
    }
}
