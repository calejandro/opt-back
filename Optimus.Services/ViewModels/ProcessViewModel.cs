﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class ProcessViewModel
    {
        public  Guid Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
    }
}
