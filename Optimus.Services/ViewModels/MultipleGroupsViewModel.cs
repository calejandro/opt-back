﻿using System;
namespace Optimus.Services.ViewModels
{
    public class MultipleGroupsViewModel
    {
        public int MultipleGroupId { get; set; }
        public int? UserId { get; set; }
        public int? UserGroupId { get; set; }

        public UserGroupViewModel UserGroup { get; set; }


        public MultipleGroupsViewModel()
        {
        }
    }
}
