﻿using Optimus.Services.ViewModels.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class SysUsersViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string InitUrl { get; set; }
        public string IsActive { get; set; }
        public int LanguageId { get; set; }
        public string ChangePass { get; set; }
        public string AgreedTerms { get; set; }
        public string Foto { get; set; }
        public DateTime? FechaAlta { get; set; }
        public string UserHorarioContacto { get; set; }
        public string Telefono { get; set; }
        public string CodAreaTelefono { get; set; }
        public bool EsAdministrador { get; set; }
        public Guid? IdUserIdentity { get; set; }
        public UserViewModel? UserIdentity { get; set; }

        public List<SysUsersPlantsViewModel>? SysUsersPlantas { get; set; }
        public List<MultipleGroupsViewModel>? Groups { get; set; }

    }
}
