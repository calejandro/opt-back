﻿using Optimus.Models.Enum;

namespace Optimus.Services.ViewModels
{
    public class ConfiguracionUmbralesAlertasPrecaucionViewModel
    {
        public  int Id { get; set; }
        public int IdPlanta { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public int ValorMinAlerta { get; set; }
        public int? ValorMaxAlerta { get; set; }
        public int ValorMinSalirAlerta { get; set; }
        public int? ValorMaxSalirAlerta { get; set; }
        public TipoAlertaParadaEnum TipoAlerta { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public EquiposViewModel Equipo { get; set; }
        public PlantasViewModel Planta { get; set; }
    }
}
