﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class TmefTmprViewModel
    {
        public int? IdLinea { get; set; }
        public int? IdEquipo { get; set; }
        public string FhInicio { get; set; }
        public string FhFin { get; set; }
        public double? Tmef { get; set; }
        public double? Tmpr { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public EquiposViewModel Equipo { get; set; }
    }
}
