﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class ConfiguracionUmbralesTmefTmprViewModel
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int IdEquipo { get; set; }
        public TiempoPromTipoReparacionEnum Tipo { get; set; }
        public double ValorMinimo { get; set; }
        public double ValorMaximo { get; set; }
        public int IdPlanta { get; set; }

        public LineasProduccionViewModel LineaProduccion { get; set; }
        public EquiposViewModel Equipo { get; set; }
        public PlantasViewModel Planta { get; set; }
    }
}
