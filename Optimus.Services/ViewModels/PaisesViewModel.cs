﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
    public class PaisesViewModel
    {
        public int IdPais { get; set; }
        public string DescPais { get; set; }
        public string FhUltModif { get; set; }
        public bool Activo { get; set; }
    }
}
