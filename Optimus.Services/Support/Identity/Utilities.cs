﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Optimus.Services.Support.Identity
{
    public class Utilities
    {
        /// <summary>
        /// Gets the hash.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string GetHash(string text)
        {
            UnicodeEncoding encoder = new UnicodeEncoding();
            SHA512Managed SHA512 = new SHA512Managed();
            return Convert.ToBase64String(SHA512.ComputeHash(encoder.GetBytes(text)));
        }
    }
}
