﻿using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Novell.Directory.Ldap;
using Optimus.Commons.Entities;
using Optimus.Commons.Ldap;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Identity;
using System;
using System.Threading.Tasks;

namespace Optimus.Services.Support.Identity
{
    public class CustomResourcePasswordValidator : IResourceOwnerPasswordValidator        
    {
        private readonly ILdapService<UserIdentity> ldapService;
        private readonly UserManager<UserIdentity> userManager;
        private readonly IUserService usersService;
        private readonly ISysUsersRepository sysUsersRepository;
        private bool isValid;
        private UserIdentity user;

        public CustomResourcePasswordValidator(
            ILdapService<UserIdentity> ldapService,
            UserManager<UserIdentity> userManager,
            IUserService usersService,
            ISysUsersRepository sysUsersRepository)
        {
            this.ldapService = ldapService;
            this.userManager = userManager;
            this.usersService = usersService;
            this.sysUsersRepository = sysUsersRepository;
        }

        public async virtual Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            await this.Validate(context);
        }

        protected async Task<(UserIdentity, bool)> Validate(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                //UserViewModel identiyUser = await this.usersService.GetAsync(null, filter: new UserFilter() { UserName = context.UserName });
                UserIdentity identiyUser = await this.userManager.FindByNameAsync(context.UserName);

                if (identiyUser?.PasswordHash != null)
                {
                    (user, isValid) = await this.ValidateIdentity(context);
                }
                else
                {
                    //Usuarios creados por SysUser
                    SysUsers sysUser = await this.sysUsersRepository.GetAsync(null, filter: new UserFilter() { UserName = context.UserName });
                    //Validar Pasword
                    if (Utilities.GetHash(context.Password) == sysUser?.Password)
                    {
                        (isValid, user) = await this.ldapService.ValidateAsync(context.UserName, context.Password);
                        if (!isValid)
                        {
                            await this.usersService.RegisterUserIdentity(new UserCreateViewModel()
                            {
                                UserName = sysUser.UserName,
                                Name = sysUser.FullName,
                                LastName = sysUser.LastName,
                                Email = sysUser.Email,
                                PhoneNumber = sysUser.Telefono,
                                IsEnabled = true
                            });
                            (isValid, user) = await this.ldapService.ValidateAsync(context.UserName, context.Password);
                            sysUser.IdUserIdentity = (user as IdentityUser<Guid>).Id;
                            await this.sysUsersRepository.UpdateAsync(sysUser);
                        }
                        context.Result = new GrantValidationResult(user.Id.ToString(), OidcConstants.AuthenticationMethods.Password);
                        await this.userManager.ResetAccessFailedCountAsync(user);
                    }
                }

                if (isValid)
                {
                    await this.ValidateAssignedRole(context, user);
                }
                else
                {
                    if (user != null)
                        await this.userManager.AccessFailedAsync(user);

                    context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Nombre de usuario o contraseña incorrecta.");
                }

                return (user, isValid);
            }
            catch (LdapException ex)
            {
                // Log exception
            }

            return (null, false);
        }

        private async Task ValidateAssignedRole(ResourceOwnerPasswordValidationContext context, UserIdentity user)
        {
            //    if (user == null)
            //        return;

            //    var userRoles = await this.userManager.GetRolesAsync(user);

            //    if (userRoles == null || userRoles.Count == 0)
            //        context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Actualmente su usuario no posee asignado un Rol. Contáctese con el administrador del sistema.");
        }
        private async Task<(UserIdentity, bool)> ValidateIdentity(ResourceOwnerPasswordValidationContext context)
        {
            (bool isValid, UserIdentity user) = await this.ldapService.ValidateAsync(context.UserName, context.Password);
            if (user != null)
            {
                var isLocked = await this.userManager.IsLockedOutAsync(user);
                if (this.userManager.SupportsUserLockout && isLocked)
                {
                    context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Actualmente su usuario esta bloqueado. Intente nuevamente mas tarde.");
                    return (null, false);
                }

                isValid = await this.userManager.CheckPasswordAsync(user, context.Password);
                if (user != null && isValid)
                {
                    context.Result = new GrantValidationResult(user.Id.ToString(), OidcConstants.AuthenticationMethods.Password);
                    await this.userManager.ResetAccessFailedCountAsync(user);
                }
            }
            return (user, isValid);
        }
    }
}
