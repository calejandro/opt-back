﻿namespace Optimus.Services.Support.Configuration
{
    public class AppConfig
    {
        public string BaseUrl { get; set; }

        public string ConfirmUrlSegment { get; set; }

        public string ResetPasswordUrlSegment { get; set; }

        public string FilePath { get; set; }


        public AppConfig()
        {
        }
    }
}
