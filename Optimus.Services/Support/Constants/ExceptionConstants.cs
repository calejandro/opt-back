﻿namespace Optimus.Services.Support.Constants
{
    /// <summary>
    /// Constantes para los mensajes en las excepciones.
    /// </summary>
    public class ExceptionConstants
    {
        #region Errores Usuarios
        public const string ERROR_USER_NOT_EXISTS = "Usuario inexistente.";
        public const string ERROR_USER_ASSIGN_SYSTEM_ROLE = "Este usuario ya posee otro rol de sistema. Solo puede tener un rol de sistema la vez.";
        public const string ERROR_USER_ALREADY_ASSIGN_ROLE = "Este usuario ya posee otro rol de unidad operativa, por lo que no puede tener asignado un rol de sistema.";
        public const string ERROR_USER_ASSIGN_NOT_SYSTEM_ROLE = "El Rol que esta intentado asignar no es un Rol de Sistema.";
        public const string ERROR_USER_HAS_ROLE_ROOT = "Este usuario ya posee rol ROOT. No puede asignarle otros roles.";
        public const string ERROR_USER_DUPLICATE_ENTRY_UPDATE = "Usuario duplicado: El nombre de usuario ya se encuentra utilizado por un usuario que fue dado de baja.";
        #endregion

        #region Errores generales
        public const string ERROR_GENERAL_DUPLICATE_ENTRY = "Esta entidad fue dada de baja.";
        public const string ERROR_GENERAL_LOG_NOT_EXISTS = "No existe un Log de ese tipo.";

        #endregion
        public const string ERROR_LPU_ID_ORDEN_SAP_NULL = "Orden Sap no puede ser nulo";

        #region Errores Orden SAP temporal
        public const string ERROR_SAP_ORDEN_EDIT_TEMPORAL = "No se puede editar una Orden Sap que no sea temporal";
        public const string ERROR_SAP_ORDEN_DELETE_TEMPORAL = "No se puede eliminar una Orden Sap que no sea temporal";
        public const string ERROR_SAP_ORDEN_LINK_TEMPORAL = "No se puede vincular una Orden Sap temporal";
        public const string ERROR_SAP_ORDEN_LINK_NO_TEMPORAL = "No se puede vincular una Orden Sap no temporal ";
        public const string ERROR__SAP_ORDEN_NO_MACH_LINEA = "No existe coincidencia en la línea de la orden SAP a vincular";
        public const string ERROR__SAP_ORDEN_NO_MACH_SKU = "No existe coincidencia en el SKU de la orden SAP a vincular ";
        public const string ERROR__SAP_ORDEN_IS_TAKEN = "No se puede vincular una orden SAP que ya fue tomada";
        public const string ERROR__SAP_ORDEN_IS_LIKE = "Ya se encuentra vinculada la orden temporal con la orden SAP seleccionada";
        #endregion
        #region Errores duplicado
        public const string ERROR_TIPO_FALLO_DUPLICADO = "Ya exististe un tipo fallo con esas características";
        public const string ERROR_CAUSA_DUPLICADO = "Ya exististe una causa con esas características";
        public const string ERROR_FALLOS_DUPLICADO = "Ya exististe un fallo con esas características";
        public const string ERROR_UMBRAL_DUPLICADO = "Ya exististe un umbral con esas características";
        public const string ERROR_ALERTA_PARADA_PROPIA = "Ya exististe una alerta parada propia con esas características";
        public const string ERROR_OBJETIVO_LINIA_PROPIA = "Ya exististe un objetivo con esas características";

        public const string ERROR_CONFIG_MERMA = "Ya exististe una configuración de merma con estas características";
        public const string ERROR_EFICIENCIA_MENSUAL_DUPLICADO = "Ya exististe una eficiencia mensual con esas características";
        #endregion
        #region Errores OptimusTask
        public const string ERROR_OPTIMUS_TASK_IS_CREADOR = "Solo el usuario que creo la tarea puede llevar a cabo esta acción";
        #endregion

    }
}
