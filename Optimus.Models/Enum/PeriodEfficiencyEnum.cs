﻿using System;
namespace Optimus.Models.Enum
{
    public enum PeriodEfficiencyEnum
    {
        Daily = 1,
        Monthly = 2,
        Annual = 3
    }
}
