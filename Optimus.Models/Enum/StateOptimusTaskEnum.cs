﻿using System;
namespace Optimus.Models.Enum
{
    public enum StateOptimusTaskEnum
    {
        Assigned = 1,
        Treatment = 2,
        Validation = 3,
        Finalized = 4,
    }
}
