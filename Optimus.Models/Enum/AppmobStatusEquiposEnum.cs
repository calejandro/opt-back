﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum AppmobStatusEquiposEnum
    {
        ParadaOperacional = 1,
        FueraProduccion = 2,
        Produccion=3,
        ParadaPropia=4,
        ParadaPorAlimentacion=5,
        ParadaPorDescarga=6,
        ParadaFuncional=7,
        SubVelocidad=8,
        NoDeterminado=0
    }
}
