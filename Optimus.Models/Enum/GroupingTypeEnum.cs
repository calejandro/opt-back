﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Optimus.Models.Enum
{   
    public enum GroupingTypeEnum
    {
        [Description("POR_LINEA_EQUIPO_HORA_A_HORA")] // primeras columas hora por hora   
        PerLineEquipmentHourByHour = 1,

        [Description("POR_LINEA_EQUIPO")] // ultimas columna
        PerLineEquipment = 2,

        [Description("POR_LINEA")] // total 1
        PerLine = 3,

        [Description("POR_LINEA_HORA_A_HORA")] // total 2 hora hora de la ultima linea de la tabla 
        PerLineHourByHour = 4
    }
}
