﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum OptimusTaskTypeEnum
    {
        Corrective = 1,
        Preventive = 2,
        Contingency = 3
    }
}
