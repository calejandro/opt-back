﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum EstadosLPAEnum
    {
        Nueva = 0,
        Informada = 1,
        Resuelto = 2,
        Cerrada = 3,
    }
}
