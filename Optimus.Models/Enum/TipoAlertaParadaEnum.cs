﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum TipoAlertaParadaEnum
    {
        [Description("8 a 15")]
        Atencion = 0,
        [Description("15 a 60")]
        Peligro = 1,
        [Description("8-")]
        Ok = 2,
        [Description("61+")]
        Fallo = 3
    }
}
