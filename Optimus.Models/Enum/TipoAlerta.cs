﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum TipoAlertEnum
    {
        NoSelecionado = 0,
        EstadoActualMaquina = 1,
        MermaBebidaRotura = 2,
    }
}
