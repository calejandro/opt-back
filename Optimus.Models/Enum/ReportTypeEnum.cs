﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum ReportTypeEnum
    {
        ShiftReport = 1,
        ProductionDayReport = 2
    }
}
