﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Models.Enum
{
   public  enum TypeParticipationEnum
    {
        Volume = 1,
        TimeInLine  = 2,
    }
}
