﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum OptimusTaskPriorityEnum
    {
        High = 1,
        Average = 2,
        Low = 3
    }
}
