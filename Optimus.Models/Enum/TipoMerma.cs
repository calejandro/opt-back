﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum TipoMerma
    {
        NoSelecionado = 0,
        MermaPorRoturaDeEnvase = 1,
        MermaPorPerdidaDeBebida = 2,
    }
}
