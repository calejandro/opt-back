﻿using System;
namespace Optimus.Models.Enum
{
    public enum TypeUserToOptimusTaskEnum
    {
        Creator = 1,
        Validator = 2,
        Assigned = 3
    }
}
