﻿using System;
namespace Optimus.Models.Enum
{
    public enum ClasificacionAlertaEnum
    {
        ParadaPropia = 1,
        Merma = 2,
        Precaucion = 3
    }
}
