﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Enum
{
    public enum TipoModoFalloEnum
    {
        NoSelecionado = 0,
        ModoFalloMaquina = 1,
        Fallo = 2,
        Causa = 3,
        ModoFalloMermaBebida = 4,
        ModoFalloRotura = 5,    
    }
}
