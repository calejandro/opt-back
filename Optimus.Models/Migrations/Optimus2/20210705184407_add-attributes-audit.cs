﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Optimus.Models.Migrations.Optimus2
{
    public partial class addattributesaudit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Task_Commentary",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Task_Commentary",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Task_Commentary",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "Task_Commentary",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Sector",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Sector",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Sector",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "Sector",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Process",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Process",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Process",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "Process",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Optimus_Task",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "Optimus_Task",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "KPI",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "KPI",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "KPI",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "KPI",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "File_Url",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "File_Url",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "File_Url",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeleteByUser",
                table: "File_Url",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "File_Url",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "File_Url",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "File_Url",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "File_Url",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByUser",
                table: "File_Url",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "Task_Commentary");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "Sector");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "Process");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "KPI");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "DeleteByUser",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "File_Url");

            migrationBuilder.DropColumn(
                name: "UpdatedByUser",
                table: "File_Url");
        }
    }
}
