﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Optimus.Models.Migrations.Optimus2
{
    public partial class initialoptimus2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Optimus_Task",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Deliver_Date = table.Column<DateTime>(nullable: true),
                    End_Date = table.Column<DateTime>(nullable: true),
                    Id_State = table.Column<int>(nullable: false),
                    Archived = table.Column<bool>(nullable: false),
                    Id_Creator_User = table.Column<Guid>(nullable: false),
                    Id_Assigned_User = table.Column<Guid>(nullable: true),
                    Id_Verifier_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Optimus_Task", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Optimus_Task_AspNetUsers_Id_Assigned_User",
                        column: x => x.Id_Assigned_User,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Optimus_Task_AspNetUsers_Id_Creator_User",
                        column: x => x.Id_Creator_User,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Optimus_Task_AspNetUsers_Id_Verifier_User",
                        column: x => x.Id_Verifier_User,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Task_Commentary",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Deliver_Date = table.Column<DateTime>(nullable: true),
                    Id_Answer = table.Column<Guid>(nullable: true),
                    Id_OptimusTask = table.Column<Guid>(nullable: true),
                    Id_Creator_User = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Task_Commentary", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Task_Commentary_Task_Commentary_Id_Answer",
                        column: x => x.Id_Answer,
                        principalTable: "Task_Commentary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Commentary_AspNetUsers_Id_Creator_User",
                        column: x => x.Id_Creator_User,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Task_Commentary_Optimus_Task_Id_OptimusTask",
                        column: x => x.Id_OptimusTask,
                        principalTable: "Optimus_Task",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

         
            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Assigned_User",
                table: "Optimus_Task",
                column: "Id_Assigned_User");

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Creator_User",
                table: "Optimus_Task",
                column: "Id_Creator_User");

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Verifier_User",
                table: "Optimus_Task",
                column: "Id_Verifier_User");

            migrationBuilder.CreateIndex(
                name: "IX_Task_Commentary_Id_Answer",
                table: "Task_Commentary",
                column: "Id_Answer");

            migrationBuilder.CreateIndex(
                name: "IX_Task_Commentary_Id_Creator_User",
                table: "Task_Commentary",
                column: "Id_Creator_User");

            migrationBuilder.CreateIndex(
                name: "IX_Task_Commentary_Id_OptimusTask",
                table: "Task_Commentary",
                column: "Id_OptimusTask");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {     
            migrationBuilder.DropTable(
                name: "Task_Commentary");

         
            migrationBuilder.DropTable(
                name: "Optimus_Task");

        }
    }
}
