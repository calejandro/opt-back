﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Optimus.Models.Migrations.Optimus2
{
    public partial class subOptimusTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Id_Optimus_Task",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Optimus_Task",
                table: "Optimus_Task",
                column: "Id_Optimus_Task");

            migrationBuilder.AddForeignKey(
                name: "FK_Optimus_Task_Optimus_Task_Id_Optimus_Task",
                table: "Optimus_Task",
                column: "Id_Optimus_Task",
                principalTable: "Optimus_Task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Optimus_Task_Optimus_Task_Id_Optimus_Task",
                table: "Optimus_Task");

            migrationBuilder.DropIndex(
                name: "IX_Optimus_Task_Id_Optimus_Task",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Id_Optimus_Task",
                table: "Optimus_Task");
        }
    }
}
