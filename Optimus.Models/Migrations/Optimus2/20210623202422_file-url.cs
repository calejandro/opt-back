﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Optimus.Models.Migrations.Optimus2
{
    public partial class fileurl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "File_Url",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Id_Task_Comment = table.Column<Guid>(nullable: true),
                    Id_Optimus_Task = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File_Url", x => x.Id);
                    table.ForeignKey(
                        name: "FK_File_Url_Optimus_Task_Id_Optimus_Task",
                        column: x => x.Id_Optimus_Task,
                        principalTable: "Optimus_Task",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_Url_Task_Commentary_Id_Task_Comment",
                        column: x => x.Id_Task_Comment,
                        principalTable: "Task_Commentary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_File_Url_Id_Optimus_Task",
                table: "File_Url",
                column: "Id_Optimus_Task");

            migrationBuilder.CreateIndex(
                name: "IX_File_Url_Id_Task_Comment",
                table: "File_Url",
                column: "Id_Task_Comment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "File_Url");
        }
    }
}
