﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Optimus.Models.Migrations.Optimus2
{
    public partial class addattributesoptimusTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Id_KPI",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Id_Process",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Id_Sector",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Identifier",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Optimus_Task",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_KPI",
                table: "Optimus_Task",
                column: "Id_KPI");

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Process",
                table: "Optimus_Task",
                column: "Id_Process");

            migrationBuilder.CreateIndex(
                name: "IX_Optimus_Task_Id_Sector",
                table: "Optimus_Task",
                column: "Id_Sector");

            migrationBuilder.AddForeignKey(
                name: "FK_Optimus_Task_KPI_Id_KPI",
                table: "Optimus_Task",
                column: "Id_KPI",
                principalTable: "KPI",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Optimus_Task_Process_Id_Process",
                table: "Optimus_Task",
                column: "Id_Process",
                principalTable: "Process",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Optimus_Task_Sector_Id_Sector",
                table: "Optimus_Task",
                column: "Id_Sector",
                principalTable: "Sector",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Optimus_Task_KPI_Id_KPI",
                table: "Optimus_Task");

            migrationBuilder.DropForeignKey(
                name: "FK_Optimus_Task_Process_Id_Process",
                table: "Optimus_Task");

            migrationBuilder.DropForeignKey(
                name: "FK_Optimus_Task_Sector_Id_Sector",
                table: "Optimus_Task");

            migrationBuilder.DropIndex(
                name: "IX_Optimus_Task_Id_KPI",
                table: "Optimus_Task");

            migrationBuilder.DropIndex(
                name: "IX_Optimus_Task_Id_Process",
                table: "Optimus_Task");

            migrationBuilder.DropIndex(
                name: "IX_Optimus_Task_Id_Sector",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Id_KPI",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Id_Process",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Id_Sector",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Identifier",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Optimus_Task");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Optimus_Task");
        }
    }
}
