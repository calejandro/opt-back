﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Views
{
    public class TimeStops
    {
        [Column("ID_Linea")]
        public int IdLinea { get; set; }
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("Hora")]
        public DateTime Hora { get; set; }

        [Column("ID_Equipo")]
        public int idEquipo { get; set; }

        [Column("Descripcion_Equipo")]
        public string DescripcionEquipo { get; set; }

        [Column("Orden_Visualizacion")]
        public byte OrdenVisualizacion { get; set; }

        [Column("Minutos_Acumulados")]
        public int MinutosAcumulados { get; set; }

        [Column("ID_Orden_SAP")]
        public int? IdOrdenSAP { get; set; }
    }
}
