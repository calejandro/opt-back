﻿using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Views
{
    public class APPMobStatusActual
    {
        [Column("ID_Linea")]
        public int IdLinea { get; set; }
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("Fecha_Hora")]
        public DateTime FechaHora { get; set; }
        [Column("ID_Equipo")]
        public int IdEquipo { get; set; }
        [Column("Descripcion_Equipo")]
        public string DescripcionEquipo { get; set; }
        [Column("Orden_Visualizacion")]
        public byte OrdenVisualizacion { get; set; }
        [Column("ID_Estado")]
        public AppmobStatusEquiposEnum IdEstado { get; set; }
        [Column("Descripcion_Estado")]
        public string DescripcionEstado { get; set; }
        [Column("Minutos_Acumulados")]
        public int MinutosAcumulados { get; set; }
        [Column("Minutos_Desde_Ultima_Parada_Propia")]
        public int MinutosDesdeUltimaParadaPropia { get; set; }


    }
}
