﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Services.ViewModels
{
   public class MarcaViewModel
    {
       public string Marca { get; set; }
       public List<SaborViewModel> Sabores { get; set; }
    }
}
