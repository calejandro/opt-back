﻿using Optimus.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class MachineStatusProductionOrder
    {
        [Column("ID_Linea")]
        public int IdLinea { get; set; }
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("ID_Orden_SAP")]
        public int IdOrdenSAP { get; set; }
        [Column("ID_SKU")]
        public int IdSKU { get; set; }
        [Column("Descripcion_SKU")]
        public string DescripcionSKU { get; set; }
        [Column("ID_Equipo")]
        public int IdEquipo { get; set; }
        [Column("Desc_Equipo")]
        public string DescEquipo { get; set; }
        [Column("Orden_Visualizacion")]
        public byte OrdenVisualizacion { get; set; }
        [Column("ID_Estado")]
        public byte IdEstado { get; set; }
        [Column("Descripcion_Estado")]
        public string DescripcionEstado { get; set; }
        [Column("Minutos_Acumulados")]
        public int MinutosAcumulados { get; set; }
        [Column("Minutos_Acumulados_Porcentaje")]
        public double MinutosAcumuladosPorcentaje { get; set; }


        public MachineStatusProductionOrder()
        {
        }
    }
}
