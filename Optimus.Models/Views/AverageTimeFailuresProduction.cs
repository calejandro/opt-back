﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Views
{
    public class AverageTimeFailuresProduction
    {
        [Column("ID_Fila")]
        public int IdFila { get; set; }
        [Column("ID_Linea")]
        public int IdLinea { get; set; }
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("ID_Equipo")]
        public int IdEquipo { get; set; }
        [Column("Descripcion_Equipo")]
        public string DescripcionEquipo { get; set; }
        [Column("Orden_Visualizacion")]
        public int OrdenVisualizacion { get; set; }
        [Column("TMEF_Turno")]
        public double TMEFTurno { get; set; }

        [Column("TMPR_Turno")]
        public double TMPRTurno { get; set; }

        [Column("Hora_Actual")]
        public DateTime? HoraActual { get; set; }

        [Column("TMEF_Hora")]
        public double TMEFHora { get; set; }

        [Column("TMPR_Hora")]
        public double TMPRHora { get; set; }
    }
}
