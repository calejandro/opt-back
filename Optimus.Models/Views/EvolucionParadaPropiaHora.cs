﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class EvolucionParadaPropiaHora
    {
        [Column("ID_Linea")]
        public int IdLinea { get; set; }

        [Column("Descripcion_Linea")]
        public string DescripDescripcion_Linea { get; set; }

        [Column("ID_Equipo")]
        public int ID_Equipo { get; set; }

        [Column("Descripcion_Equipo")]
        public string Descripcion_Equipo { get; set; }

        [Column("Banda_Horaria")]
        public DateTime Banda_Horaria { get; set; }

        [Column("Minutos_Acumulados")]
        public int Minutos_Acumulados { get; set; }

        public EvolucionParadaPropiaHora()
        {
        }
    }
}
