﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class KpiDepletion
    {
        [Column("ID_Pais")]
        public int IdPais { get; set; }
        [Column("Descripcion_Pais")]
        public string DescripcionPais { get; set; }
        [Column("ID_Planta")]
        public int IdPlanta { get; set; }
        [Column("Descripcion_Planta")]
        public string DescripcionPlanta { get; set; }
        [Column("ID_Linea")]
        public int IdLinea { get; set; }
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("ID_Equipo")]
        public int IdEquipo { get; set; }
        [Column("Descripcion_Equipo")]
        public string DescripcionEquipo { get; set; }
        [Column("Orden_Visualizacion")]
        public byte OrdenVisualizacion { get; set; }
        [Column("Fecha_Hora")]
        public DateTime FechaHora { get; set; }
        [Column("TIPO_PRODUCCION_LINEA")]
        public string TipoProduccionLinea { get; set; }
        [Column("KPI")]
        public decimal Kpi { get; set; }
        [Column("Descripcion_KPI")]
        public string DescripcionKPI { get; set; }
        [Column("Tipo_Produccion")]
        public string TipoProduccion { get; set; }
        [Column("SKU")]
        public int? IdSku { get; set; }
        [Column("Descripcion_SKU")]
        public string DescripcionSKU { get; set; }

        [Column("ID_Orden_SAP")]
        public int? IdOrdenSAP { get; set; }

    }
}
