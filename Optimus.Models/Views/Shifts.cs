﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Models.Views
{
    public class Shift
    {
        public string Turno { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public bool Paso { get; set; }

    }

}
