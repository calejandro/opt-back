﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class ProduccionEficienciaReal
    {
        [Column("Descripcion_Linea")]
        public string DescripcionLinea { get; set; }
        [Column("Fecha_Hora")]
        public DateTime FechaHora { get; set; }
        [Column("Botellas_Producidas")]
        public int BotellasProducidas { get; set; }
        [Column("Eficiencia")]
        public Double Eficiencia { get; set; }
        [Column("ID_Orden_SAP")]
        public int IdOrdenSAP { get; set; }
        [Column("Objetivo")]
        public int Objetivo { get; set; }        

        public ProduccionEficienciaReal()
        {
        }
    }
}
