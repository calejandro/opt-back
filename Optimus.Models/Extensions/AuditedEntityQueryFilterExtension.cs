﻿using Optimus.Commons.Entities;
using Optimus.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;

namespace Optimus.Models.Extensions
{
    public static class AuditedEntityQueryFilterExtension
    {
        public static void SetSoftDeleteFilter(this ModelBuilder modelBuilder, Type entityType)
        {
            SetSoftDeleteFilterMethod.MakeGenericMethod(entityType).Invoke(null, new object[] { modelBuilder });
        }

        static readonly MethodInfo SetSoftDeleteFilterMethod = typeof(AuditedEntityQueryFilterExtension)
                   .GetMethods(BindingFlags.Public | BindingFlags.Static)
                   .Single(t => t.IsGenericMethod && t.Name == "SetSoftDeleteFilter");

        public static void SetSoftDeleteFilter<TEntity>(this ModelBuilder modelBuilder)
            where TEntity : class, IAuditedEntity
        {
            modelBuilder.Entity<TEntity>().HasQueryFilter(q => q.DeletedAt == null);
        }
    }

    public static class OperativeUnitQueryFilterExtension
    {
        public static void SetOperativeUnitFilter(this ModelBuilder modelBuilder, Type entityType, Guid? operativeUnitId)
        {
            SetOperativeUnitFilterMethod.MakeGenericMethod(entityType).Invoke(null, new object[] { modelBuilder, operativeUnitId });
        }

        static readonly MethodInfo SetOperativeUnitFilterMethod = typeof(OperativeUnitQueryFilterExtension)
                   .GetMethods(BindingFlags.Public | BindingFlags.Static)
                   .Single(t => t.IsGenericMethod && t.Name == "SetOperativeUnitFilter");

    }
}
