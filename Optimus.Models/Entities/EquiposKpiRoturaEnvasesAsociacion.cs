﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_KPI_ROTURA_ENVASES_ASOCIACION")]
    public partial class EquiposKpiRoturaEnvasesAsociacion
    {
        [Key]
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Required]
        [Column("DESC_KPI_ROTURA_ENVASES")]
        [StringLength(50)]
        public string DescKpiRoturaEnvases { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("ID_EQUIPO_TOTALIZADOR_INGRESO_BOTELLAS")]
        public int IdEquipoTotalizadorIngresoBotellas { get; set; }
        [Column("ID_EQUIPO_TOTALIZADOR_EGRESO_BOTELLAS")]
        public int IdEquipoTotalizadorEgresoBotellas { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
