﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Optimus.Commons.Entities;

namespace Optimus.Models.Entities
{
    [Table("Sys_MultipleGroups")]
    public partial class SysMultipleGroups : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get => MultipleGroupId; set => MultipleGroupId = value; }
        [Key]
        public int MultipleGroupId { get; set; }
        public int? UserId { get; set; }
        public int? UserGroupId { get; set; }

        [ForeignKey(nameof(UserId))]
       // [InverseProperty(nameof(SysUsers.SysMultipleGroups))]
        public virtual SysUsers User { get; set; }
        [ForeignKey(nameof(UserGroupId))]
        //[InverseProperty(nameof(SysUsersGroups.SysMultipleGroups))]
        public virtual SysUsersGroups UserGroup { get; set; }
    }
}
