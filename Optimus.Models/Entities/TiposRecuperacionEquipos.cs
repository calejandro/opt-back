﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIPOS_RECUPERACION_EQUIPOS")]
    public partial class TiposRecuperacionEquipos
    {
        [Column("ID_TIPO_RECUP_EQUIPO")]
        public int IdTipoRecupEquipo { get; set; }
        [Required]
        [Column("DESC_TIPO_RECUP_EQUIPO")]
        [StringLength(50)]
        public string DescTipoRecupEquipo { get; set; }
    }
}
