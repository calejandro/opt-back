﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class AlertRules
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public string RuleDescription { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
