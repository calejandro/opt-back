﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class AlertsGroups
    {
        [Key]
        public long IdAlertGroup { get; set; }
        public long IdAlertCase { get; set; }
        public int UserGroupId { get; set; }
        [Required]
        [StringLength(1)]
        public string IsActive { get; set; }

        [ForeignKey(nameof(IdAlertCase))]
        [InverseProperty(nameof(AlertsCases.AlertsGroups))]
        public virtual AlertsCases IdAlertCaseNavigation { get; set; }
        [ForeignKey(nameof(UserGroupId))]
        //[InverseProperty(nameof(SysUsersGroups.AlertsGroups))]
        public virtual SysUsersGroups UserGroup { get; set; }
    }
}
