﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_EFICIENCIAS_ASOCIACION")]
    public partial class EquiposEficienciasAsociacion
    {
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Required]
        [Column("DESC_ASOCIACION")]
        [StringLength(50)]
        public string DescAsociacion { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO_CAUDAL")]
        public int IdEquipoCaudal { get; set; }
        [Column("ID_EQUIPO_ENERGIA")]
        public int IdEquipoEnergia { get; set; }
        [Column("ORDEN_VISUALIZACION")]
        public int OrdenVisualizacion { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
