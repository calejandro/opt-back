﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_OPC")]
    public partial class EquiposOpc
    {
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("MINUTO_DEL_DIA")]
        public int? MinutoDelDia { get; set; }
        [Required]
        [Column("TAG_OPC")]
        [StringLength(100)]
        public string TagOpc { get; set; }
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool? Activo { get; set; }
    }
}
