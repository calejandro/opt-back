﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TMEF_TMPR")]
    public partial class TmefTmpr: BaseCommon
    {
        [Column("ID_Linea")]
        public int? IdLinea { get; set; }
        [Column("ID_Equipo")]
        public int? IdEquipo { get; set; }
        [Column("FH_Inicio")]
        [StringLength(14)]
        public string FhInicio { get; set; }
        [Column("FH_Fin")]
        [StringLength(14)]
        public string FhFin { get; set; }
        [Column("TMEF")]
        public double? Tmef { get; set; }
        [Column("TMPR")]
        public double? Tmpr { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }

    }
}
