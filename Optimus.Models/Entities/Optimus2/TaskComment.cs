﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Entities
{
    [Table("Task_Commentary")]

    public class TaskComment : AuditedEntity
    {
        public new Guid Id { get; set; }
        [Column("Name")]
        public string Name { get; set; }
        [Column("Description")]
        public string? Description { get; set; }
        [Column("Deliver_Date")]
        public DateTime? DeliverDate { get; set; }
        [Column("Id_Answer")]
        public Guid? IdAnswer { get; set; }
        [Column("Id_OptimusTask")]
        public Guid? IdOptimusTask { get; set; }

        [Column("Id_Creator_User")]
        public Guid IdCreatorUser { get; set; }


        [ForeignKey("IdAnswer")]
        public TaskComment Answer { get; set; }
        [ForeignKey("IdOptimusTask")]
        public OptimusTask OptimusTask { get; set; }
        [ForeignKey("IdCreatorUser")]
        public UserIdentity CreatorUser { get; set; }

        [InverseProperty("TaskComment")]
        public List<FileUrl> FilesUrl { get; set; }

    }
}
