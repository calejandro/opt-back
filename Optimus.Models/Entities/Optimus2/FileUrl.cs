﻿
using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("File_Url")]
    public partial class FileUrl : AuditedEntity
    {
        public new Guid Id { get; set; }
        [Column("Id_Task_Comment")]
        public Guid? IdTaskComment { get; set; }
        [Column("Id_Optimus_Task")]
        public Guid? IdOptimusTask { get; set; }
        [Column("Name")]
        public string Name { get; set; }

        [ForeignKey("IdTaskComment")]
        public TaskComment TaskComment { get; set; }
        [ForeignKey("IdOptimusTask")]
        public OptimusTask OptimusTask { get; set; }

    }
}
