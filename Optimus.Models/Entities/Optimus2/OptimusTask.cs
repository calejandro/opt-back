﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Entities
{
    [Table("Optimus_Task")]

    public class OptimusTask : AuditedEntity
    {
        public new Guid Id { get; set; }
        [Column("Identifier")]
        public string? Identifier { get; set; }
        [Column("Name")]
        [Required]
        public string Name { get; set; }
        [Column("Description")]
        [Required]
        public string? Description { get; set; }
        [Column("Deliver_Date")]
        [Required]
        public DateTime? DeliverDate { get; set; }
        [Column("End_Date")]
        [Required]
        public DateTime? EndDate { get; set; }
        [Column("Id_State")]
        [Required]
        public StateOptimusTaskEnum IdState { get; set; }
        [Column("Archived")]
        public bool Archived { get; set; }
        [Column("Id_Creator_User")]
        [Required]
        public Guid IdCreatorUser { get; set; }
        [Column("Id_Assigned_User")]
        public Guid? IdAssignedUser { get; set; }
        [Column("Id_Verifier_User")]
        public Guid? IdVerifierUser { get; set; }
        [Column("Id_KPI")]
        public Guid? IdKpi { get; set; }
        [Column("Id_Sector")]
        public Guid? IdSector { get; set; }
        [Column("Id_Process")]
        public Guid? IdProcess { get; set; }
        public OptimusTaskTypeEnum? Type { get; set; }
        public OptimusTaskPriorityEnum? Priority { get; set; }
        [Column("Id_Optimus_Task")]
        public Guid? IdParentOptimusTask { get; set; }



        [ForeignKey("IdCreatorUser")]
        public UserIdentity CreatorUser { get; set; }
        [ForeignKey("IdAssignedUser")]
        public UserIdentity AssignedUser { get; set; }
        [ForeignKey("IdVerifierUser")]
        public UserIdentity VerifierUser { get; set; }
        [ForeignKey("IdKpi")]
        public Kpi Kpi { get; set; }
        [ForeignKey("IdSector")]
        public Sector Sector { get; set; }
        [ForeignKey("IdProcess")]
        public Process Process { get; set; }

        [ForeignKey("IdParentOptimusTask")]
        public OptimusTask ParentOptimusTask { get; set; }

        [InverseProperty("OptimusTask")]
        public List<TaskComment> TaskComments { get; set; }
        [InverseProperty("OptimusTask")]
        public List<FileUrl> FilesUrl { get; set; }
        [InverseProperty("ParentOptimusTask")]
        public List<OptimusTask> SubOptimusTask { get; set; }
    }
}
