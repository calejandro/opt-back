﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Optimus.Models.Entities
{
    [Table("Process")]

    public class Process : AuditedEntity
    {
        public new Guid Id { get; set; }
        [Column("Name")]
        public string Name { get; set; }
        [Column("Description")]
        public string? Description { get; set; }

    }
}
