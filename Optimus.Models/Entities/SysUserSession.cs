﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_UserSession")]
    public partial class SysUserSession
    {
        [Key]
        [Column("Sys_UserSessionId")]
        public int SysUserSessionId { get; set; }
        [StringLength(50)]
        public string SessionId { get; set; }
        public int? UserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? TimeStamp { get; set; }
        [StringLength(50)]
        public string Device { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastAccess { get; set; }

        [ForeignKey(nameof(UserId))]
       // [InverseProperty(nameof(SysUsers.SysUserSession))]
        public virtual SysUsers User { get; set; }
    }
}
