﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_SKU_TIPO_PRODUCCION")]
    public partial class LineasSkuTipoProduccion
    {
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_SKU")]
        public int IdSku { get; set; }
        [Column("ID_TIPO_PRODUCCION_LINEA")]
        public int IdTipoProduccionLinea { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
