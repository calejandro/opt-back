﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("SUBSECTORES")]
    public partial class Subsectores
    {
        [Column("ID_SUBSECTOR")]
        public int IdSubsector { get; set; }
        [Required]
        [Column("DESC_SUBSECTOR")]
        [StringLength(30)]
        public string DescSubsector { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
        [Column("ORDEN_VISUALIZACION")]
        public int? OrdenVisualizacion { get; set; }
        [Column("COLOR_GRAFICO")]
        [StringLength(10)]
        public string ColorGrafico { get; set; }
    }
}
