﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTRO_PRODUCCION")]
    public partial class RegistroProduccion
    {
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Required]
        [Column("FH_REGISTRO")]
        [StringLength(14)]
        public string FhRegistro { get; set; }
        [Column("PRODUCCION")]
        public int Produccion { get; set; }
        [Column("COD_PRODUCTO")]
        [StringLength(10)]
        public string CodProducto { get; set; }
        [Column("ID_SABOR")]
        public int? IdSabor { get; set; }
        [Column("ID_FORMATO")]
        public int? IdFormato { get; set; }
        [Column("FH_REGISTRO_REAL")]
        [StringLength(14)]
        public string FhRegistroReal { get; set; }
    }
}
