﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EXPORTACION_TOTALIZADORES")]
    public partial class ExportacionTotalizadores
    {
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("NUMERO_COLUMNA")]
        public int NumeroColumna { get; set; }
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool? Activo { get; set; }
    }
}
