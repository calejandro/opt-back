﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_ProcesosBatch")]
    public partial class SysProcesosBatch
    {
        public SysProcesosBatch()
        {
            SysProcesosBatchLog = new HashSet<SysProcesosBatchLog>();
        }

        [Key]
        public long IdProcesoBatch { get; set; }
        [StringLength(255)]
        public string NombreArchivo { get; set; }
        [StringLength(255)]
        public string NombreTabla { get; set; }
        [StringLength(255)]
        public string NombreSp { get; set; }
        [StringLength(20)]
        public string Tipo { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UltimaEjecucion { get; set; }
        [StringLength(5)]
        public string HoraEjec { get; set; }
        [StringLength(55)]
        public string DiaEjec { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaEjec { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ProxEjec { get; set; }
        public bool? Tomado { get; set; }

        [InverseProperty("IdProcesoBatchNavigation")]
        public virtual ICollection<SysProcesosBatchLog> SysProcesosBatchLog { get; set; }
    }
}
