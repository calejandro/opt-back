﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("MINUTOS_DEL_DIA")]
    public partial class MinutosDelDia
    {
        [Column("ID_MINUTO")]
        public int IdMinuto { get; set; }
    }
}
