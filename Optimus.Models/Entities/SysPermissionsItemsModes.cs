﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_PermissionsItemsModes")]
    public partial class SysPermissionsItemsModes
    {
        public SysPermissionsItemsModes()
        {
            SysPermissions = new HashSet<SysPermissions>();
        }

        [Key]
        public int PermissionItemModeId { get; set; }
        public int PermissionItemId { get; set; }
        [StringLength(30)]
        public string Mode { get; set; }

        [ForeignKey(nameof(PermissionItemId))]
        [InverseProperty(nameof(SysPermissionsItems.SysPermissionsItemsModes))]
        public virtual SysPermissionsItems PermissionItem { get; set; }
        [InverseProperty("PermissionItemMode")]
        public virtual ICollection<SysPermissions> SysPermissions { get; set; }
    }
}
