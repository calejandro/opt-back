﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class AlertsCases
    {
        public AlertsCases()
        {
            AlertsGroups = new HashSet<AlertsGroups>();
        }

        [Key]
        public long IdAlertCase { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(50)]
        public string AlertType { get; set; }
        [StringLength(255)]
        public string TemplateFileName { get; set; }
        [Required]
        [StringLength(255)]
        public string SubjectTemplate { get; set; }
        [Required]
        [StringLength(2000)]
        public string BodyTemplate { get; set; }

        [InverseProperty("IdAlertCaseNavigation")]
        public virtual ICollection<AlertsGroups> AlertsGroups { get; set; }
    }
}
