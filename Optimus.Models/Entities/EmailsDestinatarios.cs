﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EMAILS_DESTINATARIOS")]
    public partial class EmailsDestinatarios
    {
        [Column("ID_DESTINATARIO")]
        public int IdDestinatario { get; set; }
        [Required]
        [Column("EMAIL_DESTINATARIO")]
        [StringLength(50)]
        public string EmailDestinatario { get; set; }
        [Required]
        [Column("NOMBRE_DESTINATARIO")]
        [StringLength(50)]
        public string NombreDestinatario { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("RECIBE_REPORTE_SEMANAL")]
        public bool? RecibeReporteSemanal { get; set; }
        [Column("RECIBE_REPORTE_HISTORICO")]
        public bool? RecibeReporteHistorico { get; set; }
        [Column("RECIBE_REPORTE_CONSUMOS_ENERGIA")]
        public bool? RecibeReporteConsumosEnergia { get; set; }
    }
}
