﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Logs")]
    public partial class SysLogs
    {
        [Key]
        public int LogId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TimeStamp { get; set; }
        [Column(TypeName = "text")]
        public string Message { get; set; }
        [Required]
        [StringLength(20)]
        public string Type { get; set; }
        [StringLength(200)]
        public string Source { get; set; }
        [StringLength(200)]
        public string Module { get; set; }
        public int? TypeId { get; set; }
        public int? UserId { get; set; }

        [ForeignKey(nameof(TypeId))]
        [InverseProperty(nameof(SysLogType.SysLogs))]
        public virtual SysLogType TypeNavigation { get; set; }
        [ForeignKey(nameof(UserId))]
       //[InverseProperty(nameof(SysUsers.SysLogs))]
        public virtual SysUsers User { get; set; }
    }
}
