﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Errors")]
    public partial class SysErrors
    {
        [Key]
        public int ErrorId { get; set; }
        public int? ErrorNumber { get; set; }
        [Required]
        [Column(TypeName = "text")]
        public string Description { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        public bool Log { get; set; }
    }
}
