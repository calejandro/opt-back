﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class Alerts
    {
        [Key]
        public long IdAlert { get; set; }
        [StringLength(255)]
        public string FromAddress { get; set; }
        [Required]
        [StringLength(255)]
        public string ToAddress { get; set; }
        [Column("CCAddress")]
        [StringLength(255)]
        public string Ccaddress { get; set; }
        [StringLength(255)]
        public string Subject { get; set; }
        public string Message { get; set; }
        [StringLength(255)]
        public string FileAttachName { get; set; }
        [StringLength(255)]
        public string TemplateFileName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartDate { get; set; }
        [Required]
        [StringLength(50)]
        public string Status { get; set; }
        [Required]
        [StringLength(50)]
        public string AlertType { get; set; }
        public bool Suspended { get; set; }
        public int Retries { get; set; }
        public bool Locked { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastControl { get; set; }
        [Column(TypeName = "text")]
        public string LastError { get; set; }
        public int? UserId { get; set; }
        public bool DialogoVisto { get; set; }
        [Required]
        public bool? MostrarEnAppAndroid { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaLeida { get; set; }
        public int? SubsistemaCodigo { get; set; }

        [ForeignKey(nameof(SubsistemaCodigo))]
        [InverseProperty(nameof(Subsistemas.Alerts))]
        public virtual Subsistemas SubsistemaCodigoNavigation { get; set; }
        [ForeignKey(nameof(UserId))]
        //[InverseProperty(nameof(SysUsers.Alerts))]
        public virtual SysUsers User { get; set; }
    }
}
