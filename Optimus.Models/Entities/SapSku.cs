﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("SAP_SKU")]
    public partial class SapSku: BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdSku; set => IdSku = value; }
        [Key]
        [Column("ID_SKU")]
        public int IdSku { get; set; }
        [Required]
        [Column("DESC_SKU")]
        [StringLength(50)]
        public string DescSku { get; set; }
        [Required]
        [Column("FORMATO")]
        [StringLength(50)]
        public string Formato { get; set; }
        [Required]
        [Column("MARCA")]
        [StringLength(50)]
        public string Marca { get; set; }
        [Required]
        [Column("SABOR")]
        [StringLength(50)]
        public string Sabor { get; set; }
        [Required]
        [Column("INDICADOR_CARBONO")]
        [StringLength(50)]
        public string IndicadorCarbono { get; set; }
        [Required]
        [Column("MATERIAL_ENVASE")]
        [StringLength(50)]
        public string MaterialEnvase { get; set; }
        [Required]
        [Column("RETORNABLE")]
        [StringLength(50)]
        public string Retornable { get; set; }
        [Required]
        [Column("UNIDAD_MEDIDA")]
        [StringLength(50)]
        public string UnidadMedida { get; set; }
        [Column("CONVERSION_BOTELLAS")]
        public int ConversionBotellas { get; set; }
        [Column("CONVERSION_LITROS")]
        public double ConversionLitros { get; set; }
        [Column("ID_CENTRO")]
        public int IdCentro { get; set; }
    }
}
