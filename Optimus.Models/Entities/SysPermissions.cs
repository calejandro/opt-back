﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Permissions")]
    public partial class SysPermissions
    {
        [Key]
        public int PermissionsId { get; set; }
        public int PermissionItemModeId { get; set; }
        public int UserGroupId { get; set; }

        [ForeignKey(nameof(PermissionItemModeId))]
        [InverseProperty(nameof(SysPermissionsItemsModes.SysPermissions))]
        public virtual SysPermissionsItemsModes PermissionItemMode { get; set; }
        [ForeignKey(nameof(UserGroupId))]
        //[InverseProperty(nameof(SysUsersGroups.SysPermissions))]
        public virtual SysUsersGroups UserGroup { get; set; }
    }
}
