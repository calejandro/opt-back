﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTRO_ESTADOS")]
    public partial class RegistroEstados: BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdEquipo; set => IdEquipo = value; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Required]
        [Column("FH_REGISTRO")]
        [StringLength(14)]
        public string FhRegistro { get; set; }
        [Column("ESTADO")]
        public byte Estado { get; set; }
        [Column("COD_PRODUCTO")]
        [StringLength(10)]
        public string CodProducto { get; set; }
        [Column("ID_SABOR")]
        public int? IdSabor { get; set; }
        [Column("ID_FORMATO")]
        public int? IdFormato { get; set; }
    }
}
