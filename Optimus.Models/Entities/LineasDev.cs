﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_DEV")]
    public partial class LineasDev
    {
        [Column("ID_LINEA")]
        public int? IdLinea { get; set; }
        [Required]
        [Column("DESC_LINEA")]
        [StringLength(100)]
        public string DescLinea { get; set; }
    }
}
