﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTRO_TOTALIZADORES_POR_MINUTOS")]
    public partial class RegistroTotalizadoresPorMinutos
    {
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Required]
        [Column("FH_REGISTRO")]
        [StringLength(14)]
        public string FhRegistro { get; set; }
        [Column("TOTALIZADO", TypeName = "decimal(11, 3)")]
        public decimal? Totalizado { get; set; }
        [Required]
        [Column("FH_REGISTRO_REAL")]
        [StringLength(14)]
        public string FhRegistroReal { get; set; }
        [Column("TOTALIZADO_REAL", TypeName = "decimal(11, 3)")]
        public decimal TotalizadoReal { get; set; }
    }
}
