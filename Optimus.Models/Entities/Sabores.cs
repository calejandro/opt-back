﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("SABORES")]
    public partial class Sabores
    {
        [Column("ID_SABOR")]
        public int IdSabor { get; set; }
        [Required]
        [Column("COD_SABOR_PLC")]
        [StringLength(30)]
        public string CodSaborPlc { get; set; }
        [Required]
        [Column("DESC_SABOR")]
        [StringLength(50)]
        public string DescSabor { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
