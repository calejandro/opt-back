﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("APPMob_Status_Actual_Acumulado")]
    public partial class APPMobStatusActualAcumulado : BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        [Column("Id_Planta")]
        public int IdPlanta { get; set; }
        [Column("Id_Linea")]
        public int IdLinea { get; set; }
        [Column("Id_Equipo")]
        public int IdEquipo { get; set; }
        [Column("Acum_Estado_Precaucion")]
        public int AcumEstadoPrecaucion { get; set; }
        [Column("Acum_Estado_NoPrecaucion")]
        public int? AcumEstadoNoPrecaucion { get; set; }
        [Column("Acum_Estado_Anterior")]
        public int AcumEstadoAnterior { get; set; }
        [Column("Id_Estado_Actual")]
        public AppmobStatusEquiposEnum IdEstadoActual { get; set; }




    }
}
