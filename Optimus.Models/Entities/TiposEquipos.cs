﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIPOS_EQUIPOS")]
    public partial class TiposEquipos
    {
        [Column("ID_TIPO_EQUIPO")]
        public int IdTipoEquipo { get; set; }
        [Required]
        [Column("DESC_TIPO_EQUIPO")]
        [StringLength(50)]
        public string DescTipoEquipo { get; set; }
    }
}
