﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("CONFIGURACION_UMBRALES_ALERTAS_PRECAUCION")]
    public partial class ConfiguracionUmbralesAlertasPrecaucion : BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("VALOR_MIN_ALERTA")]
        public int ValorMinAlerta { get; set; }
        [Column("VALOR_MAX_ALERTA")]
        public int? ValorMaxAlerta { get; set; }
        [Column("VALOR_MIN_SALIR_ALERTA")]
        public int ValorMinSalirAlerta { get; set; }
        [Column("VALOR_MAX_SALIR_ALERTA")]
        public int? ValorMaxSalirAlerta { get; set; }
        [Column("TIPO_ALERTA")]
        public TipoAlertaParadaEnum TipoAlerta { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }
    }
}
