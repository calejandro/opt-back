﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("PARES_DE_MINUTOS_DEL_DIA")]
    public partial class ParesDeMinutosDelDia
    {
        [Column("ID_PAR")]
        public int IdPar { get; set; }
    }
}
