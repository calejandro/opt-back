﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("__RefactorLog")]
    public partial class RefactorLog
    {
        [Key]
        public Guid OperationKey { get; set; }
    }
}
