﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_TOTALIZADORES_SUBSECTOR_ASOCIACION")]
    public partial class EquiposTotalizadoresSubsectorAsociacion
    {
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("ID_SUBSECTOR")]
        public int IdSubsector { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
