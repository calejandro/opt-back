﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("MAESTRO_PRODUCTOS")]
    public partial class MaestroProductos
    {
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Required]
        [Column("COD_PRODUCTO")]
        [StringLength(20)]
        public string CodProducto { get; set; }
        [Required]
        [Column("DESC_PRODUCTO")]
        [StringLength(50)]
        public string DescProducto { get; set; }
        [Column("ID_FORMATO")]
        public int IdFormato { get; set; }
        [Column("ID_SABOR")]
        public int IdSabor { get; set; }
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool? Activo { get; set; }
    }
}
