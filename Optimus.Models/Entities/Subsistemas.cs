﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class Subsistemas
    {
        public Subsistemas()
        {
            Alerts = new HashSet<Alerts>();
            SysPermissionsItems = new HashSet<SysPermissionsItems>();
            SysUsersGroups = new HashSet<SysUsersGroups>();
        }

        [Key]
        public int SubsistemaId { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }
        [StringLength(50)]
        public string Code { get; set; }
        public int? Number { get; set; }
        [StringLength(1)]
        public string Active { get; set; }
        [StringLength(255)]
        public string Url { get; set; }
        [StringLength(255)]
        public string Image { get; set; }

        [InverseProperty("SubsistemaCodigoNavigation")]
        public virtual ICollection<Alerts> Alerts { get; set; }
        [InverseProperty("Subsistema")]
        public virtual ICollection<SysPermissionsItems> SysPermissionsItems { get; set; }
        [InverseProperty("Subsistema")]
        public virtual ICollection<SysUsersGroups> SysUsersGroups { get; set; }
    }
}
