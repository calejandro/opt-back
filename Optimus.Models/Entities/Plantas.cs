﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("PLANTAS")]
    public partial class Plantas : BaseCommon
    {
        public Plantas()
        {
            //SysUsersPlants = new HashSet<SysUsersPlants>();
        }

        [NotMapped]
        public override int Id { get => IdPlanta; set => IdPlanta = value; }
        [Key]
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
        [Required]
        [Column("DESC_PLANTA")]
        [StringLength(50)]
        public string DescPlanta { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
        [Column("ID_PAIS")]
        public int? IdPais { get; set; }

        [ForeignKey("IdPais")]
        public Paises Pais { get; set; }

        [InverseProperty("Plantas")]
        public List<SysUsersPlants>? SysUsersPlantas { get; set; }
    }
}
