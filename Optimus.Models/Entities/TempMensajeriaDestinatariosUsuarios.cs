﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class TempMensajeriaDestinatariosUsuarios
    {
        [Key]
        public int UserIdLogueado { get; set; }
        [Key]
        public int UserId { get; set; }

        [ForeignKey(nameof(UserId))]
      //  [InverseProperty(nameof(SysUsers.TempMensajeriaDestinatariosUsuariosUser))]
        public virtual SysUsers User { get; set; }
        [ForeignKey(nameof(UserIdLogueado))]
      //  [InverseProperty(nameof(SysUsers.TempMensajeriaDestinatariosUsuariosUserIdLogueadoNavigation))]
        public virtual SysUsers UserIdLogueadoNavigation { get; set; }
    }
}
