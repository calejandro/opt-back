﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTROS_FALLOS_ALERTAS")]
    public partial class RegistrosFallosAlertas: BaseCommon
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("ID_ALERTA")]
        public int IdAlerta { get; set; }
        [Column("ID_MODO_FALLO")]
        public int IdModoFallo { get; set; }
        [Column("ID_FALLO")]
        public int IdFallo { get; set; }
        [Column("ID_CAUSA")]
        public int IdCausa { get; set; }
        [Required]
        [Column("ACCION_TOMADA")]
        public string AccionTomada { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime? FechaCreacion { get; set; }
        [Column("BORRADO")]
        public bool? Borrado { get; set; }

        [ForeignKey("IdModoFallo")]
        public TiposFallos ModoFallo { get; set; }
        [ForeignKey("IdFallo")]
        public TiposFallos Fallo { get; set; }
        [ForeignKey("IdCausa")]
        public TiposFallos Causa { get; set; }

        [ForeignKey("IdAlerta")]
        public LineasProduccionAlertas LineasProduccionAlertas { get; set; }
    }
}
