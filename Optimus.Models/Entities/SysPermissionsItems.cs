﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_PermissionsItems")]
    public partial class SysPermissionsItems
    {
        public SysPermissionsItems()
        {
            SysPermissionsItemsModes = new HashSet<SysPermissionsItemsModes>();
        }

        [Key]
        public int PermissionItemId { get; set; }
        public int? ParentPermissionItemId { get; set; }
        [Required]
        [StringLength(50)]
        public string ResourceName { get; set; }
        [StringLength(70)]
        public string Url { get; set; }
        [StringLength(50)]
        public string Target { get; set; }
        public int? MenuItemOrder { get; set; }
        [StringLength(100)]
        public string Image { get; set; }
        public int? ImageHeight { get; set; }
        public int? ImageWidth { get; set; }
        [Required]
        [StringLength(1)]
        public string IsAuthenticationRequired { get; set; }
        [Required]
        [StringLength(1)]
        public string IsEnabled { get; set; }
        [Required]
        [StringLength(1)]
        public string IsVisible { get; set; }
        [StringLength(1)]
        public string IsMenu { get; set; }
        public int? SubsistemaId { get; set; }
        [StringLength(250)]
        public string UrlAppMobile { get; set; }
        public bool EsAdminItem { get; set; }

        [ForeignKey(nameof(SubsistemaId))]
        [InverseProperty(nameof(Subsistemas.SysPermissionsItems))]
        public virtual Subsistemas Subsistema { get; set; }
        [InverseProperty("PermissionItem")]
        public virtual ICollection<SysPermissionsItemsModes> SysPermissionsItemsModes { get; set; }
    }
}
