﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_LogType")]
    public partial class SysLogType
    {
        public SysLogType()
        {
            SysLogs = new HashSet<SysLogs>();
        }

        [Key]
        public int TypeId { get; set; }
        [StringLength(20)]
        public string TypeName { get; set; }

        [InverseProperty("TypeNavigation")]
        public virtual ICollection<SysLogs> SysLogs { get; set; }
    }
}
