﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("PAISES")]
    public partial class Paises : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdPais; set => IdPais = value; }
        [Key]
        [Column("ID_PAIS")]
        public int IdPais { get; set; }
        [Required]
        [Column("DESC_PAIS")]
        [StringLength(50)]
        public string DescPais { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
