﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Languages")]
    public partial class SysLanguages
    {
        public SysLanguages()
        {
            SysUsers = new HashSet<SysUsers>();
        }

        [Key]
        public int LanguageId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string ImageUrl { get; set; }
        [StringLength(10)]
        public string Culture { get; set; }
        [StringLength(500)]
        public string Comment { get; set; }

        [InverseProperty("Language")]
        public virtual ICollection<SysUsers> SysUsers { get; set; }
    }
}
