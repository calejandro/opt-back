﻿using Optimus.Commons.Entities;
using Optimus.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Users")]
    public partial class SysUsers: BaseCommon
    {
        //public SysUsers()
        //{
        //    Alerts = new HashSet<Alerts>();
        //    SysDevices = new HashSet<SysDevices>();
        //    SysLogs = new HashSet<SysLogs>();
        //    SysMultipleGroups = new HashSet<SysMultipleGroups>();
        //    SysUserSession = new HashSet<SysUserSession>();
        //    SysUsersPlants = new HashSet<SysUsersPlants>();
        //    TempMensajeriaDestinatariosUsuariosUser = new HashSet<TempMensajeriaDestinatariosUsuarios>();
        //    TempMensajeriaDestinatariosUsuariosUserIdLogueadoNavigation = new HashSet<TempMensajeriaDestinatariosUsuarios>();
        //}
        [NotMapped]
        public override int Id { get => UserId; set => UserId = value; }
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        [StringLength(50)]
        public string FullName { get; set; }
        [StringLength(80)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [Column("InitURL")]
        public string InitUrl { get; set; }
        [StringLength(1)]
        public string IsActive { get; set; }
        public int LanguageId { get; set; }
        [StringLength(20)]
        public string ChangePass { get; set; }
        [StringLength(1)]
        public string AgreedTerms { get; set; }
        [StringLength(100)]
        public string Foto { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaAlta { get; set; }
        [StringLength(200)]
        public string UserHorarioContacto { get; set; }
        [StringLength(10)]
        public string Telefono { get; set; }
        [StringLength(5)]
        public string CodAreaTelefono { get; set; }
        public bool EsAdministrador { get; set; }
        public Guid? IdUserIdentity { get; set; }

        [ForeignKey("IdUserIdentity")]
        public UserIdentity? UserIdentity { get; set; }
        //[ForeignKey(nameof(LanguageId))]
        //[InverseProperty(nameof(SysLanguages.SysUsers))]
        //public virtual SysLanguages Language { get; set; }
        //[InverseProperty("User")]
        //public virtual ICollection<Alerts> Alerts { get; set; }
        //[InverseProperty("User")]
        //public virtual ICollection<SysDevices> SysDevices { get; set; }
        //[InverseProperty("User")]
        //public virtual ICollection<SysLogs> SysLogs { get; set; }
        //[InverseProperty("User")]
        //public virtual ICollection<SysMultipleGroups> SysMultipleGroups { get; set; }
        //[InverseProperty("User")]
        //public virtual ICollection<SysUserSession> SysUserSession { get; set; }
        [InverseProperty("SysUsers")]
        public List<SysUsersPlants>? SysUsersPlantas { get; set; }
        [InverseProperty("User")]
        public List<SysMultipleGroups>? Groups { get; set; }
        //[InverseProperty(nameof(TempMensajeriaDestinatariosUsuarios.User))]
        //public virtual ICollection<TempMensajeriaDestinatariosUsuarios> TempMensajeriaDestinatariosUsuariosUser { get; set; }
        //[InverseProperty(nameof(TempMensajeriaDestinatariosUsuarios.UserIdLogueadoNavigation))]
        //public virtual ICollection<TempMensajeriaDestinatariosUsuarios> TempMensajeriaDestinatariosUsuariosUserIdLogueadoNavigation { get; set; }
    }
}
