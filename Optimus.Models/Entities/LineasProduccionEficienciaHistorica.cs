﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PRODUCCION_EFICIENCIA_HISTORICA")]
    public partial class LineasProduccionEficienciaHistorica
    {
        [Key]
        public int Id { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("EFICIENCIA_DIA")]
        public double EficienciaDia { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime FechaCreacion { get; set; }
    }
}
