﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_WebPageFilter")]
    public partial class SysWebPageFilter
    {
        [Key]
        public int Id { get; set; }
        public int TimePeriod { get; set; }
        public int? ProductionPlantId { get; set; }
        public int? ProductionLineId { get; set; }
        public int? ProductionOrderSkuId { get; set; }
        public int? ProductionAlertType { get; set; }
        public int UserId { get; set; }
        public byte? StatusId { get; set; }
    }
}
