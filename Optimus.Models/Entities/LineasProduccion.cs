﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PRODUCCION")]
    public partial class LineasProduccion : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdLinea; set => IdLinea = value; }
        [Key]
        [Column("ID_LINEA")]        
        public int IdLinea { get; set; }
        [Required]
        [Column("DESC_LINEA")]
        [StringLength(50)]
        public string DescLinea { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
        [Column("TIPO_LINEA")]
        public int TipoLinea { get; set; }
        [Column("PUEDE_RECUPERAR_HISTORICOS")]
        public int? PuedeRecuperarHistoricos { get; set; }
        [Column("TAG_OPC_DIA_TURNO_RECUPERAR")]
        [StringLength(100)]
        public string TagOpcDiaTurnoRecuperar { get; set; }
        [Column("TAG_OPC_FLAG_ACTIVAR_RECUPERACION")]
        [StringLength(100)]
        public string TagOpcFlagActivarRecuperacion { get; set; }
        [Column("TAG_OPC_ANIO_PLC")]
        [StringLength(100)]
        public string TagOpcAnioPlc { get; set; }
        [Column("TAG_OPC_MES_PLC")]
        [StringLength(100)]
        public string TagOpcMesPlc { get; set; }
        [Column("TAG_OPC_DIA_PLC")]
        [StringLength(100)]
        public string TagOpcDiaPlc { get; set; }
        [Column("TAG_OPC_HORA_PLC")]
        [StringLength(100)]
        public string TagOpcHoraPlc { get; set; }
        [Column("TAG_OPC_MINUTOS_PLC")]
        [StringLength(100)]
        public string TagOpcMinutosPlc { get; set; }
        [Column("TAG_OPC_SEGUNDOS_PLC")]
        [StringLength(100)]
        public string TagOpcSegundosPlc { get; set; }
        [Column("TAG_OPC_PRODUCCION_H0")]
        [StringLength(100)]
        public string TagOpcProduccionH0 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H1")]
        [StringLength(100)]
        public string TagOpcProduccionH1 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H2")]
        [StringLength(100)]
        public string TagOpcProduccionH2 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H3")]
        [StringLength(100)]
        public string TagOpcProduccionH3 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H4")]
        [StringLength(100)]
        public string TagOpcProduccionH4 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H5")]
        [StringLength(100)]
        public string TagOpcProduccionH5 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H6")]
        [StringLength(100)]
        public string TagOpcProduccionH6 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H7")]
        [StringLength(100)]
        public string TagOpcProduccionH7 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H8")]
        [StringLength(100)]
        public string TagOpcProduccionH8 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H9")]
        [StringLength(100)]
        public string TagOpcProduccionH9 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H10")]
        [StringLength(100)]
        public string TagOpcProduccionH10 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H11")]
        [StringLength(100)]
        public string TagOpcProduccionH11 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H12")]
        [StringLength(100)]
        public string TagOpcProduccionH12 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H13")]
        [StringLength(100)]
        public string TagOpcProduccionH13 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H14")]
        [StringLength(100)]
        public string TagOpcProduccionH14 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H15")]
        [StringLength(100)]
        public string TagOpcProduccionH15 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H16")]
        [StringLength(100)]
        public string TagOpcProduccionH16 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H17")]
        [StringLength(100)]
        public string TagOpcProduccionH17 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H18")]
        [StringLength(100)]
        public string TagOpcProduccionH18 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H19")]
        [StringLength(100)]
        public string TagOpcProduccionH19 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H21")]
        [StringLength(100)]
        public string TagOpcProduccionH21 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H20")]
        [StringLength(100)]
        public string TagOpcProduccionH20 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H22")]
        [StringLength(100)]
        public string TagOpcProduccionH22 { get; set; }
        [Column("TAG_OPC_PRODUCCION_H23")]
        [StringLength(100)]
        public string TagOpcProduccionH23 { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }

        [InverseProperty("LineasProduccionSap")]
        public LineasProdSapToLineasProdElp LineasProdSapToLineasProdElp { get; set; }

    }
}
