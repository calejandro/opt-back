﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIPOS_LINEAS")]
    public partial class TiposLineas
    {
        [Column("ID_TIPO_LINEA")]
        public int IdTipoLinea { get; set; }
        [Required]
        [Column("DESC_TIPO_LINEA")]
        [StringLength(50)]
        public string DescTipoLinea { get; set; }
    }
}
