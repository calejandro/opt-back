﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LICENCIAS")]
    public partial class Licencias
    {
        [Required]
        [Column("ID_SISTEMA")]
        [StringLength(30)]
        public string IdSistema { get; set; }
        [Required]
        [Column("FH_INICIO")]
        [StringLength(400)]
        public string FhInicio { get; set; }
        [Required]
        [Column("FH_FIN")]
        [StringLength(400)]
        public string FhFin { get; set; }
    }
}
