﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIPOS_PRODUCCION_LINEA")]
    public partial class TiposProduccionLinea
    {
        [Column("ID_TIPO_PRODUCCION_LINEA")]
        public int IdTipoProduccionLinea { get; set; }
        [Required]
        [Column("DESC_TIPO_PRODUCCION_LINEA")]
        [StringLength(50)]
        public string DescTipoProduccionLinea { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
