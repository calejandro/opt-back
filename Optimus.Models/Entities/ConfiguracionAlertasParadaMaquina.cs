﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("CONFIGURACION_ALERTAS_PARADA_MAQUINA")]
    public partial class ConfiguracionAlertasParadaMaquina : BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("VALOR_MINIMO")]
        public int ValorMinimo { get; set; }
        [Column("VALOR_MAXIMO")]
        public int? ValorMaximo { get; set; }
        [Column("TIPO_ALERTA")]
        public TipoAlertaParadaEnum TipoAlerta { get; set; }
        [Column("PORC_MINIMO")]
        public int? PorcMinimo { get; set; }
        [Column("PORC_MAXIMO")]
        public int? PorcMaximo { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }
    }
}
