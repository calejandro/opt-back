﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Applications")]
    public partial class SysApplications
    {
        [Key]
        public int AppId { get; set; }
        public int AppCode { get; set; }
        [Required]
        [Column("so")]
        [StringLength(50)]
        public string So { get; set; }
        [Required]
        [StringLength(100)]
        public string AppName { get; set; }
        public int LastVersion { get; set; }
        public int UpdateLevel { get; set; }
    }
}
