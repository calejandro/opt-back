﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("SAP_SUP")]
    public partial class SapSup
    {
        [Column("Column 0")]
        [StringLength(50)]
        public string Column0 { get; set; }
        [Column("Column 1")]
        [StringLength(50)]
        public string Column1 { get; set; }
        [Column("Column 2")]
        [StringLength(50)]
        public string Column2 { get; set; }
        [Column("Column 3")]
        [StringLength(50)]
        public string Column3 { get; set; }
        [Column("Column 4")]
        [StringLength(50)]
        public string Column4 { get; set; }
        [Column("Column 5")]
        [StringLength(50)]
        public string Column5 { get; set; }
        [Column("Column 6")]
        [StringLength(50)]
        public string Column6 { get; set; }
        [Column("Column 7")]
        [StringLength(50)]
        public string Column7 { get; set; }
        [Column("Column 8")]
        [StringLength(50)]
        public string Column8 { get; set; }
        [Column("Column 9")]
        [StringLength(50)]
        public string Column9 { get; set; }
        [Column("Column 10")]
        [StringLength(50)]
        public string Column10 { get; set; }
        [Column("Column 11")]
        [StringLength(50)]
        public string Column11 { get; set; }
    }
}
