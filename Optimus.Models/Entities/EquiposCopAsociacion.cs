﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_COP_ASOCIACION")]
    public partial class EquiposCopAsociacion
    {
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Required]
        [Column("DESC_ASOCIACION")]
        [StringLength(50)]
        public string DescAsociacion { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO_POT_FRIGORIFICA")]
        public int IdEquipoPotFrigorifica { get; set; }
        [Column("ID_EQUIPO_POT_ELECTRICA")]
        public int IdEquipoPotElectrica { get; set; }
        [Column("ORDEN_VISUALIZACION")]
        public int OrdenVisualizacion { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
