﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Devices")]
    public partial class SysDevices
    {
        [Key]
        public int DeviceId { get; set; }
        public int UserId { get; set; }
        [StringLength(50)]
        public string AppName { get; set; }
        [StringLength(200)]
        public string RegistrationId { get; set; }
        [Required]
        public bool? Active { get; set; }

        [ForeignKey(nameof(UserId))]
       // [InverseProperty(nameof(SysUsers.SysDevices))]
        public virtual SysUsers User { get; set; }
    }
}
