﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("SAP_ORDENES_PRODUCCION")]
    public partial class SapOrdenesProduccion: BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdOrdenSap; set => IdOrdenSap = value; }
        [Key]
        [Column("ID_ORDEN_SAP")]
        public int IdOrdenSap { get; set; }
        [Column("ID_SKU")]
        public int IdSku { get; set; }
        [Column("CANTIDAD_TOTAL_CAJONES")]
        public int CantidadTotalCajones { get; set; }
        [Column("FH_INICIO")]
        [StringLength(14)]
        public string FhInicio { get; set; }
        [Column("FH_FIN")]
        [StringLength(14)]
        public string FhFin { get; set; }
        [Required]
        [Column("ID_CENTRO")]
        [StringLength(6)]
        public string IdCentro { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Required]
        [Column("GRUPO_RECETAS")]
        [StringLength(50)]
        public string GrupoRecetas { get; set; }
        [Column("CANTIDAD_ENTREGADA")]
        public int CantidadEntregada { get; set; }
        [Column("VELOCIDAD_NOMINAL_LLENADORA")]
        public int VelocidadNominalLlenadora { get; set; }
        [Column("FH_INCIO_PRODUCCION", TypeName = "datetime")]
        public DateTime? FhIncioProduccion { get; set; }
        [Column("FH_FIN_PRODUCCION", TypeName = "datetime")]
        public DateTime? FhFinProduccion { get; set; }
        [Column("FECHA_SUPERVISION", TypeName = "datetime")]
        public DateTime? FechaSupervision { get; set; }

        [Column("ES_TEMPORAL")]
        public bool? EsTemporal { get; set; }

        [Column("ID_ORDEN_VINCULADA")]
        public int? IdOrdenVinculada { get; set;}


        [ForeignKey("IdLinea")]
        public LineasProduccion LineasProduccion { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Plantas { get; set; }

        [ForeignKey("IdSku")]
        public SapSku SapSku { get; set; }        

        [ForeignKey("IdOrdenVinculada")]
        public SapOrdenesProduccion SapOrdenesProdVinculada { get; set; }        

    }
}
