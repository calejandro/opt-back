﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PRODUCCION_USUARIO")]
    public partial class LineasProduccionUsuario : BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("USER_ID")]
        public int UserId { get; set; }
        [Column("FECHA_TOMA", TypeName = "datetime")]
        public DateTime? FechaToma { get; set; }
        [Required]
        [Column("SUPERVISADA")]
        public bool? Supervisada { get; set; }
        [Column("ID_ORDEN_SAP")]
        public int IdOrdenSap { get; set; }
        [Required]
        [Column("ORDEN_CUMPLIMENTADA")]
        public bool? OrdenCumplimentada { get; set; }
        [Column("BORRADO")]
        public bool? Borrado { get; set; }
        [Column("EXTIENDE_TURNO")]
        public bool? ExtiendeTurno { get; set; }
        [Column("FECHA_LIBERACION", TypeName = "datetime")]
        public DateTime? FechaLiberacion { get; set; }
        [Column("TURNO_FINALIZADO")]
        public bool? TurnoFinalizado { get; set; }


        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdOrdenSap")]
        public SapOrdenesProduccion SapOrdenesProduccion { get; set; }
        [ForeignKey("UserId")]
        public SysUsers SysUsers { get; set; }

        public static implicit operator List<object>(LineasProduccionUsuario v)
        {
            throw new NotImplementedException();
        }
    }
}
