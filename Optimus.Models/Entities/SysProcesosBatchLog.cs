﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_ProcesosBatchLog")]
    public partial class SysProcesosBatchLog
    {
        [Key]
        public long IdLog { get; set; }
        public long IdProcesoBatch { get; set; }
        [StringLength(255)]
        public string Descripcion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Inicio { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Fin { get; set; }
        public string Mensaje { get; set; }

        [ForeignKey(nameof(IdProcesoBatch))]
        [InverseProperty(nameof(SysProcesosBatch.SysProcesosBatchLog))]
        public virtual SysProcesosBatch IdProcesoBatchNavigation { get; set; }
    }
}
