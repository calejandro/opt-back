﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_KPI_BEBIDA_ASOCIACION")]
    public partial class EquiposKpiBebidaAsociacion
    {
        [Key]
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Required]
        [Column("DESC_KPI_BEBIDA")]
        [StringLength(50)]
        public string DescKpiBebida { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("ID_EQUIPO_TOTALIZADOR_INGRESO_BEBIDA")]
        public int IdEquipoTotalizadorIngresoBebida { get; set; }
        [Column("ID_EQUIPO_TOTALIZADOR_EGRESO_BOTELLAS")]
        public int IdEquipoTotalizadorEgresoBotellas { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
