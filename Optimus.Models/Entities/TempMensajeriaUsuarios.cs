﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class TempMensajeriaUsuarios
    {
        public int UserIdLogueado { get; set; }
        public int UserId { get; set; }
    }
}
