﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("FORMATOS")]
    public partial class Formatos
    {
        [Column("ID_FORMATO")]
        public int IdFormato { get; set; }
        [Required]
        [Column("COD_FORMATO_PLC")]
        [StringLength(30)]
        public string CodFormatoPlc { get; set; }
        [Required]
        [Column("DESC_FORMATO")]
        [StringLength(50)]
        public string DescFormato { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
    }
}
