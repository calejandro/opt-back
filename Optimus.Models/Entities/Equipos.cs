﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS")]
    public partial class Equipos : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdEquipo; set => IdEquipo = value; }
        [Key]
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Required]
        [Column("DESC_EQUIPO")]
        [StringLength(50)]
        public string DescEquipo { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_PLANTA")]
        public int? IdPlanta { get; set; }
        [Column("ORDEN_VISUALIZACION")]
        public byte? OrdenVisualizacion { get; set; }
        [Column("TIPO_EQUIPO")]
        public int? TipoEquipo { get; set; }
        [Column("PUEDE_RECUPERAR_HISTORICOS")]
        public int? PuedeRecuperarHistoricos { get; set; }
        [Column("INTERVALO_MUESTREO_MINUTOS")]
        public int? IntervaloMuestreoMinutos { get; set; }
        [Column("CANTIDAD_DECIMALES")]
        public int? CantidadDecimales { get; set; }
        [Column("AL_LEER_MULTIPLICAR_POR")]
        public double? AlLeerMultiplicarPor { get; set; }
        [Column("AL_LEER_MULTIPLICAR_POR_POTENCIA_DIEZ_A_LA")]
        public int? AlLeerMultiplicarPorPotenciaDiezALa { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
        [Column("PRORRATEAR_LECTURAS_POR_MINUTO")]
        public bool? ProrratearLecturasPorMinuto { get; set; }
        [Column("IGNORAR_VALOR_MENOR_A", TypeName = "decimal(11, 3)")]
        public decimal? IgnorarValorMenorA { get; set; }
        [Column("IGNORAR_VALOR_MAYOR_A", TypeName = "decimal(11, 3)")]
        public decimal? IgnorarValorMayorA { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }

        [InverseProperty("Equipo")]
        public List<AlertaMermaConfiguraciones> AlertaMermaConfiguraciones { get; set; }

    }
}
