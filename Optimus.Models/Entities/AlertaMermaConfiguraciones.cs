﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Models.Entities
{
    [Table("ALERTA_MERMA_CONFIGURACIONES")]
    public partial class AlertaMermaConfiguraciones : BaseCommon
    {
        [Key]
        [Column("ID")]
        public override int Id { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("MINIMO_PRIMER_ALERTA", TypeName = "decimal(11, 3)")]
        public decimal? MinimoPrimerAlerta { get; set; }
        [Column("MINIMO_SEGUNDA_ALERTA", TypeName = "decimal(11, 3)")]
        public decimal? MinimoSegundaAlerta { get; set; }
        [Column("TIPO_MERMA")]
        public TipoMerma? TipoMerma { get; set; }
        [Column("ID_SKU")]
        public int IdSku { get; set; }  
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }    
        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
        [ForeignKey("IdSku")]
        public SapSku SapSku { get; set; }
    }
}
