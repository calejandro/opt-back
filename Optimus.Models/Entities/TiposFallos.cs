﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIPOS_FALLOS")]
    public partial class TiposFallos : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdTipo; set => IdTipo = value; }
        [Key]
        [Column("ID_TIPO")]
        public int IdTipo { get; set; }
        [Required]
        [Column("NOMBRE")]
        [StringLength(150)]
        public string Nombre { get; set; }
        [Column("TIPO")]
        public TipoModoFalloEnum Tipo { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime? FechaCreacion { get; set; }
        [Column("BORRADO")]
        public bool? Borrado { get; set; }
        [Column("ACTIVO")]
        public bool? Activo { get; set; }
        [Column("ID_EQUIPO")]
        public int? IdEquipo { get; set; }
        [Column("ID_LINEA")]
        public int? IdLinea { get; set; }
        [Column("TIPO_ALERTA")]
        public TipoAlertEnum? TipoAlerta { get; set; }
        [Column("ID_PLANTA")]
        public int? IdPlanta { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }
        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
    }
}
