﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_UsersGroups")]
    public partial class SysUsersGroups: BaseCommon
    {
        [NotMapped]
        public override int Id { get => UserGroupId; set => UserGroupId = value; }
        //public SysUsersGroups()
        //{
        //    AlertsGroups = new HashSet<AlertsGroups>();
        //    SysMultipleGroups = new HashSet<SysMultipleGroups>();
        //    SysPermissions = new HashSet<SysPermissions>();
        //    SysUserReportConfiguration = new HashSet<SysUserReportConfiguration>();
        //}

        [Key]
        public int UserGroupId { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        public int? Level { get; set; }
        [Column("InitURL")]
        [StringLength(100)]
        public string InitUrl { get; set; }
        public int? SubsistemaId { get; set; }
        [StringLength(1)]
        public string IsActive { get; set; }


        //[ForeignKey(nameof(SubsistemaId))]
        //[InverseProperty(nameof(Subsistemas.SysUsersGroups))]
        //public virtual Subsistemas Subsistema { get; set; }
        //[InverseProperty("UserGroup")]
        //public virtual ICollection<AlertsGroups> AlertsGroups { get; set; }
        //[InverseProperty("UserGroup")]
        //public virtual ICollection<SysMultipleGroups> SysMultipleGroups { get; set; }
        //[InverseProperty("UserGroup")]
        //public virtual ICollection<SysPermissions> SysPermissions { get; set; }
        //[InverseProperty("UserGroup")]
        //public virtual ICollection<SysUserReportConfiguration> SysUserReportConfiguration { get; set; }
    }
}
