﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("TIME_LINE_HISTOGRAMA_2_TURNOS")]
    public partial class TimeLineHistograma2Turnos
    {
        [Column("TURNO")]
        public int Turno { get; set; }
        [Required]
        [Column("DESC_HORA_MIN")]
        [StringLength(5)]
        public string DescHoraMin { get; set; }
        [Column("ORDEN")]
        public int Orden { get; set; }
    }
}
