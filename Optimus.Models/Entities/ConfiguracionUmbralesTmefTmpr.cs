﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("CONFIGURACION_UMBRALES_TMEF_TMPR")]
    public partial class ConfiguracionUmbralesTmefTmpr : BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("TIPO")]
        public TiempoPromTipoReparacionEnum Tipo { get; set; }
        [Column("VALOR_MINIMO")]
        public double ValorMinimo { get; set; }
        [Column("VALOR_MAXIMO")]
        public double ValorMaximo { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }

        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }
    }
}
