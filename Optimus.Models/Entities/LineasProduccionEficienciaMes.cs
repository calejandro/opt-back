﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PRODUCCION_EFICIENCIA_MES")]
    public partial class LineasProduccionEficienciaMes : BaseCommon
    {
        [Key]
        public int Id { get; set; }
        [Column("ID_LINEA")]
        public int? IdLinea { get; set; }
        [Column("EFICIENCIA_MENSUAL")]
        public double EficienciaMensual { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime FechaCreacion { get; set; }
        [Column("BORRADO")]
        public bool? Borrado { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdPlanta")]
        public Plantas Planta { get; set; }
    }
}
