﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTRO_MULTICHART")]
    public partial class RegistroMultichart
    {
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Required]
        [Column("FH_REGISTRO")]
        [StringLength(14)]
        public string FhRegistro { get; set; }
        [Column("LECTURA")]
        public int Lectura { get; set; }
        [Column("CANTIDAD_DECIMALES")]
        public int? CantidadDecimales { get; set; }
        [Column("AL_LEER_MULTIPLICAR_POR")]
        public double? AlLeerMultiplicarPor { get; set; }
        [Column("AL_LEER_MULTIPLICAR_POR_POTENCIA_DIEZ_A_LA")]
        public int? AlLeerMultiplicarPorPotenciaDiezALa { get; set; }
        [Column("COD_PRODUCTO")]
        [StringLength(10)]
        public string CodProducto { get; set; }
        [Column("ID_SABOR")]
        public int? IdSabor { get; set; }
        [Column("ID_FORMATO")]
        public int? IdFormato { get; set; }
    }
}
