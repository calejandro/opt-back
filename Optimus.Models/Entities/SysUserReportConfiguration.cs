﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_UserReportConfiguration")]
    public partial class SysUserReportConfiguration: BaseCommon
    {
        [Key]
        public override int Id { get; set; }
        public int UserGroupId { get; set; }
        public ReportTypeEnum ReportType { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("UserGroupId")]
        //[InverseProperty(nameof(SysUsersGroups.SysUserReportConfiguration))]
        public  SysUsersGroups UserGroup { get; set; }
    }
}
