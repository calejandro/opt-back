﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class Parameters
    {
        [Key]
        public int ParameterId { get; set; }
        [StringLength(100)]
        public string ParameterKey { get; set; }
        public string ParameterValue { get; set; }
    }
}
