﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_DEV")]
    public partial class EquiposDev
    {
        [Column("ID_EQUIPO")]
        public int? IdEquipo { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Required]
        [Column("DESC_EQUIPO")]
        [StringLength(100)]
        public string DescEquipo { get; set; }
        [Column("ID_PLANTA")]
        public int IdPlanta { get; set; }
    }
}
