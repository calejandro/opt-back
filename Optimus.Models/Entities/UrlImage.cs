﻿
using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Url_Image")]
    public partial class UrlImage : BaseCommon
    {
        public new Guid Id { get; set; }
        [Column("Id_Entity")]
        [Required]
        public int IdEntity { get; set; }
        [Column("Entity")]
        [Required]
        public string Entity { get; set; }
        [Column("Name")]
        [Required]
        public string Name { get; set; }
        [Column("Id_Planta")]
        public int? IdPlanta { get; set; }

        [ForeignKey("IdEntity")]
        public LineasProduccionAlertas LineasProduccionAlertas { get; set; }

    }
}
