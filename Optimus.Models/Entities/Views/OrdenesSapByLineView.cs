﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class OrdenesSapByLineView : BaseCommon
    {
        [NotMapped]
        public override int Id { get; set; }
        [Column("ID_ORDEN_SAP")]
        public int IdOrdenSap { get; set; }
        [Column("FH_INCIO_PRODUCCION")]
        public DateTime FhInicioProduccion { get; set; }
        [Column("FH_FIN_PRODUCCION")]
        public DateTime FhFinProduccion { get; set; }
        [Column("Date_Diff")]
        public int DateDiff { get; set; }
        [Column("ID_SKU")]
        public int IdSku { get; set; }
        [Column("desc_sku")]
        public string DescSku { get; set; }
        [Column("marca")]
        public string Marca { get; set; }
        [Column("SABOR")]
        public string Sabor { get; set; }
        [Column("FORMATO")]
        public string Formato { get; set; }
        [Column("formatoConsolid")]
        public string FormatoConsolid { get; set; }
        [Column("saborConsolid")]
        public string SaborConsolid { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("USER_ID")]
        public int IdUser { get; set; }

    }
}
