﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    public partial class SkuByLineView : BaseCommon
    {
        [NotMapped]
        public override int Id { get; set; }
        [Column("ID_SKU")]
        public int IdSku { get; set; }
        [Column("desc_sku")]
        public string DescSku { get; set; }
        [Column("marca")]
        public string Marca { get; set; }
        [Column("formato")]
        public string Formato { get; set; }
        [Column("sabor")]
        public string Sabor { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
    }
}
