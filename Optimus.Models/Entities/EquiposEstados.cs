﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("EQUIPOS_ESTADOS")]
    public partial class EquiposEstados : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdRegistro; set => IdRegistro = value; }
        [Key]
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }
        [Column("TIPO_EQUIPO")]
        public int TipoEquipo { get; set; }
        [Column("ESTADO")]
        public byte Estado { get; set; }
        [Required]
        [Column("DESC_ESTADO")]
        [StringLength(50)]
        public string DescEstado { get; set; }
        [Required]
        [Column("FH_ULT_MODIF")]
        [StringLength(14)]
        public string FhUltModif { get; set; }
        [Column("ACTIVO")]
        public bool Activo { get; set; }
        [Column("COLOR")]
        [StringLength(6)]
        public string Color { get; set; }
    }
}
