﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("REGISTRO_NOVEDADES")]
    public partial class RegistroNovedades
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_MAQUINA")]
        public int? IdMaquina { get; set; }
        [Column("FECHA_INICIO", TypeName = "datetime")]
        public DateTime? FechaInicio { get; set; }
        [Column("FECHA_FIN", TypeName = "datetime")]
        public DateTime? FechaFin { get; set; }
        [Required]
        [Column("STORES_EJECUTAR")]
        public string StoresEjecutar { get; set; }
        [Column("PROCESADO")]
        public bool? Procesado { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime FechaCreacion { get; set; }
        [Column("CANTIDAD_INTENTOS")]
        public int CantidadIntentos { get; set; }
    }
}
