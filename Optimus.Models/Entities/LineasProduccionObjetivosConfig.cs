﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Models.Entities
{
    [Table("LineasProduccionObjetivosConfig")]
    public class LineasProduccionObjetivosConfig : BaseCommon
    {
        [Key]
        public new Guid Id { get; set; }
        public int IdLinea { get; set; }
        public int IdPlanta { get; set; }
        public TipoObjetivoEnum Tipo { get; set; }
        public decimal Valor { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion Linea { get; set; }

        public LineasProduccionObjetivosConfig()
        {
        }
    }
}
