﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("Sys_Users_Plants")]
    public partial class SysUsersPlants: BaseCommon
    {
        [NotMapped]
        public override int Id { get => UserPlantId; set => UserPlantId = value; }
        [Key]
        public int UserPlantId { get; set; }
        public int UserId { get; set; }
        public int PlantId { get; set; }

        [ForeignKey("PlantId")]
        public virtual Plantas Plantas { get; set; }

        [ForeignKey("UserId")]
        public virtual SysUsers SysUsers { get; set; }
    }
}
