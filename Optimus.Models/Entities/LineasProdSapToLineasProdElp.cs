﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PROD_SAP_TO_LINEAS_PROD_ELP")]
    public partial class LineasProdSapToLineasProdElp
    {


        [Column("ID_LINEA_ELP", Order = 0)]        
        public int IdLineaElp { get; set; }        
        [Column("ID_LINEA_SAP", Order = 1)]        
        public int IdLineaSap { get; set; }
        [Required]
        [Column("ID_CENTRO")]
        [StringLength(6)]
        public string IdCentro { get; set; }
        [Column("ID_REGISTRO")]
        public int IdRegistro { get; set; }

        [ForeignKey("IdLineaElp")]
        public LineasProduccion LineasProduccionElp { get; set; }

        [ForeignKey("IdLineaSap")]
        public LineasProduccion LineasProduccionSap { get; set; }
        //[ForeignKey("IdLineaSap")]
        //public SapOrdenesProduccion SapOrdenesProduccions { get; set; }

    }
}
