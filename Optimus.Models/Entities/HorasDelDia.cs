﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("HORAS_DEL_DIA")]
    public partial class HorasDelDia
    {
        [Column("ID_HORA")]
        public int IdHora { get; set; }
    }
}
