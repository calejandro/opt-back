﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Entities
{
    [Table("LINEAS_PRODUCCION_ALERTAS")]
    public partial class LineasProduccionAlertas : BaseCommon
    {
        [NotMapped]
        public override int Id { get => IdAlerta; set => IdAlerta = value; }
        [Key]
        [Column("ID_ALERTA")]
        public int IdAlerta { get; set; }
        [Column("ID_LINEA")]
        public int IdLinea { get; set; }
        [Column("ID_EQUIPO")]
        public int IdEquipo { get; set; }
        [Column("ESTADO")]
        public EstadosLPAEnum Estado { get; set; }
        [Column("VALOR_ACUMULADO")]
        public double ValorAcumulado { get; set; }
        [Column("ENVIO_DESVIO")]
        public bool EnvioDesvio { get; set; }
        [Column("FECHA_CREACION", TypeName = "datetime")]
        public DateTime FechaCreacion { get; set; }
        [Column("BORRADO")]
        public bool? Borrado { get; set; }
        [Column("TIPO_ALERTA")]
        public TipoAlertaParadaEnum? TipoAlerta { get; set; }
        [Column("CLASIFICACION")]
        public ClasificacionAlertaEnum? Clasificacion { get; set; }
        [Column("DESCRIPCION")]
        [StringLength(100)]
        public string Descripcion { get; set; }
        [Column("TIPO_MERMA")]
        public TipoMerma? TipoMerma { get; set; }
        [Column("FECHA_FIN", TypeName = "datetime")]
        public DateTime? FechaFin { get; set; }
        [Column("UMBRAL")]
        [StringLength(20)]
        public string Umbral { get; set; }
        [Column("FECHA_ACTUALIZACION", TypeName = "datetime")]
        public DateTime? FechaActualizacion { get; set; }
        [Column("NOTIFICACION_ENVIADA")]
        public bool? NotificacionEnviada { get; set; }

        [ForeignKey("IdLinea")]
        public LineasProduccion LineaProduccion { get; set; }
        [ForeignKey("IdEquipo")]
        public Equipos Equipo { get; set; }

        [InverseProperty("LineasProduccionAlertas")]
        public List<RegistrosFallosAlertas> RegistrosFallosAlertas { get; set; }

        [InverseProperty("LineasProduccionAlertas")]
        public List<UrlImage> UrlImages { get; set; }

        public LineasProduccionAlertas()
        {
            this.RegistrosFallosAlertas = new List<RegistrosFallosAlertas>();
            this.UrlImages = new List<UrlImage>();
        }
    }
}
