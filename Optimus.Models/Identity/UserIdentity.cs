﻿using Optimus.Commons.Entities;
using Optimus.Commons.Ldap;
using Optimus.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Identity
{
    public class UserIdentity : IdentityUser<Guid>, IBaseEntity<Guid>, IAuditedEntity
    {
        [Required(AllowEmptyStrings = false)]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(2)]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        public bool IsEnabled { get; set; }

        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        [MaxLength(50)]
        public string CreatedByUser { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        [MaxLength(50)]
        public string UpdatedByUser{ get; set; }
        public DateTime? DeletedAt { get; set; }
        public string DeletedBy { get; set; }
        [MaxLength(50)]
        public string DeleteByUser { get; set; }

        [Column("CognitoId")]
        public Guid? IdCognito { get; set; }

        public virtual ICollection<UserRoleIdentity> UserRoles { get; set; }
        
        public UserIdentity()
        {
        }
    }
}
