﻿using Optimus.Commons.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Models.Identity
{
    public class RoleIdentity : IdentityRole<Guid>, IBaseEntity<Guid>, IAuditedEntity
    {
        public string Description { get; set; }
        public bool IsSystemRole { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        [MaxLength(50)]
        public string CreatedByUser { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        [MaxLength(50)]
        public string UpdatedByUser { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string DeletedBy { get; set; }
        [MaxLength(50)]
        public string DeleteByUser { get; set; }

        public virtual ICollection<UserRoleIdentity> UserRoles { get; set; }
        public virtual ICollection<RoleClaimIdentity> RoleClaimIdentities { get; set; }
        
    }
}
