﻿using Optimus.Commons.Entities;
using Optimus.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Identity
{
    public class UserRoleIdentity : IdentityUserRole<Guid>, IBaseEntity<Guid>, IAuditedEntity
    {
        public Guid Id { get; set; }

        public virtual UserIdentity User { get; set; }

        public virtual RoleIdentity Role { get; set; }

        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        [MaxLength(50)]
        public string CreatedByUser { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        [MaxLength(50)]
        public string UpdatedByUser { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string DeletedBy { get; set; }
        [MaxLength(50)]
        public string DeleteByUser { get; set; }

    }
}