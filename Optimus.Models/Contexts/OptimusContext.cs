﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Primitives;
using Optimus.Commons.Contexts;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Models.Views;

namespace Optimus.Models.Contexts
{
    public class OptimusContext : BaseIdentityContext<UserIdentity, RoleIdentity, UserRoleIdentity, Guid, UserClaimIdentity, RoleClaimIdentity>, IBaseContext
    {
        public OptimusContext(DbContextOptions<OptimusContext> options, IHttpContextAccessor httpContextAccessor)
            : base(options, httpContextAccessor, false)
        {
        }

        public virtual DbSet<AlertaMermaConfiguraciones> AlertaMermaConfiguraciones { get; set; }
        public virtual DbSet<ConfiguracionAlertasParadaMaquina> ConfiguracionAlertasParadaMaquina { get; set; }
        public virtual DbSet<ConfiguracionUmbralesTmefTmpr> ConfiguracionUmbralesTmefTmpr { get; set; }
        //public virtual DbSet<EmailsDestinatarios> EmailsDestinatarios { get; set; }
        public virtual DbSet<Equipos> Equipos { get; set; }
        //public virtual DbSet<EquiposCopAsociacion> EquiposCopAsociacion { get; set; }
        //public virtual DbSet<EquiposDev> EquiposDev { get; set; }
        //public virtual DbSet<EquiposEficienciasAsociacion> EquiposEficienciasAsociacion { get; set; }
        public virtual DbSet<EquiposEstados> EquiposEstados { get; set; }
        //public virtual DbSet<EquiposKpiBebidaAsociacion> EquiposKpiBebidaAsociacion { get; set; }
        //public virtual DbSet<EquiposKpiRoturaEnvasesAsociacion> EquiposKpiRoturaEnvasesAsociacion { get; set; }
        //public virtual DbSet<EquiposOpc> EquiposOpc { get; set; }
        //public virtual DbSet<EquiposTotalizadoresSubsectorAsociacion> EquiposTotalizadoresSubsectorAsociacion { get; set; }
        //public virtual DbSet<ExportacionTotalizadores> ExportacionTotalizadores { get; set; }
        //public virtual DbSet<Formatos> Formatos { get; set; }
        //public virtual DbSet<HorasDelDia> HorasDelDia { get; set; }
        //public virtual DbSet<Licencias> Licencias { get; set; }
        //public virtual DbSet<LineasDev> LineasDev { get; set; }
        public virtual DbSet<LineasProdSapToLineasProdElp> LineasProdSapToLineasProdElp { get; set; }
        public virtual DbSet<LineasProduccion> LineasProduccion { get; set; }
        public virtual DbSet<LineasProduccionAlertas> LineasProduccionAlertas { get; set; }
        public virtual DbSet<LineasProduccionAlertas2> LineasProduccionAlertas2 { get; set; }

        //public virtual DbSet<LineasProduccionEficienciaHistorica> LineasProduccionEficienciaHistorica { get; set; }
        public virtual DbSet<LineasProduccionEficienciaMes> LineasProduccionEficienciaMes { get; set; }
        public virtual DbSet<LineasProduccionUsuario> LineasProduccionUsuario { get; set; }
        //public virtual DbSet<LineasSkuTipoProduccion> LineasSkuTipoProduccion { get; set; }
        //public virtual DbSet<MaestroProductos> MaestroProductos { get; set; }
        //public virtual DbSet<MinutosDelDia> MinutosDelDia { get; set; }
        public virtual DbSet<Paises> Paises { get; set; }
        //public virtual DbSet<Parameters> Parameters { get; set; }
        //public virtual DbSet<ParesDeMinutosDelDia> ParesDeMinutosDelDia { get; set; }
        public virtual DbSet<Plantas> Plantas { get; set; }
        //public virtual DbSet<RefactorLog> RefactorLog { get; set; }
        public virtual DbSet<RegistroEstados> RegistroEstados { get; set; }
        //public virtual DbSet<RegistroMultichart> RegistroMultichart { get; set; }
        //public virtual DbSet<RegistroNovedades> RegistroNovedades { get; set; }
        //public virtual DbSet<RegistroProduccion> RegistroProduccion { get; set; }
        //public virtual DbSet<RegistroTotalizadores> RegistroTotalizadores { get; set; }
        //public virtual DbSet<RegistroTotalizadoresPorMinutos> RegistroTotalizadoresPorMinutos { get; set; }
        public virtual DbSet<RegistrosFallosAlertas> RegistrosFallosAlertas { get; set; }
        //public virtual DbSet<Sabores> Sabores { get; set; }
        public virtual DbSet<SapOrdenesProduccion> SapOrdenesProduccion { get; set; }
        public virtual DbSet<SapSku> SapSku { get; set; }
        //public virtual DbSet<SapSup> SapSup { get; set; }
        //public virtual DbSet<Subsectores> Subsectores { get; set; }
        //public virtual DbSet<Subsistemas> Subsistemas { get; set; }
        //public virtual DbSet<SysApplications> SysApplications { get; set; }
        //public virtual DbSet<SysDevices> SysDevices { get; set; }
        //public virtual DbSet<SysErrors> SysErrors { get; set; }
        //public virtual DbSet<SysLanguages> SysLanguages { get; set; }
        //public virtual DbSet<SysLogType> SysLogType { get; set; }
        //public virtual DbSet<SysLogs> SysLogs { get; set; }
        public virtual DbSet<SysMultipleGroups> SysMultipleGroups { get; set; }
        //public virtual DbSet<SysPermissions> SysPermissions { get; set; }
        //public virtual DbSet<SysPermissionsItems> SysPermissionsItems { get; set; }
        //public virtual DbSet<SysPermissionsItemsModes> SysPermissionsItemsModes { get; set; }
        //public virtual DbSet<SysProcesosBatch> SysProcesosBatch { get; set; }
        //public virtual DbSet<SysProcesosBatchLog> SysProcesosBatchLog { get; set; }
        public virtual DbSet<SysUserReportConfiguration> SysUserReportConfiguration { get; set; }
        //public virtual DbSet<SysUserSession> SysUserSession { get; set; }
        public virtual DbSet<SysUsers> SysUsers { get; set; }
        public virtual DbSet<SysUsersGroups> SysUsersGroups { get; set; }
        public virtual DbSet<SysUsersPlants> SysUsersPlants { get; set; }
        //public virtual DbSet<SysWebPageFilter> SysWebPageFilter { get; set; }
        //public virtual DbSet<TempMensajeriaDestinatariosUsuarios> TempMensajeriaDestinatariosUsuarios { get; set; }
        //public virtual DbSet<TempMensajeriaUsuarios> TempMensajeriaUsuarios { get; set; }
        //public virtual DbSet<TimeLineHistograma2Turnos> TimeLineHistograma2Turnos { get; set; }
        //public virtual DbSet<TimeLineHistograma3Turnos> TimeLineHistograma3Turnos { get; set; }
        //public virtual DbSet<TimeLineHistogramaEfectividadesEnergeticas> TimeLineHistogramaEfectividadesEnergeticas { get; set; }
        //public virtual DbSet<TimeLineHistogramaTotalizadores2Turnos> TimeLineHistogramaTotalizadores2Turnos { get; set; }
        //public virtual DbSet<TimeLineHistogramaTotalizadores3Turnos> TimeLineHistogramaTotalizadores3Turnos { get; set; }
        //public virtual DbSet<TiposEquipos> TiposEquipos { get; set; }
        public virtual DbSet<TiposFallos> TiposFallos { get; set; }
        //public virtual DbSet<TiposLineas> TiposLineas { get; set; }
        //public virtual DbSet<TiposProduccionLinea> TiposProduccionLinea { get; set; }
        //public virtual DbSet<TiposRecuperacionEquipos> TiposRecuperacionEquipos { get; set; }
        //public virtual DbSet<TmefTmpr> TmefTmpr { get; set; }

        // *STORED PROCEDURES* 
        public virtual DbSet<MachineActualStatus> MachineActualStatuses { get; set; }
        public virtual DbSet<MachineStatusProductionOrder> MachineStatusProductionOrder { get; set; }
        public virtual DbSet<ProduccionEficiencia> ProduccionEficiencia { get; set; }
        public virtual DbSet<ProduccionEficienciaReal> ProduccionEficienciaReal { get; set; }
        public virtual DbSet<ProduccionEficienciaByShift> ProduccionEficienciaByShift { get; set; }
        public virtual DbSet<KpiDepletion> KpiDepletion { get; set; }
        public virtual DbSet<AverageTimeFailuresProduction> AverageTimeFailuresProduction { get; set; }
        public virtual DbSet<TimeStops> TimeStops { get; set; }
        public virtual DbSet<EvolucionParadaPropiaHora> EvolucionParadaPropiaHoras { get; set; }
        public virtual DbSet<APPMobStatusActual> APPMobStatusActual { get; set; }
        public virtual DbSet<APPMobKpiRoturaEnvases> APPMobKpiRoturaEnvases { get; set; }

  
        public virtual DbSet<LineasProduccionObjetivosConfig> LineasProduccionObjetivosConfigs { get; set; }
        public virtual DbSet<APPMobStatusActualAcumulado> APPMobStatusActualAcumulado { get; set; }
        public virtual DbSet<ConfiguracionUmbralesAlertasPrecaucion> ConfiguracionUmbralesAlertasPrecaucion { get; set; }

        public virtual DbSet<UrlImage> UrlImage { get; set; }
        public virtual DbSet<SkuByLineView> SkuByLineView { get; set; }
        public virtual DbSet<OrdenesSapByLineView> OrdenesSapByLineView { get; set; }
        public virtual DbSet<AlertByOrdenesSapView> AlertByOrdenesSapView { get; set; }

        






        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=tcp:181.142.96.45,1433;Initial Catalog=ELP;Persist Security Info=False;User ID=Darwoft;Password=DarAndina.2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        public int? GetPlantId
        {
            get
            {
                int? result = null;
                if (this.httpContextAccessor != null)
                {
                    var exists = this.httpContextAccessor?.HttpContext?.Request?.Headers?.ContainsKey("plantaId");
                    if (exists == null || exists == false)
                    {
                        return null;
                    }

                    result = int.Parse(this.httpContextAccessor.HttpContext.Request.Headers["plantaId"]);

                }

                return result;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {         

            modelBuilder.Entity<ProduccionEficienciaReal>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<LineasProdSapToLineasProdElp>(entity =>
            {
                entity.HasKey(x => new { x.IdLineaElp, x.IdLineaSap });
            });


            modelBuilder.Entity<AlertaMermaConfiguraciones>(entity =>
            {
                entity.HasQueryFilter(w => w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<ConfiguracionAlertasParadaMaquina>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<ConfiguracionUmbralesTmefTmpr>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<Equipos>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<LineasProduccion>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<LineasProduccionObjetivosConfig>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<TiposFallos>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<LineasProduccionEficienciaMes>(entity =>
            {
                entity.HasQueryFilter(w => w.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<LineasProduccionUsuario>(entity =>
            {
                entity.HasQueryFilter(w => this.GetPlantId == null || w.LineaProduccion.IdPlanta == this.GetPlantId);
            });
            modelBuilder.Entity<SkuByLineView>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<OrdenesSapByLineView>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<AlertByOrdenesSapView>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<RegistroEstados>(entity =>
            {
                entity.HasNoKey();
            });

            // *STORED PROCEDURES*
            modelBuilder.Entity<EvolucionParadaPropiaHora>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<MachineActualStatus>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<MachineStatusProductionOrder>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<ProduccionEficiencia>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<ProduccionEficienciaByShift>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<KpiDepletion>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<AverageTimeFailuresProduction>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<TimeStops>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<APPMobStatusActual>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<APPMobKpiRoturaEnvases>(entity =>
            {
                entity.HasNoKey();
            });

            base.OnModelCreating(modelBuilder);
        }

    }
}
