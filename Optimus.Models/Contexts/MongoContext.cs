﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Contexts;
using Optimus.Models.Views;

namespace Optimus.Models.Contexts
{
    public class MongoContext : BaseMongoContext
    {
        public MongoContext()
        {
        }
        public virtual DbSet<MachineActualStatus> MachineActualStatuses { get; set; }
        public virtual DbSet<MachineStatusProductionOrder> MachineStatusProductionOrder { get; set; }
        public virtual DbSet<ProduccionEficiencia> ProduccionEficiencia { get; set; }
        public virtual DbSet<ProduccionEficienciaByShift> ProduccionEficienciaByShift { get; set; }
        public virtual DbSet<KpiDepletion> KpiDepletion { get; set; }
        public virtual DbSet<AverageTimeFailuresProduction> AverageTimeFailuresProduction { get; set; }
        public virtual DbSet<TimeStops> TimeStops { get; set; }
    }
}
