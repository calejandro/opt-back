﻿using Optimus.Commons.Contexts;
using Optimus.Models.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Optimus.Commons.Entities;
using Optimus.Models.Extensions;
using System;

namespace Optimus.Models.Contexts
{
    public class IdentityContext : BaseIdentityContext<UserIdentity, RoleIdentity, UserRoleIdentity, Guid, UserClaimIdentity, RoleClaimIdentity>, IBaseContext
    {

        public IdentityContext(DbContextOptions<IdentityContext> options, IHttpContextAccessor httpContextAccessor, ILogger<IdentityContext> logger)
          : base(options, httpContextAccessor, logger)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                if (typeof(IAuditedEntity).IsAssignableFrom(entityType.ClrType))
                    builder.SetSoftDeleteFilter(entityType.ClrType);
            }

            builder.Entity<UserIdentity>(entity =>
            {
                entity.HasMany(x => x.UserRoles).WithOne(x => x.User).HasForeignKey(ur => ur.UserId).IsRequired();

            });

            builder.Entity<RoleIdentity>(entity =>
            {
                // Each Role can have many entries in the UserRole join table
                entity.HasMany(e => e.UserRoles).WithOne(e => e.Role).HasForeignKey(ur => ur.RoleId).IsRequired();
            });

            builder.Entity<UserRoleIdentity>().HasKey(hk => new { hk.UserId, hk.RoleId, hk.Id });

        }

    }
}