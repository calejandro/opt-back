﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Primitives;
using Optimus.Commons.Contexts;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Models.Views;

namespace Optimus.Models.Contexts
{
    public class Optimus2Context : BaseIdentityContext<UserIdentity, RoleIdentity, UserRoleIdentity, Guid, UserClaimIdentity, RoleClaimIdentity>, IBaseContext
    {
        public Optimus2Context(DbContextOptions<Optimus2Context> options, IHttpContextAccessor httpContextAccessor)
            : base(options, httpContextAccessor, false)
        {
        }

        public virtual DbSet<OptimusTask> OptimusTask { get; set; }
        public virtual DbSet<TaskComment> TaskComment { get; set; }
        public virtual DbSet<FileUrl> FileUrl { get; set; }
        public virtual DbSet<Kpi> Kpi { get; set; }
        public virtual DbSet<Process> Process { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }



        public int? GetPlantId
        {
            get
            {
                int? result = null;
                if (this.httpContextAccessor != null)
                {
                    var exists = this.httpContextAccessor?.HttpContext?.Request?.Headers?.ContainsKey("plantaId");
                    if (exists == null || exists == false)
                    {
                        return null;
                    }

                    result = int.Parse(this.httpContextAccessor.HttpContext.Request.Headers["plantaId"]);

                }

                return result;
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OptimusTask>(entity =>
            {
                entity.Property(m => m.IdCreatorUser).Metadata.SetAfterSaveBehavior(Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Ignore);
                entity.Property(m => m.Identifier).Metadata.SetAfterSaveBehavior(Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Ignore);
            });
            base.OnModelCreating(modelBuilder);
        }

    }

}
