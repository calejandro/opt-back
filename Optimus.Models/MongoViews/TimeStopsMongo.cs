﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class TimeStopsMongo : BaseMongoCommon
    {

        public int IdLinea { get; set; }

        public DateTime FechaHoraInicio { get; set; }

        public DateTime FechaHoraFin { get; set; }

        public string  Turno { get; set; }

        public int IdEstado { get; set; }

        public List<TimeStops> TimeStops { get; set; }


        public TimeStopsMongo()
        {
        }
    }
}
