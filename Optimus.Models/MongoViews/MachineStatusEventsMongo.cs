﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class MachineStatusEventsMongo : BaseMongoCommon
    {
        public int IdLinea { get; set; }
        public string DescripcionLinea { get; set; }
        public int IdOrdenSAP { get; set; }
        public int IdSKU { get; set; }
        public string DescripcionSKU { get; set; }
        public string Marca { get; set; }
        public string Sabor { get; set; }
        public string Formato { get; set; }
        public string SaborConsolid { get; set; }
        public string FormatoConsolid { get; set; }
        public DateTime FechaInicioOrdenSap { get; set; }
        public DateTime? FechaFinOrdenSap { get; set; }
        public int IdSupervisor  { get; set; }
        public DateTime FechaInicioEstado { get; set; }
        public DateTime? FechaFinEstado { get; set; }
        public int IdEquipo { get; set; }
        public byte IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public int MinutosAcumulados { get; set; }
        


        public MachineStatusEventsMongo()
        {
        }
    }
}
