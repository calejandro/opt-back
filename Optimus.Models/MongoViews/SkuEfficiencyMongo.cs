﻿using Optimus.Commons.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class SkuEfficiencyMongo : BaseMongoCommon
    {
        public int IdLinea { get; set; }
        public string DescripcionLinea { get; set; }
        public int IdOrdenSAP { get; set; }
        public int IdSKU { get; set; }
        public string DescripcionSKU { get; set; }
        public string Marca { get; set; }
        public string Sabor { get; set; }
        public string Formato { get; set; }
        public string SaborConsolid { get; set; }
        public string FormatoConsolid { get; set; }
        public string Turno { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int IdSupervisor  { get; set; }

        public double Eficiencia { get; set; }
        public double CantidadRegistros { get; set; }
        public int BotellasProducidas { get; set; }
        public double Volumen { get; set; }
        public int TiempoEnLineaM { get; set; }



        public SkuEfficiencyMongo()
        {
        }
    }
}
