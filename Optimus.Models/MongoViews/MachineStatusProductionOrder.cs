﻿using Optimus.Commons.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class MachineStatusHistoricMongo : BaseMongoCommon
    {
        public int IdLinea { get; set; }
        public string DescripcionLinea { get; set; }
        public int IdOrdenSAP { get; set; }
        public int IdSKU { get; set; }
        public string DescripcionSKU { get; set; }
        public int IdEquipo { get; set; }
        public string DescEquipo { get; set; }

        public string Turno { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int IdSupervisor  { get; set; }

        public string Marca { get; set; }
        public string Sabor { get; set; }
        public string Formato { get; set; }
        public string SaborConsolid { get; set; }
        public string FormatoConsolid { get; set; }

        public int NoDeterminadoMA { get; set; }
        public double NoDeterminadoMAP { get; set; }

        public int ParadaOperacionalMA { get; set; }
        public double ParadaOperacionalMAP { get; set; }

        public int FueraProduccionMA { get; set; }
        public double FueraProduccionMAP { get; set; }

        public int ProduccionMA { get; set; }
        public double ProduccionMAP { get; set; }

        public int ParadaPropiaMA { get; set; }
        public double ParadaPropiaMAP { get; set; }

        public int ParadaAlimentacionMA { get; set; }
        public double ParadaAlimentacionMAP { get; set; }

        public int ParadaDescargaMA { get; set; }
        public double ParadaDescargaMAP { get; set; }

        public int ParadaFuncionalMA { get; set; }
        public double ParadaFuncionalMAP { get; set; }

        public int SubVelocidadMA { get; set; }
        public double SubVelocidadMAP { get; set; }


        public double? TMEF { get; set; }
        public double? TMPR { get; set; }



        public MachineStatusHistoricMongo()
        {
        }
    }
}
