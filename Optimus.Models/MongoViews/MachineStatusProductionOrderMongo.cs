﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class MachineStatusProductionOrderMongo : BaseMongoCommon
    {

        public int IdLinea { get; set; }
        public DateTime FechaHoraInicio { get; set; }

        public DateTime FechaHoraFin { get; set; }

        public string  Turno { get; set; }


        public List<MachineStatusProductionOrder> MachineStatusProductionOrder { get; set; }


        public MachineStatusProductionOrderMongo()
        {
        }
    }
}
