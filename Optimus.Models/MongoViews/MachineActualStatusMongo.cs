﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class MachineActualStatusMongo : BaseMongoCommon
    {

        public int IdLinea { get; set; }

        public List<MachineActualStatus> MachineActualStatus { get; set; }

        public MachineActualStatusMongo()
        {
        }
    }
}
