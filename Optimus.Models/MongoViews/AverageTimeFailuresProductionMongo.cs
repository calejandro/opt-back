﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class AverageTimeFailuresProductionMongo : BaseMongoCommon
    {

        public int IdLinea { get; set; }
        public DateTime FechaHoraInicio { get; set; }

        public DateTime FechaHoraFin { get; set; }

        public string  Turno { get; set; }

        public List<AverageTimeFailuresProduction> AverageTimeFailuresProduction { get; set; }

        public AverageTimeFailuresProductionMongo()
        {
        }
    }
}
