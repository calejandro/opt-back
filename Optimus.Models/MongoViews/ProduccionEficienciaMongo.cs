﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Models.Views
{
    public class ProduccionEficienciaMongo: BaseMongoCommon
    {

        public int IdLinea { get; set; }
        public DateTime FechaHoraInicio { get; set; }

        public DateTime FechaHoraFin { get; set; }

        public string  Turno { get; set; }


        public List<ProduccionEficiencia> ProduccionEficiencia { get; set; }


        public ProduccionEficienciaMongo()
        {
        }
    }
}
