﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;


namespace Optimus.Models.Views
{
    public class ConfigJobMongo : BaseMongoCommon
    {
        public string Tipo { get; set; }
        public DateTime Fecha { get; set; }
        public string Data { get; set; }
    }
}
