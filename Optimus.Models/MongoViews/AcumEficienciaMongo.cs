﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;


namespace Optimus.Models.Views
{
    public class AcumEficienciaMongo  : BaseMongoCommon
    {
        public int IdLinea { get; set; }
        public int IdPlanta { get; set; }
        public double? Eficiencia { get; set;}
        public DateTime Fecha { get; set; }
        public PeriodEfficiencyEnum PeriodoEficiencia { get; set; }
    }
}
