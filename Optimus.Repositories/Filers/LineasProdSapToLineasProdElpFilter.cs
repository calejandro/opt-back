﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class LineasProdSapToLineasProdElpFilter : CommonFilter
    {
        public int? IdLineaElp { get; set; }
        public int? IdLineaSap { get; set; }
    }
}
