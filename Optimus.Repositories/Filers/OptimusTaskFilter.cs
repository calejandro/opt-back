﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;

namespace Optimus.Repositories.Filers
{
    public class OptimusTaskFilter : BaseFilter
    {
        public Guid? Id { get; set; }

        public Guid? IdCreatorUser { get; set; }
        [CustomPropertyFilter]
        public Guid? IdAssignedUser { get; set; }
        public Guid? IdVerifierUser { get; set; }
        public bool? Archived { get; set; }
        public StateOptimusTaskEnum? IdState { get; set; }
        public Guid? IdKpi { get; set; }
        public Guid? IdSector { get; set; }
        public Guid? IdProcess { get; set; }
        public OptimusTaskTypeEnum? Type { get; set; }
        public OptimusTaskPriorityEnum? Priority { get; set; }
        public Guid? IdParentOptimusTask { get; set; }
        

        [CustomPropertyFilter]
        public string? Name { get; set; }
        [CustomPropertyFilter]
        public string? Description { get; set; }
        [CustomPropertyFilter]
        public Guid? CreOrAssiOrVerf { get; set; }
        [CustomPropertyFilter]
        public string? Identifier { get; set; }
        [CustomPropertyFilter]
        public string? DesOrNamOrIde{ get; set; }

        [CustomPropertyFilter]
        public bool? IsSubOptimusTask { get; set; }

        [CustomPropertyFilter]
        public StateOptimusTaskEnum? NeIdState { get; set; }
        



    }
}
