﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class LineaFilters: CommonFilter
    {
        public int? TipoLinea { get; set; }
        public bool? Activo { get; set; }
        public bool? Borrado { get; set; }
    }
}
