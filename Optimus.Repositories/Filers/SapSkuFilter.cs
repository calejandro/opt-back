﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class SapSkuFilter: BaseFilter
    {
        [CustomPropertyFilter]
        public string? Search { get; set; }
    }
}
