﻿using Optimus.Commons.Entities;
using System;

namespace Optimus.Repositories.Filers
{
    public class SysUserPlantsFilter : BaseFilter
    {
        public int? UserId { get; set; }

    }
}
