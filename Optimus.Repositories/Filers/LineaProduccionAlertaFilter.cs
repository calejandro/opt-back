﻿using System;
using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Repositories.Filers
{
    public class LineaProduccionAlertaFilter : CommonFilter
    {
        [CustomPropertyFilter]
        public DateTime? FechaInicio { get; set; }
        [CustomPropertyFilter]
        public DateTime? FechaFin { get; set; }
        public EstadosLPAEnum? Estado { get; set; }
        public ClasificacionAlertaEnum? Clasificacion { get; set; }
        public TipoAlertaParadaEnum? TipoAlerta { get; set; }
        public TipoMerma? TipoMerma { get; set; }

        public LineaProduccionAlertaFilter()
        {
        }
    }
}
