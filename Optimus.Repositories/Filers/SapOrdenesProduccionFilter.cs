﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class SapOrdenesProduccionFilter : CommonFilter
    {
        [CustomPropertyFilter]
        public string? Search { get; set; }
        public int? IdOrdenSap { get; set; }
        public string? EsTemporal { get; set; }
        public string? IdOrdenVinculada { get; set; }
        [CustomPropertyFilter]
        public List<DateTime> FhInicio { get; set; }
        [CustomPropertyFilter]
        public List<DateTime> FhFin { get; set; }
        [CustomPropertyFilter]
        public bool? NoVinculadasTemporales { get; set; }        
    }
}
