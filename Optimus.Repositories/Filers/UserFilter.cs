﻿using Optimus.Commons.Entities;
using System;

namespace Optimus.Repositories.Filers
{
    public class UserFilter : BaseFilter
    {
        [CustomPropertyFilter]
        public string? Search { get; set; }
        public string UserName { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Guid? IdUserIdentity { get; set; }
        public Guid? IdCognito { get; set; }


    }
}
