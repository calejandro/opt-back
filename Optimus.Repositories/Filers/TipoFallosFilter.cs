﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class TipoFallosFilter : CommonFilter
    {
        public TipoModoFalloEnum? Tipo { get; set; }
        public TipoAlertEnum? TipoAlerta { get; set; }
    }
}
