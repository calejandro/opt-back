﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class FileUrlFilter : CommonFilter
    {
        public string? Name { get; set; }
        public Guid? IdOptimusTask { get; set; }
        public Guid? IdTaskComment { get; set; }
}
}
