﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class SysUserReportConfigurationFilter: BaseFilter
    {
       public ReportTypeEnum? ReportType { get; set; }

    }
}
