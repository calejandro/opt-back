﻿using System;
using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Repositories.Filers
{
    public class LineasProduccionObjetivosConfigFilter : BaseFilter
    {
        public TipoObjetivoEnum? Tipo { get; set; }

        public int? IdLinea { get; set; }

        public LineasProduccionObjetivosConfigFilter()
        {
        }
    }
}
