﻿using System;
using Optimus.Commons.Entities;

namespace Optimus.Repositories.Filers
{
    public class LineasProduccionUsuarioFilter : CommonFilter
    {
        public int? UserId { get; set; }
        public int? IdOrdenSap { get; set; }
        [CustomPropertyFilter]
        public bool? IsTake { get; set; }
        [CustomPropertyFilter]
        public DateTime? FechaInicio { get; set; }
        [CustomPropertyFilter]
        public DateTime? FechaFin { get; set; }
        public bool? Supervisada { get; set; }
        
    }
}
