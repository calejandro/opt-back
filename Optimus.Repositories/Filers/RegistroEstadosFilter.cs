﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class RegistroEstadosFilter: CommonFilter
    {
        [CustomPropertyFilter]
        public DateTime? DateFrom { get; set; }
        [CustomPropertyFilter]
        public DateTime? DateTo { get; set; }
    }
}
