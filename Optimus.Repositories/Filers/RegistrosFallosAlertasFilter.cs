﻿using System;
using Optimus.Commons.Entities;

namespace Optimus.Repositories.Filers
{
    public class RegistrosFallosAlertasFilter : BaseFilter
    {
        public int? IdAlerta { get; set; }
        public int? IdModoFallo { get; set; }
        public int? IdFallo { get; set; }
        public int? IdCausa { get; set; }

        public RegistrosFallosAlertasFilter()
        {
        }
    }
}
