﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class TaskCommentFilter : BaseFilter
    {
        public Guid? IdOptimusTask { get; set; }
        public Guid? IdCreatorUser { get; set; }
    }
}
