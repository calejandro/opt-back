﻿using System;
using Optimus.Commons.Entities;

namespace Optimus.Repositories.Filers
{
    public class CommonFilter : BaseFilter
    {
        public int? IdLinea { get; set; }
        public int? IdEquipo { get; set; }

        public CommonFilter()
        {
        }
    }
}
