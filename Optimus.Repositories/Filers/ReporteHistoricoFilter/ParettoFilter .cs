﻿

using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Repositories.Filers
{
    public class ParettoFilter : ReporteHistoricoFilter
    {
        [CustomPropertyFilter]
        public  AppmobStatusEquiposEnum StatusEquipos { get; set; }
    }
}
