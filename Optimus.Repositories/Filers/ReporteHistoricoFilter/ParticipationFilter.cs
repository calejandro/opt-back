﻿

using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Repositories.Filers
{
    public class ParticipationFilter:ReporteHistoricoFilter
    {
        [CustomPropertyFilter]
        public TypeParticipationEnum TypeParticipationEnum { get; set; }
    }
}
