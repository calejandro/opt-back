﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;

namespace Optimus.Repositories.Filers
{
    public class EfficiencyFilter: ReporteHistoricoFilter
    {
        [CustomPropertyFilter]
        public PeriodEfficiencyEnum PeriodEfficiency { get; set; }
        [CustomPropertyFilter]
        public TypeEfficiencyEnum TypeEfficiency { get; set; }
    }
}
