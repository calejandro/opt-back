﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class StateEvolutionFilter : ReporteHistoricoFilter
    {
        [CustomPropertyFilter]
        public AppmobStatusEquiposEnum StatusEquipo { get; set; }
        [CustomPropertyFilter]
        public int? IdMachine { get; set; }

    }
}
