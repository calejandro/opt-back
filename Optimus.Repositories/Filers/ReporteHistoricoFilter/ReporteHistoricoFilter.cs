﻿
using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;

namespace Optimus.Repositories.Filers
{
    public class ReporteHistoricoFilter:  BaseFilter
    {
        [CustomPropertyFilter]
        public DateTime DateFrom { get; set; }
        [CustomPropertyFilter]
        public DateTime DateTo { get; set; }
        [CustomPropertyFilter]
        public List<int> IdLines { get; set; }
        [CustomPropertyFilter]
        public int IdLine { get; set; }
        [CustomPropertyFilter]
        public List<int> IdSku { get; set; }
        [CustomPropertyFilter]
        public List<string> Brands { get; set; }
        [CustomPropertyFilter]
        public List<string> Flavors { get; set; }
        [CustomPropertyFilter]
        public List<string> Formats { get; set; }
        [CustomPropertyFilter]
        public List<string> IdKpi { get; set; }
        [CustomPropertyFilter]
        public int? IdSupervisor { get; set; }
    }
}
