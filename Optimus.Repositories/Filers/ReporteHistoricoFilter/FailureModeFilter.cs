﻿using Optimus.Commons.Entities;
using Optimus.Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class FailureModeFilter: ReporteHistoricoFilter
    {
        [CustomPropertyFilter]
        public int? IdMachine { get; set; }
        [CustomPropertyFilter]
        public int? IdFalureMode { get; set; }
    }
}
