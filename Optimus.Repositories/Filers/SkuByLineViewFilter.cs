﻿using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimus.Repositories.Filers
{
    public class SkuByLineViewFilter : BaseFilter
    {
        [CustomPropertyFilter]
        public int[]? IdLines { get; set; }
        [CustomPropertyFilter]
        public List<string> Brands { get; set; }
        [CustomPropertyFilter]

        public List<string> Flavors { get; set; }
    }
}
