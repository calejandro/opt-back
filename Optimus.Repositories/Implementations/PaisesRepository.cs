﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class PaisesRepository : GenericRepository<OptimusContext, Paises, int, BaseFilter>, IPaisesRepository
    {
        public PaisesRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
