﻿using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class RegistrosFallosAlertasRepository : GenericRepository<OptimusContext, RegistrosFallosAlertas, int, RegistrosFallosAlertasFilter>, IRegistrosFallosAlertasRepository
    {
        public RegistrosFallosAlertasRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
