﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class EquiposRepository : GenericRepository<OptimusContext, Equipos, int, CommonFilter>, IEquiposRepository
    {
        public EquiposRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
