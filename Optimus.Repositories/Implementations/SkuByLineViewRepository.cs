﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class SkuByLineViewRepository : GenericRepository<OptimusContext, SkuByLineView, int, SkuByLineViewFilter>, ISkuByLineViewRepository
    {
        public SkuByLineViewRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public override async Task<List<SkuByLineView>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, SkuByLineViewFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            query = this.CustomFilter(filter, query);

            if (noTracking)
                query = query.AsNoTracking();

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }
        protected IQueryable<SkuByLineView> CustomFilter(SkuByLineViewFilter filter, IQueryable<SkuByLineView> query)
        {
            if (filter.IdLines?.Length > 0)
            {
                query = query.Where(x => filter.IdLines.Contains(x.IdLinea));
            }
            if (filter.Brands?.Count() > 0)
            {
                query = query.Where(x => filter.Brands.Contains(x.Marca));
            }
            if (filter.Flavors?.Count() > 0)
            {
                query = query.Where(x => filter.Flavors.Contains(x.Sabor));
            }
            return query;
        }
    }

}
