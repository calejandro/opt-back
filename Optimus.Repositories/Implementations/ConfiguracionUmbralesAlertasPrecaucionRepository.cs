﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class ConfiguracionUmbralesAlertasPrecaucionRepository : GenericRepository<OptimusContext, ConfiguracionUmbralesAlertasPrecaucion, int, CommonFilter>, IConfiguracionUmbralesAlertasPrecaucionRepository
    {
        public ConfiguracionUmbralesAlertasPrecaucionRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
