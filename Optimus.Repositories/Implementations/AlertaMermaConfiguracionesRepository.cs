﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class AlertaMermaConfiguracionesRepository : GenericRepository<OptimusContext, AlertaMermaConfiguraciones, int, AlertaMermaFilter>, IAlertaMermaConfiguracionesRepository
    {
        public AlertaMermaConfiguracionesRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public async Task<bool> ExistsEntityAsync(AlertaMermaConfiguraciones entity, bool isUpdate)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdEquipo == entity.IdEquipo &&
            x.IdLinea == entity.IdLinea &&
            x.IdSku == entity.IdSku);

            if (isUpdate)
            {
                query = query.Where(w => w.Id != entity.Id);
            }

            query = query.AsNoTracking();
            var entityExist = await query.FirstOrDefaultAsync();
            return entityExist != null;
        }
    }

}
