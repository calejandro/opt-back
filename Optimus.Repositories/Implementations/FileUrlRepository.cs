﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class FileUrlRepository : GenericRepository<Optimus2Context, FileUrl, Guid, FileUrlFilter>, IFileUrlRepository
    {        
        public FileUrlRepository(Optimus2Context apiContext, IdentityContext identityContext) : base(apiContext)
        {
            
        }      
        
    }

}
