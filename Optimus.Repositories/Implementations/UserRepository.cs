﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class UserRepository : GenericRepository<IdentityContext, UserIdentity, Guid, UserFilter>, IUserRepository
    {
        public UserRepository(IdentityContext apiContext) : base(apiContext)
        {
        }

        public async Task<UserIdentity> GetByCognitoId(Guid cognitoId)
        {
            return await this.dbSet.Where(w => w.IdCognito == cognitoId).SingleOrDefaultAsync();
        }

        public async override Task<List<UserIdentity>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, UserFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {

            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);

            if (filter.Search != null)
            {
                query = query.Where(x => x.UserName.Contains(filter.Search) || x.Name.Contains(filter.Search)
                                || x.LastName.Contains(filter.Search));
            }

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public override Task<int> CountAsync(UserFilter filter = null, string[] includes = null)
        {
            var query = this.Filter(filter, this.dbSet.AsQueryable());
            query = this.GetIncludeQuery(query, includes);
            if (filter.Search != null)
            {
                query = query.Where(x => x.UserName.Contains(filter.Search) || x.Name.Contains(filter.Search)
                                || x.LastName.Contains(filter.Search));
            }
                
            return query.CountAsync();
        }
    }

}
