﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class SapSkuRepository : GenericRepository<OptimusContext, SapSku, int, SapSkuFilter>, ISapSkuRepository
    {
        public SapSkuRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public async override Task<List<SapSku>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, SapSkuFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);
            if (filter?.Search != null)
                query = query.Where(x => x.DescSku.Contains(filter.Search));
            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }
    }

}
