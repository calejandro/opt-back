﻿using System;
using System.Threading.Tasks;
using Optimus.Models.Contexts;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using Optimus.Models.Enum;
using Edasa.Common.Repositories.Extensions;
using System.Linq;

namespace Optimus.Repositories.Implementations
{
    public class StoreProceduresRepository : IStoreProceduresRepository
    {
        private readonly OptimusContext optimusContext;
        public StoreProceduresRepository(OptimusContext optimusContext)
        {
            this.optimusContext = optimusContext;
        }

        //ALERTA PARADA_PROPIA
        public Task<List<APPMobStatusActual>> GetAPPMobStatusActual(int idLinea)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),

            };
            return this.optimusContext.APPMobStatusActual.FromSqlRaw(
                "EXEC SP_APPMOB_STATUS_ACTUAL @ID_Linea", parameter).ToListAsync();
        }
        //ALERTA_MERMA
        public Task<List<APPMobKpiRoturaEnvases>> GetAPPMobKpiRoturaEnvases(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)

            };
            return this.optimusContext.APPMobKpiRoturaEnvases.FromSqlRaw(
                "EXEC SP_APPMOB_KPI_ROTURA_ENVASES_POR_MINUTOS @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).ToListAsync();
        }

        //SHIFT
        public Shift GetDay(DateTime fecha)
        {
            // horas turnos 

            TimeSpan te_i = new TimeSpan(22, 00, 0);
            TimeSpan te_f = new TimeSpan(21, 59, 0);

            Shift turnoTday = new Shift();
            turnoTday.Turno = "DiaEntero";
            turnoTday.Fecha = fecha;
            turnoTday.Inicio = fecha.AddDays(-1).Date + te_i;
            turnoTday.Fin = fecha.Date + te_f;
            turnoTday.Paso = true;


            return turnoTday;
        }
        public Shift GetCurrentDay(DateTime fecha)
        {
            // horas turnos 

            TimeSpan te_i = new TimeSpan(22, 00, 0);
            TimeSpan te_f = new TimeSpan(21, 59, 0);

            Shift turnoTday = new Shift();
            turnoTday.Turno = "DiaEntero";
            turnoTday.Fecha = fecha;
            if (fecha.Hour >= 22)
            {
                turnoTday.Inicio = fecha.Date + te_i;
                turnoTday.Fin = fecha.AddDays(1).Date + te_f;
            }
            else
            {
                turnoTday.Inicio = fecha.AddDays(-1).Date + te_i;
                turnoTday.Fin = fecha.Date + te_f;
            }


            turnoTday.Paso = true;


            return turnoTday;
        }
        public List<Shift> GetShifts(DateTime fecha)
        {
            // horas turnos 
            TimeSpan tn_i = new TimeSpan(22, 00, 0);
            TimeSpan tn_f = new TimeSpan(5, 59, 0);
            TimeSpan tm_i = new TimeSpan(6, 00, 0);
            TimeSpan tm_f = new TimeSpan(13, 59, 0);
            TimeSpan tt_i = new TimeSpan(14, 00, 0);
            TimeSpan tt_f = new TimeSpan(21, 59, 0);

            List<Shift> shifts = new List<Shift>();
            var plusDay = 0;
            var subtractDay = 0;
            var today = DateTime.Now;

            if (fecha.Hour >= 22)
                plusDay = 1;
            else
                subtractDay = -1;
            Shift turnoN = new Shift();
            turnoN.Turno = "Noche";
            turnoN.Fecha = fecha;
            turnoN.Inicio = fecha.AddDays(subtractDay).Date + tn_i;
            turnoN.Fin = fecha.AddDays(plusDay).Date + tn_f;
            turnoN.Paso = turnoN.Inicio <= today;
            shifts.Add(turnoN);


            Shift turnoM = new Shift();
            turnoM.Turno = "Mañana";
            turnoM.Fecha = fecha;
            turnoM.Inicio = fecha.AddDays(plusDay).Date + tm_i;
            turnoM.Fin = fecha.AddDays(plusDay).Date + tm_f;
            turnoM.Paso = turnoM.Inicio <= today;

            shifts.Add(turnoM);

            Shift turnoT = new Shift();
            turnoT.Turno = "Tarde";
            turnoT.Fecha = fecha;
            turnoT.Inicio = fecha.AddDays(plusDay).Date + tt_i;
            turnoT.Fin = fecha.AddDays(plusDay).Date + tt_f;
            turnoT.Paso = turnoT.Inicio <= today;

            shifts.Add(turnoT);


            return shifts;
        }

        public Shift GetCurrentShift(DateTime dateTime)
        {
            List<Shift> shifts = this.GetShifts(dateTime);
            return shifts.Where(w => w.Inicio <= dateTime && w.Fin >= dateTime).Single();
        }
        public string GetNameShift(DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            if (fechaHoraInicio.Hour - fechaHoraFin.Hour == 1)
            {
                return "DiaEntero";
            }
            var turno = this.GetCurrentShift(fechaHoraInicio.AddHours(1));
            return turno.Turno;
        }
        // *EFICIENCIA*
        public Task<List<ProduccionEficiencia>> GetEfficiencyProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)
            };
            return this.optimusContext.ProduccionEficiencia.FromSqlRaw(
                "EXEC SP_APPMOB_PRODUCCION_EFICIENCIA_HORARIA @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).ToListAsync();
        }
        // *EFICIENCIA REAL*
        public async Task<ProduccionEficienciaReal> GetEfficiencyTrueProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)
            };
            var result = await this.optimusContext.ProduccionEficienciaReal.FromSqlRaw(
                "EXEC SP_APPMOB_PRODUCCION_EFICIENCIA_HORARIA_CON_PRODUCCION_VERDADERA @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).AsNoTracking()
                .ToListAsync();

            return result.FirstOrDefault();

        }
        public Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShift(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)
            };
            return this.optimusContext.ProduccionEficienciaByShift.FromSqlRaw(
                "EXEC SP_APPMOB_PRODUCCION_EFICIENCIA_HORARIA_POR_TURNO @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).ToListAsync();
        }

        // *PERFORMANCE DE EQUIPOS*
        public Task<List<MachineActualStatus>> GetMachineActualStatus(int idLinea)
        {
            return this.optimusContext.MachineActualStatuses.FromSqlRaw("EXEC SP_APPMOB_STATUS_ACTUAL @ID_Linea",
                new SqlParameter("@ID_Linea", idLinea)).ToListAsync();
        }
        public Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrders(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
           {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)
           };
            return this.optimusContext.MachineStatusProductionOrder.FromSqlRaw(
                "EXEC SP_APPMOB_STATUS_EQUIPOS_POR_ORDEN_PRODUCCION @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).ToListAsync();
        }
        public Task<List<TimeStops>> GetTimeStops(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, int idEstado)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin),
                new SqlParameter("@ID_Estado", idEstado)
            };
            return this.optimusContext.TimeStops.FromSqlRaw(
                "EXEC SP_APPMOB_DETENCIONES_HORARIAS @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin, @ID_Estado", parameter).ToListAsync();
        }

        public Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropiaHora(int idLinea, int idEquipo)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@ID_Equipo", idEquipo),                
            };
            return this.optimusContext.EvolucionParadaPropiaHoras.FromSqlRaw(
                "EXEC SP_APPMOB_EVOLUCION_HORA_MOVIL @ID_Linea, @ID_Equipo", parameter).ToListAsync();
        }
        public Task<List<AverageTimeFailuresProduction>> GetAverageTimeFailuresProduction(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin)
            };
            return this.optimusContext.AverageTimeFailuresProduction.FromSqlRaw(
                "EXEC SP_APPMOB_TMEF_TMPR @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin", parameter).ToListAsync();
        }
       
        // *MERMAS*
        public Task<List<KpiDepletion>> GetKpiDepletionForBottles(int idLinea, DateTime fechaHoraInicio, DateTime fechaHoraFin, GroupingTypeEnum tipoAgrupacion)
        {
            var parameter = new object[]
            {
                new SqlParameter("@ID_Linea", idLinea),
                new SqlParameter("@Fecha_Hora_Inicio", fechaHoraInicio),
                new SqlParameter("@Fecha_Hora_Fin", fechaHoraFin),
                new SqlParameter("@Tipo_Agrupacion", tipoAgrupacion.GetDescription())
            };
            return this.optimusContext.KpiDepletion.FromSqlRaw(
                "EXEC SP_APPMOB_KPI_ROTURA_ENVASES @ID_Linea, @Fecha_Hora_Inicio, @Fecha_Hora_Fin, @Tipo_Agrupacion", parameter).ToListAsync();
        }

    }
}
