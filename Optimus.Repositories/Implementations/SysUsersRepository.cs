﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class SysUsersRepository : GenericRepository<OptimusContext, SysUsers, int, UserFilter>, ISysUsersRepository
    {
        public SysUsersRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public  async override Task<List<SysUsers>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, UserFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {

            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);

            if(filter.Search != null)
            query = query.Where(x => x.FullName.Contains(filter.Search) || x.LastName.Contains(filter.Search));

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public async override Task<SysUsers> GetAsync(int id, string[] includes = null)
        {
            SysUsers entity = null;
            if (includes == null || includes.Length == 0)
            {
                entity = await dbSet.FindAsync(id);
            }
            else
            {
                var parameter = Expression.Parameter(typeof(SysUsers), "e");
                var property = Expression.PropertyOrField(parameter, "UserId");
                var eq = Expression.Equal(property, Expression.Constant(id));
                var query = this.dbSet.Where(Expression.Lambda<Func<SysUsers, bool>>(eq, parameter));

                query = this.GetIncludeQuery(query, includes);
                entity = await query.SingleOrDefaultAsync();
            }
            return entity;
        }

        public async Task<SysUsers> GetAsync(Guid id, string[] includes = null)
        {
            SysUsers entity = null;
            if (includes == null || includes.Length == 0)
            {
                entity = dbSet.Where(w => w.IdUserIdentity == id).SingleOrDefault();
            }
            else
            {
                var query = this.dbSet.Where(w => w.IdUserIdentity == id);
                query = this.GetIncludeQuery(query, includes);
                entity = await query.SingleOrDefaultAsync();
            }
            return entity;
        }
    }

}
