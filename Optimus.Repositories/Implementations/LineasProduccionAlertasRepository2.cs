﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class LineasProduccionAlertasRepository2 : GenericRepository<OptimusContext, LineasProduccionAlertas2, int, LineaProduccionAlertaFilter>, ILineasProduccionAlertasRepository2
    {
        public LineasProduccionAlertasRepository2(OptimusContext apiContext) : base(apiContext)
        {
        }

        public override async Task<List<LineasProduccionAlertas2>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, LineaProduccionAlertaFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = this.dbSet.AsQueryable();

            query = this.Filter(filter, query);

            if (filter.FechaInicio != null)
            {
                query = query.Where(w => w.FechaCreacion >= filter.FechaInicio);
            }

            if (filter.FechaFin != null)
            {
                query = query.Where(w => w.FechaCreacion <= filter.FechaFin);
            }

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public override Task<int> CountAsync(LineaProduccionAlertaFilter filter = null, string[] includes = null)
        {
            var query = this.dbSet.AsQueryable();
            query = this.GetIncludeQuery(query, includes);
            query = this.Filter(filter, query);

            if (filter.FechaInicio != null)
            {
                query = query.Where(w => w.FechaCreacion >= filter.FechaInicio);
            }

            if (filter.FechaFin != null)
            {
                query = query.Where(w => w.FechaCreacion <= filter.FechaFin);
            }

            return query.CountAsync();
        }
    }

}
