﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class AlertByOrdenesSapViewRepository : GenericRepository<OptimusContext, AlertByOrdenesSapView, int, FailureModeFilter>, IAlertByOrdenesSapViewRepository
    {
        public AlertByOrdenesSapViewRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public override async Task<List<AlertByOrdenesSapView>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, FailureModeFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            query = this.CustomFilter(filter, query);


            if (noTracking)
                query = query.AsNoTracking();

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }
        protected IQueryable<AlertByOrdenesSapView> CustomFilter(FailureModeFilter filter, IQueryable<AlertByOrdenesSapView> query)
        {
            if (filter.DateFrom != null)
            {
                query = query.Where(x => x.FhFinProduccion >= filter.DateFrom  );
            }
            if (filter.DateTo != null)
            {
                query = query.Where(x => x.FhFinProduccion <= filter.DateTo);
            }

            if (filter.IdLines?.Count() > 0)
            {
                query = query.Where(x => filter.IdLines.Contains(x.IdLinea));
            }
            if (filter.Brands?.Count() > 0)
            {
                query = query.Where(x => filter.Brands.Contains(x.Marca));
            }
            if (filter.Flavors?.Count() > 0)
            {
                query = query.Where(x => filter.Flavors.Contains(x.SaborConsolid));
            }
            if (filter.Formats?.Count() > 0)
            {
                query = query.Where(x => filter.Formats.Contains(x.FormatoConsolid));
            }
            if (filter.IdSku?.Count() > 0)
            {
                query = query.Where(x => filter.IdSku.Contains(x.IdSku));
            }
            if (filter.IdSupervisor != null)
            {
                query = query.Where(x => filter.IdSupervisor == x.IdUser);
            }
            if (filter.IdMachine != null)
            {
                query = query.Where(x => filter.IdMachine == x.IdEquipo);
            }
            if (filter.IdFalureMode != null)
            {
                query = query.Where(x => filter.IdFalureMode == x.IdModoFallo);
            }
            return query;
        }
    }

}
