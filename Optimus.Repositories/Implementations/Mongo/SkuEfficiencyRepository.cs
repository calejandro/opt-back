﻿using MongoDB.Driver;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class SkuEfficiencyRepository : GenericMongoRepository<SkuEfficiencyMongo, MongoContext>, ISkuEfficiencyRepository
    { 
        public SkuEfficiencyRepository(MongoContext apiContext) : base(apiContext, "SkuEfficiencyMongo")
        {
        }
        public async Task<List<SkuEfficiencyMongo>> ListAsyncFilter(ReporteHistoricoFilter filter)
        {
            if (filter.DateFrom == filter.DateTo)
                filter.DateTo.AddDays(1);
            FilterDefinition<SkuEfficiencyMongo> filterMong2 = null;
            if (filter != null)
            {
                var builder = Builders<SkuEfficiencyMongo>.Filter;
                filterMong2 = builder.Gte(widget => widget.FechaFin, filter.DateFrom)
                  & builder.Lte(widget => widget.FechaFin, filter.DateTo)
                  & builder.In(widget => widget.IdLinea, filter.IdLines)
                  & (filter.IdSku != null ? builder.In(widget => widget.IdSKU, filter.IdSku) : builder.Empty)
                  & (filter.Brands != null ? builder.In(widget => widget.Marca, filter.Brands) : builder.Empty)
                  & (filter.Flavors != null ? builder.In(widget => widget.SaborConsolid, filter.Flavors) : builder.Empty)
                  & (filter.Formats != null ? builder.In(widget => widget.FormatoConsolid, filter.Formats) : builder.Empty)
                  & (filter.IdSupervisor != null ? builder.Eq(widget => widget.IdSupervisor, filter.IdSupervisor) : builder.Empty)
                  & builder.Eq(widget => widget.Turno, "DiaEntero");
            }

            return await base.ListAsync(filterMong2);
        }
    }

}
