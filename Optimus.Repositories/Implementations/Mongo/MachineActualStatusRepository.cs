﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class MachineActualStatusRepository : GenericMongoRepository<MachineActualStatusMongo, MongoContext>, IMachineActualStatusRepository
    { 
        public MachineActualStatusRepository(MongoContext apiContext) : base(apiContext, "MachineActualStatusMongo")
        {
        }
    }

}
