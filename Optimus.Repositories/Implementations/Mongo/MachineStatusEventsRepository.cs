﻿using MongoDB.Driver;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class MachineStatusEventsRepository : GenericMongoRepository<MachineStatusEventsMongo, MongoContext>, IMachineStatusEventsRepository
    { 
        public MachineStatusEventsRepository(MongoContext apiContext) : base(apiContext, "MachineStatusEventsMongo")
        {
        }
        public Task<List<MachineStatusEventsMongo>> ListAsyncFilter(StateEvolutionFilter filter = null)
        {
            if (filter.DateFrom == filter.DateTo)
                filter.DateTo = filter.DateTo.AddDays(1);
            FilterDefinition<MachineStatusEventsMongo> filterMong = null;
            if (filter != null)
            {
                var builder = Builders<MachineStatusEventsMongo>.Filter;
                filterMong = builder.Gte(widget => widget.FechaFinEstado, filter.DateFrom.Date)
                  & builder.Lte(widget => widget.FechaFinEstado, filter.DateTo.Date)
                  & builder.Eq(widget => widget.IdLinea, filter.IdLine)
                  & builder.Eq(widget => widget.IdEquipo, filter.IdMachine)
                  & builder.Eq(widget => widget.IdEstado, filter.StatusEquipo.GetHashCode())
                  & (filter.IdSku != null ? builder.In(widget => widget.IdSKU, filter.IdSku) : builder.Empty)
                  & (filter.Brands != null ? builder.In(widget => widget.Marca, filter.Brands) : builder.Empty)
                  & (filter.Flavors != null ? builder.In(widget => widget.SaborConsolid, filter.Flavors) : builder.Empty)
                  & (filter.Formats != null ? builder.In(widget => widget.FormatoConsolid, filter.Formats) : builder.Empty)
                  & (filter.IdSupervisor != null ? builder.Eq(widget => widget.IdSupervisor, filter.IdSupervisor) : builder.Empty);
            }
            return base.ListAsync(filterMong);
        }
    }

}
