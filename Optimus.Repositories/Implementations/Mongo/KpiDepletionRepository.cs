﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class KpiDepletionRepository : GenericMongoRepository<KpiDepletionMongo, MongoContext>, IKpiDepletionRepository
    { 
        public KpiDepletionRepository(MongoContext apiContext) : base(apiContext, "KpiDepletionMongo")
        {
        }
    }

}
