﻿using MongoDB.Driver;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class MachineStatusHistoricRepository : GenericMongoRepository<MachineStatusHistoricMongo, MongoContext>, IMachineStatusHistoricRepository
    { 
        public MachineStatusHistoricRepository(MongoContext apiContext) : base(apiContext, "MachineStatusHistoricMongo")
        {
        }
        public  Task<List<MachineStatusHistoricMongo>> ListAsyncFilter(ReporteHistoricoFilter filter = null)
        {
            if (filter.DateFrom == filter.DateTo)
                filter.DateTo = filter.DateTo.AddDays(1);
            FilterDefinition<MachineStatusHistoricMongo> filterMong = null;
            if (filter != null)
            {
                var builder = Builders<MachineStatusHistoricMongo>.Filter;
                filterMong = builder.Gte(widget => widget.FechaFin, filter.DateFrom.Date)
                  & builder.Lte(widget => widget.FechaFin, filter.DateTo.Date)
                  //& builder.In(widget => widget.IdLinea, filter.IdLines)
                  & builder.Eq(widget => widget.IdLinea, filter.IdLine)
                  & (filter.IdSku != null ? builder.In(widget => widget.IdSKU, filter.IdSku) : builder.Empty)
                  & (filter.Brands != null ? builder.In(widget => widget.Marca, filter.Brands) : builder.Empty)
                  & (filter.Flavors != null ? builder.In(widget => widget.SaborConsolid, filter.Flavors) : builder.Empty)
                  & (filter.Formats != null ? builder.In(widget => widget.FormatoConsolid, filter.Formats) : builder.Empty)
                  & (filter.IdSupervisor != null ? builder.Eq(widget => widget.IdSupervisor, filter.IdSupervisor) : builder.Empty)
                  & builder.Eq(widget => widget.Turno, "DiaEntero");
            }
            return base.ListAsync(filterMong);
        }
    }

}
