﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class ConfigJobRepository : GenericMongoRepository<ConfigJobMongo, MongoContext>, IConfigJobRepository
    { 
        public ConfigJobRepository(MongoContext apiContext) : base(apiContext, "ConfigJobMongo")
        {
        }
    }

}
