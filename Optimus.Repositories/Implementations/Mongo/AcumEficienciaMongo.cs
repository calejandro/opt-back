﻿using MongoDB.Driver;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class AcumEficienciaRepository : GenericMongoRepository<AcumEficienciaMongo, MongoContext>, IAcumEficienciaRepository
    {
        private readonly IStoreProceduresRepository storeProceduresRepository;
        public AcumEficienciaRepository(MongoContext apiContext, IStoreProceduresRepository storeProceduresRepository) : base(apiContext, "AcumEficienciaMongo")
        {
            this.storeProceduresRepository = storeProceduresRepository;
        }

        public async Task CreateEficienciaDia(LineasProduccion linea, DateTime date, Shift turno)
        {
            var eficiencia = await storeProceduresRepository.GetEfficiencyProductionByShift(linea.IdLinea, turno.Inicio, turno.Fin);

            //EFICIENCIA POR DIA
            var builder2 = Builders<AcumEficienciaMongo>.Filter;
            var filter = builder2.Eq(widget => widget.Fecha, date)
                & builder2.Eq(widget => widget.IdLinea, linea.IdLinea)
                & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Daily);

            var eficienciaDia = await base.GetAsync(filter);
            if (eficienciaDia == null)
            {
                eficienciaDia = new AcumEficienciaMongo();
                eficienciaDia.IdPlanta = linea.IdPlanta;
                eficienciaDia.IdLinea = linea.IdLinea;
                eficienciaDia.PeriodoEficiencia = PeriodEfficiencyEnum.Daily;
                eficienciaDia.Fecha = date;
                eficienciaDia.Eficiencia = eficiencia?.FirstOrDefault()?.Eficiencia;
                eficienciaDia.Created = DateTime.Now;
                await base.InsertAsync(eficienciaDia);
            }
            else
            {
                eficienciaDia.Updated = DateTime.Now;
                eficienciaDia.Eficiencia = eficiencia?.FirstOrDefault()?.Eficiencia;
                await base.UpdateAsync(eficienciaDia);
            }
        }
        public async Task CreateEficienciaMes(LineasProduccion linea, DateTime date)
        {
            try
            {
                var builder2 = Builders<AcumEficienciaMongo>.Filter;
                var filter = builder2.Eq(widget => widget.Fecha, date)
                   & builder2.Eq(widget => widget.IdLinea, linea.IdLinea)
                   & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Monthly);
                var eficienciaMes = await base.GetAsync(filter);

                builder2 = Builders<AcumEficienciaMongo>.Filter;
                filter = builder2.Gte(widget => widget.Fecha, date)
                   & builder2.Lt(widget => widget.Fecha, date.AddMonths(1))
                   & builder2.Eq(widget => widget.IdLinea, linea.IdLinea)
                   & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Daily);

                var eficienciaDias = await base.ListAsync(filter);

                if (eficienciaMes == null)
                {
                    eficienciaMes = new AcumEficienciaMongo();
                    eficienciaMes.IdPlanta = linea.IdPlanta;
                    eficienciaMes.IdLinea = linea.IdLinea;
                    eficienciaMes.PeriodoEficiencia = PeriodEfficiencyEnum.Monthly;
                    eficienciaMes.Fecha = date;
                    eficienciaMes.Created = DateTime.Now;
                    eficienciaMes.Eficiencia = eficienciaDias?.Average(x => x.Eficiencia);
                    await base.InsertAsync(eficienciaMes);
                }
                else
                {
                    eficienciaMes.Updated = DateTime.Now;
                    eficienciaMes.Eficiencia = eficienciaDias?.Average(x => x.Eficiencia);
                    await base.UpdateAsync(eficienciaMes);
                }
            }
            catch (Exception e)
            {

                throw;
            }
          
        }
        public async Task CreateEficienciaAño(LineasProduccion linea, DateTime date)
        {
           var builder2 = Builders<AcumEficienciaMongo>.Filter;
            var filter = builder2.Eq(widget => widget.Fecha, date)
               & builder2.Eq(widget => widget.IdLinea, linea.IdLinea)
               & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Annual);
            var eficienciaAño = await base.GetAsync(filter);

            builder2 = Builders<AcumEficienciaMongo>.Filter;
            filter = builder2.Gte(widget => widget.Fecha, date)
               & builder2.Lt(widget => widget.Fecha, date.AddYears(1))
               & builder2.Eq(widget => widget.IdLinea, linea.IdLinea)
               & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Monthly);

            var eficienciaMeses = await base.ListAsync(filter);

            if (eficienciaAño == null)
            {
                eficienciaAño = new AcumEficienciaMongo();

                eficienciaAño.IdPlanta = linea.IdPlanta;
                eficienciaAño.IdLinea = linea.IdLinea;
                eficienciaAño.PeriodoEficiencia = PeriodEfficiencyEnum.Annual;
                eficienciaAño.Fecha = date;
                eficienciaAño.Created = DateTime.Now;
                eficienciaAño.Eficiencia = eficienciaMeses?.Average(x => x.Eficiencia);
                await base.InsertAsync(eficienciaAño);
            }
            else
            {
                eficienciaAño.Updated = DateTime.Now;
                eficienciaAño.Eficiencia = eficienciaMeses?.Average(x => x.Eficiencia);
                await base.UpdateAsync(eficienciaAño);
            }
        }

        public async Task DeleteEficienciaDia(int idLinea, DateTime date)
        {
            var turno = storeProceduresRepository.GetDay(date);
            var builder2 = Builders<AcumEficienciaMongo>.Filter;
            var filter = builder2.Eq(widget => widget.Fecha, turno.Fin.Date)
                & builder2.Eq(widget => widget.IdLinea, idLinea)
                & builder2.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Daily);
            var eficienciaDia = await base.GetAsync(filter);

            if (eficienciaDia != null)
                await base.DeleteAsync(eficienciaDia._Id);
        }
    }

        }
