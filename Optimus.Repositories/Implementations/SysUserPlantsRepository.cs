﻿using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class SysUserPlantsRepository : GenericRepository<OptimusContext, SysUsersPlants, int, SysUserPlantsFilter>, ISysUserPlantsRepository
    {
        public SysUserPlantsRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
