﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class ConfiguracionUmbralesTmefTmprRepository : GenericRepository<OptimusContext, ConfiguracionUmbralesTmefTmpr, int, CommonFilter>, IConfiguracionUmbralesTmefTmprRepository
    {
        public ConfiguracionUmbralesTmefTmprRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public async Task<bool> ExistsEntityAsync(ConfiguracionUmbralesTmefTmpr entity, bool isUpdate)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdEquipo == entity.IdEquipo &&
            x.IdLinea == entity.IdLinea &&
            x.Tipo == entity.Tipo);

            if (isUpdate)
            {
                query = query.Where(w => w.Id != entity.Id);
            }

            query = query.AsNoTracking();
            var entityExist = await query.FirstOrDefaultAsync();
            return entityExist != null;
        }
    }

}
