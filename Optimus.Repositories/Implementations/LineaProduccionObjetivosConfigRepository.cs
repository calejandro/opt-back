﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class LineaProduccionObjetivosConfigRepository : GenericRepository<OptimusContext, LineasProduccionObjetivosConfig, Guid, LineasProduccionObjetivosConfigFilter>, ILineaProduccionObjetivosConfigRepository
    {
        public LineaProduccionObjetivosConfigRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public async Task<bool> ExistsEntityAsync(LineasProduccionObjetivosConfig entity, bool isUpdate)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdLinea == entity.IdLinea &&
             x.Tipo == entity.Tipo);

            if (isUpdate)
            {
                query = query.Where(w => w.Id != entity.Id);
            }

            query = query.AsNoTracking();
            var entityExist = await query.FirstOrDefaultAsync();
            return entityExist != null;
        }
    }
}
