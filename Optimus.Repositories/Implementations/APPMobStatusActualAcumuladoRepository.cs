﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class APPMobStatusActualAcumuladoRepository : GenericRepository<OptimusContext, APPMobStatusActualAcumulado, int, CommonFilter>, IAPPMobStatusActualAcumuladoRepository
    {
        public APPMobStatusActualAcumuladoRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
