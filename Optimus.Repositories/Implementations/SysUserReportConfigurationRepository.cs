﻿using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class SysUserReportConfigurationRepository : GenericRepository<OptimusContext, SysUserReportConfiguration, int, SysUserReportConfigurationFilter>, ISysUserReportConfigurationRepository
    {
        public SysUserReportConfigurationRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
