﻿using System;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class UserGroupRepository : GenericRepository<OptimusContext, SysUsersGroups, int, BaseFilter>, IUserGroupRepository
    {
        public UserGroupRepository(OptimusContext optimusContext): base(optimusContext)
        {
        }
    }
}
