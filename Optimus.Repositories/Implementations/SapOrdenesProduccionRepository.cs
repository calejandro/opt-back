﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class SapOrdenesProduccionRepository : GenericRepository<OptimusContext, SapOrdenesProduccion, int, SapOrdenesProduccionFilter>, ISapOrdenesProduccionRepository
    {
        public SapOrdenesProduccionRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public  async override Task<List<SapOrdenesProduccion>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, SapOrdenesProduccionFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {

            var query = dbSet.AsQueryable();
            query = this.Filter(filter, query);

            if(filter?.Search != null)
            {
                query = query.Where(x => x.IdOrdenSap.ToString().Contains(filter.Search));
            }            

            if (filter?.NoVinculadasTemporales == true)
            {
                query = query.Where(x => !(x.EsTemporal == true && x.IdOrdenVinculada != null));
            }                
            if (filter.FhInicio != null && filter.FhInicio.Count > 0)
            {
                var inicio = filter.FhInicio.Select(s => s.ToString("yyyyMMdd")).ToList();
                query = query.Where(w => inicio.Any(a => w.FhInicio == a));
            }

            if (filter.FhFin != null && filter.FhFin.Count > 0)
            {
                var fin = filter.FhFin.Select(s => s.ToString("yyyyMMdd")).ToList();
                query = query.Where(w => fin.Any(a => w.FhFin == a));
            }
            if (noTracking)
                query = query.AsNoTracking();

            query = this.GetIncludeQuery(query, includes);


            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public bool HasProductOrder(int sapLineId)
        {
            string[] froms = new string[] { DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.AddDays(-1).ToString("yyyyMMdd"),
            DateTime.Now.AddDays(-2).ToString("yyyyMMdd")};
            string[] tos = new string[] { DateTime.Now.AddDays(1).ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd") };
            var query = this.dbContext.SapOrdenesProduccion.Where(w => w.IdLinea == sapLineId &&
            froms.Any(a => w.FhInicio == a));
            if(DateTime.Now.Hour >= 22)
            {
                var l = query.ToList();
                query = query.Where(w => w.FhIncioProduccion == null || w.FhIncioProduccion.Value.Hour <= 22);
            }

            return query.Count() > 0;
        }
    }

}
