﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class ConfiguracionAlertasParadaMaquinaRepository : GenericRepository<OptimusContext,ConfiguracionAlertasParadaMaquina, int, ConfigAlertasParadaMaqFilter>, IConfiguracionAlertasParadaMaquinaRepository
    {
        public ConfiguracionAlertasParadaMaquinaRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public async Task<bool> ExistsEntityAsync(ConfiguracionAlertasParadaMaquina entity, bool isUpdate)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdEquipo == entity.IdEquipo &&
            x.IdLinea == entity.IdLinea &&
            x.TipoAlerta == entity.TipoAlerta);

            if(isUpdate)
            {
                query = query.Where(w => w.Id != entity.Id);
            }

            query = query.AsNoTracking();
            var entityExist = await query.FirstOrDefaultAsync();
            return entityExist != null;
        }
    }

}
