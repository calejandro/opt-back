﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class OptimusTaskRepository : GenericRepository<Optimus2Context, OptimusTask, Guid, OptimusTaskFilter>, IOptimusTaskRepository
    {        
        public OptimusTaskRepository(Optimus2Context apiContext) : base(apiContext)
        {
            
        }
        public async override Task<List<OptimusTask>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, OptimusTaskFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable();
            query = this.Filter(filter, query);
            query = this.CustomFilter(filter, query);
            query = this.GetIncludeQuery(query, includes);
            query = this.GetQueryOrder(query, orders);
            if (noTracking)
                query = query.AsNoTracking();
            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }
        public async override Task<int> CountAsync(OptimusTaskFilter filter = null, string[] includes = null)
        {
            var query = this.Filter(filter, this.dbSet.AsQueryable());
            query = this.CustomFilter(filter, query);
            query = this.GetIncludeQuery(query, includes);
            return await query.CountAsync();
        }
        public override async Task<OptimusTask> GetAsync(Guid id, string[] includes = null)
        {
            var query = this.dbSet.Where( x => x.Id == id);
            query = this.GetIncludeQuery(query, includes);
            query = query.AsNoTracking();
            return await query.SingleOrDefaultAsync();
        }
        private IQueryable<OptimusTask> CustomFilter(OptimusTaskFilter filter, IQueryable<OptimusTask> query)
        {
            if (filter.Name != null)
            {
                query = query.Where(x => x.Name.Contains(filter.Name));
            }
            if (filter.Description != null)
            {
                query = query.Where(x => x.Description.Contains(filter.Description));
            }

            if (filter.CreOrAssiOrVerf != null)
            {
                query = query.Where(x => x.IdAssignedUser == filter.CreOrAssiOrVerf || x.IdCreatorUser == filter.CreOrAssiOrVerf || x.IdVerifierUser == filter.CreOrAssiOrVerf);
            }
            if (filter.DesOrNamOrIde != null)
            {
                query = query.Where(x => x.Description.Contains(filter.DesOrNamOrIde) || x.Identifier.Contains(filter.DesOrNamOrIde) || x.Name.Contains(filter.DesOrNamOrIde));
            }
            if (filter.Identifier != null)
            {
                query = query.Where(x => x.Identifier.Contains(filter.Identifier));
            }
            if (filter.IsSubOptimusTask != null)
            {
                if (filter.IsSubOptimusTask == true)
                {
                    query = query.Where(x => x.IdParentOptimusTask != null);
                }
                else
                {
                    query = query.Where(x => x.IdParentOptimusTask == null);
                }
            }
            if (filter.NeIdState != null)
            {
                query = query.Where(x => x.IdState != filter.NeIdState);
            } 
            if(filter.IdAssignedUser != null)
            {
                query = query.Where(x => x.IdAssignedUser == filter.IdAssignedUser ||  x.SubOptimusTask.Any(x => x.IdAssignedUser == filter.IdAssignedUser));
            }
           
              return query;
        }

    }

}
