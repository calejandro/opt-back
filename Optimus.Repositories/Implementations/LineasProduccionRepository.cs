﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class LineasProduccionRepository : GenericRepository<OptimusContext, LineasProduccion, int, LineaFilters>, ILineasProduccionRepository
    {
        public LineasProduccionRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        
    }

}
