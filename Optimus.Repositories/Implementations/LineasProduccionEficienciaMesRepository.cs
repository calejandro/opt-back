﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class LineasProduccionEficienciaMesRepository : GenericRepository<OptimusContext, LineasProduccionEficienciaMes, int, LineaFilters>, ILineasProduccionEficienciaMesRepository
    {
        public LineasProduccionEficienciaMesRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public override async Task<bool> ExistsEntityAsync(LineasProduccionEficienciaMes entity)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdLinea == entity.IdLinea);
            query = query.AsNoTracking();
            var entityExist = await query.SingleOrDefaultAsync();

            if (entity.Id != entityExist?.Id)
                return entityExist != null;
            else
                return false;
        }
    }

}
