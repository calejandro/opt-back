﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class PlantasRepository : GenericRepository<OptimusContext, Plantas, int, BaseFilter>, IPlantasRepository
    {
        public PlantasRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
    }

}
