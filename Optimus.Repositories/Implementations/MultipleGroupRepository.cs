﻿using System;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Implementations
{
    public class MultipleGroupRepository : GenericRepository<OptimusContext, SysMultipleGroups, int, BaseFilter>, IMultipleGroupRepository
    {
        public MultipleGroupRepository(OptimusContext optimusContext): base(optimusContext)
        {
        }
    }
}
