﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class LineasProdSapToLineasProdElpRepository : GenericRepository<OptimusContext, LineasProdSapToLineasProdElp, int, LineasProdSapToLineasProdElpFilter>, ILineasProdSapToLineasProdElpRepository
    {
        public LineasProdSapToLineasProdElpRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public override Task<LineasProdSapToLineasProdElp> GetAsync(int id, string[] includes = null)
        {
            return this.dbContext.LineasProdSapToLineasProdElp.Where(w => w.IdLineaElp == id).SingleOrDefaultAsync();
        }
    }

}
