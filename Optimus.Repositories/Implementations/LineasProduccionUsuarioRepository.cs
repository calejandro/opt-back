﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;


namespace Optimus.Repositories.Implementations
{
    public class LineasProduccionUsuarioRepository : GenericRepository<OptimusContext, LineasProduccionUsuario, int, LineasProduccionUsuarioFilter>, ILineasProduccionUsuarioRepository
    {
        public LineasProduccionUsuarioRepository(OptimusContext apiContext) : base(apiContext)
        {
        }

        public override async Task<List<LineasProduccionUsuario>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, LineasProduccionUsuarioFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = this.dbSet.AsQueryable();

            query = this.Filter(filter, query);            

            if(filter.FechaInicio != null)
            {
                query = query.Where(w => w.FechaToma >= filter.FechaInicio);
            }

            if(filter.FechaFin != null)
            {
                query = query.Where(w => w.FechaToma <= filter.FechaFin);
            }
            if (filter.FechaFin != null)
            {
                query = query.Where(w => w.FechaToma <= filter.FechaFin);
            }

            if (filter.IsTake != null)
            {
                if (filter.IsTake == true)
                {
                    query = query.Where(w => w.FechaLiberacion == null);
                }
                else
                {
                    query = query.Where(w => w.FechaLiberacion != null);
                }
            }
                     

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public override async Task<int> CountAsync(LineasProduccionUsuarioFilter filter = null, string[] includes = null)
        {
            var query = this.Filter(filter, this.dbSet.AsQueryable());
            query = this.GetIncludeQuery(query, includes);
            if (filter.FechaInicio != null)
            {
                query = query.Where(w => w.FechaToma >= filter.FechaInicio);
            }

            if (filter.FechaFin != null)
            {
                query = query.Where(w => w.FechaToma <= filter.FechaFin);
            }

            if (filter.IsTake != null)
            {
                if (filter.IsTake == true)
                {
                    query = query.Where(w => w.FechaLiberacion == null);
                }
                else
                {
                    query = query.Where(w => w.FechaLiberacion != null);
                }
            }
            return await query.CountAsync();
        }

        public async Task<List<LineasProduccion>> GetLineasConTomas(DateTime fechaInicio, DateTime fechaFin)
        {
            List<LineasProduccion> linea;
            if (fechaFin.Date < DateTime.Now.Date) 
            {
                linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin)
                   .Select(s => s.LineaProduccion).Distinct().ToListAsync();
            }
            else
            {
                linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && (w.FechaLiberacion == null || w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin))
                  .Select(s => s.LineaProduccion).Distinct().ToListAsync();
            }

            return linea;
        }
        public async Task<List<LineasProduccionUsuario>> GetLineasUsuarioConTomas(DateTime fechaInicio, DateTime fechaFin)
        {
            List<LineasProduccionUsuario> linea;
            if (fechaFin.Date < DateTime.Now.Date)
            {
                linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin)
                    .ToListAsync();
            }
            else
            {
                linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && (w.FechaLiberacion == null || w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin))
                    .ToListAsync();
            }

            return linea;
        }
        public async Task<bool> IsLineaConToma(int idLinea,DateTime fechaInicio, DateTime fechaFin)
        {
            List<LineasProduccion> linea ;
            if (fechaFin.Date < DateTime.Now.Date)
            {
                 linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin && w.IdLinea == idLinea)
                    .Select(s => s.LineaProduccion).Distinct().ToListAsync();
            }
            else
            {
                linea = await this.dbSet.Where(w => w.FechaToma >= fechaInicio && (w.FechaLiberacion == null || w.FechaLiberacion.Value.AddMilliseconds(-w.FechaLiberacion.Value.Millisecond) <= fechaFin) && w.IdLinea == idLinea)
                  .Select(s => s.LineaProduccion).Distinct().ToListAsync();
            }
            return linea?.Count > 0;
        }
    }

    

}