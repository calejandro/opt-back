﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class UrlImageRepository : GenericRepository<Optimus2Context, UrlImage, Guid, BaseFilter>, IUrlImageRepository
    {
        public UrlImageRepository(Optimus2Context apiContext) : base(apiContext)
        {
        }
        public override Task<UrlImage> InsertAsync(UrlImage entity)
        {
            entity.IdPlanta = dbContext.GetPlantId;
            return base.InsertAsync(entity);
        }
        public override Task InsertAllAsync(List<UrlImage> list)
        {
            list.Select(x => x.IdPlanta = dbContext.GetPlantId);
            return base.InsertAllAsync(list);
        }
        public async Task<List<UrlImage>> ListAsync(int idEntity, string entity)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x =>  x.IdEntity == idEntity && x.Entity == entity );
            return await query.ToListAsync();
        }
    }

}
