﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class RegistroEstadosRepository : GenericRepository<OptimusContext, RegistroEstados, int, RegistroEstadosFilter>, IRegistroEstadosRepository
    {
        public RegistroEstadosRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public override async Task<List<RegistroEstados>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, RegistroEstadosFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable();

            query = this.Filter(filter, query);

            query = query.Where(x => Convert.ToInt64( x.FhRegistro ) >= this.DateInt(filter.DateFrom.Value) && Convert.ToInt64(x.FhRegistro) <= this.DateInt(filter.DateTo.Value));

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);

            if (noTracking)
                query = query.AsNoTracking();

            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }
        private long DateInt(DateTime dateTime)
        {
            return Convert.ToInt64( dateTime.Year 
                + (dateTime.Month > 9 ? dateTime.Month.ToString() : ("0" + dateTime.Month)) 
                + (dateTime.Day > 9 ? dateTime.Day.ToString() : ("0" + dateTime.Day))
                + (dateTime.Hour > 9 ? dateTime.Hour.ToString() : ("0" + dateTime.Hour))
                + (dateTime.Minute > 9 ? dateTime.Minute.ToString() : ("0" + dateTime.Minute))
                + "00");
        }
    }

}
