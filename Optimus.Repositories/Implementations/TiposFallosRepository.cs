﻿using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Repositories;
using Optimus.Models.Contexts;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Repositories.Implementations
{
    public class TiposFallosRepository : GenericRepository<OptimusContext, TiposFallos, int, TipoFallosFilter>, ITiposFallosRepository
    {
        public TiposFallosRepository(OptimusContext apiContext) : base(apiContext)
        {
        }
        public override async  Task<bool> ExistsEntityAsync(TiposFallos entity)
        {
            var query = dbSet.AsQueryable();
            query = query.Where(x => x.IdPlanta == dbContext.GetPlantId &&
            x.IdEquipo == entity.IdEquipo &&
            x.IdLinea == entity.IdLinea &&
            x.Tipo == entity.Tipo &&
            x.TipoAlerta == entity.TipoAlerta &&
            x.Nombre == entity.Nombre);

            query = query.AsNoTracking();
            var entityExist = await query.SingleOrDefaultAsync();

            if (entity.Id != entityExist?.Id)
                return entityExist != null;
            else
                return false;
        }
    }

}
