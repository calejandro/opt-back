﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Optimus.Repositories.Implementations;
using Optimus.Repositories.Interfaces;

namespace Optimus.Repositories.Extensions
{
    public static class ApiRepositoryExtension
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IAlertaMermaConfiguracionesRepository, AlertaMermaConfiguracionesRepository>();
            services.AddScoped<IConfiguracionAlertasParadaMaquinaRepository, ConfiguracionAlertasParadaMaquinaRepository>();
            services.AddScoped<IConfiguracionUmbralesTmefTmprRepository, ConfiguracionUmbralesTmefTmprRepository>();
            services.AddScoped<IEquiposEstadosRepository, EquiposEstadosRepository>();
            services.AddScoped<IEquiposRepository, EquiposRepository>();
            services.AddScoped<ILineasProduccionAlertasRepository, LineasProduccionAlertasRepository>();
            services.AddScoped<ILineasProduccionRepository, LineasProduccionRepository>();
            services.AddScoped<ILineasProduccionUsuarioRepository, LineasProduccionUsuarioRepository>();
            services.AddScoped<IPaisesRepository, PaisesRepository>();
            services.AddScoped<IPlantasRepository, PlantasRepository>();
            services.AddScoped<ITiposFallosRepository, TiposFallosRepository>();
            services.AddScoped<ISapSkuRepository, SapSkuRepository>();
            services.AddScoped<ICausasFallosRepository, CausasFallosRepository>();
            services.AddScoped<ILineasProduccionEficienciaMesRepository, LineasProduccionEficienciaMesRepository>();
            services.AddScoped<ISysUserReportConfigurationRepository, SysUserReportConfigurationRepository>();
            services.AddScoped<ISapOrdenesProduccionRepository, SapOrdenesProduccionRepository>();
            services.AddScoped<ISysUsersRepository, SysUsersRepository>();
            services.AddScoped<IStoreProceduresRepository, StoreProceduresRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IProduccionEficienciaRepository, ProduccionEficienciaRepository>();
            services.AddScoped<IProduccionEficienciaByShiftRepository, ProduccionEficienciaByShiftRepository>();
            services.AddScoped<IKpiDepletionRepository, KpiDepletionRepository>();
            services.AddScoped<IMachineActualStatusRepository, MachineActualStatusRepository>();
            services.AddScoped<IMachineStatusProductionOrderRepository, MachineStatusProductionOrderRepository>();
            services.AddScoped<ITimeStopsRepository, TimeStopsRepository>();
            services.AddScoped<IAverageTimeFailuresProductionRepository, AverageTimeFailuresProductionRepository>();
            services.AddScoped<ISysUserPlantsRepository, SysUserPlantsRepository>();
            services.AddScoped<ILineasProdSapToLineasProdElpRepository, LineasProdSapToLineasProdElpRepository>();
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<IMultipleGroupRepository, MultipleGroupRepository>();
            services.AddScoped<IUrlImageRepository, UrlImageRepository>();
            services.AddScoped<ILineaProduccionObjetivosConfigRepository, LineaProduccionObjetivosConfigRepository>();
            services.AddScoped<ILineasProduccionAlertasRepository2, LineasProduccionAlertasRepository2>();
            services.AddScoped<IAPPMobStatusActualAcumuladoRepository, APPMobStatusActualAcumuladoRepository>();
            services.AddScoped<IConfiguracionUmbralesAlertasPrecaucionRepository, ConfiguracionUmbralesAlertasPrecaucionRepository>();
            services.AddScoped<IAcumEficienciaRepository, AcumEficienciaRepository>();
            services.AddScoped<IConfigJobRepository, ConfigJobRepository>();
            services.AddScoped<IOptimusTaskRepository, OptimusTaskRepository>();
            services.AddScoped<ITaskCommentRepository, TaskCommentRepository>();
            services.AddScoped<IFileUrlRepository, FileUrlRepository>();
            services.AddScoped<IKpiRepository, KpiRepository>();
            services.AddScoped<ISectorRepository, SectorRepository>();
            services.AddScoped<IProcessRepository, ProcessRepository>();
            services.AddScoped<IMachineStatusHistoricRepository, MachineStatusHistoricRepository>();
            services.AddScoped<ISkuByLineViewRepository, SkuByLineViewRepository>();
            services.AddScoped<ISkuEfficiencyRepository, SkuEfficiencyRepository>();
            services.AddScoped<IOrdenesSapByLineViewRepository, OrdenesSapByLineViewRepository>();
            services.AddScoped<IRegistrosFallosAlertasRepository, RegistrosFallosAlertasRepository>();
            services.AddScoped<IAlertByOrdenesSapViewRepository, AlertByOrdenesSapViewRepository>();
            services.AddScoped<IMachineStatusEventsRepository, MachineStatusEventsRepository>();
            services.AddScoped<IRegistroEstadosRepository, RegistroEstadosRepository>();











            return services;
        }
    }
}
