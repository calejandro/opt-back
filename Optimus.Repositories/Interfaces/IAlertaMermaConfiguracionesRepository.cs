﻿using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IAlertaMermaConfiguracionesRepository : IGenericRepository<AlertaMermaConfiguraciones, int, AlertaMermaFilter>
    {
        Task<bool> ExistsEntityAsync(AlertaMermaConfiguraciones entity, bool isUpdate);
    }
}
