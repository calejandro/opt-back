﻿using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IConfiguracionUmbralesTmefTmprRepository : IGenericRepository<ConfiguracionUmbralesTmefTmpr, int, CommonFilter>
    {
        Task<bool> ExistsEntityAsync(ConfiguracionUmbralesTmefTmpr entity, bool isUpdate);
    }
}
