﻿using System;
using System.Threading.Tasks;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface ILineaProduccionObjetivosConfigRepository : IGenericRepository<LineasProduccionObjetivosConfig, Guid, LineasProduccionObjetivosConfigFilter>
    {
        Task<bool> ExistsEntityAsync(LineasProduccionObjetivosConfig entity, bool isUpdate);
    }
}
