﻿using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using System;

namespace Optimus.Repositories.Interfaces
{
    public interface IOptimusTaskRepository : IGenericRepository<OptimusTask, Guid, OptimusTaskFilter>
    {
    }
}
