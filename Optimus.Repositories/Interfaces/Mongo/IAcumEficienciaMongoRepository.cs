﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Entities;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using System;
using System.Threading.Tasks;

namespace Optimus.Repositories.Interfaces
{
    public interface IAcumEficienciaRepository : IGenericMongoRepository<AcumEficienciaMongo>
    {
        Task CreateEficienciaDia(LineasProduccion linea, DateTime date, Shift turno);
        Task CreateEficienciaMes(LineasProduccion linea, DateTime date);
        Task CreateEficienciaAño(LineasProduccion linea, DateTime date);
        Task DeleteEficienciaDia(int idLinea, DateTime date);
    }
}
