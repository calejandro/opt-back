﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Repositories.Interfaces
{
    public interface IMachineStatusHistoricRepository : IGenericMongoRepository<MachineStatusHistoricMongo>
    {
        Task<List<MachineStatusHistoricMongo>> ListAsyncFilter(ReporteHistoricoFilter filter = null);
    }
}
