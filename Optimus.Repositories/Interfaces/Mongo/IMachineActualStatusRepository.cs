﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Repositories.Cosmos;
using Optimus.Models.Identity;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IMachineActualStatusRepository : IGenericMongoRepository<MachineActualStatusMongo>
    {
    }
}
