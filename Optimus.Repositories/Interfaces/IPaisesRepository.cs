﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;


namespace Optimus.Repositories.Interfaces
{
    public interface IPaisesRepository : IGenericRepository<Paises, int,BaseFilter>
    {
    }
}
