﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using System;

namespace Optimus.Repositories.Interfaces
{
    public interface ISectorRepository : IGenericRepository<Sector, Guid, BaseFilter>
    {
    }
}
