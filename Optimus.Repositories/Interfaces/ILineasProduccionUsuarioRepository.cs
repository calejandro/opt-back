﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface ILineasProduccionUsuarioRepository : IGenericRepository<LineasProduccionUsuario, int, LineasProduccionUsuarioFilter>
    {
        Task<List<LineasProduccion>> GetLineasConTomas(DateTime fechaInicio, DateTime fechaFin);
        Task<bool> IsLineaConToma(int idLinea, DateTime fechaInicio, DateTime fechaFin);

        Task<List<LineasProduccionUsuario>> GetLineasUsuarioConTomas(DateTime fechaInicio, DateTime fechaFin);
    }
}
