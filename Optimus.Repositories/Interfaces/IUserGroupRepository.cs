﻿using System;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;

namespace Optimus.Repositories.Interfaces
{
    public interface IUserGroupRepository : IGenericRepository<SysUsersGroups, int, BaseFilter>
    {
    }
}
