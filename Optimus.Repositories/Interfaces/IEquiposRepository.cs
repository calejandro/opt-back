﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IEquiposRepository : IGenericRepository<Equipos, int, CommonFilter>
    {
    }
}
