﻿using System;
using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Identity;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IUserRepository : IGenericRepository<UserIdentity, Guid, UserFilter>
    {
        Task<UserIdentity> GetByCognitoId(Guid cognitoId);
    }
}
