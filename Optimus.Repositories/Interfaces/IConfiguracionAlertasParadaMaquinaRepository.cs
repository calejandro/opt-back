﻿using System.Threading.Tasks;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IConfiguracionAlertasParadaMaquinaRepository : IGenericRepository<ConfiguracionAlertasParadaMaquina, int, ConfigAlertasParadaMaqFilter>
    {
        Task<bool> ExistsEntityAsync(ConfiguracionAlertasParadaMaquina entity, bool isUpdate);
    }
}
