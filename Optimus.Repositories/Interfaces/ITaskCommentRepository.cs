﻿using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using System;

namespace Optimus.Repositories.Interfaces
{
    public interface ITaskCommentRepository : IGenericRepository<TaskComment, Guid, TaskCommentFilter>
    {
    }
}
