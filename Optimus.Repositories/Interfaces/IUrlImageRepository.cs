﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Repositories.Interfaces
{
    public interface IUrlImageRepository : IGenericRepository<UrlImage, Guid, BaseFilter>
    {
        Task<List<UrlImage>> ListAsync(int idEntity, string entity);
    }
}
