﻿using System;
using System.Threading.Tasks;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface ISysUsersRepository : IGenericRepository< SysUsers, int, UserFilter>
    {
        Task<SysUsers> GetAsync(Guid id, string[] includes = null);
    }
}
