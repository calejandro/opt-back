﻿using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Models.Entities;
using Optimus.Repositories.Filers;

namespace Optimus.Repositories.Interfaces
{
    public interface IConfiguracionUmbralesAlertasPrecaucionRepository : IGenericRepository<ConfiguracionUmbralesAlertasPrecaucion, int, CommonFilter>
    {
    }
}
