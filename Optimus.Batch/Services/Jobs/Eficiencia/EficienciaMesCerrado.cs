﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Jobs.Eficiencia
{
    public class EficienciaMesCerrado : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<EficienciaMesCerrado> logger;
        public EficienciaMesCerrado(IServiceProvider serviceProvider, ILogger<EficienciaMesCerrado> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var acumEficienciaRepository = scope.ServiceProvider.GetService<IAcumEficienciaRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();

                    var builder = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EficienciaMesCerrado"))).FirstOrDefault();
                    var meses = 0;
                    if (config != null)
                        meses = (DateTime.Now.Date - config.Fecha.Date).Days / 30;
                    else
                        meses = 5;

                    for (int i = meses; i >= 0 ; i--)
                    {
                        var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month - i, 1);
                        var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(firstDayOfMonth, firstDayOfMonth.AddMonths(1).AddDays(-1));

                        foreach (var linea in lineas)
                        {                         
                            await acumEficienciaRepository.CreateEficienciaMes(linea, firstDayOfMonth.Date.AddHours(-3));
                        }
                    }
                    if (config == null)
                    {
                        config = new ConfigJobMongo();
                        config.Tipo = "EficienciaMesCerrado";
                        config.Fecha = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        await configJobRepository.InsertAsync(config);
                    }
                    else
                    {
                        config.Fecha = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        await configJobRepository.UpdateAsync(config);
                    }

                    // AÑO 
                    builder = Builders<ConfigJobMongo>.Filter;
                    config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EficienciaAñoCerrado"))).FirstOrDefault();
                    var año = 0;
                    if (config != null)
                        año = (DateTime.Now.Date - config.Fecha.Date).Days / 365;


                    for (int i = año; i >= 0; i--)
                    {
                        var firstDayOfYear = new DateTime(DateTime.Now.Year-i, 1, 1);
                        var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(firstDayOfYear, firstDayOfYear.AddDays(365));

                        foreach (var linea in lineas)
                        {
                            await acumEficienciaRepository.CreateEficienciaAño(linea, firstDayOfYear.Date.AddHours(-3));
                        }
                    }
                    if (config == null)
                    {
                        config = new ConfigJobMongo();
                        config.Tipo = "EficienciaAñoCerrado";
                        config.Fecha = new DateTime(DateTime.Now.Year,1, 1);
                        await configJobRepository.InsertAsync(config);
                    }
                    else
                    {
                        config.Fecha = new DateTime(DateTime.Now.Year, 1, 1);
                        await configJobRepository.UpdateAsync(config);
                    }
                }

            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");

                throw;
            }

        }
    }
}
