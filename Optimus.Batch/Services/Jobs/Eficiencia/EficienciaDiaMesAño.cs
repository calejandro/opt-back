﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Jobs.Eficiencia
{
    public class EficienciaDiaMesAño : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<EficienciaDiaMesAño> logger;

        public EficienciaDiaMesAño(IServiceProvider serviceProvider, ILogger<EficienciaDiaMesAño> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var acumEficienciaRepository = scope.ServiceProvider.GetService<IAcumEficienciaRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();


                    var builder = Builders<ConfigJobMongo>.Filter;

                    var turnoDia = storeProcedures.GetDay(DateTime.Now);

                    var today = turnoDia.Fin.Date.AddHours(-3) ;
                    var firstDayOfMonth = new DateTime(today.Year, today.Month, 1).Date.AddHours(-3);
                    var firstDayOfYear= new DateTime(today.Year, 1, 1).Date.AddHours(-3);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoDia.Inicio, turnoDia.Fin);

                    foreach (var linea in lineas)
                    {

                        //EFICIENCIA POR DIA
                        await acumEficienciaRepository.CreateEficienciaDia(linea, today, turnoDia);
                        // EFICIENCIA POR MES
                        await acumEficienciaRepository.CreateEficienciaMes(linea, firstDayOfMonth);
                        // EFICIENCIA POR AÑO
                        await acumEficienciaRepository.CreateEficienciaAño(linea, firstDayOfYear);

                    }
                }

            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");

                throw;
            }

        }
    }
}
