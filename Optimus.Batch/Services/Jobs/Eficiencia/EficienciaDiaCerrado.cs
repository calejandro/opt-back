﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Commons.Services.Quartz;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Jobs.Eficiencia
{
    public class EficienciaDiaCerrado : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<EficienciaDiaCerrado> logger;

        public EficienciaDiaCerrado(IServiceProvider serviceProvider,ILogger<EficienciaDiaCerrado> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var acumEficienciaRepository = scope.ServiceProvider.GetService<IAcumEficienciaRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();
                    var quartzService = scope.ServiceProvider.GetService<IQuartzService>();


                    var builder = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EficienciaDiaCerrado"))).FirstOrDefault();
                    var turnoDia =  storeProcedures.GetDay(DateTime.Now);
                    var dias = 0;
                    if (config != null)
                    {
                        dias = (turnoDia.Fin.Date - config.Fecha.Date).Days;
                    }else
                    {
                        var aux = new DateTime(DateTime.Now.Year, 1, 1);
                        dias = (DateTime.Now.Date - aux).Days;
                    }

                    for (int i = dias; i >= 1; i--)
                    {

                        var dia = DateTime.Now.AddDays(-i);
                         turnoDia = storeProcedures.GetDay(dia);
                        var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoDia.Inicio, turnoDia.Fin);

                        foreach (var linea in lineas)
                        {
                           await acumEficienciaRepository.CreateEficienciaDia(linea, dia.Date.AddHours(-3), turnoDia);
                        }
                    }
                    if (config == null)
                    {
                        config = new ConfigJobMongo();
                        config.Tipo = "EficienciaDiaCerrado";
                        config.Fecha = DateTime.Now.Date;
                        await configJobRepository.InsertAsync(config);
                    }
                    else
                    {
                        config.Fecha = DateTime.Now.Date;
                        await configJobRepository.UpdateAsync(config);
                    }

                    // Historico se  ejecutar Eficiencia mes si corresponde 
                    config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EficienciaMesCerrado"))).FirstOrDefault();
                    var meses = 2;
                    if (config != null)
                        meses = (DateTime.Now - config.Fecha).Days / 30;

                    if(meses > 0)
                    await quartzService.RunNowJob("EficienciaMesCerrado");

                }

            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }

        }
    }
}
