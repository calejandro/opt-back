﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Commons.Services.Quartz;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Jobs.Eficiencia
{
    public class SkuEficienciaHistorico : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<SkuEficienciaHistorico> logger;

        public SkuEficienciaHistorico(IServiceProvider serviceProvider,ILogger<SkuEficienciaHistorico> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed inicio FH:{DateTime.Now}");
                logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var skuEfficiencyRepository = scope.ServiceProvider.GetService<ISkuEfficiencyRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();
                    var quartzService = scope.ServiceProvider.GetService<IQuartzService>();
                    var sapOrdenesProduccionRepository = scope.ServiceProvider.GetService<ISapOrdenesProduccionRepository>();



                    var builder = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "SkuEficienciaHistorico"))).FirstOrDefault();
                    var turnoDia =  storeProcedures.GetCurrentDay(DateTime.Now);
                    var dias = 0;
                    if (config != null)
                    {
                        dias = (turnoDia.Fin.Date - config.Fecha.Date).Days;
                    }
                    else
                    {
                        var aux = new DateTime(DateTime.Now.Year, 1, 1);
                        dias = (DateTime.Now.Date - aux).Days;
                    }

                    for ( int i = dias; i >= 0; i--)
                    {

                        var dia = DateTime.Now.AddDays(-i);
                        turnoDia = storeProcedures.GetCurrentDay(dia);
                        var lineas = await lineasProduccionUsuarioRepository.GetLineasUsuarioConTomas(turnoDia.Inicio, turnoDia.Fin); // por orden sap
                        var lineasGroup = lineas.GroupBy(x => x.IdLinea);
                        foreach (var linea in lineasGroup)
                        {
                               
                                var eficiencias = await storeProcedures.GetEfficiencyProduction(linea.Key, turnoDia.Inicio, turnoDia.Fin);
                                var skuEfficiencies = new List<SkuEfficiencyMongo>();

                                var listIdOrdenSAP =  eficiencias.GroupBy(x => x.IdOrdenSAP);
                                foreach (var idOrdenSAP in listIdOrdenSAP)
                                {
                                    SkuEfficiencyMongo skuEfficiency = null;
                                    var builder2 = Builders<SkuEfficiencyMongo>.Filter;
                                    var filter = builder2.Eq(widget => widget.IdOrdenSAP, idOrdenSAP.Key) &
                                        builder2.Eq(widget => widget.Turno, turnoDia.Turno);

                                    var skuEfficiencyMongo= await skuEfficiencyRepository.ListAsync(filter);
                                    if (skuEfficiencyMongo?.Count > 0)
                                    {
                                        continue;
                                    }

                                    skuEfficiency = new SkuEfficiencyMongo();
                                    var lisEfiBySap = eficiencias.Where(x => x.IdOrdenSAP == idOrdenSAP.Key);
                                    skuEfficiency.Eficiencia = lisEfiBySap.Average(x => x.Eficiencia);
                                    skuEfficiency.CantidadRegistros = lisEfiBySap.Count();
                                    skuEfficiency.BotellasProducidas = lisEfiBySap.Sum(x => x.BotellasProducidas);

                                    // TIMPO LINEA
                                    ProduccionEficiencia previous = null;
                                    int sum = 0;
                                    foreach (var efiBySap in lisEfiBySap)
                                    {
                                        if(previous != null)
                                        {
                                            sum +=  ( efiBySap.FechaHora - previous.FechaHora).Minutes;
                                        }
                                        previous = efiBySap;
                                    }
                                    skuEfficiency.TiempoEnLineaM = sum;

                         

                                    skuEfficiency.IdLinea = linea.Key;
                                    skuEfficiency.DescripcionLinea = lisEfiBySap.FirstOrDefault()?.DescripcionLinea;
                                    skuEfficiency.IdOrdenSAP = idOrdenSAP.Key;

                                    var sapOrden = (await sapOrdenesProduccionRepository.ListAsync(null, new string[] { "SapSku" }, 
                                        new SapOrdenesProduccionFilter() {IdOrdenSap = idOrdenSAP.Key })).FirstOrDefault();
                                    if(sapOrden == null)
                                    {
                                        continue;
                                    }
                                    skuEfficiency.IdSKU = sapOrden.IdSku;
                                    skuEfficiency.DescripcionSKU = sapOrden.SapSku?.DescSku;
                                    skuEfficiency.Marca = sapOrden.SapSku?.Marca;
                                    skuEfficiency.Sabor = sapOrden.SapSku?.Sabor;
                                    skuEfficiency.Formato = sapOrden.SapSku?.Formato;
                                    skuEfficiency.SaborConsolid = skuEfficiency.Marca + " - "+ skuEfficiency.Sabor;
                                    skuEfficiency.FormatoConsolid = skuEfficiency.Marca + " - " + skuEfficiency.Sabor + " - " + skuEfficiency.Formato;

                                    // VOLUMEN
                                    double formato = Convert.ToDouble( skuEfficiency.Formato.Replace("LTR", "").Replace("ML", ""));
                                    skuEfficiency.Volumen = (formato * skuEfficiency.BotellasProducidas) / 5.688;

                                    skuEfficiency.Turno = turnoDia.Turno;
                                    skuEfficiency.FechaInicio = turnoDia.Inicio.AddHours(-3);
                                    skuEfficiency.FechaFin = turnoDia.Fin.AddHours(-3);
                                    skuEfficiency.IdSupervisor = lineas.Find(x => x.IdLinea == linea.Key) == null ? 0 : lineas.Find(x => x.IdLinea == linea.Key).UserId;

                                    skuEfficiency.CreatedBy = "job";
                                    skuEfficiency.Created = DateTime.Now.AddHours(-3);

                                    skuEfficiencies.Add(skuEfficiency);
                                }
                                if(skuEfficiencies?.Count > 0)
                                await skuEfficiencyRepository.InsertManyAsync(skuEfficiencies);
                                
                        }
                        logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed parcial FH:{DateTime.Now}, Dia procesado:{DateTime.Now.AddDays(-i)}");

                        if (config == null)
                        {
                            config = new ConfigJobMongo();
                            config.Tipo = "SkuEficienciaHistorico";
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.Created = DateTime.Now.AddHours(-3);
                            config.CreatedBy = "Job";
                            await configJobRepository.InsertAsync(config);
                        }
                        else
                        {
                            config.Updated= DateTime.Now.AddHours(-3);
                            config.UpdatedBy = "Job";
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            await configJobRepository.UpdateAsync(config);
                        }
                        config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "SkuEficienciaHistorico"))).FirstOrDefault();
                    }
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed final FH:{DateTime.Now} ");

                }

            }
            catch (Exception e)
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }

        }
    }
}
