﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Quartz;

namespace Optimus.Batch.Services.Jobs
{
    public class MachineStatusEvents : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<MachineStatusEvents> logger;

        public MachineStatusEvents(IServiceProvider serviceProvider, ILogger<MachineStatusEvents> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed inicio FH:{DateTime.Now}" );
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProceduresRepository = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var machineStatusEventsRepository = scope.ServiceProvider.GetService<IMachineStatusEventsRepository>();
                    var registroEstadosRepository = scope.ServiceProvider.GetService<IRegistroEstadosRepository>();
                    var equiposRepository = scope.ServiceProvider.GetService<IEquiposRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();
                    var ordenesSapByLineViewRepository = scope.ServiceProvider.GetService<IOrdenesSapByLineViewRepository>();


                    var builder2 = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder2.Eq(widget => widget.Tipo, "MachineStatusEvents"))).FirstOrDefault();

                    var dias = 0;
                    if (config != null)
                    {
                        dias = (DateTime.Now.Date - config.Fecha.Date).Days;
                    }
                    else
                    {
                        var aux = new DateTime(DateTime.Now.Year, 1, 1);
                        dias = (DateTime.Now.Date - aux).Days;
                    }


                    for (int i = dias; i >= 2; i--)
                    {
                        var shift = storeProceduresRepository.GetDay(DateTime.Now.AddDays(-i));

                        var ordersSapUser = await ordenesSapByLineViewRepository.ListFromToAsync(shift.Inicio, shift.Fin);
                        var ordersSap = ordersSapUser.Select(x => new
                        { x.IdOrdenSap, x.FhFinProduccion, x.FhInicioProduccion, x.IdSku, x.DescSku, x.Marca, x.Sabor, x.Formato, x.SaborConsolid, x.FormatoConsolid, x.IdLinea }).Distinct();
                        foreach (var orderSap in ordersSap)
                        {
                            var builder = Builders<MachineStatusEventsMongo>.Filter;
                            var filter = builder.Eq(widget => widget.IdOrdenSAP, orderSap.IdOrdenSap);
                            var statesEvents = await machineStatusEventsRepository.ListAsync(filter);
                            if (statesEvents?.Count() > 0)
                                continue;

                            var machinesByLine = await equiposRepository.ListAsync(null, null, new CommonFilter() { IdLinea = orderSap.IdLinea });
                            foreach (var machineByLine in machinesByLine)
                            {



                                var statesRegistration = await registroEstadosRepository.ListAsync(null, null,
                                    new RegistroEstadosFilter() { IdEquipo = machineByLine.IdEquipo, DateFrom = orderSap.FhInicioProduccion, DateTo = orderSap.FhFinProduccion }); // controlar fecha fin menor a la fecha actual

                                byte? stateLast = null;
                                var acum = 0;
                                foreach (var stateRegistered in statesRegistration)
                                {
                                    if (stateRegistered.Estado == stateLast)
                                    {
                                        acum += 1;
                                    }
                                    else
                                    {
                                        if (acum > 0)
                                        {
                                            await machineStatusEventsRepository.InsertAsync(new MachineStatusEventsMongo()
                                            {
                                                IdLinea = orderSap.IdLinea,
                                                IdOrdenSAP = orderSap.IdOrdenSap,
                                                IdSKU = orderSap.IdSku,
                                                DescripcionSKU = orderSap.DescSku,
                                                Marca = orderSap.Marca,
                                                Sabor = orderSap.Sabor,
                                                Formato = orderSap.Formato,
                                                SaborConsolid = orderSap.SaborConsolid,
                                                FormatoConsolid = orderSap.FormatoConsolid,
                                                FechaInicioOrdenSap = orderSap.FhInicioProduccion.AddHours(-3),
                                                FechaFinOrdenSap = orderSap.FhFinProduccion.AddHours(-3),
                                                IdSupervisor = ordersSapUser.Find(x => x.IdOrdenSap == orderSap.IdOrdenSap).IdUser,
                                                FechaInicioEstado = this.StringToDate(stateRegistered.FhRegistro).AddHours(-3).AddMinutes(-acum),
                                                FechaFinEstado = this.StringToDate(stateRegistered.FhRegistro).AddHours(-3),
                                                IdEquipo = machineByLine.IdEquipo,
                                                IdEstado = stateLast.Value,
                                                MinutosAcumulados = acum,
                                                Created = DateTime.Now.AddHours(-3),
                                                CreatedBy = "job"

                                            });

                                        }

                                        acum = 1;
                                        stateLast = stateRegistered.Estado;
                                    }
                                }

                                if (acum > 0)
                                {

                                    await machineStatusEventsRepository.InsertAsync(new MachineStatusEventsMongo()
                                    {
                                        IdLinea = orderSap.IdLinea,
                                        IdOrdenSAP = orderSap.IdOrdenSap,
                                        IdSKU = orderSap.IdSku,
                                        DescripcionSKU = orderSap.DescSku,
                                        Marca = orderSap.Marca,
                                        Sabor = orderSap.Sabor,
                                        Formato = orderSap.Formato,
                                        SaborConsolid = orderSap.SaborConsolid,
                                        FormatoConsolid = orderSap.FormatoConsolid,
                                        FechaInicioOrdenSap = orderSap.FhInicioProduccion.AddHours(-3),
                                        FechaFinOrdenSap = orderSap.FhFinProduccion.AddHours(-3),
                                        IdSupervisor = ordersSapUser.Find(x => x.IdOrdenSap == orderSap.IdOrdenSap).IdUser,
                                        FechaInicioEstado = this.StringToDate(statesRegistration.Last().FhRegistro).AddHours(-3).AddMinutes(-acum + 1),
                                        FechaFinEstado = this.StringToDate(statesRegistration.Last().FhRegistro).AddHours(-3).AddMinutes(1),
                                        IdEquipo = machineByLine.IdEquipo,
                                        IdEstado = statesRegistration.Last().Estado,
                                        MinutosAcumulados = acum,
                                        Created = DateTime.Now.AddHours(-3),
                                        CreatedBy = "job"

                                    });

                                }

                            }

                        }
                        logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed parcial FH:{DateTime.Now}, Dia procesado:{DateTime.Now.AddDays(-i)}");
                        if (config == null)
                        {
                            config = new ConfigJobMongo();
                            config.Tipo = "MachineStatusEvents";
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.CreatedBy = "Job";
                            config.Created = DateTime.Now.AddHours(-3);
                            await configJobRepository.InsertAsync(config);
                        }
                        else
                        {
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.Updated = DateTime.Now.AddHours(-3);
                            config.UpdatedBy = "Job";
                            await configJobRepository.UpdateAsync(config);
                        }
                        config = (await configJobRepository.ListAsync(builder2.Eq(widget => widget.Tipo, "MachineStatusEvents"))).FirstOrDefault();

                    }
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed final FH:{DateTime.Now} ");

                   
                }

            }
            catch (Exception e)
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }
        }
        private DateTime StringToDate(string date)
        {
           var dateTime =  new DateTime(
                Convert.ToInt32(date.Substring(0, 4)),
                Convert.ToInt32(date.Substring(4, 2)),
                Convert.ToInt32(date.Substring(6, 2))
                );
            return dateTime.Date + new TimeSpan(Convert.ToInt32(date.Substring(8, 2)), Convert.ToInt32(date.Substring(10, 2)), 00);
        }


    }
}

