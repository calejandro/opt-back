﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Commons.Services.Quartz;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Jobs.Eficiencia
{
    public class EquiposHistorico : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<EquiposHistorico> logger;

        public EquiposHistorico(IServiceProvider serviceProvider,ILogger<EquiposHistorico> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var machineStatusHistoricRepository = scope.ServiceProvider.GetService<IMachineStatusHistoricRepository>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();
                    var sapSkuRepository = scope.ServiceProvider.GetService<ISapSkuRepository>();
                    var quartzService = scope.ServiceProvider.GetService<IQuartzService>();


                    var builder = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EquiposHistorico"))).FirstOrDefault();
                    var turnoDia =  storeProcedures.GetDay(DateTime.Now);
                    var dias = 0;
                    if (config != null)
                    {
                        dias = (turnoDia.Fin.Date - config.Fecha.Date).Days;
                    }
                    else
                    {
                        var aux = new DateTime(DateTime.Now.Year, 1, 1);
                        dias = (DateTime.Now.Date - aux).Days;
                    }

                    for (int i = dias; i >= 0; i--)
                    {

                       var dia = DateTime.Now.AddDays(-i);
                       var turnos = storeProcedures.GetShifts(dia);
                        turnoDia = storeProcedures.GetDay(dia);
                        turnos.Add(turnoDia);
                        var lineas = await lineasProduccionUsuarioRepository.GetLineasUsuarioConTomas(turnoDia.Inicio, turnoDia.Fin);
                        var lineasGroup = lineas.GroupBy(x => x.IdLinea);
                        foreach (var linea in lineasGroup)
                        {
                            foreach (var turno in turnos)
                            {
                                var previousIdEquipo = 0;
                                MachineStatusHistoricMongo machineHistoric = null;
                                var machinesHistoric = new List<MachineStatusHistoricMongo>();
                                 var builder2 = Builders<MachineStatusHistoricMongo>.Filter;
                                var filter = builder2.Eq(widget => widget.IdLinea, linea.Key) &
                                    builder2.Eq(widget => widget.FechaInicio, turno.Inicio.AddHours(-3)) &
                                    builder2.Eq(widget => widget.FechaFin, turno.Fin.AddHours(-3));

                               var  machinesStatusHistoricMongo = await machineStatusHistoricRepository.ListAsync(filter);
                                if(machinesStatusHistoricMongo?.Count > 0)
                                {
                                    continue;
                                }
                                var machines = await storeProcedures.GetMachineStatusProductionOrders(linea.Key, turno.Inicio, turno.Fin);
                                var AveragesTimes = await storeProcedures.GetAverageTimeFailuresProduction(linea.Key, turno.Inicio, turno.Fin);
                                
                                foreach (var machine in machines)
                                {
                                    if (machine.IdEquipo != previousIdEquipo)
                                    {
                                        previousIdEquipo = machine.IdEquipo;
                                        if(machineHistoric != null)
                                        {

                                            machinesHistoric.Add(machineHistoric);
                                        }
                                        machineHistoric = new MachineStatusHistoricMongo();
                                        machineHistoric.IdLinea = machine.IdLinea;
                                        machineHistoric.DescripcionLinea = machine.DescripcionLinea;
                                        machineHistoric.IdOrdenSAP = machine.IdOrdenSAP;
                                        machineHistoric.DescripcionSKU = machine.DescripcionSKU;
                                        machineHistoric.IdEquipo = machine.IdEquipo;
                                        machineHistoric.DescEquipo = machine.DescEquipo;
                                        machineHistoric.IdSKU = machine.IdSKU;

                                        var sku = await sapSkuRepository.GetAsync(machine.IdSKU);
                                        machineHistoric.Marca = sku?.Marca;
                                        machineHistoric.Sabor = sku?.Sabor;
                                        machineHistoric.Formato = sku?.Formato;
                                        machineHistoric.SaborConsolid = sku?.Marca + " - " + sku?.Sabor;
                                        machineHistoric.FormatoConsolid = sku?.Marca + " - " + sku?.Sabor + " - " + sku?.Formato;

                                        machineHistoric.Turno = turno.Turno;
                                        machineHistoric.FechaInicio = turno.Inicio.AddHours(-3);
                                        machineHistoric.FechaFin = turno.Fin.AddHours(-3);
                                        machineHistoric.IdSupervisor = lineas.Find(x => x.IdLinea == linea.Key) == null ? 0: lineas.Find(x => x.IdLinea == linea.Key).UserId;
                                        machineHistoric.CreatedBy = "job";
                                        machineHistoric.Created = DateTime.Now.AddHours(-3);


                                        var AverageTime =  AveragesTimes.Find(x => x.IdEquipo == machine.IdEquipo);
                                        machineHistoric.TMEF = AverageTime?.TMEFTurno;
                                        machineHistoric.TMPR = AverageTime?.TMPRTurno;


                                        if (machine.IdEstado == 0)
                                        {
                                            machineHistoric.NoDeterminadoMA = machine.MinutosAcumulados;
                                            machineHistoric.NoDeterminadoMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                    }
                                    else
                                    {

                                        if (machine.IdEstado == 1)
                                        {
                                            machineHistoric.ParadaOperacionalMA = machine.MinutosAcumulados;
                                            machineHistoric.ParadaOperacionalMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 2)
                                        {
                                            machineHistoric.FueraProduccionMA = machine.MinutosAcumulados;
                                            machineHistoric.FueraProduccionMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 3)
                                        {
                                            machineHistoric.ProduccionMA = machine.MinutosAcumulados;
                                            machineHistoric.ProduccionMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 4)
                                        {
                                            machineHistoric.ParadaPropiaMA = machine.MinutosAcumulados;
                                            machineHistoric.ParadaPropiaMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 5)
                                        {
                                            machineHistoric.ParadaAlimentacionMA = machine.MinutosAcumulados;
                                            machineHistoric.ParadaAlimentacionMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 6)
                                        {
                                            machineHistoric.ParadaDescargaMA = machine.MinutosAcumulados;
                                            machineHistoric.ParadaDescargaMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 7)
                                        {
                                            machineHistoric.NoDeterminadoMA = machine.MinutosAcumulados;
                                            machineHistoric.NoDeterminadoMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                        if (machine.IdEstado == 8)
                                        {
                                            machineHistoric.SubVelocidadMA = machine.MinutosAcumulados;
                                            machineHistoric.SubVelocidadMAP = machine.MinutosAcumuladosPorcentaje;
                                        }
                                    }
                                }
                                if(machinesHistoric?.Count > 0)
                                await machineStatusHistoricRepository.InsertManyAsync(machinesHistoric);
                            }
                                
                        }
                        if (config == null)
                        {
                            config = new ConfigJobMongo();
                            config.Tipo = "EquiposHistorico";
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.CreatedBy = "Job";
                            config.Created = DateTime.Now.AddHours(-3);
                            await configJobRepository.InsertAsync(config);
                        }
                        else
                        {
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.Updated = DateTime.Now.AddHours(-3);
                            config.UpdatedBy = "Job";
                            await configJobRepository.UpdateAsync(config);
                        }
                        config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "EquiposHistorico"))).FirstOrDefault();
                    }



                }

            }
            catch (Exception e)
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }

        }
    }
}
