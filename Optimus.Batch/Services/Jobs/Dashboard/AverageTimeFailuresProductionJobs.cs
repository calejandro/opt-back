﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Repositories.Interfaces;
using Optimus.Batch.Services.Interfaces;

namespace Optimus.Batch.Jobs
{

    public class AverageTimeFailuresProductionJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        public AverageTimeFailuresProductionJobs(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var averageTimeFailuresProductionService = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionService>();
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var averageTimeFailuresProductionMongo = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionRepository>();


                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);


                    foreach (var linea in lineas)
                    {
                        await averageTimeFailuresProductionService.InsertAverageTimeFailuresProduction(linea.IdLinea, turnoDia);
                    }
                }
            }
            catch (Exception e )
            {
                throw e;
            }

        }
    }
}
