﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;

using Optimus.Batch.Services.Interfaces;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Optimus.Models.Views;
using System.Linq;

namespace Optimus.Batch.Jobs
{

    public class OffsetCorrectionJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<OffsetCorrectionJobs> logger;

        public OffsetCorrectionJobs(IServiceProvider serviceProvider, ILogger<OffsetCorrectionJobs> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed FH:{DateTime.Now}");
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var equiposEstadosRepository = scope.ServiceProvider.GetService<IEquiposEstadosRepository>();

                    var averageTimeFailuresProductionService = scope.ServiceProvider.GetService<IAverageTimeFailuresProductionService>();
                    var kpiDepletionService = scope.ServiceProvider.GetService<IKpiDepletionService>();
                    var machineStatusProductionOrderService = scope.ServiceProvider.GetService<IMachineStatusProductionOrderService>();
                    var produccionEficienciaByShiftService = scope.ServiceProvider.GetService<IProduccionEficienciaByShiftService>();
                    var produccionEficienciaService = scope.ServiceProvider.GetService<IProduccionEficienciaService>();
                    var timeStopsService = scope.ServiceProvider.GetService<ITimeStopsService>();
                    var configJobRepository = scope.ServiceProvider.GetService<IConfigJobRepository>();

                    var builder = Builders<ConfigJobMongo>.Filter;
                    ConfigJobMongo config = (await configJobRepository.ListAsync(builder.Eq(widget => widget.Tipo, "OffsetCorrectionJobs"))).FirstOrDefault();
                    var turnoDia = storeProcedures.GetCurrentDay(DateTime.Now);
                    var dias = 0;
                    if (config != null)
                    {
                        dias = (turnoDia.Fin.Date - config.Fecha.Date).Days;
                    }
                    else
                    {
                        dias = 5;
                    }
                    for (int i = dias; i >= 1; i--)
                    {
                        var turnoDiaEntero = storeProcedures.GetDay(DateTime.Now.AddDays(-i));
                        var turnos = storeProcedures.GetShifts(DateTime.Now.AddDays(-i));

                        turnos.Add(turnoDiaEntero);

                        var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoDiaEntero.Inicio, turnoDiaEntero.Fin.AddMinutes(1));
                        var estados = await equiposEstadosRepository.ListAsync(null, null, new EquiposEstadosFilter() { TipoEquipo = 1 });

                        foreach (var linea in lineas)
                        {
                            await produccionEficienciaByShiftService.InsertProduccionEficienciaByShift(linea.IdLinea, turnos);
                            await averageTimeFailuresProductionService.InsertAverageTimeFailuresProduction(linea.IdLinea, turnoDiaEntero);
                            await kpiDepletionService.InsertKpiDepletion(linea.IdLinea, turnos);
                            await machineStatusProductionOrderService.InsertMachineStatusProductionOrder(linea.IdLinea, turnoDiaEntero);
                            await produccionEficienciaService.InsertProduccionEficiencia(linea.IdLinea, turnos);
                            await timeStopsService.InsertTimeStops(linea.IdLinea, turnoDiaEntero, estados);
                        }

                        if (config == null)
                        {
                            await configJobRepository.InsertAsync(new ConfigJobMongo()
                            {
                                Tipo = "OffsetCorrectionJobs",
                                Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3),
                                Created = DateTime.Now.AddHours(-3),
                                CreatedBy = "Job Batch"
                            });
                        }
                        else
                        {
                            config.Fecha = DateTime.Now.AddDays(-i).Date.AddHours(-3);
                            config.Updated = DateTime.Now.AddHours(-3);
                            config.UpdatedBy = "Job Batch";
                            await configJobRepository.UpdateAsync(config);
                        }
                        logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed Parcial FH:{DateTime.Now}");
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw e;
            }

        }
    }
}
