﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Repositories.Interfaces;
using Optimus.Batch.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace Optimus.Batch.Jobs
{

    public class ProduccionEficienciaByShiftJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<ProduccionEficienciaByShiftJobs> logger;

        public ProduccionEficienciaByShiftJobs(IServiceProvider serviceProvider, ILogger<ProduccionEficienciaByShiftJobs> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var produccionEficienciaByShiftService = scope.ServiceProvider.GetService<IProduccionEficienciaByShiftService>();

                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var turnos = storeProcedures.GetShifts(DateTime.Now);
                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    turnos.Add(turnoDia);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);

                    foreach (var linea in lineas)
                    {
                       await produccionEficienciaByShiftService.InsertProduccionEficienciaByShift(linea.IdLinea, turnos);
                    }
                }
            }
            catch (Exception e )
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }
           
        }
    }
}
