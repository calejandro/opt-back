﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Repositories.Filers;
using Microsoft.Extensions.Logging;
using Optimus.Batch.Services.Interfaces;


namespace Optimus.Batch.Jobs
{

    public class TimeStopsJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<TimeStopsJobs> logger;

        public TimeStopsJobs(IServiceProvider serviceProvider, ILogger<TimeStopsJobs> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var equiposEstadosRepository = scope.ServiceProvider.GetService<IEquiposEstadosRepository>();
                    var timeStopsService = scope.ServiceProvider.GetService<ITimeStopsService>();

                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);
                    var estados = await equiposEstadosRepository.ListAsync(null, null, new EquiposEstadosFilter() { TipoEquipo = 1 });



                    foreach (var linea in lineas)
                    {
                        await timeStopsService.InsertTimeStops(linea.IdLinea, turnoDia, estados);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }

        }
    }
}
