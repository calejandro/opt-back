﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Threading.Tasks;
using Optimus.Repositories.Interfaces;
using Optimus.Batch.Services.Interfaces;

namespace Optimus.Batch.Jobs
{

    public class KpiDepletionJobs : IJob
    {
        private readonly IServiceProvider serviceProvider;
        public KpiDepletionJobs(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProcedures = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var kpiDepletionService = scope.ServiceProvider.GetService<IKpiDepletionService>();

                    var turnos = storeProcedures.GetShifts(DateTime.Now);
                    var turnoDia = storeProcedures.GetDay(DateTime.Now);
                    var turnoActual = storeProcedures.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(turnoActual.Inicio, turnoActual.Fin);
                    turnos.Add(turnoDia);
                    foreach (var linea in lineas)
                    {
                        await kpiDepletionService.InsertKpiDepletion(linea.IdLinea, turnos);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

        }
    }
}
