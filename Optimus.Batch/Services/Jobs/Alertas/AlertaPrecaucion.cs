﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Quartz;

namespace Optimus.Batch.Services.Jobs
{
    public class AlertaPrecaucion : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<AlertaPrecaucion> logger;

        public AlertaPrecaucion(IServiceProvider serviceProvider, ILogger<AlertaPrecaucion> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                    logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                    var storeProceduresRepository = scope.ServiceProvider.GetService<IStoreProceduresRepository>();
                    var lineasProduccionAlertasRepository = scope.ServiceProvider.GetService<ILineasProduccionAlertasRepository2>();
                    var configuracionUmbralesAlertasPrecaucionRepository = scope.ServiceProvider.GetService<IConfiguracionUmbralesAlertasPrecaucionRepository>();
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();
                    var appMobStatusActualAcumulado = scope.ServiceProvider.GetService<IAPPMobStatusActualAcumuladoRepository>();

                    var shift = storeProceduresRepository.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(shift.Inicio, shift.Fin);

                    foreach (var linea in lineas)
                    {
                        var listStatusActual = await storeProceduresRepository.GetAPPMobStatusActual(linea.IdLinea);
                        var alertas = await lineasProduccionAlertasRepository.ListAsync(null, null, new LineaProduccionAlertaFilter
                        {
                            IdLinea = linea.IdLinea,
                            Clasificacion = ClasificacionAlertaEnum.Precaucion,
                            FechaInicio = shift.Inicio
                        }, null, true);


                        foreach (var statusActual in listStatusActual)
                        {
                            var alerta = alertas.Find(x => x.IdEquipo == statusActual.IdEquipo && (x.Estado == EstadosLPAEnum.Nueva || x.Estado == EstadosLPAEnum.Informada));
                            var umbralEquipo =( await configuracionUmbralesAlertasPrecaucionRepository.ListAsync(null, null, new CommonFilter { IdLinea = linea.IdLinea,IdEquipo = statusActual.IdEquipo })).FirstOrDefault();
                            var statusActualAcum = await appMobStatusActualAcumulado.GetAsync(null, new CommonFilter { IdLinea = linea.IdLinea, IdEquipo = statusActual.IdEquipo });
                            var acumEstadoPrecaucion = 0;
                            var acumEstadoNoPrecaucion = 0;
                            var acumEstadoAnterior = 0;

                            if (umbralEquipo == null)
                            {
                                // cargamos datos predefinidos
                                umbralEquipo = new ConfiguracionUmbralesAlertasPrecaucion();
                                umbralEquipo.IdPlanta = linea.IdPlanta;
                                umbralEquipo.IdLinea = linea.IdLinea;
                                umbralEquipo.IdEquipo = statusActual.IdEquipo;
                                umbralEquipo.TipoAlerta = TipoAlertaParadaEnum.Atencion;
                                umbralEquipo.ValorMinAlerta = 60;
                                umbralEquipo.ValorMinSalirAlerta = 10;
                                await configuracionUmbralesAlertasPrecaucionRepository.InsertAsync(umbralEquipo);

                            }

                            if (statusActual.IdEstado != AppmobStatusEquiposEnum.Produccion
                               && statusActual.IdEstado != AppmobStatusEquiposEnum.FueraProduccion
                               && statusActual.IdEstado != AppmobStatusEquiposEnum.NoDeterminado
                               && statusActual.IdEstado != AppmobStatusEquiposEnum.SubVelocidad)
                            {
                                if (statusActualAcum != null)
                                {
                                    if (statusActualAcum.IdEstadoActual != statusActual.IdEstado)
                                        acumEstadoPrecaucion = statusActualAcum.AcumEstadoPrecaucion + statusActual.MinutosAcumulados;
                                    else
                                        acumEstadoPrecaucion = (statusActualAcum.AcumEstadoPrecaucion - statusActualAcum.AcumEstadoAnterior) + statusActual.MinutosAcumulados;
                                }
                                else
                                {
                                    acumEstadoPrecaucion = statusActual.MinutosAcumulados;
                                }
                                acumEstadoAnterior = statusActual.MinutosAcumulados;
                            }
                            else
                            {
                                if (statusActualAcum != null)
                                {
                                    if (statusActualAcum.IdEstadoActual != statusActual.IdEstado)
                                        acumEstadoNoPrecaucion = statusActualAcum.AcumEstadoPrecaucion + statusActual.MinutosAcumulados;
                                    else
                                        acumEstadoNoPrecaucion = (statusActualAcum.AcumEstadoPrecaucion - statusActualAcum.AcumEstadoAnterior) + statusActual.MinutosAcumulados;
                                }
                                else
                                {
                                    acumEstadoNoPrecaucion = statusActual.MinutosAcumulados;
                                }
                                acumEstadoAnterior = statusActual.MinutosAcumulados;
                            }

                            // actualizar APPMobStatusActualAcumulado
                            if (statusActualAcum != null)
                            {
                                statusActualAcum.IdEstadoActual = statusActual.IdEstado;
                                statusActualAcum.AcumEstadoPrecaucion = acumEstadoPrecaucion;
                                statusActualAcum.AcumEstadoNoPrecaucion = acumEstadoNoPrecaucion;
                                statusActualAcum.AcumEstadoAnterior = acumEstadoAnterior;
                                await appMobStatusActualAcumulado.UpdateAsync(statusActualAcum);
                            }
                            else
                            {
                                var newStatusActualAcum = new APPMobStatusActualAcumulado();
                                newStatusActualAcum.IdPlanta = linea.IdPlanta;
                                newStatusActualAcum.IdLinea = statusActual.IdLinea;
                                newStatusActualAcum.IdEquipo = statusActual.IdEquipo;
                                newStatusActualAcum.IdEstadoActual = statusActual.IdEstado;
                                newStatusActualAcum.AcumEstadoPrecaucion = acumEstadoPrecaucion;
                                newStatusActualAcum.AcumEstadoNoPrecaucion = acumEstadoNoPrecaucion;
                                newStatusActualAcum.AcumEstadoAnterior = acumEstadoAnterior;
                                await appMobStatusActualAcumulado.InsertAsync(newStatusActualAcum);
                            }

                            // actualizar alerta
                            if (acumEstadoPrecaucion > umbralEquipo.ValorMinAlerta)
                            {
                                if (alerta != null)
                                {
                                    //paso de OK a Atencion
                                    alerta.TipoAlerta = TipoAlertaParadaEnum.Atencion;
                                    alerta.FechaActualizacion = DateTime.Now;
                                    alerta.FechaFin = null;
                                    alerta.ValorAcumulado = acumEstadoPrecaucion;
                                }
                                else
                                {
                                    var newAlerta = new LineasProduccionAlertas2();
                                    newAlerta.IdLinea = linea.IdLinea;
                                    newAlerta.IdEquipo = statusActual.IdEquipo;
                                    newAlerta.Estado = EstadosLPAEnum.Nueva;
                                    newAlerta.ValorAcumulado = acumEstadoPrecaucion;
                                    newAlerta.FechaCreacion = DateTime.Now;
                                    newAlerta.Borrado = false;
                                    newAlerta.TipoAlerta = umbralEquipo.TipoAlerta;
                                    newAlerta.Clasificacion = ClasificacionAlertaEnum.Precaucion;
                                    await lineasProduccionAlertasRepository.InsertAsync(newAlerta);
                                }
                            }
                            else if (acumEstadoNoPrecaucion > umbralEquipo.ValorMinSalirAlerta)
                            {
                                if (alerta != null)
                                {
                                    if(alerta.Estado == EstadosLPAEnum.Nueva)
                                    {
                                        //paso de Atencion a OK
                                        alerta.TipoAlerta = TipoAlertaParadaEnum.Ok;
                                        alerta.FechaActualizacion = DateTime.Now;
                                        alerta.FechaFin = DateTime.Now;
                                        alerta.ValorAcumulado = acumEstadoNoPrecaucion;
                                    }
                                    else
                                    {
                                        //pasa a cerrada 
                                        //alerta.TipoAlerta = TipoAlertaParadaEnum.Ok;
                                        alerta.FechaActualizacion = DateTime.Now;
                                        alerta.FechaFin = DateTime.Now;
                                        alerta.ValorAcumulado = acumEstadoNoPrecaucion;
                                        alerta.Estado = EstadosLPAEnum.Cerrada;
                                    }

                                }
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }
        }

    }
}

