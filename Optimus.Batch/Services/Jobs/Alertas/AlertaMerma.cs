﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Quartz;

namespace Optimus.Batch.Services.Jobs
{
    public class AlertaMerma : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<AlertaMerma> logger;

        public AlertaMerma(IServiceProvider serviceProvider, ILogger<AlertaMerma> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;

        }
        public async Task Execute(IJobExecutionContext context)
        {
            logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
            logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");
            try
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var storeProceduresRepository = scope.ServiceProvider.GetService<IStoreProceduresRepository>(); ;
                    var lineasProduccionAlertasRepository = scope.ServiceProvider.GetService<ILineasProduccionAlertasRepository2>(); ;
                    var configuracionUmbralesTmefTmprRepository = scope.ServiceProvider.GetService<IConfiguracionUmbralesTmefTmprRepository>(); ;
                    var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>(); ;


                    var shift = storeProceduresRepository.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(shift.Inicio, shift.Fin);

                    foreach (var idLinea in lineas.Select(x => x.IdLinea))
                    {
                        DateTime fechaInicio, fechafin;
                        calculateDates(out fechaInicio, out fechafin);

                        var listKpiRoturaEnvases = await storeProceduresRepository.GetAPPMobKpiRoturaEnvases(idLinea, fechaInicio, fechafin);
                        var umbrales = await configuracionUmbralesTmefTmprRepository.ListAsync(null, null, new CommonFilter { IdLinea = idLinea });
                        var alertas = await lineasProduccionAlertasRepository.ListAsync(null, null, new LineaProduccionAlertaFilter
                        {
                            IdLinea = idLinea,
                            Clasificacion = ClasificacionAlertaEnum.Merma,
                            FechaInicio = shift.Inicio
                        }, null, true);

                        // SANTI: por base de datos se nota que siempre se crea una nueva alerta- Proponer que sea una alerta por turno 
                        // porque fecha fin = fecha creacion 


                       foreach (var KpiRoturaEnvases in listKpiRoturaEnvases)
                        {
                            var alerta = alertas.Find(x => x.IdEquipo == KpiRoturaEnvases.IdEquipo && (x.Estado == EstadosLPAEnum.Nueva || x.Estado == EstadosLPAEnum.Informada));
                            var umbralMerma = umbrales.Find(x => x.IdEquipo == KpiRoturaEnvases.IdEquipo && x.Tipo == TiempoPromTipoReparacionEnum.Tmef);

                            TipoAlertaParadaEnum TipoAlerta = TipoAlertaParadaEnum.Ok;
                            if (KpiRoturaEnvases.KPI > Convert.ToDecimal(umbralMerma.ValorMinimo) && KpiRoturaEnvases.KPI < Convert.ToDecimal(umbralMerma.ValorMaximo))
                            {
                                TipoAlerta = TipoAlertaParadaEnum.Peligro;
                            }
                            else if (KpiRoturaEnvases.KPI > Convert.ToDecimal(umbralMerma.ValorMaximo))
                            {
                                TipoAlerta = TipoAlertaParadaEnum.Fallo;
                            }

                            //if (alerta == null)
                            //{
                                var newAlerta = new LineasProduccionAlertas2();
                                newAlerta.IdLinea = idLinea;
                                newAlerta.IdEquipo = KpiRoturaEnvases.IdEquipo;
                                newAlerta.Estado = EstadosLPAEnum.Nueva;
                                newAlerta.ValorAcumulado = Convert.ToDouble(KpiRoturaEnvases.KPI);
                                newAlerta.FechaCreacion = DateTime.Now;
                                newAlerta.Borrado = false;
                                newAlerta.TipoAlerta = TipoAlerta;
                                newAlerta.Clasificacion = ClasificacionAlertaEnum.Merma;
                                await lineasProduccionAlertasRepository.InsertAsync(newAlerta);
                            //}
                            //else
                            //{
                            //    alerta.FechaActualizacion = DateTime.Now;
                            //    alerta.FechaFin = null;
                            //    alerta.ValorAcumulado = Convert.ToDouble(KpiRoturaEnvases.KPI);
                            //    alerta.TipoAlerta = TipoAlerta;
                            //}
                        }

                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                throw;
            }
        }

        private static void calculateDates(out DateTime fechaInicio, out DateTime fechafin)
        {
            //calculo de fechas 
            var fecha = DateTime.Now;
            fechaInicio = fecha;
            fechafin = fecha;
            TimeSpan oclock = new TimeSpan(fecha.Hour, 00, 0);
            TimeSpan half = new TimeSpan(fecha.Hour, 30, 0);
            if (fecha.Minute > 36)
            {
                fechaInicio = fechaInicio.Date + half;
                fechafin = (fechafin.Date + oclock).AddHours(1);
            }
            else
            {
                fechaInicio = fechaInicio.Date + oclock;
                fechafin = fechafin.Date + half;
            }
        }
    }
}

