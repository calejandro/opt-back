﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Optimus.Models.Entities;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Quartz;


namespace Optimus.Batch.Jobs
{
    public class AlertaParadaPropia : IJob
    {

        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<AlertaParadaPropia> logger;

        public AlertaParadaPropia(IServiceProvider serviceProvider, ILogger<AlertaParadaPropia> logger)

        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;

        }
        public async Task Execute(IJobExecutionContext context)
        {
            using (var scope = this.serviceProvider.CreateScope())
            {
                logger.LogInformation($"Job {context.JobDetail.Key.Name} Executed");
                logger.LogInformation($"Job Type {context.JobDetail.JobType.ToString()}");

                var storeProceduresRepository = scope.ServiceProvider.GetService<IStoreProceduresRepository>(); ;
                var lineasProduccionAlertasRepository = scope.ServiceProvider.GetService<ILineasProduccionAlertasRepository2>(); ;
                var configuracionAlertasParadaMaquinaRepository = scope.ServiceProvider.GetService<IConfiguracionAlertasParadaMaquinaRepository>(); ;
                var lineasProduccionUsuarioRepository = scope.ServiceProvider.GetService<ILineasProduccionUsuarioRepository>();

                try
                {
                    var shift = storeProceduresRepository.GetCurrentShift(DateTime.Now);
                    var lineas = await lineasProduccionUsuarioRepository.GetLineasConTomas(shift.Inicio, shift.Fin);

                    foreach (var idLinea in lineas.Select(x => x.IdLinea))
                    {
                        var listStatusActual = await storeProceduresRepository.GetAPPMobStatusActual(idLinea);
                        var umbrales = await configuracionAlertasParadaMaquinaRepository.ListAsync(null, null, new ConfigAlertasParadaMaqFilter { IdLinea = idLinea });
                        var alertas = await lineasProduccionAlertasRepository.ListAsync(null, null, new LineaProduccionAlertaFilter
                        {
                            IdLinea = idLinea,
                            Clasificacion = ClasificacionAlertaEnum.ParadaPropia,
                            FechaInicio = shift.Inicio
                        }, null, true);

                        foreach (var StatusActual in listStatusActual)
                        {
                            var alerta = alertas.Find(x => x.IdEquipo == StatusActual.IdEquipo && (x.Estado == EstadosLPAEnum.Nueva || x.Estado == EstadosLPAEnum.Informada));
                            var umbralesEquipo = umbrales.FindAll(x => x.IdEquipo == StatusActual.IdEquipo);
                            if (alerta != null)
                            {
                                if (StatusActual.IdEstado == AppmobStatusEquiposEnum.ParadaPropia) // 4 = parada propia
                                {
                                    // sige en parada propia 
                                    //modifico la alerta segun el umbral 

                                    foreach (var umbral in umbralesEquipo)
                                    {
                                        if (StatusActual.MinutosAcumulados > umbral.ValorMinimo && StatusActual.MinutosAcumulados < umbral.ValorMaximo)
                                        {
                                            alerta.FechaActualizacion = DateTime.Now;
                                            alerta.TipoAlerta = umbral.TipoAlerta;
                                            alerta.ValorAcumulado = StatusActual.MinutosAcumulados;
                                            alerta.FechaFin = null;
                                            await lineasProduccionAlertasRepository.UpdateAsync(alerta);
                                            break;
                                        }
                                    }

                                }
                                else
                                {
                                    var unbralOK = umbralesEquipo.Find(x => x.TipoAlerta == TipoAlertaParadaEnum.Ok);
                                    if (StatusActual.MinutosDesdeUltimaParadaPropia > unbralOK.ValorMaximo)
                                    {
                                        // dejo de estar en parada propia el tiempo suficiente 
                                        alerta.FechaActualizacion = DateTime.Now;
                                        alerta.FechaFin = DateTime.Now;
                                        alerta.ValorAcumulado = StatusActual.MinutosAcumulados;

                                        if (alerta.Estado == EstadosLPAEnum.Nueva)
                                        {
                                            // cambiar el tipo de alerta a 2                                  
                                            alerta.TipoAlerta = TipoAlertaParadaEnum.Ok;
                                            await lineasProduccionAlertasRepository.UpdateAsync(alerta);
                                        }
                                        else
                                        {
                                            // si esta en estado Informada se pasa a Estado cerrado
                                            alerta.Estado = EstadosLPAEnum.Cerrada;
                                            await lineasProduccionAlertasRepository.UpdateAsync(alerta);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (StatusActual.IdEstado == AppmobStatusEquiposEnum.ParadaPropia) // 4 = parada propia
                                {
                                    // la alerta no existe por lo que creo la alerta a partir del umbral 
                                    foreach (var umbral in umbralesEquipo)
                                    {
                                        if (StatusActual.MinutosAcumulados > umbral.ValorMinimo && StatusActual.MinutosAcumulados < umbral.ValorMaximo)
                                        {
                                            var newAlerta = new LineasProduccionAlertas2();
                                            newAlerta.IdLinea = idLinea;
                                            newAlerta.IdEquipo = StatusActual.IdEquipo;
                                            newAlerta.Estado = EstadosLPAEnum.Nueva;
                                            newAlerta.ValorAcumulado = StatusActual.MinutosAcumulados;
                                            newAlerta.FechaCreacion = DateTime.Now;
                                            newAlerta.Borrado = false;
                                            newAlerta.TipoAlerta = umbral.TipoAlerta;
                                            newAlerta.Clasificacion = ClasificacionAlertaEnum.ParadaPropia;
                                            await lineasProduccionAlertasRepository.InsertAsync(newAlerta);
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
                catch (Exception e)
                {
                    logger.LogError($"Job {context.JobDetail.Key.Name} Executed: Error {e.Message}");
                    throw;
                }
            }
        }
    }
}
