﻿using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Filers;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Interfaces
{
    public interface IProduccionEficienciaService
    {
        Task InsertProduccionEficiencia(int IdLinea, List<Shift> turnos);
    }
}
