﻿
using Optimus.Models.Views;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Interfaces
{
    public interface IAverageTimeFailuresProductionService
    {
        Task InsertAverageTimeFailuresProduction(int IdLinea, Shift turno);
    }
}
