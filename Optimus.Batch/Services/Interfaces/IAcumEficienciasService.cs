﻿using Optimus.Models.Enum;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Interfaces
{
    public interface IAcumEficienciasService
    {
        Task<AcumEficienciasViewModels> GetAcumEficienciaDiariaMensulAnual(int idLinia, DateTime date);
        Task<AcumEficienciasPorPeriodoViewModels> GetAcumEficienciaPorPeriodo(int idLinia, DateTime date, PeriodEfficiencyEnum periodo);

    }
}
