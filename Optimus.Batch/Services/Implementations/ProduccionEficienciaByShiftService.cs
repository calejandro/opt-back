﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Batch.Services.Interfaces;
using Optimus.Batch.Services.SignalR;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Implementations
{
    public class ProduccionEficienciaByShiftService : IProduccionEficienciaByShiftService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository;
        private readonly IStoreProceduresRepository storeProcedures;
        public ProduccionEficienciaByShiftService(IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresRepository storeProcedures)
        {
            this.produccionEficienciaByShiftRepository = produccionEficienciaByShiftRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertProduccionEficienciaByShift(int IdLinea, List<Shift> turnos)
        {
            try
            {
                List<ProduccionEficienciaByShiftMongo> eficienciasMongo = new List<ProduccionEficienciaByShiftMongo>();
                foreach (var turno in turnos)
                {
                    ProduccionEficienciaByShiftMongo eficienciaMongo = new ProduccionEficienciaByShiftMongo();
                    if (turno.Paso)
                    {
                        List<ProduccionEficienciaByShift> eficiencia = await storeProcedures.GetEfficiencyProductionByShift(IdLinea, turno.Inicio, turno.Fin);
                        // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                        //filtros
                        var builder = Builders<ProduccionEficienciaByShiftMongo>.Filter;
                        var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                            builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio.AddHours(-3)) &
                            builder.Eq(widget => widget.FechaHoraFin, turno.Fin.AddHours(-3));
                        eficienciaMongo = await produccionEficienciaByShiftRepository.GetAsync(filter);

                        if (eficienciaMongo != null)
                        {
                            eficienciaMongo.ProduccionEficienciaByShift = eficiencia;
                            eficienciaMongo.Updated = DateTime.Now.AddHours(-3);
                            eficienciaMongo.UpdatedBy = "job";
                            eficienciaMongo.Turno = turno.Turno;
                            await produccionEficienciaByShiftRepository.UpdateAsync(eficienciaMongo);
                        }
                        else
                        {

                            await produccionEficienciaByShiftRepository.InsertAsync(new ProduccionEficienciaByShiftMongo()
                            {
                                IdLinea = IdLinea,
                                FechaHoraInicio = turno.Inicio.AddHours(-3),
                                FechaHoraFin = turno.Fin.AddHours(-3),
                                Turno = turno.Turno,
                                ProduccionEficienciaByShift = eficiencia,
                                Created = DateTime.Now.AddHours(-3),
                                CreatedBy = "job"
                            });
                            eficienciaMongo = await produccionEficienciaByShiftRepository.GetAsync(filter);
                        }
                    }
                    eficienciasMongo.Add(eficienciaMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("EficienciaByShift", eficienciasMongo);
            }
            catch (Exception e)
            {

                throw;
            }


        }
    }
}
