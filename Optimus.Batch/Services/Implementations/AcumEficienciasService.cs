﻿using MongoDB.Driver;
using Optimus.Batch.Services.Interfaces.Mongo;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Implementations.Mongo
{
    public class AcumEficienciasService : IAcumEficienciasService
    {
        private readonly IAcumEficienciaRepository acumEficienciaRepository;
        public AcumEficienciasService(IAcumEficienciaRepository acumEficienciaRepository)
        {
            this.acumEficienciaRepository = acumEficienciaRepository;
        }

        public async Task<AcumEficienciasViewModels> GetAcumEficienciaDiariaMensulAnual(int idLinia, DateTime date)
        {
            try
            {
                var dateAux = date.Date;

                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1) ;
                firstDayOfMonth = firstDayOfMonth.Date;
                var firstDayOfYear = new DateTime(date.Year, 1, 1);
                firstDayOfYear = firstDayOfYear.Date;
                var acumEficienciaMongo = new AcumEficienciasViewModels();
                var builder = Builders<AcumEficienciaMongo>.Filter;
                var filter = builder.Eq(widget => widget.Fecha, dateAux)
                   & builder.Eq(widget => widget.IdLinea, idLinia)
                   & builder.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Daily);

                acumEficienciaMongo.EficienciaDiaria = (await acumEficienciaRepository.GetAsync(filter))?.Eficiencia;

                builder = Builders<AcumEficienciaMongo>.Filter;
                filter = builder.Eq(widget => widget.Fecha, firstDayOfMonth)
                   & builder.Eq(widget => widget.IdLinea, idLinia)
                   & builder.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Monthly);
                acumEficienciaMongo.EficienciaMensual = (await acumEficienciaRepository.GetAsync(filter))?.Eficiencia;

                builder = Builders<AcumEficienciaMongo>.Filter;
                filter = builder.Eq(widget => widget.Fecha, firstDayOfYear)
                   & builder.Eq(widget => widget.IdLinea, idLinia)
                   & builder.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Annual);
                acumEficienciaMongo.EficienciaAnual = (await acumEficienciaRepository.GetAsync(filter))?.Eficiencia;

                return acumEficienciaMongo;
            }
            catch (Exception e)
            {

                throw;
            }
            

        }
        public async Task<AcumEficienciasPorPeriodoViewModels> GetAcumEficienciaPorPeriodo(int idLinia, DateTime date, PeriodEfficiencyEnum periodo)
        {
            try
            {
                var dateAux = date;
                switch (periodo)
                {
                    case PeriodEfficiencyEnum.Daily:
                        dateAux = date.Date; //+ new TimeSpan(3, 00, 0);
                        break;
                    case PeriodEfficiencyEnum.Monthly:
                        dateAux = new DateTime(date.Year, date.Month, 1);
                        dateAux = dateAux.Date; //+ new TimeSpan(3, 00, 0);
                        break;
                    case PeriodEfficiencyEnum.Annual:
                        dateAux = new DateTime(date.Year, 1, 1);
                        dateAux = dateAux.Date; //+ new TimeSpan(3, 00, 0);
                        break;
                    default:
                        break;
                }

            


                var acumEficienciaMongo = new AcumEficienciasPorPeriodoViewModels();
                var builder = Builders<AcumEficienciaMongo>.Filter;
                var  filter = builder.Eq(widget => widget.Fecha, dateAux)
                   & builder.Eq(widget => widget.IdLinea, idLinia)
                   & builder.Eq(widget => widget.PeriodoEficiencia, periodo);

                acumEficienciaMongo.Eficiencia = (await acumEficienciaRepository.GetAsync(filter))?.Eficiencia;
                acumEficienciaMongo.PeriodoEficiencia = periodo;
                acumEficienciaMongo.IdLinea = idLinia;
                acumEficienciaMongo.Fecha = dateAux;



                return acumEficienciaMongo;
            }
            catch (Exception e)
            {

                throw;
            }


        }

    }
}
