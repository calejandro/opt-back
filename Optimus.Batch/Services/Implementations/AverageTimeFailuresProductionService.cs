﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Batch.Services.Interfaces;
using Optimus.Batch.Services.SignalR;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Implementations
{
    public class AverageTimeFailuresProductionService : IAverageTimeFailuresProductionService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository;
        private readonly IStoreProceduresRepository storeProcedures;
        public AverageTimeFailuresProductionService(IAverageTimeFailuresProductionRepository averageTimeFailuresProductionRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresRepository storeProcedures)
        {
            this.averageTimeFailuresProductionRepository = averageTimeFailuresProductionRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertAverageTimeFailuresProduction(int IdLinea, Shift turno)
        {
            try
            {
                List<AverageTimeFailuresProduction> eficiencia = await storeProcedures.GetAverageTimeFailuresProduction(IdLinea, turno.Inicio, turno.Fin);

                // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                //filtros
                var builder = Builders<AverageTimeFailuresProductionMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio.AddHours(-3)) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin.AddHours(-3));

                AverageTimeFailuresProductionMongo eficienciaMongo = await averageTimeFailuresProductionRepository.GetAsync(filter);

                if (eficienciaMongo != null)
                {
                    eficienciaMongo.AverageTimeFailuresProduction = eficiencia;
                    eficienciaMongo.Updated = DateTime.Now.AddHours(-3);
                    eficienciaMongo.UpdatedBy = "job";
                    await averageTimeFailuresProductionRepository.UpdateAsync(eficienciaMongo);
                }
                else
                {

                    await averageTimeFailuresProductionRepository.InsertAsync(new AverageTimeFailuresProductionMongo()
                    {
                        IdLinea = IdLinea,
                        FechaHoraInicio = turno.Inicio.AddHours(-3),
                        FechaHoraFin = turno.Fin.AddHours(-3),
                        Turno = turno.Turno,
                        AverageTimeFailuresProduction = eficiencia,
                        Created = DateTime.Now.AddHours(-3),
                        CreatedBy = "job"
                    });
                    eficienciaMongo = await averageTimeFailuresProductionRepository.GetAsync(filter);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("AverageTime", eficienciaMongo);
            }
            catch (Exception e)
            {
                throw;
            }
              
        }
    }
}
