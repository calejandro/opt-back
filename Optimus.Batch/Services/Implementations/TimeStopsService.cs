﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Batch.Services.Interfaces;
using Optimus.Batch.Services.SignalR;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Implementations
{
    public class TimeStopsService : ITimeStopsService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly ITimeStopsRepository timeStopsRepository;
        private readonly IStoreProceduresRepository storeProcedures;
        public TimeStopsService(ITimeStopsRepository timeStopsRepository, IHubContext<OptimusHub> hubContext, IStoreProceduresRepository storeProcedures, IEquiposEstadosRepository equiposEstadosRepository)
        {
            this.timeStopsRepository = timeStopsRepository;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertTimeStops(int IdLinea, Shift turno, List<EquiposEstados> estados)
        {
            try
            {
                List<TimeStopsMongo> timesStopsMongo = new List<TimeStopsMongo>();
                foreach (var estado in estados)
                {
                    TimeStopsMongo timeStopsMongo = new TimeStopsMongo();
                    List<TimeStops> timeStops = await storeProcedures.GetTimeStops(IdLinea, turno.Inicio, turno.Fin, estado.Id);

                    // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                    //filtros
                    var builder = Builders<TimeStopsMongo>.Filter;
                    var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                        builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio.AddHours(-3)) &
                        builder.Eq(widget => widget.FechaHoraFin, turno.Fin.AddHours(-3)) &
                        builder.Eq(widget => widget.IdEstado, estado.Id);

                    timeStopsMongo = await timeStopsRepository.GetAsync(filter);

                    if (timeStopsMongo != null)
                    {
                        timeStopsMongo.TimeStops = timeStops;
                        timeStopsMongo.Updated = DateTime.Now.AddHours(-3);
                        timeStopsMongo.UpdatedBy = "job";
                        await timeStopsRepository.UpdateAsync(timeStopsMongo);
                    }
                    else
                    {

                        await timeStopsRepository.InsertAsync(new TimeStopsMongo()
                        {
                            IdLinea = IdLinea,
                            FechaHoraInicio = turno.Inicio.AddHours(-3),
                            FechaHoraFin = turno.Fin.AddHours(-3),
                            Turno = turno.Turno,
                            IdEstado = estado.Id,
                            TimeStops = timeStops,
                            Created = DateTime.Now.AddHours(-3),
                            CreatedBy = "job"
                        });
                        timeStopsMongo = await timeStopsRepository.GetAsync(filter);
                    }
                    timesStopsMongo.Add(timeStopsMongo);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("TimeStops", timesStopsMongo);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
