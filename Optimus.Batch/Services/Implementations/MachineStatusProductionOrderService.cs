﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using Optimus.Batch.Services.Interfaces;
using Optimus.Batch.Services.SignalR;
using Optimus.Models.Entities;
using Optimus.Models.Views;
using Optimus.Repositories.Interfaces;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Batch.Services.Implementations
{
    public class MachineStatusProductionOrderService : IMachineStatusProductionOrderService
    {
        private readonly IHubContext<OptimusHub> hubContext;
        private readonly IMachineStatusProductionOrderRepository machineStatusProductionOrderMongo;
        private readonly IStoreProceduresRepository storeProcedures;
        public MachineStatusProductionOrderService(IMachineStatusProductionOrderRepository machineStatusProductionOrderMongo, IHubContext<OptimusHub> hubContext, IStoreProceduresRepository storeProcedures)
        {
            this.machineStatusProductionOrderMongo = machineStatusProductionOrderMongo;
            this.hubContext = hubContext;
            this.storeProcedures = storeProcedures;
        }
        public async Task InsertMachineStatusProductionOrder(int IdLinea, Shift turno)
        {
            try
            {
                List<MachineStatusProductionOrder> eficiencia = await storeProcedures.GetMachineStatusProductionOrders(IdLinea, turno.Inicio, turno.Fin);

                // busco en mongo si ya esta creada la colecion con esa linia para ese turno 
                //filtros
                var builder = Builders<MachineStatusProductionOrderMongo>.Filter;
                var filter = builder.Eq(widget => widget.IdLinea, IdLinea) &
                    builder.Eq(widget => widget.FechaHoraInicio, turno.Inicio.AddHours(-3)) &
                    builder.Eq(widget => widget.FechaHoraFin, turno.Fin.AddHours(-3));

                MachineStatusProductionOrderMongo eficienciaMongo = await machineStatusProductionOrderMongo.GetAsync(filter);

                if (eficienciaMongo != null)
                {
                    eficienciaMongo.MachineStatusProductionOrder = eficiencia;
                    eficienciaMongo.Updated = DateTime.Now.AddHours(-3);
                    eficienciaMongo.UpdatedBy = "job";
                    await machineStatusProductionOrderMongo.UpdateAsync(eficienciaMongo);
                }
                else
                {

                    await machineStatusProductionOrderMongo.InsertAsync(new MachineStatusProductionOrderMongo()
                    {
                        IdLinea = IdLinea,
                        FechaHoraInicio = turno.Inicio.AddHours(-3),
                        FechaHoraFin = turno.Fin.AddHours(-3),
                        Turno = turno.Turno,
                        MachineStatusProductionOrder = eficiencia,
                        Created = DateTime.Now.AddHours(-3),
                        CreatedBy = "job"
                    });
                    eficienciaMongo = await machineStatusProductionOrderMongo.GetAsync(filter);
                }
                await hubContext.Clients.Group(IdLinea.ToString()).SendAsync("MachineStatusProductionOrder", eficienciaMongo);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
