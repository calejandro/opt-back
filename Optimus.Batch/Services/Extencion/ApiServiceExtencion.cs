﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Optimus.Batch.Jobs;
using Optimus.Batch.Services.Implementations;
using Optimus.Batch.Services.Interfaces;
using Optimus.Batch.Services.Jobs;
using Optimus.Batch.Services.Jobs.Eficiencia;
using Optimus.Commons.Services;


namespace Optimus.Batch.Extencion
{
    public static class ApiServiceExtencion
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IAverageTimeFailuresProductionService, AverageTimeFailuresProductionService>();
            services.AddScoped<IKpiDepletionService, KpiDepletionService>();
            services.AddScoped<IMachineStatusProductionOrderService, MachineStatusProductionOrderService>();
            services.AddScoped<IProduccionEficienciaByShiftService, ProduccionEficienciaByShiftService>();
            services.AddScoped<IProduccionEficienciaService, ProduccionEficienciaService>();
            services.AddScoped<ITimeStopsService, TimeStopsService>();

            return services;
        }
        public static IServiceCollection AddJobsServices(this IServiceCollection services, IConfiguration configuration)
        {
            //DASHBOARD
            //   services.AddSingleton<ProduccionEficienciaJobs>();
            //   services.AddSingleton<ProduccionEficienciaByShiftJobs>();
            //   services.AddSingleton<KpiDepletionJobs>();
            //   services.AddSingleton<MachineStatusProductionOrderJobs>();
            //   services.AddSingleton<TimeStopsJobs>();
            //   services.AddSingleton<AverageTimeFailuresProductionJobs>();

            services.AddSingleton<OffsetCorrectionJobs>();
            services.AddSingleton(new JobSchedule(
             jobKey: "OffsetCorrectionJobs",
             jobType: typeof(OffsetCorrectionJobs),
             cronExpression: "0 5 6,14,22 ? * * *",
             description: "",
             runStart: true)
          );

            //   services.AddSingleton(new JobSchedule(
            //    jobKey: "ProduccionEficienciaJobs",
            //    jobType: typeof(ProduccionEficienciaJobs),
            //    cronExpression: "50 0/1 * ? * * *",
            //    description: "",
            //    runStart: true)
            // );

            //   services.AddSingleton(new JobSchedule(
            //   jobKey: "ProduccionEficienciaByShiftJobs",
            //   jobType: typeof(ProduccionEficienciaByShiftJobs),
            //   cronExpression: "50 0/1 * ? * * *",
            //   description: "",
            //   runStart: true)
            //);
            //   services.AddSingleton(new JobSchedule(
            //   jobKey: "KpiDepletionJobs",
            //   jobType: typeof(KpiDepletionJobs),
            //   cronExpression: "0 0/30 * ? * * *",
            //   description: "",
            //   runStart: true)
            // );
            //   services.AddSingleton(new JobSchedule(
            //   jobKey: "MachineStatusProductionOrderJobs",
            //   jobType: typeof(MachineStatusProductionOrderJobs),
            //   cronExpression: "50 0/1 * ? * * *",
            //   description: "",
            //   runStart: true)
            //);
            //   services.AddSingleton(new JobSchedule(
            //   jobKey: "TimeStopsJobs",
            //   jobType: typeof(TimeStopsJobs),
            //   cronExpression: "50 0/1 * ? * * *",
            //   description: "",
            //   runStart: true)
            //);
            //   services.AddSingleton(new JobSchedule(
            //   jobKey: "AverageTimeFailuresProductionJobs",
            //   jobType: typeof(AverageTimeFailuresProductionJobs),
            //   cronExpression: "50 0/1 * ? * * *",
            //   description: "",
            //   runStart: true)
            //);

            //ALERTAS
            services.AddSingleton<AlertaParadaPropia>();
            services.AddSingleton(new JobSchedule(
             jobKey: "ParadaPropiaJob",
             jobType: typeof(AlertaParadaPropia),
             cronExpression: "0 0/2 * ? * * *",
             description: "",
             runStart: false)
          );
            services.AddSingleton<AlertaMerma>();
            services.AddSingleton(new JobSchedule(
             jobKey: "AlertaMermaJob",
             jobType: typeof(AlertaMerma),
             cronExpression: "0 6,36 * ? * * *",
             description: "",
             runStart: false)
          );
            services.AddSingleton<AlertaPrecaucion>();
            services.AddSingleton(new JobSchedule(
             jobKey: "AlertaPrecaucionJob",
             jobType: typeof(AlertaPrecaucion),
             cronExpression: "0 0/2 * ? * * *",
             description: "",
             runStart: false)
          );

            //EFICIENCIA
            services.AddSingleton<EficienciaDiaCerrado>();
            services.AddSingleton(new JobSchedule(
             jobKey: "EficienciaDiaCerrado",
             jobType: typeof(EficienciaDiaCerrado),
             cronExpression: "0 3 22 ? * * *",
             description: "",
             runStart: true)
          );
            services.AddSingleton<EficienciaMesCerrado>();
            services.AddSingleton(new JobSchedule(
             jobKey: "EficienciaMesCerrado",
             jobType: typeof(EficienciaMesCerrado),
             cronExpression: "0 0 0 1 * ? *",
             description: "",
             runStart: true)
          );
            services.AddSingleton<EficienciaDiaMesAño>();
            services.AddSingleton(new JobSchedule(
             jobKey: "EficienciaDiaMesAño",
             jobType: typeof(EficienciaDiaMesAño),
             cronExpression: "0 0/10 * ? * * *",
             description: "",
             runStart: true)
          );
            //HISTORICO
            services.AddSingleton<EquiposHistorico>();
            services.AddSingleton(new JobSchedule(
             jobKey: "EquiposHistorico",
             jobType: typeof(EquiposHistorico),
             cronExpression: "0 5 22 ? * * *",
             description: "",
             runStart: true)
          );
            services.AddSingleton<SkuEficienciaHistorico>();
            services.AddSingleton(new JobSchedule(
             jobKey: "SkuEficienciaHistorico",
             jobType: typeof(SkuEficienciaHistorico),
             cronExpression: "0 5 22 ? * * *",
             description: "",
             runStart: true)
          );
            services.AddSingleton<MachineStatusEvents>();
            services.AddSingleton(new JobSchedule(
             jobKey: "MachineStatusEvents",
             jobType: typeof(MachineStatusEvents),
             cronExpression: "0 5 23 ? * * *",
             description: "",
             runStart: true)
          );
            return services;

        }
    }
}
