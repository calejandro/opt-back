
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Optimus.Batch.Extencion;
using Optimus.Commons.Extensions;
using Optimus.Commons.Services.Quartz;
using Optimus.Models.Contexts;
using Optimus.Repositories.Extensions;
using Serilog;
using Optimus.Batch.Services.SignalR;
using System.Collections.Generic;
using System.Globalization;
using Swashbuckle.AspNetCore.SwaggerUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Filters;

namespace Optimus.Batch
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddJobsServices(Configuration);

            services.AddCommonsConfigurations(Configuration);

            services.AddCommonsServices();

            services.AddRepositories();

            services.AddSqlServerDbContext<OptimusContext>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<Optimus2Context>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<IdentityContext>(Configuration.GetConnectionString("DefaultConnection"));
            var mongoAuditDatabase = Configuration.GetSection("MongoDB:Database").Value;
            services.AddMongo<MongoContext>(Configuration.GetConnectionString("Mongo"), mongoAuditDatabase, (setting) =>
            {
                setting.UseTls = false;
            });
            services.AddQuartz();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Optimus Batch Web API", Version = "v1" });
                c.EnableAnnotations();
                c.AddSecurityDefinition("oauth2", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                    Name = "Authorization",
                    Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                //c.OperationFilter<FileUploadOperation>();
            });
            services.Configure<RequestLocalizationOptions>(options =>{
               var supportedCultures = new List<CultureInfo>{new CultureInfo("es-ES"),};
               options.DefaultRequestCulture = new RequestCulture(culture: "es-ES", uiCulture: "es-ES");
               options.SupportedCultures = supportedCultures;
               options.SupportedUICultures = supportedCultures;
             });
            services.AddSignalR().AddNewtonsoftJsonProtocol(options =>
            {
                options.PayloadSerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.PayloadSerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            });
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerfactory)
        {
            loggerfactory.AddSerilog();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Optimus Batch");
                c.DocExpansion(DocExpansion.None);
            });
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapGet("/", async context =>
            //    {
            //        await context.Response.WriteAsync("Hello World!");
            //    });
            //});
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute().RequireAuthorization();
            });
        }
    }
}
