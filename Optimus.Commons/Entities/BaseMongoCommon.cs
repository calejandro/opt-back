﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Optimus.Commons.Entities
{
    public abstract class BaseMongoCommon
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _Id { get; set; }

        [BsonElement("id")]
        public string Id { get; set; }

        [BsonElement("created")]
        public DateTime Created { get; set; }

        [BsonElement("updated")]
        public DateTime? Updated { get; set; }

        [BsonElement("deleted")]
        public DateTime? Deleted { get; set; }

        [BsonElement("updatedBy")]
        public string UpdatedBy { get; set; }

        [BsonElement("createdBy")]
        public string CreatedBy { get; set; }

        [BsonElement("deletedBy")]
        public string DeletedBy { get; set; }

        public BaseMongoCommon()
        {
        }        
    }
}
