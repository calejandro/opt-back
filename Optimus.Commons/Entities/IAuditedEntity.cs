﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Optimus.Commons.Entities
{
    public interface IAuditedEntity
    {
        DateTime CreatedAt { get; set; }

        [MaxLength(50)]
        string CreatedBy { get; set; }

        [MaxLength(50)]
        string CreatedByUser { get; set; }

        DateTime? UpdatedAt { get; set; }

        [MaxLength(50)]
        string UpdatedBy { get; set; }

        [MaxLength(50)]
        string UpdatedByUser { get; set; }

        DateTime? DeletedAt { get; set; }

        [MaxLength(50)]
        string DeleteByUser { get; set; }

        [MaxLength(50)]
        string DeletedBy { get; set; }
       
    }
}
