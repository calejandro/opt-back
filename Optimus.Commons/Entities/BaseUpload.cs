﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Commons.Entities
{
    public class BaseUpload : BaseCommon
    {
        [MaxLength(1000)]
        public string FileUrl { get; set; }

        public BaseUpload()
        {
        }
    }
}
