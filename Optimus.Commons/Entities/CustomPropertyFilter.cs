﻿using System;
namespace Optimus.Commons.Entities
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class CustomPropertyFilter : Attribute
    {
        public CustomPropertyFilter()
        {            
        }
    }
}
