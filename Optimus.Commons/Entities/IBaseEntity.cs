﻿using System;
namespace Optimus.Commons.Entities
{
    public interface IBaseEntity<T> where T : IEquatable<T>
    {
        T Id { get; set; }
    }
}
