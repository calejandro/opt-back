﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Commons.Entities
{
    public abstract class BaseEntity<T> : IBaseEntity<T> where T : IEquatable<T> 
    {
        [Key]
        public virtual T Id { get; set; }

    }
}
