﻿using System;
namespace Optimus.Commons.Entities
{
    public abstract class BaseCosmosCommon
    {
        public string Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public DateTime? Deleted { get; set; }
       
        public string UpdatedBy { get; set; }

        public string CreatedBy { get; set; }

        public string DeletedBy { get; set; }

        public BaseCosmosCommon()
        {
        }        
    }
}
