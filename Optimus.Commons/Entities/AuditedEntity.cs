﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Optimus.Commons.Entities
{
    /// <summary>
    /// Entidades con información de auditoría.
    /// </summary>
    public abstract class AuditedEntity : BaseEntity<Guid>, IAuditedEntity
    {
        public DateTime CreatedAt { get; set; }

        [MaxLength(50)]
        public string CreatedBy { get; set; }

        [MaxLength(50)]
        public string CreatedByUser { get; set; }

        public DateTime? UpdatedAt { get; set; }

        [MaxLength(50)]
        public string UpdatedBy { get; set; }

        [MaxLength(50)]
        public string UpdatedByUser { get; set; }

        public DateTime? DeletedAt { get; set; }

        [MaxLength(50)]
        public string DeletedBy { get; set; }

        [MaxLength(50)]
        public string DeleteByUser { get; set; }
        
    }
}
