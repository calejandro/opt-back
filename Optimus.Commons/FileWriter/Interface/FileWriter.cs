﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Optimus.Commons.Exceptions;
using Optimus.Commons.FileWriter.Helper;
using Optimus.Commons.FileWriter.Implementation;

namespace Optimus.Commons.FileWriter.Interface
{
    public class FileWriter : IFileWriter
    {
        public async Task<string> UploadImage(IFormFile file, string url)
        {
            if (CheckIfImageFile(file))
            {
                return await WriteFile(file, url);
            }
            throw new BussinessException("Invalid image file", BusinessExceptionCodeEnumeration.ENTITY_DuplicateEntryUpdate);
        }

        /// <summary>
        /// Method to check if file is image file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return WriterHelper.GetImageFormat(fileBytes) != WriterHelper.ImageFormat.unknown;
        }

        /// <summary>
        /// Method to write file onto the disk
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<string> WriteFile(IFormFile file, string url)
        {
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = Guid.NewGuid().ToString(); //Create a new Name 
                                                                  //for the file due to security reasons.
                var path = Path.Combine(url, fileName);

                using (var bits = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(bits);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return fileName;
        }
        public async Task<List<string>> ReadURL(IEnumerable<string> names, string url)
        {
            List<string> URLs = new List<string>();
            foreach (var name in names)
            {
               URLs.Add( Path.Combine(url, name));
            }
            return URLs;
        }

    }
}
