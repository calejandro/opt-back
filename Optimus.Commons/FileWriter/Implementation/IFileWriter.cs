﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Commons.FileWriter.Implementation
{
    public interface IFileWriter
    {
        Task<string> UploadImage(IFormFile file ,string url);
        Task<List<string>> ReadURL(IEnumerable<string> names, string url);
        Task<string> WriteFile(IFormFile file, string url);
    }
}
