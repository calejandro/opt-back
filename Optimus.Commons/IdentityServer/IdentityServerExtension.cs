﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography.X509Certificates;

namespace Optimus.Commons.IdentityServer
{
    public static class IdentityServerExtension
    {
        public static IIdentityServerBuilder AddIdentityServer4<TUser, TKey>(this IServiceCollection services, IConfiguration configuration, string connectionStringName, string migrationsAssembly)
            where TUser : IdentityUser<TKey>            
            where TKey : IEquatable<TKey>
        {            
            var builder = services.AddIdentityServer()
                .LoadSigningCredentialFrom(configuration["certificates:signing"])
                .AddAspNetIdentity<TUser>()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(configuration.GetConnectionString(connectionStringName),
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(configuration.GetConnectionString(connectionStringName),
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwtOptions =>
            {
                jwtOptions.RequireHttpsMetadata = false;
                jwtOptions.MetadataAddress = configuration["JwtBearerConfiguration:Metadata"];
                jwtOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = false,
                    ValidateIssuer = false
                };
            });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
            });

            return builder;
        }

        public static IIdentityServerBuilder LoadSigningCredentialFrom(this IIdentityServerBuilder builder, string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                builder.AddSigningCredential(new X509Certificate2(path));
            }
            else
            {
                builder.AddDeveloperSigningCredential();
            }

            return builder;
        }
    }
}
