﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace Optimus.Commons.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource("api1", "Optimus")
            };


        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // client credentials flow client
                new Client
                {
                    ClientId = "client",
                    ClientName = "Optimus Api",
                    AllowOfflineAccess = true,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "openid", "profile", "api1" },

                    AllowedCorsOrigins = new List<string>()
                    {
                       "http://localhost:4200",
                       "https://localhost:4200",
                       "http://181.142.96.26:8081",
                    }
                }
            };
    }
}
