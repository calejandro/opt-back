﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Fluent;
using MongoDB.Bson;
using MongoDB.Driver;
using Optimus.Commons.Contexts;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Repositories.Cosmos
{
    public abstract class GenericMongoRepository<TEntity, TMongoContext> :
        IGenericMongoRepository<TEntity>
        where TEntity : BaseMongoCommon        
        where TMongoContext : BaseMongoContext
    {

        protected TMongoContext cosmosContext;

        private string collectionName;       

        public GenericMongoRepository(TMongoContext cosmosContext, string collection)
        {
            this.cosmosContext = cosmosContext;
            this.collectionName = collection;
        }        

        protected IMongoCollection<TEntity> Collection
        {
            get
            {
                return this.cosmosContext.Database.GetCollection<TEntity>(collectionName);
            }            
        }
        
        public async virtual Task DeleteAsync(object id)
        {            
            await this.Collection.DeleteOneAsync<TEntity>(e => e._Id.Equals(id));
        }

        public async virtual Task<TEntity> GetAsync(string id)
        {
            var result = await this.Collection.FindAsync<TEntity>(f => f._Id.Equals(id));
            return result.FirstOrDefault();
        }

        public async virtual Task<TEntity> GetAsync(FilterDefinition<TEntity> filterDefinition)
        {
            var result = await this.Collection.FindAsync(filterDefinition);
            return result.FirstOrDefault();
        }

        public async virtual Task InsertAsync(TEntity entity)
        {
           await this.Collection.InsertOneAsync(entity);
        }
        public async virtual Task InsertManyAsync(List<TEntity> entity)
        {
            await this.Collection.InsertManyAsync(entity);
        }

        public async virtual Task<List<TEntity>> ListAsync(FilterDefinition<TEntity> filter = null)
        {
            if(filter == null)
            {
                return (await this.Collection.FindAsync<TEntity>(f => true)).ToList();
            }
            else
            {
                return (await this.Collection.FindAsync<TEntity>(filter)).ToList();
            }
            
        }

        public async virtual Task UpdateAsync(TEntity entity)
        {
            await this.Collection.ReplaceOneAsync<TEntity>(f => f._Id == entity._Id, entity);
        }

        public async virtual Task UpdateAllAsync(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                await this.Collection.ReplaceOneAsync<TEntity>(f => f.Id == entity.Id, entity);
            }            
        }
    }
}
