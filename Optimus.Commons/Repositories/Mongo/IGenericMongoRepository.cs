﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Repositories.Cosmos
{
    public interface IGenericMongoRepository<TEntity> where TEntity : BaseMongoCommon
    {
        Task<TEntity> GetAsync(string id);

        Task<TEntity> GetAsync(FilterDefinition<TEntity> filterDefinition);

        Task InsertAsync(TEntity entity);

        Task InsertManyAsync(List<TEntity> entity);

        Task<List<TEntity>> ListAsync(FilterDefinition<TEntity> filter = null);

        Task DeleteAsync(object id);

        Task UpdateAsync(TEntity entity);

        Task UpdateAllAsync(List<TEntity> entities);
    }
}
