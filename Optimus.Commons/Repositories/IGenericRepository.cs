﻿using Microsoft.EntityFrameworkCore.Storage;
using Optimus.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Services;

namespace Optimus.Commons.Repositories
{
   public interface IGenericRepository<TEntity, TKey, TFilter> : IDisposable where TEntity : class where TFilter : BaseFilter where TKey: IEquatable<TKey>
   {
      IDbContextTransaction BeginTransaction();

      void Detached(TEntity entity);

      IExecutionStrategy CreateExecutionStrategy();

      Task<int> CountAsync(TFilter filter = null, string[] includes = null);

      Task DeleteAsync(TKey id);

      Task DeleteAllAsync(TKey[] ids);

      Task<bool> ExistsAsync(TKey id);
      Task<bool> ExistsEntityAsync(TEntity entity);
      Task<TEntity> GetAsync(TKey id, string[] includes = null);
      Task<TEntity> GetAsync(string[] includes = null, TFilter filter = null, bool includeDeleted = false);

      Task<TEntity> InsertAsync(TEntity entity);

      Task InsertAllAsync(List<TEntity> list);

      Task<List<TEntity>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, TFilter filter = null, List<Order> orders = null, bool noTracking = false);

      Task<TEntity> UpdateAsync(TEntity entity);

      Task UpdateAllAsync(List<TEntity> list);

      string CurrentUser();
   }
}
