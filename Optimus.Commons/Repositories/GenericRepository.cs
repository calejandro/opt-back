﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Optimus.Commons.Contexts;
using Optimus.Commons.Entities;
using Optimus.Commons.Extensions;
using Optimus.Commons.Services;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Internal;
using System.Reflection;

namespace Optimus.Commons.Repositories
{
    public class GenericRepository<TContext, TEntity, TKey, TFilter> : IGenericRepository<TEntity, TKey, TFilter>, IDisposable
        where TEntity : class
        where TFilter : BaseFilter
        where TContext : DbContext
        where TKey : IEquatable<TKey>
    {
        protected readonly TContext dbContext;

        protected DbSet<TEntity> dbSet;

        public GenericRepository(TContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<TEntity>();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return dbContext.Database.BeginTransaction();
        }

        public void Detached(TEntity entity)
        {
            this.dbContext.Entry<TEntity>(entity).State = EntityState.Detached;
        }


        public IExecutionStrategy CreateExecutionStrategy()
        {
            return dbContext.Database.CreateExecutionStrategy();
        }

        public virtual async Task<int> CountAsync(TFilter filter = null, string[] includes = null)
        {
            var query = this.Filter(filter, this.dbSet.AsQueryable());
            query = this.GetIncludeQuery(query, includes);
                return await query.CountAsync();
        }

        public virtual async Task DeleteAsync(TKey id)
        {
            TEntity existing = await dbSet.FindAsync(id);
            dbSet.Remove(existing);
            await dbContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAllAsync(TKey[] ids)
        {
            foreach (TKey id in ids)
            {
                var entity = await dbSet.FindAsync(id);
                if (entity != null)
                { dbSet.Remove(entity); }
            }

            await dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
        }

        public virtual async Task<bool> ExistsAsync(TKey id)
        {
            var entityExists = await dbSet.FindAsync(id);
            return entityExists != null;

        }
        public virtual async Task<bool> ExistsEntityAsync(TEntity entity)
        {
            var entityExists = await dbSet.FindAsync(entity);
            return entityExists != null;
        }

        public virtual async Task<TEntity> GetAsync(TKey id, string[] includes = null)
        {
            TEntity entity = null;
            if (includes == null || includes.Length == 0)
            {
                entity = await dbSet.FindAsync(id);
            }
            else
            {
                var parameter = Expression.Parameter(typeof(TEntity), "e");
                var property = Expression.PropertyOrField(parameter, "Id");
                var eq = Expression.Equal(property, Expression.Constant(id));
                var query = this.dbSet.Where(Expression.Lambda<Func<TEntity, bool>>(eq, parameter));

                query = this.GetIncludeQuery(query, includes);
                entity = await query.SingleOrDefaultAsync();
            }
            return entity;
        }
        public async virtual Task<TEntity> GetAsync(string[] includes = null, TFilter filter = null, bool includeDeleted = false)
        {
            var query = dbSet.AsQueryable<TEntity>();

            query = this.Filter(filter, query);
            if (includeDeleted)
                query = this.FilterIncludeDeleted(query);

            query = this.GetIncludeQuery(query, includes);

            return await query.SingleOrDefaultAsync();
        }
        private IQueryable<TEntity> FilterIncludeDeleted(IQueryable<TEntity> query)
        {
            query = query.IgnoreQueryFilters();
            var parameter = Expression.Parameter(typeof(TEntity), "e");

            var property = Expression.PropertyOrField(parameter, "DeletedAt");
            var ne = Expression.NotEqual(property, Expression.Constant(null));
            query = query.Where(Expression.Lambda<Func<TEntity, bool>>(ne, parameter));

            property = Expression.PropertyOrField(parameter, "DeletedBy");
            ne = Expression.NotEqual(property, Expression.Constant(null));
            query = query.Where(Expression.Lambda<Func<TEntity, bool>>(ne, parameter));
            return query;
        }
        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            await dbContext.Set<TEntity>().AddAsync(entity);
            await dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task InsertAllAsync(List<TEntity> list)
        {
            await dbContext.Set<TEntity>().AddRangeAsync(list);
            await dbContext.SaveChangesAsync();
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            dbContext.Set<TEntity>().Update(entity);
            await dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task UpdateAllAsync(List<TEntity> list)
        {
            dbContext.Set<TEntity>().UpdateRange(list);
            await dbContext.SaveChangesAsync();
        }

        protected virtual IQueryable<TEntity> GetIncludeQuery(IQueryable<TEntity> query, string[] includes = null)
        {
            if (includes != null && includes.Length > 0)
            {
                foreach (var item in includes)
                {
                    var props = item.Split(".");
                    foreach (var p in props)
                    {
                        props[props.IndexOf(p)] = Char.ToUpperInvariant(p[0]) + p.Substring(1);
                    }
                    query = query.Include(string.Join(".", props));
                }
            }

            return query;
        }

        protected IQueryable<TEntity> GetQueryOrder(IQueryable<TEntity> query, List<Order> orders)
        {
            if (orders != null)
            {
                foreach (var order in orders)
                {
                    if (!string.IsNullOrEmpty(order.Property))
                    {
                        var prop = Char.ToUpperInvariant(order.Property[0]) + order.Property.Substring(1);
                        query = query.OrderByPropertyName(prop, order.Descending);
                    }
                }
            }

            return query;
        }

        protected IQueryable<TEntity> GetQueryPaging(IQueryable<TEntity> query, PaginatorRequestModel paginatorRequestModel)
        {
            if (paginatorRequestModel != null && paginatorRequestModel.RowsPage > 0)
            {
                query = query.Skip(paginatorRequestModel.Page * paginatorRequestModel.RowsPage).Take(paginatorRequestModel.RowsPage);
            }

            return query;
        }

        public async virtual Task<List<TEntity>> ListAsync(PaginatorRequestModel paginatorRequestModel = null, string[] includes = null, TFilter filter = null, List<Order> orders = null, bool noTracking = false)
        {
            var query = dbSet.AsQueryable<TEntity>();

            query = this.Filter(filter, query);

            query = this.GetIncludeQuery(query, includes);

            query = this.GetQueryOrder(query, orders);
            if (noTracking)
                query = query.AsNoTracking();
            return await this.GetQueryPaging(query, paginatorRequestModel).ToListAsync();
        }

        public string CurrentUser()
        {
            if (this is IBaseContext)
            {
                return (this.dbContext as IBaseContext).CurrentUser();
            }

            return "Unknown";
        }

        protected IQueryable<TEntity> Filter(TFilter filter, IQueryable<TEntity> query)
        {
            if (filter != null)
            {
                var properties = filter.GetType().GetProperties().Where(w => !w.CustomAttributes.Any(e => e.AttributeType == typeof(CustomPropertyFilter))).ToList();
                var parameter = Expression.Parameter(typeof(TEntity), "e");
                foreach (var item in properties)
                {
                    var value = item.GetValue(filter);
                    if (value != null && item.Name != "Search")
                    {
                        try
                        {
                            var filterExpresion = GenericRepositoryFilter.ApplyFilter<TEntity, object>(item.Name, GenericRepositoryFilter.FilterOperation.Equal, item.GetValue(filter));
                            query = query.Where(filterExpresion);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }

            return query;
        }
    }
}
