﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Repositories.Cosmos
{
    public interface IGenericCosmosRepository<TEntity> where TEntity : BaseCosmosCommon
    {
        Task<TEntity> GetAsync(string id);

        Task InsertAsync(TEntity entity);

        Task<List<TEntity>> ListAsync(string query);

        Task DeleteAsync(string id);

        Task UpdateAsync(TEntity entity);
    }
}
