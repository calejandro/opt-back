﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Fluent;
using Optimus.Commons.Contexts;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Repositories.Cosmos
{
    public abstract class GenericCosmosRepository<TEntity, TCosmosContext> :
        IGenericCosmosRepository<TEntity>
        where TEntity : BaseCosmosCommon        
        where TCosmosContext : BaseCosmosContext
    {

        protected TCosmosContext cosmosContext;

        private string collectionName;       

        public GenericCosmosRepository(TCosmosContext cosmosContext, string collection)
        {
            this.cosmosContext = cosmosContext;
            this.collectionName = collection;
        }        

        protected Container Container
        {
            get
            {
                return Task.Run(async () =>
                {
                    return await this.cosmosContext.Database.CreateContainerIfNotExistsAsync(collectionName, "id");

                }).Result;
            }            
        }
        
        public async virtual Task DeleteAsync(string id)
        {
            await this.Container.DeleteItemAsync<TEntity>(id, new PartitionKey(id));
        }

        public async virtual Task<TEntity> GetAsync(string id)
        {
            try
            {
                var itemResponse = await this.Container.ReadItemAsync<ItemResponse<TEntity>>(id, new PartitionKey(id));
                return itemResponse.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }            
        }

        public async virtual Task InsertAsync(TEntity entity)
        {
            await this.Container.CreateItemAsync<TEntity>(entity, new PartitionKey(entity.Id));
        }

        public async virtual Task<List<TEntity>> ListAsync(string queryString)
        {
            var query = this.Container.GetItemQueryIterator<TEntity>(new QueryDefinition(queryString));
            List<TEntity> results = new List<TEntity>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }

            return results;
        }

        public async virtual Task UpdateAsync(TEntity entity)
        {
            await this.Container.UpsertItemAsync<TEntity>(entity, new PartitionKey(entity.Id));
        }
    }
}
