﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Entities;
using System.Runtime.CompilerServices;

namespace Optimus.Commons.Contexts
{
    public class BaseContext : DbContext, IBaseContext
    {
        protected readonly IHttpContextAccessor httpContextAccessor;

        protected readonly bool isSofDelete;

        public BaseContext(DbContextOptions options, IHttpContextAccessor httpContextAccessor) : base(options)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public BaseContext(DbContextOptions options, IHttpContextAccessor httpContextAccessor, bool isSofDelete) : base(options)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public override int SaveChanges()
        {
            //CustomSaveChanges();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            //CustomSaveChanges();
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public string CurrentUser()
        {
            var currentUser = "Anonymous";
            if (this.httpContextAccessor != null)
            {
                var user = this.httpContextAccessor?.HttpContext?.User;

                if (user != null && user.Identity.IsAuthenticated && !string.IsNullOrEmpty(user.FindFirst(ClaimTypes.NameIdentifier).Value))
                {
                    currentUser = user.FindFirst(ClaimTypes.NameIdentifier).Value;
                }
            }
            

            return currentUser;
        }

        /*private void CustomSaveChanges()
        {

            var currentUser = CurrentUser();

            foreach (var entry in ChangeTracker.Entries<BaseCommon>())
            {
                if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
                {
                    if (entry.State == EntityState.Added)
                    {
                        if (entry.Entity.Id == null || string.IsNullOrEmpty(entry.Entity.Id.ToString()))
                        {
                            entry.Entity.Id = Guid.NewGuid();
                        }
                        entry.Entity.Created = DateTime.UtcNow;
                        entry.Entity.CreatedBy = currentUser;
                    }
                    else
                    {
                        entry.Property(p => p.Created).IsModified = false;
                        entry.Property(p => p.CreatedBy).IsModified = false;
                    }

                    entry.Entity.Updated = DateTime.UtcNow;
                    entry.Entity.UpdatedBy = currentUser;
                }
                if(this.isSofDelete)
                {
                    if (entry.State == EntityState.Deleted)
                    {
                        entry.Entity.Deleted = DateTime.UtcNow;
                        entry.Entity.DeletedBy = currentUser;
                        entry.State = EntityState.Modified;
                    }
                }
                
            }
        }*/
    }
}
