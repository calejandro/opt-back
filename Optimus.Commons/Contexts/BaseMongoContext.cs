﻿using System;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Optimus.Commons.Contexts
{
    public class BaseMongoContext
    {
        private MongoClient mongoClient;

        private IMongoDatabase database;

        public IMongoDatabase Database { get { return database; } }

        public MongoClient Client { get { return mongoClient; } }

        public BaseMongoContext()
        {
        }

        public static TMongoContext Create<TMongoContext>(MongoClientSettings mongoClientSettings, string database) where TMongoContext : BaseMongoContext
        {
            TMongoContext mongoContext = Activator.CreateInstance<TMongoContext>();
            mongoContext.mongoClient = new MongoClient(mongoClientSettings);
            mongoContext.database = mongoContext.mongoClient.GetDatabase(database);
            return mongoContext;
        }
    }
}
