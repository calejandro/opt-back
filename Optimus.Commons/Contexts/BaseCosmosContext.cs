﻿using System;
using System.Security.Policy;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Fluent;

namespace Optimus.Commons.Contexts
{
    public class BaseCosmosContext
    {
        private CosmosClient cosmosClient;

        protected Database database;

        public CosmosClient Client { get { return this.cosmosClient; } }

        public Database Database
        {
            get
            {
                return this.database;
            }
        }

        
        public static async Task<TCosmosContext> CreateAsync<TCosmosContext>(CosmosClientBuilder cosmosClientBuilder, string database) where TCosmosContext: BaseCosmosContext
        {
            TCosmosContext cosmosContext = Activator.CreateInstance<TCosmosContext>();
            cosmosContext.cosmosClient = cosmosClientBuilder.Build();
            cosmosContext.database = await cosmosContext.Client.CreateDatabaseIfNotExistsAsync(database);
            return cosmosContext;
        }

    }
}
