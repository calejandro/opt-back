﻿using Optimus.Commons.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Optimus.Commons.Contexts
{
    public class BaseIdentityContext<TUser, TRole, TUserRole, TKey, TUserClaim, TRoleClaim> :
        IdentityDbContext<TUser, TRole, TKey, IdentityUserClaim<TKey>, TUserRole,
        IdentityUserLogin<TKey>, IdentityRoleClaim<TKey>, IdentityUserToken<TKey>>
        where TUser : IdentityUser<TKey>
        where TRole : IdentityRole<TKey>
        where TUserRole : IdentityUserRole<TKey>
        where TKey : IEquatable<TKey>
        where TUserClaim : IdentityUserClaim<TKey>
        where TRoleClaim : IdentityRoleClaim<TKey>
    {
        protected readonly IHttpContextAccessor httpContextAccessor;

        protected readonly bool isSofDelete;


        private readonly ILogger<BaseIdentityContext<TUser, TRole, TUserRole, TKey, TUserClaim, TRoleClaim>> Logger;

        public BaseIdentityContext(DbContextOptions options,
            IHttpContextAccessor httpContextAccessor,
            ILogger<BaseIdentityContext<TUser, TRole, TUserRole, TKey, TUserClaim, TRoleClaim>> logger) : base(options)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.isSofDelete = false;
            this.Logger = logger;
            this.ChangeTracker.LazyLoadingEnabled = true;
        }

        public BaseIdentityContext(DbContextOptions options, IHttpContextAccessor httpContextAccessor, bool isSofDelete) : base(options)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.isSofDelete = isSofDelete;
            this.ChangeTracker.LazyLoadingEnabled = true;
        }

        public override int SaveChanges()
        {
            CustomSaveChanges();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            CustomSaveChanges();
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public string CurrentUser()
        {
            var currentUser = "Anonymous";
            if (this.httpContextAccessor != null)
            {
                var user = this.httpContextAccessor?.HttpContext?.User;

                if (user != null && user.Identity.IsAuthenticated && !string.IsNullOrEmpty(user.FindFirst(ClaimTypes.NameIdentifier).Value))
                {
                    currentUser = user.FindFirst(ClaimTypes.NameIdentifier).Value;
                }
            }


            return currentUser;
        }

        public string CurrentUserName()
        {
            var currentUser = "Anonymous";
            if (this.httpContextAccessor != null)
            {
                var user = this.httpContextAccessor?.HttpContext?.User;

                if (user != null && user.Identity.IsAuthenticated && !string.IsNullOrEmpty(user.FindFirst("name")?.Value))
                {
                    currentUser = user.FindFirst("name").Value;
                }
                else if(user != null && user.Identity.IsAuthenticated && !string.IsNullOrEmpty(user.FindFirst("username")?.Value))
                {
                    currentUser = user.FindFirst("username").Value;
                }
            }


            return currentUser;
        }

        private void CustomSaveChanges()
        {
            var currentUser = CurrentUser();

            foreach (var auditedEntity in ChangeTracker.Entries<IAuditedEntity>())
            {
                if (auditedEntity.State == EntityState.Added || auditedEntity.State == EntityState.Modified)
                {
                    //var auditedEntity = entry.Entity as BioWeb3.Commons.Entities.IAuditedEntity;
                    if (auditedEntity != null)
                    {                        
                        if (auditedEntity.State == EntityState.Added)
                        {
                            var baseEntity = auditedEntity.Entity as Optimus.Commons.Entities.IBaseEntity<TKey>;

                            // TODO: PReguntar a santi autogeneracion int
                            //if (baseEntity.Id == null || string.IsNullOrEmpty(baseEntity.Id.ToString()) || baseEntity.Id == Guid.Empty)
                            //{
                            //    baseEntity.Id = int.NewGuid(); /
                            //}
                            auditedEntity.Entity.CreatedAt = DateTime.UtcNow;
                            auditedEntity.Entity.CreatedBy = currentUser;
                            auditedEntity.Entity.UpdatedAt = auditedEntity.Entity.CreatedAt;
                            auditedEntity.Entity.UpdatedBy = currentUser;
                            auditedEntity.Entity.CreatedByUser = CurrentUserName();
                        }
                        else
                        {
                            // If the entity is UserIdentity the UserName property
                            // can not be changed.
                            var userIdentityEntity = auditedEntity.Entity.GetType().GetProperty("UserName");
                            if (userIdentityEntity != null)
                            {
                                auditedEntity.Property("UserName").IsModified = false;
                                auditedEntity.Property("NormalizedUserName").IsModified = false;
                            }
                            auditedEntity.Property("CreatedAt").IsModified = false;
                            auditedEntity.Property("CreatedBy").IsModified = false;
                            auditedEntity.Entity.UpdatedAt = DateTime.UtcNow;
                            auditedEntity.Entity.UpdatedBy = currentUser;
                            auditedEntity.Entity.UpdatedByUser = CurrentUserName();

                        }                        

                    }
                }
                if (this.isSofDelete)
                {
                    if (auditedEntity.State == EntityState.Deleted)
                    {
                        //var auditedEntity = entry.Entity as BioWeb3.Commons.Entities.IAuditedEntity;
                        if (auditedEntity != null)
                        {
                            auditedEntity.Entity.DeletedAt = DateTime.UtcNow;
                            auditedEntity.Entity.DeletedBy = currentUser;
                            auditedEntity.State = EntityState.Modified;
                            auditedEntity.Entity.DeleteByUser = CurrentUserName();
                        }
                    }
                }


            }
        }
    }
}
