﻿using System;
namespace Optimus.Commons.Contexts
{
    public interface IBaseContext
    {
        string CurrentUser();
    }
}
