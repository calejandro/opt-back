﻿using System;
using System.Threading.Tasks;
using Novell.Directory.Ldap;

namespace Optimus.Commons.Ldap
{
    public interface ILdapService<TUser> where TUser : class
    {
        Task<(bool, TUser)> ValidateAsync(string userName, string password);

        LdapEntry SearchUserByName(string userName);
    }
}
