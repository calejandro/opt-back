﻿using System;
using System.Threading.Tasks;
using Optimus.Commons.Configurations;
using Optimus.Commons.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Options;
using Novell.Directory.Ldap;

namespace Optimus.Commons.Ldap
{
    public class LdapService<TUser> : ILdapService<TUser> where TUser : class
    {

        readonly UserManager<TUser> userManager;
        readonly SignInManager<TUser> signInManager;
        readonly LdapConfig ldapConfig;

        public LdapService(
            UserManager<TUser> userManager,
            SignInManager<TUser> signInManager,
            IOptions<LdapConfig> ldapOptions)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.ldapConfig = ldapOptions.Value;
        }

        public async Task<(bool, TUser)> ValidateAsync(string userName, string password)
        {
            TUser user = await this.userManager.FindByNameAsync(userName);
            try
            {
                if (user != null)
                {


                    return (true, user);


                }

            }
            catch (LdapException)
            {
                return (false, user);
            }

            return (false, user);
        }

        public LdapEntry SearchUserByName(string userName)
        {
            var connection = this.ConnectLdap(ldapConfig);
            return this.Search(userName, connection);
        }

        private LdapEntry Search(string userName,
            LdapConnection connection)
        {
            LdapSearchQueue queue = connection.Search(ldapConfig.BaseDn,
                LdapConnection.ScopeSub,
                $"sAMAccountName={userName}",
                 null,
                 false,
                (LdapSearchQueue)null,
                (LdapSearchConstraints)null);

            LdapMessage message;
            while ((message = queue.GetResponse()) != null)
            {
                if (message is LdapSearchResult)
                {
                    LdapEntry entry = ((LdapSearchResult)message).Entry;
                    return entry;
                }
            }

            return null;
        }

        private LdapConnection ConnectLdap(LdapConfig ldapConfig)
        {
            var connection = new LdapConnection { SecureSocketLayer = false };
            connection.ConnectionTimeout = 10000;
            connection.Connect(ldapConfig.Host, ldapConfig.Port);
            connection.Bind(ldapConfig.UserDn, ldapConfig.Password);
            return connection;
        }
    }

    public static class LdapEntryExtension
    {
        public static string GetAttributeValue(this LdapEntry ldapEntry, string valueName)
        {
            try
            {
                return ldapEntry.GetAttribute(valueName).StringValue;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
