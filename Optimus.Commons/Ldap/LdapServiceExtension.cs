﻿using System;
using Optimus.Commons.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Optimus.Commons.Ldap
{
    public static class LdapServiceExtension
    {
        public static IServiceCollection AddLdapServices<TUser>(this IServiceCollection services, IConfiguration configuration) where TUser : class
        {
            services.AddScoped<ILdapService<TUser>, LdapService<TUser>>();
            services.Configure<LdapConfig>(configuration.GetSection("LdapConfig"));
            return services;
        }
    }
}
