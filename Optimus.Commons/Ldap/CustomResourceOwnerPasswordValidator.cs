﻿using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Novell.Directory.Ldap;
using System.Threading.Tasks;

namespace Optimus.Commons.Ldap
{
    public class CustomResourceOwnerPasswordValidator<TUser> : IResourceOwnerPasswordValidator
        where TUser : class
    {
        private readonly ILdapService<TUser> ldapService;
        private readonly UserManager<TUser> userManager;

        public CustomResourceOwnerPasswordValidator(
            ILdapService<TUser> ldapService,
            UserManager<TUser> userManager)
        {
            this.ldapService = ldapService;
            this.userManager = userManager;
        }

        public async virtual Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            await this.Validate(context);
        }

        protected async Task<(TUser, bool)> Validate(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                (bool isValid, TUser user) = await this.ldapService.ValidateAsync(context.UserName, context.Password);

                if (user != null)
                {
                    var isLocked = await this.userManager.IsLockedOutAsync(user);
                    if (this.userManager.SupportsUserLockout && isLocked)
                    {
                        context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Actualmente su usuario esta bloqueado. Intente nuevamente mas tarde.");
                        return (null, false);
                    }
                }

                //if ((user as ILdapUser)?.IsADUser == true && isValid)
                //{
                //    context.Result = new GrantValidationResult((user as ILdapUser).Id.ToString(), OidcConstants.AuthenticationMethods.Password);
                //    await this.userManager.ResetAccessFailedCountAsync(user);
                //}
                //else
                //{
                    isValid = await this.userManager.CheckPasswordAsync(user, context.Password);
                    if (user != null && isValid)
                    {
                        context.Result = new GrantValidationResult((user as ILdapUser).Id.ToString(), OidcConstants.AuthenticationMethods.Password);
                        await this.userManager.ResetAccessFailedCountAsync(user);
                    }
                //}

                if (isValid)
                {
                    await this.ValidateAssignedRole(context, user);
                }
                else
                {
                    if (user != null)
                        await this.userManager.AccessFailedAsync(user);

                    context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Nombre de usuario o contraseña incorrecta.");
                }

                return (user, isValid);
            }
            catch (LdapException ex)
            {
                // Log exception
            }

            return (null, false);
        }

        private async Task ValidateAssignedRole(ResourceOwnerPasswordValidationContext context, TUser user)
        {
        //    if (user == null)
        //        return;

        //    var userRoles = await this.userManager.GetRolesAsync(user);

        //    if (userRoles == null || userRoles.Count == 0)
        //        context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "Actualmente su usuario no posee asignado un Rol. Contáctese con el administrador del sistema.");
        }
    }
}
