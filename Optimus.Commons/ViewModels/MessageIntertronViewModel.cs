﻿namespace Optimus.Commons.ViewModels
{
    public class MessageIntertronViewModel
    {
        public string org_id { get; set; }
        public string reference_name { get; set; }
        public string reference_id { get; set; }
        public string msg_id { get; set; }
        public string short_code { get; set; }
        public long msisdn { get; set; }
        public int priority { get; set; }
        public string text_message { get; set; }
        public string url_dlr { get; set; }
    }
}
