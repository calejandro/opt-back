﻿using System.ComponentModel.DataAnnotations;

namespace Optimus.Commons.ViewModels
{
    public class EmailViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Message { get; set; }
    }
}
