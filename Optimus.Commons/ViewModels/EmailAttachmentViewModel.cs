﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Optimus.Commons.ViewModels
{
    public class EmailAttachmentViewModel
    {
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
    }
}
