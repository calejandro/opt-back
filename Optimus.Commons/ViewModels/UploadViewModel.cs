﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;

namespace Optimus.Commons.ViewModels
{
    public class UploadViewModel
    {
        public IFormFile File { get; set; }

        public string Container { get; set; }

        public string SubFolder { get; set; }

        public UploadViewModel()
        {
        }
    }
}
