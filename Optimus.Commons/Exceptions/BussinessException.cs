﻿using AutoWrapper.Wrappers;
using System;
namespace Optimus.Commons.Exceptions
{
    public class BussinessException : ApiException
    {
        public BusinessExceptionCodeEnumeration ErrorCode { get; set; }

        public BussinessException(string message, BusinessExceptionCodeEnumeration businessExceptionCode = BusinessExceptionCodeEnumeration.Unknown) : base(message)
        {
            this.ErrorCode = businessExceptionCode;
            base.ReferenceErrorCode = string.Format("{0} - {1}", (int)this.ErrorCode, this.ErrorCode.ToString());
        }
    }
}

