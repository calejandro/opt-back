﻿using System;
namespace Optimus.Commons.Exceptions
{
    public enum BusinessExceptionCodeEnumeration
    {
        Unknown = 0,

        USER_DUPLICATEENTRYCREATE = 705,
        USER_DUPLICATEENTRYUPDATE = 706,

        ENTITY_DuplicateEntryCreate = 805,
        ENTITY_DuplicateEntryUpdate = 806,

        //Security
        SEC_General = 300,
        SEC_UserNotExists = 301,
        SEC_VioationAssignRootRole = 302,
        SEC_VioationAssignSystemRole = 303,

        //Sap Ordenes Produccion
        LPU_IdOrdenSap_Null = 601,
        SAP_OrdenTemporal = 602,

        OPTTASK_idCreador = 801,

    }
}
