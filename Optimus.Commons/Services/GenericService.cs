﻿using Microsoft.EntityFrameworkCore.Storage;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Commons.Services
{
   public abstract class GenericService<T, TKey, TFilter, TGenericRepository> : IGenericService<T, TKey, TFilter>
       where T : class
       where TFilter : BaseFilter
       where TGenericRepository : IGenericRepository<T, TKey, TFilter>
       where TKey : IEquatable<TKey>
   {
      protected TGenericRepository genericRepository;

      public GenericService(TGenericRepository genericRepository)
      {
         this.genericRepository = genericRepository;
      }

      public async virtual Task DeleteAsync(TKey id)
      {
         await genericRepository.DeleteAsync(id);
      }

      public async virtual Task<T> GetAsync(TKey id, string[] includes = null)
      {
         return await genericRepository.GetAsync(id, includes);
      }

      public async virtual Task<T> GetAsync(TKey id)
      {
         return await this.GetAsync(id, null);
      }

      public async virtual Task<T> InsertAsync(T entity)
      {
         return await genericRepository.InsertAsync(entity);
      }

      public async virtual Task<T> UpdateAsync(T entity)
      {
         return await genericRepository.UpdateAsync(entity);
      }

      public virtual async Task<List<T>> ListAsync()
      {
         return await genericRepository.ListAsync();
      }

      ~GenericService()
      {
         Dispose(false);
      }

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
         // This method will remove current object from garbage collector's queue 
         // and stop calling finilize method twice 
      }

      public virtual void Dispose(bool disposer)
      {
         if (disposer)
         {
            // dispose the managed objects
            genericRepository.Dispose();
         }
         // dispose the unmanaged objects
      }

      public async virtual Task DeleteAllAsync(TKey[] ids)
      {
         await genericRepository.DeleteAllAsync(ids);
      }

      public async Task UpdateAllAsync(List<T> list)
      {
         await genericRepository.UpdateAllAsync(list);
      }

      public async virtual Task InsertAllAsync(List<T> list)
      {
         await genericRepository.InsertAllAsync(list);
      }

      public virtual IDbContextTransaction BeginTransaction()
      {
         return this.genericRepository.BeginTransaction();
      }
   }
}
