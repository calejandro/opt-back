﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories.Cosmos;

namespace Optimus.Commons.Services.Azure.Cosmos
{
    public class GenericCosmosService<TEntity, TMapper, TCosmosRepository> : IGenericCosmosService<TMapper>
        where TEntity : BaseCosmosCommon
        where TMapper : class
        where TCosmosRepository : IGenericCosmosRepository<TEntity>
    {

        protected TCosmosRepository cosmosRepository;
        protected IMapper mapper;

        public GenericCosmosService(TCosmosRepository cosmosRepository, IMapper mapper)
        {
            this.cosmosRepository = cosmosRepository;
            this.mapper = mapper;
        }

        public async virtual Task DeleteAsync(string id)
        {
            await this.cosmosRepository.DeleteAsync(id);
        }

        public virtual async Task<TMapper> GetAsync(string id)
        {
            return this.mapper.Map<TMapper>(await this.cosmosRepository.GetAsync(id));
        }

        public async virtual Task InsertAsync(TMapper entity)
        {
            await this.cosmosRepository.InsertAsync(this.mapper.Map<TEntity>(entity));
        }

        public async virtual Task<List<TMapper>> ListAsync(string query)
        {
            return this.mapper.Map<List<TMapper>>(await this.cosmosRepository.ListAsync(query));
        }

        public async virtual Task UpdateAsync(TMapper entity)
        {
            await this.cosmosRepository.UpdateAsync(this.mapper.Map<TEntity>(entity));
        }
    }
}
