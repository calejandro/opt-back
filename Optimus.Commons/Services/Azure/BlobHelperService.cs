﻿using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Optimus.Commons.Configurations;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Optimus.Commons.Services.Azure
{
    public class BlobHelperService : IBlobHelperService
    {
        private readonly AzureStorageConfig azureStorageConfig;

        private readonly StorageCredentials storageCredentials;

        private static CloudStorageAccount _amstorageAccount;

        private static CloudQueueClient queueClient;

        private static CloudBlobClient sourceCloudBlobClient;

        public BlobHelperService(IOptions<AzureStorageConfig> azureStorageConfigOption)
        {
            azureStorageConfig = azureStorageConfigOption.Value;
            storageCredentials =
               new StorageCredentials(azureStorageConfig.StorageName, azureStorageConfig.StorageKey);

            if (_amstorageAccount == null)
            {
                _amstorageAccount = new CloudStorageAccount(storageCredentials, true);
                sourceCloudBlobClient = _amstorageAccount.CreateCloudBlobClient();
                queueClient = _amstorageAccount.CreateCloudQueueClient();
            }
        }

        public async Task<MemoryStream> GetBlob(string containerName, string blobName)
        {
            try
            {
                var container = await GetCloudBlobContainer(containerName);
                var blockBlob = container.GetBlockBlobReference(blobName);
                var ms = new MemoryStream();
                await blockBlob.DownloadToStreamAsync(ms);
                return ms;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<MemoryStream> GetBlob(string containerName, string subFolder, string blobName)
        {
            var container = await GetCloudBlobContainer(containerName);
            var directory = container.GetDirectoryReference(subFolder);

            var blockBlob = directory.GetBlockBlobReference(blobName);
            var fileExists = await blockBlob.ExistsAsync();
            if (!fileExists)
            {
                return null;
            }
            var ms = new MemoryStream();
            await blockBlob.DownloadToStreamAsync(ms);

            return ms;
        }

        public async Task<MemoryStream> GetBlob(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            var uri = new UriBuilder(url);
            var cloudBlockBlob = new CloudBlockBlob(uri.Uri, storageCredentials);

            if (await cloudBlockBlob.ExistsAsync())
            {

                var ms = new MemoryStream();
                await cloudBlockBlob.DownloadToStreamAsync(ms).ConfigureAwait(false);
                ms.Seek(0, SeekOrigin.Begin);
                return ms;
            }

            return null;
        }

        public async Task<CloudBlobContainer> GetCloudBlobContainer(string containerName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off)
        {
            var sourceCloudBlobClient = _amstorageAccount.CreateCloudBlobClient();
            var container = sourceCloudBlobClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync(blobContainerPublicAccessType, null, null);
            return container;
        }

        public async Task<CloudQueue> GetCloudQueue(string name)
        {
            var queue = queueClient.GetQueueReference(name);
            // Create the queue if it doesn't already exist
            await queue.CreateIfNotExistsAsync();
            return queue;
        }

        public async Task RemoveImageAsync(string containerName, string blobName)
        {
            var container = await GetCloudBlobContainer(containerName);
            var blockBlob = container.GetBlockBlobReference(blobName);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task RemoveImageAsync(string containerName, string blobName, string subFolder)
        {
            var container = await GetCloudBlobContainer(containerName);
            var directory = container.GetDirectoryReference(subFolder);
            var blockBlob = directory.GetBlockBlobReference(blobName);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task<CloudBlockBlob> UploadAsync(Stream stream, string containerName, string subFolder, string contentType, string blobName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off)
        {
            if (stream is MemoryStream)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }
            var container = await GetCloudBlobContainer(containerName, blobContainerPublicAccessType);
            var directory = container.GetDirectoryReference(subFolder);            
            var blockBlob = directory.GetBlockBlobReference(blobName);
            blockBlob.Properties.ContentType = contentType;
            await blockBlob.UploadFromStreamAsync(stream);
            return blockBlob;
        }

        public async Task<CloudBlockBlob> UploadImageAsync(Stream stream, string containerName, string subFolder, string blobName, bool isVideo = false, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off)
        {
            if (stream is MemoryStream)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }

            var container = await GetCloudBlobContainer(containerName, blobContainerPublicAccessType);
            var directory = container.GetDirectoryReference(subFolder);
            var blockBlob = directory.GetBlockBlobReference(blobName);
            if (isVideo)
            {
                blockBlob.Properties.ContentType = "video/mp4";
            }

            await blockBlob.UploadFromStreamAsync(stream);
            return blockBlob;
        }

        public async Task<CloudBlockBlob> UploadTemplateAsync(Stream stream, string containerName, string contentType, string blobName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off)
        {
            if (stream is MemoryStream)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }
            var container = await GetCloudBlobContainer(containerName, blobContainerPublicAccessType);
            var blockBlob = container.GetBlockBlobReference(blobName);
            blockBlob.Properties.ContentType = contentType;
            await blockBlob.UploadFromStreamAsync(stream);
            return blockBlob;
        }
    }
}
