﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Services.Azure.Cosmos
{
    public interface IGenericMongoService<TMapper> where TMapper : class
    {
        Task<TMapper> GetAsync(string id);

        Task InsertAsync(TMapper entity);

        Task<List<TMapper>> ListAsync(string query);

        Task DeleteAsync(string id);

        Task UpdateAsync(TMapper entity);
    }
}
