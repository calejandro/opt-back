﻿using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System.IO;
using System.Threading.Tasks;

namespace Optimus.Commons.Services.Azure
{
    public interface IBlobHelperService
    {
        Task<MemoryStream> GetBlob(string containerName, string blobName);

        Task<MemoryStream> GetBlob(string containerName, string subFolder, string blobName);

        Task<MemoryStream> GetBlob(string url);

        Task<CloudBlobContainer> GetCloudBlobContainer(string containerName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off);

        Task<CloudQueue> GetCloudQueue(string name);

        Task RemoveImageAsync(string containerName, string blobName, string subFolder);

        Task RemoveImageAsync(string containerName, string blobName);

        Task<CloudBlockBlob> UploadAsync(Stream stream, string containerName, string subFolder, string contentType, string blobName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off);

        Task<CloudBlockBlob> UploadImageAsync(Stream stream, string containerName, string subFolder, string blobName, bool video = false, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off);

        Task<CloudBlockBlob> UploadTemplateAsync(Stream stream, string containerName, string contentType, string blobName, BlobContainerPublicAccessType blobContainerPublicAccessType = BlobContainerPublicAccessType.Off);
    }
}
