﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using Optimus.Commons.Services.Azure;
using Optimus.Commons.ViewModels;

namespace Optimus.Commons.Services
{
   public class UploadService<T, TFilter, TKey, TMapper, TCreate, TGenericRepository> :
        GenericServiceMapper<T, TCreate, TKey, TFilter, TMapper, TGenericRepository>,
        IUploadService<TMapper, TCreate, TKey, TFilter>
       where T : class
       where TFilter : BaseFilter
       where TMapper : class
       where TCreate : class
       where TGenericRepository : IGenericRepository<T, TKey, TFilter>
       where TKey : IEquatable<TKey>
   {
      private readonly IBlobHelperService blobHelperService;

      public UploadService(TGenericRepository genericRepository, IMapper mapper,
          IBlobHelperService blobHelperService) : base(genericRepository, mapper)
      {
         this.blobHelperService = blobHelperService;
      }

      public virtual async Task<TMapper> UploadAsync(TKey entityId, UploadViewModel uploadViewModel)
      {
         var entity = await this.genericRepository.GetAsync(entityId);
         if (entity is BaseUpload)
         {
            var fileExtension = Path.GetExtension(uploadViewModel.File.FileName);
            var blob = await this.blobHelperService.UploadAsync(uploadViewModel.File.OpenReadStream(), uploadViewModel.Container,
                uploadViewModel.SubFolder, uploadViewModel.File.ContentType, $"{Guid.NewGuid()}{fileExtension}");
            (entity as BaseUpload).FileUrl = blob.Uri.ToString();
            await this.genericRepository.UpdateAsync(entity);
         }

         return this.mapper.Map<TMapper>(entity);
      }
   }
}
