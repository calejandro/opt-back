﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Services
{
   public interface IGenericServiceMapper<TMapper, TCreate, Tkey, TFilter> : IDisposable
        where TMapper : class
       where TFilter : BaseFilter
       where Tkey : IEquatable<Tkey>
   {
      Task<int> CountAsync(TFilter filter, string[] includes = null);

      Task DeleteAsync(Tkey id);

      Task DeleteAllAsync(Tkey[] ids);

      Task<bool> ExistsAsync(Tkey id);

      Task<TMapper> GetAsync(Tkey id, string[] includes = null);
      Task<TMapper> GetAsync(string[] includes = null, TFilter filter = null, bool includeDeleted = false);
      Task<TMapper> InsertAsync(TCreate entity);

      Task InsertAllAsync(IList<TMapper> list);

      Task<List<TMapper>> ListAsync(string[] includes = null, TFilter filter = null, PaginatorRequestModel paginatorRequestModel = null, List<Order> orders = null);

      Task<TMapper> UpdateAsync(TCreate entity);

      Task UpdateAllAsync(IList<TMapper> entity);

   }
}
