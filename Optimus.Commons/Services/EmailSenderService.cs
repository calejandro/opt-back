﻿using Optimus.Commons.Configurations;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Commons.Services
{
    public class EmailSenderService : IEmailSenderService
    {
        public EmailSenderService(IOptions<SendgridConfig> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public SendgridConfig Options { get; }

        public Task SendEmailAsync(List<string> emails, string subject, string message)
        {
            return Execute(subject, message, emails);
        }

        public Task Execute(string subject, string message, List<string> emails)
        {
            var client = new SendGridClient(Options.SendGridKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(Options.SendGridEmailSender, Options.SendGridSender),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };

            foreach (var email in emails)
            {
                msg.AddTo(new EmailAddress(email));
            }

            Task<Response> response = client.SendEmailAsync(msg);
            return response;
        }

        public Task<Response> SendEmailAsync(List<string> emails, string templateId, Object templateData = null)
        {
            return ExecuteTemplate(emails, templateId, templateData);
        }

        public Task<Response> SendEmailAsync(List<string> emails, string templateId, Object templateData = null, List<string> attachBase64Images = null)
        {
            return ExecuteTemplate(emails, templateId, templateData, attachBase64Images);
        }

        public Task<Response> ExecuteTemplate(List<string> emails, string templateId, Object templateData = null, List<string> attachBase64Images = null)
        {
            var client = new SendGridClient(Options.SendGridKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(Options.SendGridEmailSender, Options.SendGridSender),
                TemplateId = templateId
            };

            if (templateData != null)
            {
                msg.SetTemplateData(templateData);
            }

            foreach (var email in emails)
            {
                msg.AddTo(new EmailAddress(email));
            }

            int contentIndex = 1;
            if(attachBase64Images != null)
            {
                foreach (var base64image in attachBase64Images)
                {
                    msg.AddAttachment(new Attachment()
                    {
                        Content = base64image,
                        ContentId = $"contentId_{contentIndex}",
                        Disposition = "inline",
                        Filename = $"contentId_{contentIndex}.png",
                        Type = "image/png"
                    });

                    contentIndex++;
                }
            }

            return client.SendEmailAsync(msg);
        }
    }
}
