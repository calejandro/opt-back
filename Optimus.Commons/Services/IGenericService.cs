﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Services
{
   public interface IGenericService<T, TKey, TFilter> : IDisposable
       where T : class
       where TFilter : BaseFilter
       where TKey : IEquatable<TKey>
   {
      Task DeleteAsync(TKey id);

      Task DeleteAllAsync(TKey[] ids);

      Task<T> GetAsync(TKey id, string[] includes = null);

      Task<T> GetAsync(TKey id);

      Task<T> InsertAsync(T entity);

      Task InsertAllAsync(List<T> list);

      Task<List<T>> ListAsync();

      Task<T> UpdateAsync(T entity);

      Task UpdateAllAsync(List<T> entity);
   }
}
