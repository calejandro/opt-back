﻿using System;
namespace Optimus.Commons.Services.Email
{
    public class EmailConfigOptions
    {

        public string From { get; set; }

        public string FromEmail { get; set; }

        public string SmtpUser { get; set; }

        public string SmtpUri { get; set; }

        public int? SmtpPort { get; set; }

        public string SmtpPassword { get; set; }

        public bool IsDebug { get; set; }

        public string SendGridApiKey { get; set; }

        public bool SendToTestUsers { get; set; }

        public string TestUserIds { get; set; }

        public bool Ssl { get; set; }

        public EmailConfigOptions()
        {
            Ssl = false;
        }
    }
}
