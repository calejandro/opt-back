﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Optimus.Commons.ViewModels;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Optimus.Commons.Services.Email
{
    public class EmailSender : IEmailSender
    {
        private ILogger<EmailRecipientsViewModel> logger;
        public EmailSender(IOptions<EmailConfigOptions> emailOptions,
                            ILogger<EmailRecipientsViewModel> logger)
        {
            EmailConfig = emailOptions.Value;
            this.logger = logger;
        }

        public EmailConfigOptions EmailConfig { get; private set; }

        public bool UseIntenalEmailService => EmailConfig.IsDebug;

        public bool SendTestEmail => EmailConfig.SendToTestUsers;

        public string TestUsers => EmailConfig.TestUserIds;

        public void SendEmail(string email, string subject, string resource, Dictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        public async Task SendEmailAsync(List<EmailRecipientsViewModel> emailRecipients, string subject, string message, EmailAttachmentViewModel attachment, bool isHtml = true)
        {
            await this.SendEmailAsync(emailRecipients, subject, message, new List<EmailAttachmentViewModel>(new EmailAttachmentViewModel[] { attachment }), isHtml);
        }

        public async Task SendEmailAsync(List<EmailRecipientsViewModel> emailRecipients, string subject, string message, List<EmailAttachmentViewModel> attachment, bool isHtml = true)
        {
            if (UseIntenalEmailService)
            {
                try
                {
                    // Send using Sendgrid
                    var client = new SendGridClient(EmailConfig.SendGridApiKey);
                    var from = new EmailAddress(EmailConfig.FromEmail);
                    var tos = emailRecipients.Select(t => new EmailAddress(t.Email, t.Name)).ToList();
                    SendGridMessage gridMessage;
                    if (isHtml)
                    {
                         gridMessage = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, string.Empty, message);
                    }
                    else
                    {
                         gridMessage = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, message, string.Empty);
                    }

                    if (attachment != null)
                    {
                        foreach (var item in attachment)
                        {
                            gridMessage.AddAttachment(new SendGrid.Helpers.Mail.Attachment
                            {
                                Content = Convert.ToBase64String(item.FileContent),
                                ContentId = "ReportPdf",
                                Disposition = "attachment",
                                Type = System.Net.Mime.MediaTypeNames.Application.Pdf,
                                Filename = item.FileName
                            });
                        }
                       
                    }

                    var emailResponse = await client.SendEmailAsync(gridMessage);
                    if (emailResponse.StatusCode == HttpStatusCode.Accepted)
                    {
                        logger.LogInformation($"Envio de email a {string.Join(',', emailRecipients.Select(t => t.Email).ToArray())} con el estado {emailResponse.StatusCode}");
                    }
                    else
                    {
                        var logMessage = emailResponse.Body.ReadAsStringAsync().Result;
                        logger.LogInformation($@"Envio de email a {string.Join(',', emailRecipients.Select(t => t.Email).ToArray())} con el estado {emailResponse.StatusCode} y el 
                                             mensaje de error {logMessage}");
                    }
                }
                catch (Exception e)
                {
                    logger.LogInformation($@"Envio de email Excepcion disparada con  el mensaje de error {e.Message} - {e.InnerException}");
                    throw;
                }

            }
            else
            {

                MailAddress from = new MailAddress(EmailConfig.FromEmail, EmailConfig.From);

                MailMessage messageToSend = new MailMessage();
                emailRecipients.ForEach(t => messageToSend.To.Add(new MailAddress(t.Email, t.Name)));
                messageToSend.From = new MailAddress(EmailConfig.FromEmail);
                messageToSend.Subject = subject;
                messageToSend.Body = message;
                messageToSend.IsBodyHtml = isHtml;

                if (attachment != null)
                {
                    foreach (var item in attachment)
                    {
                        messageToSend.Attachments.Add(new System.Net.Mail.Attachment(new MemoryStream(item.FileContent), item.FileName, System.Net.Mime.MediaTypeNames.Application.Pdf));
                        logger.LogInformation("Attachment agregado al mensaje");
                    }
                    
                }

                SmtpClient client = EmailConfig.SmtpPort.HasValue ? new SmtpClient(EmailConfig.SmtpUri, EmailConfig.SmtpPort.Value) : new SmtpClient(EmailConfig.SmtpUri);
                if (!string.IsNullOrEmpty(EmailConfig.SmtpUser))
                {
                    client.Credentials = new NetworkCredential(EmailConfig.SmtpUser, EmailConfig.SmtpPassword);
                }
                client.EnableSsl = EmailConfig.Ssl;

                try
                {
                    client.Send(messageToSend);
                    logger.LogInformation($"Envio de email a los siguientes usarios {string.Join(',', emailRecipients.Select(p => p.Email).ToArray())} ");
                }
                catch (Exception ex)
                {
                    logger.LogInformation($@"Envio de email Excepcion disparada con  el mensaje de error {ex.Message} - {ex.InnerException}");
                    // throw new SgiException(ex.Message);
                    throw;
                }
            }
        }
    }
}
