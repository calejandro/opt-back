﻿using Optimus.Commons.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Commons.Services.Email
{
    public interface IEmailSender
    {
        bool UseIntenalEmailService { get; }

        bool SendTestEmail { get; }

        string TestUsers { get;  }

        Task SendEmailAsync(List<EmailRecipientsViewModel> emailRecipients, string subject, string message, List<EmailAttachmentViewModel> attachment, bool isHtml = true);

        Task SendEmailAsync(List<EmailRecipientsViewModel> emailRecipients, string subject, string message, EmailAttachmentViewModel attachment, bool isHtml = true);

        void SendEmail(string email, string subject, string resource, Dictionary<string,string> parameters);
    }
}
