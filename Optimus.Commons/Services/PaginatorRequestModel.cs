﻿using System.Collections.Generic;
using Optimus.Commons.Entities;

namespace Optimus.Commons.Services
{
   public class PaginatorRequestModel
   {
      public int RowsPage { get; set; }

      public int Page { get; set; }

      public PaginatorRequestModel()
      {
      }
   }
}
