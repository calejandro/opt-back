﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage;
using Optimus.Commons.Entities;
using Optimus.Commons.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Commons.Services
{
   public abstract class GenericServiceMapper<T, TCreate, TKey, TFilter, TMapper, TGenericRepository> :
       IGenericServiceMapper<TMapper, TCreate, TKey, TFilter>
       where T : class
       where TCreate : class
       where TFilter : BaseFilter
       where TMapper : class
       where TGenericRepository : IGenericRepository<T, TKey, TFilter>
       where TKey : IEquatable<TKey>
   {
      protected TGenericRepository genericRepository;
      protected readonly IMapper mapper;

      public GenericServiceMapper(TGenericRepository genericRepository, IMapper mapper)
      {
         this.genericRepository = genericRepository;
         this.mapper = mapper;
      }

      public async virtual Task DeleteAsync(TKey id)
      {
         await genericRepository.DeleteAsync(id);
      }

      public async virtual Task<TMapper> GetAsync(TKey id, string[] includes = null)
      {
         return mapper.Map<TMapper>(await genericRepository.GetAsync(id, includes));
      }

      public async virtual Task<TMapper> GetAsync(TKey id)
      {
         return mapper.Map<TMapper>(await genericRepository.GetAsync(id, null));
      }
        public async virtual Task<TMapper> GetAsync(string[] includes = null, TFilter filter = null, bool includeDeleted = false)
        {
            return mapper.Map<TMapper>(await genericRepository.GetAsync(includes,filter,includeDeleted));
        }
        public async virtual Task<bool> ExistsAsync(TKey id)
      {
         return await genericRepository.ExistsAsync(id);
      }

      public async virtual Task<TMapper> InsertAsync(TCreate entity)
      {
         return mapper.Map<TMapper>(await genericRepository.InsertAsync(mapper.Map<T>(entity)));

      }

      public async virtual Task<TMapper> UpdateAsync(TCreate entity)
      {
         return mapper.Map<TMapper>(await genericRepository.UpdateAsync(mapper.Map<T>(entity)));
      }

      public virtual async Task<int> CountAsync()
      {
         return await genericRepository.CountAsync();
      }

      ~GenericServiceMapper()
      {
         Dispose(false);
      }

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
         // This method will remove current object from garbage collector's queue 
         // and stop calling finilize method twice 
      }

      public void Dispose(bool disposer)
      {
         if (disposer)
         {
            // dispose the managed objects
            genericRepository.Dispose();
         }
         // dispose the unmanaged objects
      }

      public async virtual Task DeleteAllAsync(TKey[] ids)
      {
         await genericRepository.DeleteAllAsync(ids);
      }

      public async Task UpdateAllAsync(IList<TMapper> list)
      {
         await genericRepository.UpdateAllAsync(mapper.Map<List<T>>(list));
      }

      public async virtual Task InsertAllAsync(IList<TMapper> list)
      {
         await genericRepository.InsertAllAsync(mapper.Map<List<T>>(list));
      }

      public virtual IDbContextTransaction BeginTransaction()
      {
         return this.genericRepository.BeginTransaction();
      }

      public virtual async Task<List<TMapper>> ListAsync(TFilter filter)
      {
         return await this.ListAsync(null, filter);
      }

      public virtual Task<int> CountAsync(TFilter filter, string[] includes = null)
      {
         return this.genericRepository.CountAsync(filter, includes);
      }

      public virtual async Task<List<TMapper>> ListAsync(string[] includes = null, TFilter filter = null, PaginatorRequestModel requestModel = null, List<Order> orders = null)
      {
         var result = await genericRepository.ListAsync(requestModel, includes, filter, orders);
         return this.mapper.Map<List<TMapper>>(result);
      }
   }
}
