﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Commons.Services
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(List<string> emails, string subject, string message);

        Task<Response> SendEmailAsync(List<string> emails, string templateId, Object templateData = null);

        Task<Response> SendEmailAsync(List<string> emails, string templateId, Object templateData = null, List<string> attachBase64Images = null);
    }
}
