﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Quartz;

namespace Optimus.Commons.Services.Quartz
{
    public interface IQuartzService
    {
        Task<(IJobDetail, ITrigger)> AddOrUpdateJob(JobSchedule jobSchedule);
        Task StopAsync(CancellationToken cancellationToken);
        Task StartAsync(CancellationToken cancellationToken);
        Task<bool> ResumeJob(string key);
        Task<bool> PauseJob(string key);
        Task<bool> ExistJob(string key);
        Task<bool> RemoveJob(string key);
        Task<bool> RunNowJob(string key);
    }
}
