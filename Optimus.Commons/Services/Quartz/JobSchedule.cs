﻿using System;

namespace Optimus.Commons.Services
{
    public class JobSchedule
    {
        public JobSchedule(string jobKey,
            Type jobType,
            string cronExpression,
            string description,
            bool runStart = false)
        {
            JobType = jobType;
            CronExpression = cronExpression;
            JobKey = jobKey;
            Description = description;
            this.RunStart = runStart;
        }

        public bool RunStart { get; set; }
        public Type JobType { get; }
        public string JobKey { get; }
        public string CronExpression { get; }
        public string Description { get; }
        public bool IsNoProgram { get { return string.IsNullOrEmpty(this.CronExpression?.Trim()); } }
    }
}
