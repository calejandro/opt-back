﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace Optimus.Commons.Services.Quartz
{
    public static class QuartzServicesExtension
    {
        public static IServiceCollection AddQuartz(this IServiceCollection services)
        {
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<IQuartzService, QuartzService>();
            services.AddHostedService<QuartzHostedService>();
            return services;
        }
    }
}
