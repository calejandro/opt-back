﻿using Optimus.Commons.Services.Quartz;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Optimus.Commons.Services
{
    public class QuartzHostedService : IHostedService
    {
        private readonly IEnumerable<JobSchedule> jobSchedules;        
        private readonly IQuartzService quartzService;

        public QuartzHostedService(            
            IEnumerable<JobSchedule> jobSchedules,            
            IQuartzService quartzService)
        {            
            this.jobSchedules = jobSchedules;                        
            this.quartzService = quartzService;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            List<string> runJobStart = new List<string>();
            foreach (var jobSchedule in jobSchedules)
            {
                (var job, var trigger) = await this.quartzService.AddOrUpdateJob(jobSchedule);
                if(jobSchedule.RunStart)
                {
                    runJobStart.Add(jobSchedule.JobKey);
                }
            }            

            await this.quartzService.StartAsync(cancellationToken);

            foreach (var item in runJobStart)
            {
                await this.quartzService.RunNowJob(item);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await this.quartzService.StopAsync(cancellationToken);
        }
    }
}
