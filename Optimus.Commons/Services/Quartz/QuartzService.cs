﻿using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Spi;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Optimus.Commons.Services.Quartz
{
    public class QuartzService : IQuartzService
    {
        private readonly ISchedulerFactory schedulerFactory;
        private readonly IJobFactory jobFactory;
        private readonly ILogger<QuartzService> logger;
        public IScheduler Scheduler { get; set; }

        public QuartzService(ISchedulerFactory schedulerFactory,
                             IJobFactory jobFactory,
                             ILogger<QuartzService> logger)
        {
            this.schedulerFactory = schedulerFactory;
            this.jobFactory = jobFactory;
            this.logger = logger;
            Scheduler = schedulerFactory.GetScheduler().Result;
            Scheduler.JobFactory = jobFactory;
        }

        public async Task<(IJobDetail, ITrigger)> AddOrUpdateJob(JobSchedule jobSchedule)
        {
            try
            {
                var jobKey = new JobKey(jobSchedule.JobKey);
                var exist = await Scheduler.CheckExists(jobKey);
                IJobDetail jobDetail = null;
                ITrigger trigger = null;
                trigger = CreateTrigger(jobSchedule);
                if (!exist)
                {
                    jobDetail = CreateJob(jobSchedule);
                    await Scheduler.ScheduleJob(jobDetail, trigger);
                }
                else
                {
                    await Scheduler.RescheduleJob(new TriggerKey($"{jobSchedule.JobKey}.trigger"), trigger);
                }
                this.logger.LogInformation("Job {0} Next Fire: {1}", jobSchedule.Description, trigger.GetNextFireTimeUtc().Value.LocalDateTime);
                return (jobDetail, trigger);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                throw ex;
            }
        }

        public async Task<bool> ResumeJob(string key)
        {
            var jobKey = new JobKey(key);
            if (await Scheduler.CheckExists(jobKey))
            {
                await Scheduler.ResumeJob(jobKey);
                this.logger.LogInformation($"Job {key} Resume");
                return true;
            }

            return false;
        }

        public async Task<bool> RemoveJob(string key)
        {
            var jobKey = new JobKey(key);
            if (await Scheduler.CheckExists(jobKey))
            {
                await Scheduler.DeleteJob(jobKey);
                this.logger.LogInformation($"Job {key} Remove");
                return true;
            }

            return false;
        }

        public async Task<bool> PauseJob(string key)
        {
            var jobKey = new JobKey(key);
            if (await Scheduler.CheckExists(jobKey))
            {
                await Scheduler.PauseJob(jobKey);
                this.logger.LogInformation($"Job {key} Paused");
                return true;
            }

            return false;
        }

        public async Task<bool> ExistJob(string key)
        {
            return await Scheduler.CheckExists(new JobKey(key));
        }

        public async Task<bool> RunNowJob(string key)
        {
            var jobKey = new JobKey(key);
            if (await Scheduler.CheckExists(jobKey))
            {
                await Scheduler.TriggerJob(jobKey);
                return true;
            }           

            return false;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Scheduler?.Shutdown(cancellationToken);
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await Scheduler?.Start(cancellationToken);
        }

        private static IJobDetail CreateJob(JobSchedule schedule)
        {
            var jobType = schedule.JobType;
            return JobBuilder
                .Create(jobType)
                .WithIdentity(new JobKey(schedule.JobKey))
                .WithDescription(jobType.Name)
                .Build();
        }

        private static ITrigger CreateTrigger(JobSchedule schedule)
        {
            var trigger = TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobKey}.trigger")
                .WithDescription(schedule.Description);

            if (!schedule.IsNoProgram)
            {
                trigger = trigger.WithCronSchedule(schedule.CronExpression);
            }
            else
            {
                trigger = trigger.WithCronSchedule("0 0 0 ? * * 2099/1");
                
            }
            
            return trigger.Build();
        }
    }
}
