﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Optimus.Commons.Entities;
using Optimus.Commons.ViewModels;

namespace Optimus.Commons.Services
{
   public interface IUploadService<TMapper, TCreate, TKey, TFilter> : IGenericServiceMapper<TMapper, TCreate, TKey, TFilter>
      where TMapper : class
      where TFilter : BaseFilter
      where TCreate : class
      where TKey : IEquatable<TKey>
   {
      Task<TMapper> UploadAsync(TKey entityId, UploadViewModel uploadViewModel);
   }
}
