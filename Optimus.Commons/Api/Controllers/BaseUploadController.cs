﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Commons.ViewModels;

namespace Optimus.Commons.Api.Controllers
{
   public class BaseUploadController<T, TKey, TFilter, TGenericSerivce> : BaseApiMapperController<T, T, TKey, TFilter, TGenericSerivce> where T : class
    where TFilter : BaseFilter
    where TGenericSerivce : IUploadService<T, T, TKey, TFilter>
    where TKey : IEquatable<TKey>
   {
      public BaseUploadController(TGenericSerivce genericService, IMapper mapper) : base(genericService, mapper)
      {
      }

      [HttpPost("upload")]
      [RequestSizeLimit(52428800)]
      public async Task<T> Upload([FromForm] UploadViewModel uploadViewModel, [FromQuery] TKey id)
      {
         return await this.genericService.UploadAsync(id, uploadViewModel);
      }
   }
}