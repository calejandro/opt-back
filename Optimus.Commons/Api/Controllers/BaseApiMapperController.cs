﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Swashbuckle.AspNetCore.Annotations;
using Newtonsoft.Json;
using IdentityServer4.Extensions;

namespace Optimus.Commons.Api.Controllers
{
   [ApiController]
   [Route("api/[controller]")]
   [Authorize]

    public class BaseApiMapperController<T, TCreate, TKey, TFilter, TGenericSerivce> : ControllerBase
        where T : class
        where TCreate : class
        where TFilter : BaseFilter
        where TGenericSerivce : IGenericServiceMapper<T, TCreate, TKey, TFilter>
        where TKey : IEquatable<TKey>
   {
      protected readonly TGenericSerivce genericService;
      private readonly IMapper mapper;

      public BaseApiMapperController(TGenericSerivce genericService, IMapper mapper)
      {
         this.genericService = genericService;
         this.mapper = mapper;
      }

      [HttpGet]
      public virtual async Task<IEnumerable<T>> Get([FromQuery] string[] includes = null, [FromQuery, SwaggerParameter(Description = "Filter")] TFilter filter = null, [FromQuery, SwaggerParameter(Description = "Paginator")] PaginatorRequestModel paginator = null,
          [FromQuery, SwaggerParameter(Description = "Order")] string order = null)
      {
           //var filters = filter == null ? null : JsonConvert.DeserializeObject<TFilter>(filter);
         return await genericService.ListAsync(includes,
              filter,
             paginator,
             order == null ? null : JsonConvert.DeserializeObject<List<Order>>(order));
      }

      [HttpGet("{id}")]
      public virtual async Task<T> Get([FromRoute] TKey id, [FromQuery] string[] includes = null)
      {
         return await genericService.GetAsync(id, includes);
      }

      [HttpGet("{id}/exists")]
      public virtual async Task<bool> Exists([FromRoute] TKey id)
      {
            return await genericService.ExistsAsync(id);
      }

      [HttpGet("count")]
      public virtual async Task<int> GetCount([FromQuery, SwaggerParameter(Description = "Filter")] TFilter filter = null,
          [FromQuery, SwaggerParameter(Description = "Includes")] string[] includes = null)
      {
         return await genericService.CountAsync(filter, includes);
      }

      [HttpPost]
      public virtual async Task<ActionResult<T>> Create([FromBody] TCreate entity)
      {
         return await genericService.InsertAsync(entity);
      }

      [HttpPut]
      public virtual async Task<T> Update([FromBody] TCreate entity)
      {
         return await genericService.UpdateAsync(entity);
      }

      [HttpDelete("{id}")]
      public virtual async Task DeleteAsync([FromRoute] TKey id)
      {
         await genericService.DeleteAsync(id);
      }
        protected Guid? UserId
        {
            get
            {
                Guid? userId = null;
                if (this.User.IsAuthenticated())
                {
                    var claim = this.User.Claims.Where(w => w.Type == ClaimTypes.NameIdentifier).SingleOrDefault();
                    if (claim != null)
                    {
                        userId = new Guid(claim.Value);
                    }
                }

                return userId;
            }
        }

        protected string UserName
        {
            get
            {
                string userId = null;
                if (this.User.IsAuthenticated())
                {
                    var claim = this.User.Claims.Where(w => w.Type == "name").SingleOrDefault();
                    if (claim != null)
                    {
                        userId = claim.Value;
                    }
                }

                return userId;
            }
        }

        protected string GiveName
        {
            get
            {
                string userId = null;
                if (this.User.IsAuthenticated())
                {
                    var claim = this.User.Claims.Where(w => w.Type == "giveName").SingleOrDefault();
                    if (claim != null)
                    {
                        userId = claim.Value;
                    }
                }

                return userId;
            }
        }

        protected string LastName
        {
            get
            {
                string userId = null;
                if (this.User.IsAuthenticated())
                {
                    var claim = this.User.Claims.Where(w => w.Type == "surName").SingleOrDefault();
                    if (claim != null)
                    {
                        userId = claim.Value;
                    }
                }

                return userId;
            }
        }
    }
}
