﻿using System;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Optimus.Commons.Exceptions;
using Microsoft.Extensions.Logging;

namespace Optimus.Commons.Api.Middlewares
{
    public class ErrorHandlingMiddleware
    {
		private readonly RequestDelegate next;

		public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context, ex);
            }
        }

		private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            
			var code = HttpStatusCode.InternalServerError;
            BusinessExceptionCodeEnumeration errorCode = BusinessExceptionCodeEnumeration.Unknown;
            if (exception is BussinessException)
            {                
                code = HttpStatusCode.BadRequest;
                errorCode = (exception as BussinessException).ErrorCode;
            }

            var result = JsonConvert.SerializeObject(new { message = exception.Message, errorCode = errorCode, stack = exception.StackTrace });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
