﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Optimus.Commons.Contexts;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionDbContextExtension
    {
        public static IServiceCollection AddSqlServerDbContext<TContext>(this IServiceCollection services, string connectionString, int poolConnection = 5) where TContext : DbContext
        {
            return services.AddDbContext<TContext>(options =>
            {
                options.EnableSensitiveDataLogging(true);
                options.UseSqlServer(connectionString, providerOptions =>
                {
                    providerOptions.EnableRetryOnFailure();
                    providerOptions.CommandTimeout(180);
                });
            }, ServiceLifetime.Scoped);
        }


        public static IServiceCollection AddInMemoryDbContext<TContext>(this IServiceCollection services, string databaseName) where TContext : BaseContext
        {
            return services.AddDbContext<TContext>(options =>
            {
                options.EnableSensitiveDataLogging(true);
                options.UseInMemoryDatabase(databaseName);
            }, ServiceLifetime.Scoped);
        }
    }

}
