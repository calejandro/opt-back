﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionSwaggerExtension
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services, string apiName, string version = "v1", Action<SwaggerGenOptions> config = null)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            return services.AddSwaggerGen(c =>
            {                
                c.SwaggerDoc(version, new Microsoft.OpenApi.Models.OpenApiInfo { Title = apiName });
                //c.EnableAnnotations();
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Scheme = "Bearer"
                });
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                config(c);
            });
        }

        
    }
}
