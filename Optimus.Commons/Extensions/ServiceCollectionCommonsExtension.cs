﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Optimus.Commons.Services.Azure;
using Optimus.Commons.Services.Background;
using Microsoft.AspNetCore.Http;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionCommonsExtension
    {
        public static IServiceCollection AddCommonsServices(this IServiceCollection services)
        {
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            services.AddScoped<IBlobHelperService, BlobHelperService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            return services;
        }
    }
}
