﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Optimus.Commons.Extensions
{
    public static class OrderExtensions
    {
        public static Func<object, object> CreateOrderField(Type runtimeType, string propertyName)
        {
            var propertyInfo = runtimeType.GetProperty(propertyName);
            var expression = Expression.Parameter(typeof(object), "obj");
            var typeAs = Expression.TypeAs(expression, runtimeType);
            var property = Expression.Property(typeAs, propertyInfo);

            var convert = Expression.TypeAs(property, typeof(object));
            return (Func<object, object>)Expression.Lambda(convert, expression).Compile();
        }

        public static IOrderedQueryable<T> OrderByPropertyName<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, false);
        }

        public static IOrderedQueryable<T> OrderByPropertyName<T>(this IQueryable<T> source, string propertyName, bool descending)
        {
            return OrderingHelper(source, propertyName, descending, false);
        }

        public static IOrderedQueryable<T> ThenByPropertyName<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, true);
        }

        public static IOrderedQueryable<T> ThenByPropertyName<T>(this IOrderedQueryable<T> source, string propertyName, bool descending)
        {
            return OrderingHelper(source, propertyName, descending, true);
        }

        private static IOrderedQueryable<T> OrderingHelper<T>(IQueryable<T> source, string propertyName, bool descending, bool anotherLevel)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var property = Expression.PropertyOrField(param, propertyName);
            var sort = Expression.Lambda(property, param);

            var call = Expression.Call(
                typeof(Queryable),
                (!anotherLevel ? "OrderBy" : "ThenBy") + (descending ? "Descending" : string.Empty),
                new[] { typeof(T), property.Type },
                source.Expression,
                Expression.Quote(sort));

            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(call);
        }
    }
}
