﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Optimus.Commons.Extensions
{
    public static class IdentityExtension
    {
        public static IdentityBuilder AddIdentity<TUser, TRole, TKey, TContext, TErrorDescriber>(this IServiceCollection services, IConfiguration configuration)
            where TUser : IdentityUser<TKey>
            where TRole : IdentityRole<TKey>
            where TContext : DbContext
            where TErrorDescriber : IdentityErrorDescriber
            where TKey : IEquatable<TKey>
        {
            return services.AddIdentity<TUser, TRole>(options => configuration.Bind("IdentityOptions", options))
                                          .AddEntityFrameworkStores<TContext>()
                                          .AddDefaultTokenProviders()
                                          .AddErrorDescriber<TErrorDescriber>();


        }
    }
}
