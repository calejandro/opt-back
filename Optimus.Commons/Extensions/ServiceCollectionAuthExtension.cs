﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionAuthExtension
    {
        public static IServiceCollection AddFusionisB2CAuthentication(this IServiceCollection services, IConfiguration configurations)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(jwtOptions =>
           {
               jwtOptions.MetadataAddress = configurations["JwtBearerConfiguration:MetadataAddress"];
               jwtOptions.Audience = configurations["JwtBearerConfiguration:Audience"];
               jwtOptions.Events = new JwtBearerEvents
               {
                   OnAuthenticationFailed = AuthenticationFailed
               };
               jwtOptions.TokenValidationParameters = new TokenValidationParameters()
               {
                   ValidAudience = configurations["JwtBearerConfiguration:Audience"],
                   ValidIssuer = configurations["JwtBearerConfiguration:ValidIssuer"]
               };
           });

            return services.AddAuthorization(configure =>
            {
                configure.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
            });
        }

        private static Task AuthenticationFailed(AuthenticationFailedContext arg)
        {
            // For debugging purposes only!
            var s = $"AuthenticationFailed: {arg.Exception.Message}";
            arg.Response.ContentLength = s.Length;
            arg.Response.Body.Write(Encoding.UTF8.GetBytes(s), 0, s.Length);
            return Task.FromException(arg.Exception);
        }
    }
}
