﻿using System;
using Microsoft.Azure.Cosmos.Fluent;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Optimus.Commons.Contexts;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionMongo
    {
        public static IServiceCollection AddMongo<TMongoContext>(this IServiceCollection services, string connectionString, string database, Action<MongoClientSettings> config)
            where TMongoContext : BaseMongoContext
        {
            services.AddScoped<TMongoContext>(sp =>
            {
                var settings = MongoClientSettings.FromConnectionString(connectionString);
                config(settings);
                return BaseMongoContext.Create<TMongoContext>(settings, database);
            });

            return services;
        }        
    }
}