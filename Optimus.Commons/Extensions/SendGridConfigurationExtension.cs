﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Optimus.Commons.Configurations;
using Optimus.Commons.Services;

namespace Optimus.Commons.Extensions
{
    public static class SendGridConfigurationExtension
    {
        public static IServiceCollection AddSendGridConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<SendgridConfig>(configuration.GetSection("SendGridConfig"));
            services.AddTransient<IEmailSenderService, EmailSenderService>();

            return services;
        }
    }
}
