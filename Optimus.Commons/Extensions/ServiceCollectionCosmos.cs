﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Fluent;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Optimus.Commons.Configurations;
using Optimus.Commons.Contexts;

namespace Optimus.Commons.Extensions
{
    public static class ServiceCollectionCosmos
    {
        public static IServiceCollection AddCosmos<TCosmosContext>(this IServiceCollection services, string accountEndpoint, string key, string database)
            where TCosmosContext : BaseCosmosContext
        {
            services.AddScoped<TCosmosContext>(sp =>
            {
                return Task<TCosmosContext>.Run(async () =>
                {
                    return await BaseCosmosContext.CreateAsync<TCosmosContext>(new CosmosClientBuilder(accountEndpoint, key), database);

                }).Result;                
            });

            return services;
        }        
    }
}
