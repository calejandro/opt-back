﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Optimus.Commons.Configurations;
using Microsoft.AspNetCore.Http;

namespace Optimus.Commons.Extensions
{
    public static class CommonsConfigurationExtension
    {
        public static IServiceCollection AddCommonsConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();

            services.AddSingleton<IConfiguration>(configuration);

            services.Configure<AzureStorageConfig>(configuration.GetSection("AzureStorageConfig"));

            return services;
        }
    }
}
