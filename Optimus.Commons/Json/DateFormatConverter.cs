﻿using System;
using Newtonsoft.Json.Converters;

namespace Optimus.Commons.Json
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}