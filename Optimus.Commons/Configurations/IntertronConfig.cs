﻿namespace Optimus.Commons.Configurations
{
    public class IntertronConfig
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string SendMessage { get; set; }
        public string RecivedDlr { get; set; }
        public string BatchSize { get; set; }
    }
}
