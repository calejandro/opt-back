﻿using System;
namespace Optimus.Commons.Configurations
{
    public class JwtBearerConfiguration
    {
        public JwtBearerConfiguration()
        {
        }

        public string Audience { get; set; }

        public string Authority { get; set; }

        public string MetadataAddress { get; set; }

        public Guid TenantId { get; set; }

        public string ValidIssuer { get; set; }
    }
}
