﻿namespace Optimus.Commons.Configurations
{
    public class AzureStorageConfig
    {
        public string PublisherHostKey { get; set; }

        public string StorageContainer { get; set; }

        public string StorageKey { get; set; }

        public string StorageMediaContentFolder { get; set; }

        public string StorageName { get; set; }
    }
}
