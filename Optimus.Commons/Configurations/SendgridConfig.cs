﻿namespace Optimus.Commons.Configurations
{
    public class SendgridConfig
    {
        public string SendGridSender { get; set; }
        public string SendGridEmailSender { get; set; }
        public string SendGridKey { get; set; }
        public string SendGridSubscriptionReceiver { get; set; }
        public EmailTemplates SendGridTemplates { get; set; }
    }

    public class EmailTemplates
    {
        public string EmailConfirmationTemplateId { get; set; }
        
    }
}
