using jsreport.AspNetCore;
using jsreport.Binary;
using jsreport.Local;
using jsreport.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Optimus.Commons.Extensions;
using Optimus.Commons.Services.Quartz;
using Optimus.Models.Contexts;
using Optimus.Report.Services.Extensions;
using Optimus.Repositories.Extensions;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace Optimus.Report
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServices();
            services.AddCors((options) =>
            {
                options.AddPolicy("allcors", (policy) =>
                {
                    policy.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            services.AddCommonsConfigurations(Configuration);

            services.AddCommonsServices();

            services.AddRepositories();

            services.AddSqlServerDbContext<OptimusContext>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<Optimus2Context>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<IdentityContext>(Configuration.GetConnectionString("DefaultConnection"));
            var mongoAuditDatabase = Configuration.GetSection("MongoDB:Database").Value;
            services.AddMongo<MongoContext>(Configuration.GetConnectionString("Mongo"), mongoAuditDatabase, (setting) =>
            {
                setting.UseTls = false;
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Optimus Reports Web API", Version = "v1" });
                c.EnableAnnotations();
                c.AddSecurityDefinition("oauth2", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                    Name = "Authorization",
                    Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                //c.OperationFilter<FileUploadOperation>();
            });


            services.AddControllersWithViews().AddRazorRuntimeCompilation().AddJsonOptions(options => {
                options.JsonSerializerOptions.Converters.Add(new System.Text.Json.Serialization.JsonStringEnumConverter());
            });

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            var localReporting = new LocalReporting();
            localReporting = localReporting.KillRunningJsReportProcesses();

            var rs = localReporting.UseBinary(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                    ? JsReportBinary.GetBinary()
                    : jsreport.Binary.OSX.JsReportBinary.GetBinary())
                .Configure((cfg) =>
                {
                    cfg.AllowedLocalFilesAccess().BaseUrlAsWorkingDirectory();
                    cfg.HttpPort = int.Parse(Configuration.GetSection("ReportPort").Value);
                    cfg.TempDirectory = Path.Combine("../", Configuration.GetSection("ReportTempPath").Value, "temp");
                    cfg.Chrome = new ChromeConfiguration { Timeout = 60000 };
                    return cfg;
                }).AsUtility().Create();

            services.AddJsReport(rs);

            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
                options.MimeTypes =
                ResponseCompressionDefaults.MimeTypes.Concat(
                new[] { "text/plain", "text/html" });
            });

            services.Configure<RequestLocalizationOptions>(options => {
                var supportedCultures = new List<CultureInfo> { new CultureInfo("es-ES"), };
                options.DefaultRequestCulture = new RequestCulture(culture: "es-ES", uiCulture: "es-ES");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();
            app.UseRouting();
            app.UseCors("allcors");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();            
            app.UseRouting();
            app.UseAuthorization();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Optimus Reports API");
                c.RoutePrefix = string.Empty;
                c.DocExpansion(DocExpansion.None);
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute().RequireAuthorization();
                endpoints.MapRazorPages();
            });
        }
    }
}
