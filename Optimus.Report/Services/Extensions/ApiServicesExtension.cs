﻿using Microsoft.Extensions.DependencyInjection;
using Optimus.Report.Services.Implementations;
using Optimus.Report.Services.Interfaces;

namespace Optimus.Report.Services.Extensions
{
    public static class ApiServicesExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IEfficiencyServices, EfficiencyServices>();
            services.AddScoped<IPerformanceServices, PerformanceServices>();
            services.AddScoped<IMachineService, MachineService>();





            return services;
        }

    }
}
