﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Services.Extensions
{
    public  class ConstantReport
    {
        public const string LightGreen = "#D7ECE6";
        public const string Green = "#2D9797";
        public const string LightRed = "#FBDFD6";
        public const string Red = "#F65929";
        public const string Blue = "#1F66F4";

    }
}
