﻿using Optimus.Report.Services.Interfaces;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using Optimus.Report.Models;
using System.Threading.Tasks;
using System;
using Optimus.Models.Enum;
using System.Collections.Generic;
using Optimus.Models.Views;
using Optimus.Report.Models.Option;
using Optimus.Report.Services.Extensions;
using System.Linq;

namespace Optimus.Report.Services.Implementations
{
    public class PerformanceServices: IPerformanceServices
    {
        public readonly IMachineStatusHistoricRepository machineStatusHistoricRepository;
        public readonly IConfiguracionAlertasParadaMaquinaRepository configuracionAlertasParadaMaquinaRepository;
        public readonly IConfiguracionUmbralesTmefTmprRepository configuracionUmbralesTmefTmprRepository;

        public PerformanceServices( IMachineStatusHistoricRepository machineStatusHistoricRepository,
            IConfiguracionAlertasParadaMaquinaRepository configuracionAlertasParadaMaquinaRepository,
            IConfiguracionUmbralesTmefTmprRepository configuracionUmbralesTmefTmprRepository)
        {
            this.machineStatusHistoricRepository = machineStatusHistoricRepository;
            this.configuracionAlertasParadaMaquinaRepository = configuracionAlertasParadaMaquinaRepository;
            this.configuracionUmbralesTmefTmprRepository = configuracionUmbralesTmefTmprRepository;
        }

        public async Task<PerformanceTableModel> GeneratePerformanceTableModel(ReporteHistoricoFilter filter)
        {
            try
            {
                var machinesStatus = await this.machineStatusHistoricRepository.ListAsyncFilter(filter);
                

                var machinesStatusGroup = machinesStatus.GroupBy(x => x.IdEquipo);
                var performanceTableModel = new PerformanceTableModel();
                foreach (var idMachine in machinesStatusGroup)
                {
                    var machineData = new MachineData();
                    var eqMachinesStatus = machinesStatus.Where(x => x.IdEquipo == idMachine.Key);
                    var alertsPP = await this.configuracionAlertasParadaMaquinaRepository.ListAsync(null, null, new ConfigAlertasParadaMaqFilter() 
                    { TipoAlerta= TipoAlertaParadaEnum.Atencion, IdEquipo = idMachine.Key, IdLinea = filter.IdLine });
                    var umbrales = await this.configuracionUmbralesTmefTmprRepository.ListAsync(null, null, new CommonFilter() { IdEquipo = idMachine.Key, IdLinea = filter.IdLine });
                    machineData.IdEquipo = idMachine.Key;
                    machineData.DescEquipo = eqMachinesStatus.FirstOrDefault().DescEquipo;
                    machineData.ND = eqMachinesStatus.Where(x => x.NoDeterminadoMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.NoDeterminadoMAP > 0)?.Average(x => x.NoDeterminadoMAP) : null;
                    machineData.PO = eqMachinesStatus.Where(x => x.ParadaOperacionalMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ParadaOperacionalMAP > 0)?.Average(x => x.ParadaOperacionalMAP) : null;
                    machineData.FP = eqMachinesStatus.Where(x => x.FueraProduccionMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.FueraProduccionMAP > 0)?.Average(x => x.FueraProduccionMAP) : null;
                    machineData.PR = eqMachinesStatus.Where(x => x.ProduccionMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ProduccionMAP > 0)?.Average(x => x.ProduccionMAP) : null;
                    machineData.SV = eqMachinesStatus.Where(x => x.SubVelocidadMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.SubVelocidadMAP > 0)?.Average(x => x.SubVelocidadMAP) : null;
                    machineData.PF = eqMachinesStatus.Where(x => x.ParadaFuncionalMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ParadaFuncionalMAP > 0)?.Average(x => x.ParadaFuncionalMAP) : null;
                    machineData.PA = eqMachinesStatus.Where(x => x.ParadaAlimentacionMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ParadaAlimentacionMAP > 0)?.Average(x => x.ParadaAlimentacionMAP) : null;
                    machineData.PD = eqMachinesStatus.Where(x => x.ParadaDescargaMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ParadaDescargaMAP > 0)?.Average(x => x.ParadaDescargaMAP) : null;
                    machineData.PP = eqMachinesStatus.Where(x => x.ParadaPropiaMAP > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.ParadaPropiaMAP > 0)?.Average(x => x.ParadaPropiaMAP) : null;
                    machineData.TMEF = eqMachinesStatus.Where(x => x.TMEF > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.TMEF > 0)?.Average(x => x.TMEF) : null;
                    machineData.TMPR = eqMachinesStatus.Where(x => x.TMPR > 0).Count() > 0 ? eqMachinesStatus.Where(x => x.TMPR > 0)?.Average(x => x.TMPR) : null;

                    if(machineData.PP > alertsPP.First().PorcMinimo)
                    {
                        machineData.PPAler = TipoAlertaParadaEnum.Atencion;
                        if(machineData.PP > alertsPP.First().PorcMaximo )
                            machineData.PPAler = TipoAlertaParadaEnum.Peligro;
                    }
                    if (machineData.TMEF < umbrales.Where( x => x.Tipo == TiempoPromTipoReparacionEnum.Tmef).First().ValorMaximo)
                    {
                        machineData.TMEFAler = TipoAlertaParadaEnum.Atencion;
                        if (machineData.TMEF < umbrales.Where(x => x.Tipo == TiempoPromTipoReparacionEnum.Tmef).First().ValorMinimo)
                            machineData.TMEFAler = TipoAlertaParadaEnum.Peligro;
                    }
                    if (machineData.TMPR > umbrales.Where(x => x.Tipo == TiempoPromTipoReparacionEnum.Tmpr).First().ValorMinimo)
                    {
                        machineData.TMPRAler = TipoAlertaParadaEnum.Atencion;
                        if (machineData.TMPR > umbrales.Where(x => x.Tipo == TiempoPromTipoReparacionEnum.Tmpr).First().ValorMaximo)
                            machineData.TMPRAler = TipoAlertaParadaEnum.Peligro;
                    }


                    performanceTableModel.MachineData.Add(machineData);
                }
                return performanceTableModel;
            }
            catch (Exception e)
            {
                throw;
            }

        }
        public async Task<BarModel> GenerateParettoModel(ParettoFilter filter)
        {
            try
            {
                var machinesStatus = await this.machineStatusHistoricRepository.ListAsyncFilter(filter);
                if (machinesStatus.Count() == 0)
                    return new BarModel();

                var model = new BarModel();
                var dataAxis = new List<string>();
                var dataSerie = new List<Data>();
                IEnumerable<MachineStatusHistoricMongo> eqMachineStatus = null;
                List<(double, string)> lisAux = new List<(double, string)>();

                var machinesStatusGroup = machinesStatus.GroupBy(x => x.IdEquipo);
                foreach (var idMachine in machinesStatusGroup)
                {
                    var eqMachinesStatus = machinesStatus.Where(x => x.IdEquipo == idMachine.Key);
                    double value = 0;
                    switch (filter.StatusEquipos)
                    {
                        case AppmobStatusEquiposEnum.ParadaOperacional:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaOperacionalMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaOperacionalMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.FueraProduccion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.FueraProduccionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.FueraProduccionMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.Produccion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ProduccionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ProduccionMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPropia:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaPropiaMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaPropiaMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPorAlimentacion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaAlimentacionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaAlimentacionMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPorDescarga:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaDescargaMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaDescargaMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.ParadaFuncional:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaFuncionalMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaFuncionalMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.SubVelocidad:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.SubVelocidadMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.SubVelocidadMA) : 0;
                            break;
                        case AppmobStatusEquiposEnum.NoDeterminado:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.NoDeterminadoMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.NoDeterminadoMA) : 0;
                            break;
                        default:
                            break;

                    }
                    lisAux.Add((Math.Round(value, 2), eqMachinesStatus.FirstOrDefault().DescEquipo));
                }

                foreach (var aux in lisAux.OrderBy(x => x.Item1))
                {
                    dataSerie.Add(new Data() { Value = aux.Item1 });
                    dataAxis.Add(aux.Item2);
                }
                
                model.YAxis = new Axis() { Data = dataAxis };
                model.Series = new List<Serie>() {
                new Serie() {
                    Data = dataSerie}
            };
                return model;
            }
            catch (Exception e)
            {
                throw;
            }

        }
        public async Task<BarModel> DifferenceBarsModel(ParettoFilter filter)
        {
            try
            {
                var machinesStatus = await this.machineStatusHistoricRepository.ListAsyncFilter(filter);
                var daysDiff = (filter.DateFrom - filter.DateTo).Days;
                filter.DateTo = filter.DateFrom.AddDays(-1);
                filter.DateFrom = filter.DateFrom.AddDays(daysDiff);
                var machinesStatusLastPeriod = await this.machineStatusHistoricRepository.ListAsyncFilter(filter);


                var model = new BarModel();
                var dataAxis = new List<string>();
                var dataSerie = new List<Data>();
                var datasDiff = new List<MachinesStatusDataDiff>();
                var machinesStatusGroup = machinesStatus.GroupBy(x => x.IdEquipo);
                IEnumerable<MachineStatusHistoricMongo> eqMachineStatus = null;
                foreach (var idMachine in machinesStatusGroup)
                {
                    var eqMachinesStatus = machinesStatus.Where(x => x.IdEquipo == idMachine.Key);
                    var eqMachinesStatusLastPeriod = machinesStatusLastPeriod.Where(x => x.IdEquipo == idMachine.Key);

                    double value = 0;
                    double valuePeriod = 0;
                    var dataDiff = new MachinesStatusDataDiff();
                    switch (filter.StatusEquipos)
                    {
                        case AppmobStatusEquiposEnum.ParadaOperacional:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaOperacionalMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaOperacionalMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ParadaOperacionalMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaOperacionalMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.FueraProduccion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.FueraProduccionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.FueraProduccionMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.FueraProduccionMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.FueraProduccionMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.Produccion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ProduccionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ProduccionMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ProduccionMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ProduccionMA) : 0;
                            dataDiff.Diff = value -valuePeriod;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPropia:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaPropiaMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaPropiaMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ParadaPropiaMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaPropiaMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPorAlimentacion:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaAlimentacionMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaAlimentacionMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ParadaAlimentacionMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaAlimentacionMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.ParadaPorDescarga:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaDescargaMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaDescargaMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ParadaDescargaMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaDescargaMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.ParadaFuncional:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.ParadaFuncionalMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaFuncionalMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.ParadaFuncionalMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.ParadaFuncionalMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.SubVelocidad:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.SubVelocidadMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.SubVelocidadMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.SubVelocidadMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.SubVelocidadMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        case AppmobStatusEquiposEnum.NoDeterminado:
                            eqMachineStatus = eqMachinesStatus.Where(x => x.NoDeterminadoMA > 0);
                            value = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.NoDeterminadoMA) : 0;
                            eqMachineStatus = eqMachinesStatusLastPeriod.Where(x => x.NoDeterminadoMA > 0);
                            valuePeriod = eqMachineStatus.Count() > 0 ? eqMachineStatus.Average(x => x.NoDeterminadoMA) : 0;
                            dataDiff.Diff = valuePeriod - value;
                            break;
                        default:
                            break;

                    }
                    dataDiff.DesEquipo = eqMachinesStatus.FirstOrDefault().DescEquipo;

                    datasDiff.Add(dataDiff);



                }
                var datasDiffOrder =  datasDiff.OrderByDescending(x => x.Diff).ToList();
                var limit = 5;
                if(datasDiffOrder.Count < 10)
                    limit = datasDiffOrder.Count / 2;

                for (int i = 0; i < limit; i++)
                {
                    var dataDiffOrder = datasDiffOrder[i];
                    if(dataDiffOrder.Diff > 0)
                    {
                        dataSerie.Add(new Data() { 
                            Value = Math.Round(dataDiffOrder.Diff, 2),
                            ItemStyle = new ItemStyle() { Color = ConstantReport.LightGreen },
                            Label = new Label() { Color = ConstantReport.Green }
                        });
                        dataAxis.Add(dataDiffOrder.DesEquipo);
                    }
                  
                }
                for (int i = (datasDiffOrder.Count - limit); i < datasDiffOrder.Count; i++)
                {
                    var dataDiffOrder = datasDiffOrder[i];
                    if (dataDiffOrder.Diff < 0)
                    {
                        dataSerie.Add(new Data() { 
                            Value = Math.Round(dataDiffOrder.Diff, 2),
                            ItemStyle = new ItemStyle() { Color = ConstantReport.LightRed },
                            Label = new Label() { Color = ConstantReport.Red }

                        });
                        dataAxis.Add(dataDiffOrder.DesEquipo);
                    }
                }
                model.XAxis = new Axis() { Data = dataAxis };
                model.Series = new List<Serie>() {
                new Serie() {
                    Data = dataSerie}
            };
                return model;
            }
            catch (Exception e)
            {
                throw;
            }

        }

    }
}
