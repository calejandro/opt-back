﻿using MongoDB.Driver;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Report.Models;
using Optimus.Report.Services.Extensions;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Services.Implementations
{
    public class EfficiencyServices : IEfficiencyServices
    {
        private readonly IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository;
        private readonly ILineaProduccionObjetivosConfigRepository lineaProduccionObjetivosConfigRepository;
        private readonly IAcumEficienciaRepository acumEficienciaRepository;
        private readonly ILineasProduccionRepository lineasProduccionRepository;
        private readonly ISkuEfficiencyRepository skuEfficiencyRepository;
        private readonly IOrdenesSapByLineViewRepository ordenesSapByLineViewRepository;
        public EfficiencyServices(
            IProduccionEficienciaByShiftRepository produccionEficienciaByShiftRepository,
            ILineaProduccionObjetivosConfigRepository lineaProduccionObjetivosConfigRepository,
            IAcumEficienciaRepository acumEficienciaRepository,
            ILineasProduccionRepository lineasProduccionRepository,
            ISkuEfficiencyRepository skuEfficiencyRepository,
            IOrdenesSapByLineViewRepository ordenesSapByLineViewRepository)
        {
            this.produccionEficienciaByShiftRepository = produccionEficienciaByShiftRepository;
            this.lineaProduccionObjetivosConfigRepository = lineaProduccionObjetivosConfigRepository;
            this.acumEficienciaRepository = acumEficienciaRepository;
            this.lineasProduccionRepository = lineasProduccionRepository;
            this.skuEfficiencyRepository = skuEfficiencyRepository;
            this.ordenesSapByLineViewRepository = ordenesSapByLineViewRepository;
        }
        public async Task<BarModel> GenerateEfficiencyModel(EfficiencyFilter filter)
        {


            switch (filter.TypeEfficiency)
            {
                case TypeEfficiencyEnum.Sku:
                    return await this.GenerateSkuEfficiencyModel(filter);
                case TypeEfficiencyEnum.Format:
                    return await this.GenerateFormatoEfficiencyModel(filter);
                case TypeEfficiencyEnum.Consolidated:
                    return await this.GenerateLineaEfficiencyAcumModel(filter);
            }
            return new BarModel();
        }
        public async Task<PieModel> GenerateParticipationModel(ParticipationFilter filter)
        {

            var performanceModel = new PieModel();
            performanceModel.Serie = new Serie();
            performanceModel.Serie.Data = new List<Data>();
            performanceModel.Data = new List<Data>();
            var data = new List<Data>();
            if (filter.TypeParticipationEnum == TypeParticipationEnum.Volume)
            {
                var efficienciesSku = await this.skuEfficiencyRepository.ListAsyncFilter(filter);

                //FORMAT
                var LisEfiFormato = efficienciesSku.GroupBy(x => x.Formato);
                foreach (var efiFormato in LisEfiFormato)
                {
                    var efficiencySku = efficienciesSku.Where(x => x.Formato == efiFormato.Key && x.Volumen > 0);
                    var volumen = efficiencySku.Sum(x => x.Volumen);
                    performanceModel.Serie.Data.Add(new Data() { Value = Math.Round(volumen, 2), Name = efiFormato.Key });
                }
                if (efficienciesSku?.Count() > 0)
                {
                    performanceModel.Max = Math.Round(performanceModel.Serie.Data.Max(x => x.Value).Value);
                    performanceModel.Min = Math.Round(performanceModel.Serie.Data.Min(x => x.Value).Value);
                }


                //SKU
                var LisEfisku = efficienciesSku.GroupBy(x => x.IdSKU);
                var sumTotal = efficienciesSku.Sum(x => x.Volumen);

                foreach (var efisku in LisEfisku)
                {
                    var efficiencySku = efficienciesSku.Where(x => x.IdSKU == efisku.Key && x.Volumen > 0);
                    var volumen = efficiencySku.Sum(x => x.Volumen);
                    var averVolumen = (volumen * 100) / sumTotal;

                    data.Add(new Data() { Value = Math.Round(averVolumen, 2), Name = efficiencySku.FirstOrDefault()?.DescripcionSKU });
                }
                performanceModel.Data.Add(new Data() { Value = 100, Children = data });
            }
            else
            {
                var orderSap = await this.ordenesSapByLineViewRepository.ListAsync(null, null, filter);
                //FORMAT
                var LisEfiFormato = orderSap.GroupBy(x => x.Formato);
                foreach (var efiFormato in LisEfiFormato)
                {
                    var efficiencySku = orderSap.Where(x => x.Formato == efiFormato.Key && x.DateDiff > 0)
                        .Select(x => new { x.IdOrdenSap, x.DateDiff }).Distinct();
                    double time = efficiencySku.Sum(x => x.DateDiff) / 60.00;
                    performanceModel.Serie.Data.Add(new Data() { Value = Math.Round(time, 2), Name = efiFormato.Key });
                }
                if (orderSap?.Count() > 0)
                {
                    performanceModel.Max = Math.Round(performanceModel.Serie.Data.Max(x => x.Value).Value);
                    performanceModel.Min = Math.Round(performanceModel.Serie.Data.Min(x => x.Value).Value);
                }


                //SKU
                var LisEfisku = orderSap.GroupBy(x => x.IdSku);
                var sumTotal = orderSap.Select(x => new { x.IdOrdenSap, x.DateDiff }).Distinct().Sum(x => x.DateDiff);

                foreach (var efisku in LisEfisku)
                {
                    var efficiencySku = orderSap.Where(x => x.IdSku == efisku.Key && x.DateDiff > 0)
                        .Select(x => new { x.IdOrdenSap, x.DescSku, x.DateDiff }).Distinct();
                    double time = efficiencySku.Sum(x => x.DateDiff);
                    double avertime = (time * 100.00) / sumTotal;

                    data.Add(new Data() { Value = Math.Round(avertime, 2), Name = efficiencySku.FirstOrDefault()?.DescSku });
                }
                performanceModel.Data.Add(new Data() { Value = 100, Children = data });
            }

            return performanceModel;
        }

        private async Task<BarModel> GenerateLineaEfficiencyAcumModel(EfficiencyFilter filter)
        {
            var model = new BarModel
            {
                Series = new List<Serie>(new Serie[] { new Serie() { Type = "bar" } }),
                YAxis = new Axis { Type = "value" },
                XAxis = new Axis { Data = new List<string>() }
            };
            var dataSerie = new List<Data>();
            FilterDefinition<AcumEficienciaMongo> filterMong = null;
            if (filter != null)
            {
                switch (filter.PeriodEfficiency)
                {
                    case PeriodEfficiencyEnum.Daily:
                        filter.DateFrom = filter.DateFrom.AddDays(-1);
                        break;
                    case PeriodEfficiencyEnum.Monthly:
                        filter.DateFrom = new DateTime(filter.DateFrom.Year, filter.DateFrom.Month,1 );
                        filter.DateTo = new DateTime(filter.DateTo.Year, filter.DateTo.Month, 1);
                        break;
                    case PeriodEfficiencyEnum.Annual:
                        filter.DateFrom = new DateTime(filter.DateFrom.Year, 1,1 );
                        filter.DateTo = new DateTime(filter.DateTo.Year, 1, 1);
                        break;
                    default:
                        break;
                }
                var builder = Builders<AcumEficienciaMongo>.Filter;
                filterMong = builder.Gte(widget => widget.Fecha, filter.DateFrom.AddDays(-1))
                  & builder.Lte(widget => widget.Fecha, filter.DateTo)
                  & builder.In(widget => widget.IdLinea, filter.IdLines)
                  & builder.Eq(widget => widget.PeriodoEficiencia, filter.PeriodEfficiency);
            }
           
               

            var efficiencies = await this.acumEficienciaRepository.ListAsync(filterMong);

            var tipo = TipoObjetivoEnum.MENSUAL;
            if (filter.PeriodEfficiency == PeriodEfficiencyEnum.Annual)
                tipo = TipoObjetivoEnum.ANUAL;

            foreach (var linea in filter.IdLines)
            {
                var obj = (await this.lineaProduccionObjetivosConfigRepository.ListAsync(null, null, new LineasProduccionObjetivosConfigFilter() { Tipo = tipo, IdLinea = linea })).FirstOrDefault();
                var efi = efficiencies.Where(x => x.IdLinea == linea).OrderBy(x=> x.Fecha);


                foreach (var efficiency in efi)
                {
                    var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                    var colorLab = ConstantReport.Green;
                    var color = ConstantReport.LightGreen;
                    if (Convert.ToDouble(obj.Valor) > efficiency.Eficiencia)
                    {
                        color = ConstantReport.LightRed;
                        colorLab = ConstantReport.Red;
                    }
                    var lab = "";
                    switch (filter.PeriodEfficiency)
                    {
                        case PeriodEfficiencyEnum.Daily:
                            lab = efficiency.Fecha.Day + "/" + efficiency.Fecha.Month;
                            break;
                        case PeriodEfficiencyEnum.Monthly:
                            lab = efficiency.Fecha.Month + "/" + efficiency.Fecha.Year;
                            break;
                        case PeriodEfficiencyEnum.Annual:
                            lab = efficiency.Fecha.Year.ToString();
                            break;
                        default:
                            break;
                    }
                    model.XAxis.Data.Add("L" + desLin.Substring(6) + " " + lab);

                    dataSerie.Add(new Data()
                    {
                        Value = Math.Round(efficiency.Eficiencia.Value, 1),
                        ItemStyle = new ItemStyle() { Color = color },
                        Label = new Label() { Color = colorLab }
                    });
                }
                model.Series = new List<Serie>() {
                new Serie() {
                    Label = new Label(){ FontSize =  await CalculeFontSize(dataSerie.Count)},
                    Data = dataSerie,
                    Type="bar"}
                };
            }
            return model;

        }
        private async Task<BarModel> GenerateLineaEfficiencySkuModel(EfficiencyFilter filter)
        {
            try
            {
                var model = new BarModel
                {
                    Series = new List<Serie>(new Serie[] { new Serie() { Type = "bar" } }),
                    YAxis = new Axis { Type = "value" },
                    XAxis = new Axis { Data = new List<string>() }
                };
                var dataSerie = new List<Data>();
                var tipo = TipoObjetivoEnum.MENSUAL;
                if (filter.PeriodEfficiency == PeriodEfficiencyEnum.Annual)
                    tipo = TipoObjetivoEnum.ANUAL;
                var dateFrom = filter.DateFrom;
                foreach (var linea in filter.IdLines)
                {
                    filter.IdLines = new List<int>() { linea };
                    var obj = (await this.lineaProduccionObjetivosConfigRepository.ListAsync(null, null, new LineasProduccionObjetivosConfigFilter() { Tipo = tipo, IdLinea = linea })).FirstOrDefault();
                    switch (filter.PeriodEfficiency)
                    {

                        case PeriodEfficiencyEnum.Daily:
                            var effiday = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
                            if (effiday.Count() == 0)
                                break;

                            effiday = effiday.Where(x => x.Eficiencia > 0).ToList();
                            var effisDistinct = effiday.Select(x => new { x.IdLinea, x.FechaFin }).Distinct();
                            foreach (var effiDistinct in effisDistinct)
                            {
                                var eqDatas = effiday.Where(x => x.FechaFin == effiDistinct.FechaFin).ToList();
                                var effi =  await this.AverageEfficiency(eqDatas.Count(), eqDatas);

                                var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                                var color = ConstantReport.LightGreen;
                                var colorLab = ConstantReport.Green;
                                if (Convert.ToDouble(obj.Valor) > effi)
                                {
                                    color = ConstantReport.LightRed;
                                    colorLab = ConstantReport.Red;
                                }
                                model.XAxis.Data.Add("L" + desLin.Substring(6) + " " + effiDistinct.FechaFin.Day + "/" + effiDistinct.FechaFin.Month);
                                dataSerie.Add(new Data()
                                {
                                    Value = Math.Round(effi, 1),
                                    ItemStyle = new ItemStyle() { Color = color },
                                    Label = new Label() { Color = colorLab }
                                });
                            }
                            break;
                        case PeriodEfficiencyEnum.Monthly:
                            var countMonth = (filter.DateTo - filter.DateFrom).Days /31;
                            if(countMonth >= 0)
                            {
                                for (int i = 0; i <= countMonth; i++)
                                {
                                    filter.DateFrom = new DateTime(dateFrom.AddMonths(i).Year, dateFrom.AddMonths(i).Month, 1);
                                    filter.DateTo = filter.DateFrom.AddMonths(1).AddDays(-1);
                                    var effiMonth = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
                                    if (effiMonth.Count() == 0)
                                        break;

                                    effiMonth = effiMonth.Where(x => x.Eficiencia > 0).ToList();
                                    var effiMonthDistinct = effiMonth.Select(x => new { x.IdLinea, x.FechaFin }).Distinct();
                                    var aux = 0.0;
                                    foreach (var effiDistinct in effiMonthDistinct)
                                    {
                                        var eqDatas = effiMonth.Where(x => x.FechaFin == effiDistinct.FechaFin).ToList();
                                         aux += await this.AverageEfficiency(eqDatas.Count(), eqDatas);

                                    }
                                     var effiMonthVal = aux/ effiMonthDistinct.Count();
                                   

                                    var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                                    var color = ConstantReport.LightGreen;
                                    var colorLab = ConstantReport.Green;
                                    if (Convert.ToDouble(obj.Valor) > effiMonthVal)
                                    {
                                        color = ConstantReport.LightRed;
                                        colorLab = ConstantReport.Red;
                                    }
                                    model.XAxis.Data.Add("L" + desLin.Substring(6) + " " + filter.DateFrom.Month + "/" + filter.DateFrom.Year);
                                    dataSerie.Add(new Data()
                                    {
                                        Value = Math.Round(effiMonthVal, 1),
                                        ItemStyle = new ItemStyle() { Color = color },
                                        Label  = new Label() { Color = colorLab }
                                    });
                                }
                            }
                     
                            break;
                        case PeriodEfficiencyEnum.Annual:
                            var countYear = filter.DateTo.Year - filter.DateFrom.Year;
                            for (int i = 0; i <= countYear; i++)
                            {
                                filter.DateFrom = new DateTime(dateFrom.AddYears(i).Year, 1, 1);
                                filter.DateTo = filter.DateFrom.AddYears(1).AddDays(-1);
                                var effiYear = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
                                if (effiYear.Count() == 0)
                                    break;

                                effiYear = effiYear.Where(x => x.Eficiencia > 0).ToList();
                                var effiYearDistinct = effiYear.Select(x => new { x.IdLinea, x.FechaFin }).Distinct();
                                var aux = 0.0;
                                foreach (var effiDistinct in effiYearDistinct)
                                {
                                    var eqDatas = effiYear.Where(x => x.FechaFin == effiDistinct.FechaFin).ToList();
                                    aux += await this.AverageEfficiency(eqDatas.Count(), eqDatas);

                                }
                                var effiYearVal = aux / effiYearDistinct.Count();
                                //var effiYearVal = 0.0;
                                //if (effiYear.Count() > 0)
                                //     effiYearVal = effiYear.Where(x => x.Eficiencia > 0).Average(x => x.Eficiencia);

                                var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                                var color = ConstantReport.LightGreen;
                                var colorLab = ConstantReport.Green;
                                if (Convert.ToDouble(obj.Valor) > effiYearVal)
                                {
                                    color = ConstantReport.LightRed;
                                    colorLab = ConstantReport.Red;
                                }
                                model.XAxis.Data.Add("L" + desLin.Substring(6) + " " + filter.DateFrom.Year);
                                dataSerie.Add(new Data()
                                {
                                    Value = Math.Round(effiYearVal, 2),
                                    ItemStyle = new ItemStyle() { Color = color },
                                    Label = new Label() { Color = colorLab }
                                });
                            }
                            break;
                        default:
                            break;
                    }

                }
                model.Series = new List<Serie>() {
                new Serie() {
                    Label = new Label(){ FontSize =  await CalculeFontSize(dataSerie.Count)},
                    Data = dataSerie,
                    Type="bar"}
                };
                return model;
            }
            catch (Exception e)
            {
                throw;
            }
         

        }
        private async Task<double> AverageEfficiency(int length, List<SkuEfficiencyMongo> eqDatas)
        {
            if (length > 0)
            {
                if (length != 1)
                {
                    //Promedio Pesado
                    var aux = 0.00;
                    foreach (var eqData in eqDatas)
                    {
                        aux += (eqData.Eficiencia * eqData.CantidadRegistros);
                    }
                    return aux / eqDatas.Sum(x => x.CantidadRegistros);
                }
                else
                {
                    return eqDatas[0].Eficiencia;
                }

            }
            return 0;
        }
        private async Task<BarModel> GenerateSkuEfficiencyModel(EfficiencyFilter filter)
        {
            var model = new BarModel();
            var dataAxis = new List<string>();
            var dataSerie = new List<Data>();
            var efficienciesSku = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
            var tipo = TipoObjetivoEnum.MENSUAL;
            if (filter.PeriodEfficiency == PeriodEfficiencyEnum.Annual)
                tipo = TipoObjetivoEnum.ANUAL;
            foreach (var linea in filter.IdLines)
            {
                var obj = (await this.lineaProduccionObjetivosConfigRepository.ListAsync(null, null, new LineasProduccionObjetivosConfigFilter() { Tipo = tipo, IdLinea = linea })).FirstOrDefault();
                var LisEfiSku = efficienciesSku.Where(x => x.IdLinea == linea && x.Eficiencia > 0).GroupBy(x => x.IdSKU);

                foreach (var efiSku in LisEfiSku)
                {
                    var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                    var efficiencySku = efficienciesSku.Where(x => x.IdLinea == linea && x.IdSKU == efiSku.Key && x.Eficiencia > 0);
                    var efficiency = efficiencySku.Average(x => x.Eficiencia);

                    var color = ConstantReport.LightGreen;
                    var colorLab = ConstantReport.Green;
                    if (Convert.ToDouble(obj.Valor) > efficiency)
                    {
                        color = ConstantReport.LightRed;
                        colorLab = ConstantReport.Red;
                    }
                    dataAxis.Add("L" + desLin.Substring(6) + " " + efficiencySku.FirstOrDefault()?.DescripcionSKU);
                    dataSerie.Add(new Data()
                    {
                        Value = Math.Round(efficiency, 1),
                        ItemStyle = new ItemStyle() { Color = color },
                        Label = new Label() { Color = colorLab }
                    });
                }
            }

            model.XAxis = new Axis() { Type = "category", Data = dataAxis };
            model.YAxis = new Axis() { Type = "value" };
            model.Series = new List<Serie>() {
                new Serie() {
                    Label = new Label(){ FontSize =  await CalculeFontSize(dataSerie.Count)},
                    Data = dataSerie,
                    Type="bar"}
            };

            return model;
        }
        private async Task<BarModel> GenerateFormatoEfficiencyModel(EfficiencyFilter filter)
        {
            try
            {
                var model = new BarModel();
                var dataAxis = new List<string>();
                var dataSerie = new List<Data>();

                var efficienciesSku = await this.skuEfficiencyRepository.ListAsyncFilter(filter);

                var tipo = TipoObjetivoEnum.MENSUAL;
                if (filter.PeriodEfficiency == PeriodEfficiencyEnum.Annual)
                    tipo = TipoObjetivoEnum.ANUAL;
                foreach (var linea in filter.IdLines)
                {
                    var obj = (await this.lineaProduccionObjetivosConfigRepository.ListAsync(null, null, new LineasProduccionObjetivosConfigFilter() { Tipo = tipo, IdLinea = linea })).FirstOrDefault();
                    var LisEfiFormato = efficienciesSku.Where(x => x.IdLinea == linea && x.Eficiencia > 0).GroupBy(x => x.Formato);

                    foreach (var efiFormato in LisEfiFormato)
                    {
                        var desLin = (await this.lineasProduccionRepository.GetAsync(linea)).DescLinea;
                        var efficiencySku = efficienciesSku.Where(x => x.IdLinea == linea && x.Formato == efiFormato.Key && x.Eficiencia > 0);
                        var efficiency = efficiencySku.Average(x => x.Eficiencia);

                        var color = ConstantReport.LightGreen;
                        var colorLab = ConstantReport.Green;
                        if (Convert.ToDouble(obj.Valor) > efficiency)
                        {
                            color = ConstantReport.LightRed;
                            colorLab = ConstantReport.Red;
                        }
                        dataAxis.Add("L" + desLin.Substring(6) + " " + efficiencySku.FirstOrDefault()?.Formato);
                        dataSerie.Add(new Data()
                        {
                            Value = Math.Round(efficiency, 1),
                            ItemStyle = new ItemStyle() { Color = color },
                            Label = new Label() { Color = colorLab }
                        });
                    }
                }

                model.XAxis = new Axis() { Type = "category", Data = dataAxis };
                model.YAxis = new Axis() { Type = "value" };
                                   
                model.Series = new List<Serie>() {
                new Serie() {
                    Label = new Label(){ FontSize =  await CalculeFontSize(dataSerie.Count)},
                    Data = dataSerie,
                    Type="bar"}
            };

                return model;
            }
            catch (Exception e)
            {
                throw;
            }

        }
        private async Task<int> CalculeFontSize(int count )
        {
            var fontSize = 16;
            if (count > 10)
            {
                fontSize = 14;
                if (count > 20)
                {
                    fontSize = 10;
                    if (count > 30)
                    {
                        fontSize = 8;
                        if (count > 40)
                            fontSize = 6;
                    }
                }
            }
            return fontSize;
        }
        public async Task<CardModel> GenerateCardLineaEfficiencyModel(EfficiencyFilter filter)
        {
            var model = new CardModel();
            var dateFrom = filter.DateFrom;
            var dateTo =  filter.DateTo;
            var diff = (filter.DateFrom - filter.DateTo).Days;

            var effi = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
            filter.DateTo = filter.DateFrom.AddDays(-1); ;
            filter.DateFrom = filter.DateFrom.AddDays(diff);
            var effiLastMonth = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
            filter.DateFrom = dateFrom.AddYears(-1);
            filter.DateTo = dateTo.AddYears(-1);
            var effiLastYear = await this.skuEfficiencyRepository.ListAsyncFilter(filter);

            var tipo = TipoObjetivoEnum.MENSUAL;
            if (filter.PeriodEfficiency == PeriodEfficiencyEnum.Annual)
                tipo = TipoObjetivoEnum.ANUAL;




            double? aveEffi = null;
            double? aveEffiLastMonth = null;
            double? aveEffiLastYear = null;
            if (effi.Count() > 0)
                aveEffi = effi.Where(x => x.Eficiencia > 0).Average(x => x.Eficiencia);
            if (effiLastMonth.Count() > 0)
                aveEffiLastMonth = effiLastMonth.Where(x => x.Eficiencia > 0).Average(x => x.Eficiencia);
            if (effiLastYear.Count() > 0)
                aveEffiLastYear = effiLastYear.Where(x => x.Eficiencia > 0).Average(x => x.Eficiencia);


            var diffMonth = aveEffiLastMonth ;
            var diffYear =  aveEffiLastYear ;

            model.Icon = "bar-chart-outline";
            model.Title = "Eficiencia Líneas";
            model.PrincipalValue = aveEffi.HasValue ? Math.Round(aveEffi.Value, 2) + "%" : null;
            model.MonthValue = diffMonth.HasValue ? Math.Round(diffMonth.Value, 2) + "%" : null;
            model.YearValue = diffYear.HasValue ? Math.Round(diffYear.Value, 2) + "%" : null;
           
            model.PrincipalColor = ConstantReport.Blue;
            model.MonthColor = ConstantReport.Blue;
            model.YearColor = ConstantReport.Blue;



            if (filter.IdLines.Count() == 1)
            {
                var obj = (await this.lineaProduccionObjetivosConfigRepository.ListAsync(null, null, new LineasProduccionObjetivosConfigFilter() { Tipo = tipo, IdLinea = filter.IdLines[0] })).FirstOrDefault().Valor;
                var desLin = (await this.lineasProduccionRepository.GetAsync(filter.IdLines[0])).DescLinea;

                model.ObjValue = Math.Round(obj, 2) + "%";
                model.PrincipalColor = ConstantReport.Green;
                model.MonthColor = ConstantReport.Green;
                model.YearColor = ConstantReport.Green;

                if (Convert.ToDouble(obj) > aveEffi)
                    model.PrincipalColor = ConstantReport.Red;
                if (Convert.ToDouble(obj) > diffMonth)
                    model.MonthColor = ConstantReport.Red;
                if (Convert.ToDouble(obj) > diffYear)
                    model.YearColor = ConstantReport.Red;
                model.Title = "Eficiencia Línea " + desLin.Substring(5);
            }

            return model;

        }
        public async Task<CardModel> GenerateCardProducedTimeModel(ReporteHistoricoFilter filter)
        {
            var model = new CardModel();

            var prodTime = await this.ordenesSapByLineViewRepository.ListAsync(null, null, filter);
            filter.DateFrom = filter.DateFrom.AddMonths(-1);
            filter.DateTo = filter.DateTo.AddMonths(-1);
            var prodTimeLastMonth = await this.ordenesSapByLineViewRepository.ListAsync(null, null, filter);
            filter.DateFrom = filter.DateFrom.AddMonths(1).AddYears(-1);
            filter.DateTo = filter.DateTo.AddMonths(1).AddYears(-1);
            var prodTimeLastYear = await this.ordenesSapByLineViewRepository.ListAsync(null, null, filter);




            double? aveProdTime = null;
            double? aveProdTimeLastMonth = null;
            double? aveProdTimeLastYear = null;
            if (prodTime.Count() > 0)
                aveProdTime = prodTime.Select(x => new { x.DateDiff, x.IdOrdenSap }).Distinct().Sum(x => x.DateDiff) / 60.00;
            if (prodTimeLastMonth.Count() > 0)
                aveProdTimeLastMonth = prodTimeLastMonth.Select(x => new { x.DateDiff, x.IdOrdenSap }).Distinct().Sum(x => x.DateDiff) / 60.00;
            if (prodTimeLastYear.Count() > 0)
                aveProdTimeLastYear = prodTimeLastYear.Select(x => new { x.DateDiff, x.IdOrdenSap }).Distinct().Sum(x => x.DateDiff) / 60.00;


            var diffMonth =  aveProdTimeLastMonth;
            var diffYear = aveProdTimeLastYear;
            model.Icon = "clock-outline";
            model.Title = "Tiempo Producido";
            model.PrincipalValue = aveProdTime.HasValue ? Math.Round(aveProdTime.Value, 2) + "hs" : null;
            model.MonthValue = diffMonth.HasValue ? Math.Round(diffMonth.Value, 2) + "hs" : null;
            model.YearValue = diffYear.HasValue ? Math.Round(diffYear.Value, 2) + "hs" : null;

            model.PrincipalColor = ConstantReport.Blue;
            model.MonthColor = ConstantReport.Blue;
            model.YearColor = ConstantReport.Blue;
          
            return model;

        }
        public async Task<CardModel> GenerateCardUnitCaseModel(ReporteHistoricoFilter filter)
        {
            var model = new CardModel();

            var unitCase = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
            filter.DateFrom = filter.DateFrom.AddMonths(-1);
            filter.DateTo = filter.DateTo.AddMonths(-1);
            var unitCaseLastMonth = await this.skuEfficiencyRepository.ListAsyncFilter(filter);
            filter.DateFrom = filter.DateFrom.AddMonths(1).AddYears(-1);
            filter.DateTo = filter.DateTo.AddMonths(1).AddYears(-1);
            var unitCaseLastYear = await this.skuEfficiencyRepository.ListAsyncFilter(filter);


            double? aveUnitCase = null;
            double? aveUnitCaseLastMonth = null;
            double? aveUnitCaseLastYear = null;
            if (unitCase.Count() > 0)
                aveUnitCase = unitCase.Where(x => x.Volumen > 0).Sum(x => x.Volumen) / 1000;
            if (unitCaseLastMonth.Count() > 0)
                aveUnitCaseLastMonth = unitCaseLastMonth.Where(x => x.Volumen > 0).Sum(x => x.Volumen) / 1000;
            if (unitCaseLastYear.Count() > 0)
                aveUnitCaseLastYear = unitCaseLastYear.Where(x => x.Volumen > 0).Sum(x => x.Volumen) / 1000;


            var diffMonth =  aveUnitCaseLastMonth ;
            var diffYear = aveUnitCaseLastYear ;

            model.Icon = "radio-button-on-outline";
            model.Title = "Unit Case";
            model.PrincipalValue = aveUnitCase.HasValue ? Math.Round(aveUnitCase.Value, 2) + "M" : null;
            model.MonthValue = diffMonth.HasValue ? Math.Round(diffMonth.Value, 2) + "M" : null;
            model.YearValue = diffYear.HasValue ? Math.Round(diffYear.Value, 2) + "M" : null;

            model.PrincipalColor = ConstantReport.Blue;
            model.MonthColor = ConstantReport.Blue;
            model.YearColor = ConstantReport.Blue;


            return model;

        }

        private async Task<FilterDefinition<AcumEficienciaMongo>> AcumEficienciaMongoFilter(ReporteHistoricoFilter filter, int type)
        {
            FilterDefinition<AcumEficienciaMongo> filterMong = null;
            if (filter != null)
            {
                var builder = Builders<AcumEficienciaMongo>.Filter;
                filterMong = builder.In(widget => widget.IdLinea, filter.IdLines)
                  & builder.Eq(widget => widget.PeriodoEficiencia, PeriodEfficiencyEnum.Daily);
                switch (type)
                {
                    case 1:
                        filterMong = filterMong & builder.Gte(widget => widget.Fecha, filter.DateFrom)
                         & builder.Lte(widget => widget.Fecha, filter.DateTo);
                        break;
                    case 2:
                        filterMong = filterMong & builder.Gte(widget => widget.Fecha, filter.DateFrom.AddMonths(-1))
                        & builder.Lte(widget => widget.Fecha, filter.DateTo.AddMonths(-1));
                        break;
                    case 3:
                        filterMong = filterMong & builder.Gte(widget => widget.Fecha, filter.DateFrom.AddYears(-1))
                        & builder.Lte(widget => widget.Fecha, filter.DateTo.AddYears(-1));
                        break;
                    default:
                        break;
                }

            }
            return filterMong;

        }

    }
}
