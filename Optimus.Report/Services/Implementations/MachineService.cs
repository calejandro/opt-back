﻿using Optimus.Report.Models;
using Optimus.Report.Services.Interfaces;
using Optimus.Repositories.Filers;
using Optimus.Repositories.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Globalization;
using Optimus.Models.Enum;

namespace Optimus.Report.Services.Implementations
{
    public class MachineService : IMachineService
    {
        public readonly IAlertByOrdenesSapViewRepository alertByOrdenesSapViewRepository;
        public readonly IConfiguracionAlertasParadaMaquinaRepository configuracionAlertasParadaMaquinaRepository;
        public readonly IMachineStatusEventsRepository machineStatusEventsRepository;
        public MachineService(IAlertByOrdenesSapViewRepository alertByOrdenesSapViewRepository,
            IConfiguracionAlertasParadaMaquinaRepository configuracionAlertasParadaMaquinaRepository,
            IMachineStatusEventsRepository machineStatusEventsRepository)
        {
            this.alertByOrdenesSapViewRepository = alertByOrdenesSapViewRepository;
            this.configuracionAlertasParadaMaquinaRepository = configuracionAlertasParadaMaquinaRepository;
            this.machineStatusEventsRepository = machineStatusEventsRepository;
        }
        public async Task<BarModel> GenerateFailureModeModel(FailureModeFilter filter)
        {
            var model = new BarModel();
            var dataAxis = new List<string>();
            var dataSerie = new List<Data>();

            var alertByOrdenesSap = await this.alertByOrdenesSapViewRepository.ListAsync(null, null, filter);
            var alertByOrdenesSapDis = alertByOrdenesSap.Select(x => new { x.IdAlerta, x.IdModoFallo, x.NombreTipoFallo, x.ValorAcumulado }).Distinct();
            var alertByOrdenesSapGrop = alertByOrdenesSapDis.GroupBy(item => item.IdModoFallo).Select(group => new { IdModoFallo = group.Key, Sum = group.Sum(item => item.ValorAcumulado) })
                .OrderBy(or => or.Sum);
            List<(double, string)> lisAux = new List<(double, string)>();

            foreach (var item in alertByOrdenesSapGrop)
            {
                var desc = alertByOrdenesSapDis.Where(x => x.IdModoFallo == item.IdModoFallo);
                lisAux.Add((Math.Round(item.Sum, 2), desc.FirstOrDefault().NombreTipoFallo));

            }
            foreach (var aux in lisAux.OrderBy(x => x.Item1))
            {
                dataSerie.Add(new Data() { Value = aux.Item1 });
                dataAxis.Add(aux.Item2);
            }
            model.YAxis = new Axis() { Data = dataAxis };
            model.Series = new List<Serie>() {
                new Serie() {
                    Data = dataSerie}
            };
            return model;
        }
        public async Task<StackedBarModel> GenerateFailureModeEvolutionModel(FailureModeFilter filter)
        {
            try
            {
                var model = new StackedBarModel();
                var dataAxis = new List<string>();
                var dataLegend = new List<string>();
                var yAxis1 = new Axis();
                var yAxis2 = new Axis();
                var serie1 = new Serie();
                var serie2 = new Serie();
                var serie3 = new Serie();
                var serie4 = new Serie();
                var serie5 = new Serie();

                await this.WeekSetting(filter);
                (int weekFrom, int weekTo) = await this.WeeklyPeriod(filter);

                 var alertByOrdenesSap = await this.alertByOrdenesSapViewRepository.ListAsync(null, null, filter);

                var alertByOrdenesSapDis = alertByOrdenesSap.
                    Select(x => new { x.IdAlerta, x.IdModoFallo, x.FechaCreacionAlerta, x.NombreTipoFallo, x.ValorAcumulado, x.IdUmbral, x.ValorMaximoUmb, x.ValorMinimoUmb }).Distinct().OrderBy(x => x.FechaCreacionAlerta);

                var umbsOrder = (await this.configuracionAlertasParadaMaquinaRepository.ListAsync(null, null, new ConfigAlertasParadaMaqFilter() { IdEquipo = filter.IdMachine, IdLinea = filter.IdLine }))
                               .Where(x => x.TipoAlerta != TipoAlertaParadaEnum.Ok).OrderBy(x => x.ValorMinimo).ToList();

                foreach (var umb in umbsOrder)
                {
                    var name = umb.ValorMinimo + "-" + umb.ValorMaximo;
                    dataLegend.Add(name);
                    if (umb.TipoAlerta == TipoAlertaParadaEnum.Atencion)
                        serie1.Name = name;
                    if (umb.TipoAlerta == TipoAlertaParadaEnum.Peligro)
                        serie2.Name = name;
                    if (umb.TipoAlerta == TipoAlertaParadaEnum.Fallo)
                        serie3.Name = name;
                }
                for (int i = weekFrom; i <= weekTo; i++)
                {
                    var alertByOrdenesweek = alertByOrdenesSapDis.Where(x => x.FechaCreacionAlerta >= filter.DateFrom && x.FechaCreacionAlerta < filter.DateFrom.AddDays(7));
                    if (alertByOrdenesweek.Count() > 0)
                    {

                        serie1.Data.Add(new Data() { Value = alertByOrdenesweek.Where(x => x.IdUmbral == umbsOrder[0]?.Id).Count() });
                        serie2.Data.Add(new Data() { Value = alertByOrdenesweek.Where(x => x.IdUmbral == umbsOrder[1]?.Id).Count() });
                        serie3.Data.Add(new Data() { Value = alertByOrdenesweek.Where(x => x.IdUmbral == umbsOrder[2]?.Id).Count() });

                        serie4.Data.Add(new Data() { Value = alertByOrdenesweek.Sum(x => x.ValorAcumulado) });
                        serie5.Data.Add(new Data() { Value = alertByOrdenesweek.Count() });

                    }
                    else
                    {
                        serie1.Data.Add(new Data() { Value = 0 });
                        serie2.Data.Add(new Data() { Value = 0 });
                        serie3.Data.Add(new Data() { Value = 0 });

                        serie4.Data.Add(new Data() { Value = 0 });
                        serie5.Data.Add(new Data() { Value = 0 });
                    }

                    dataAxis.Add("S" + i);
                    filter.DateFrom = filter.DateFrom.AddDays(7);
                }

                yAxis1.Interval = this.Interval(serie5.Data);
                yAxis1.Max = (int)serie5.Data.Max(x => x.Value) + yAxis1.Interval;

                yAxis2.Interval = this.Interval(serie4.Data);
                yAxis2.Max = (int)serie4.Data.Max(x => x.Value) + yAxis2.Interval;

                model.YAxis = new List<Axis>() { yAxis1, yAxis2 };
                model.XAxis = new Axis() { Data = dataAxis };
                model.DataLegend = dataLegend;
                model.Series = new List<Serie>() { serie1, serie2, serie3, serie4 };
                return model;
            }
            catch (Exception e)
            {
                throw;
            }

        }    
        public async Task<StackedBarModel> GenerateStateEvolutionModel(StateEvolutionFilter filter)
        {

            try
            {
                var model = new StackedBarModel();
                var dataAxis = new List<string>();
                var dataLegend = new List<string>();
                var yAxis1 = new Axis();
                var yAxis2 = new Axis();
                var serie1 = new Serie();
                var serie2 = new Serie();
                var serie3 = new Serie();
                var serie4 = new Serie();
                var serie5 = new Serie();

                

                var statesEvents = await machineStatusEventsRepository.ListAsyncFilter(filter);
                // configuracion unbrales
                var name = "0-5";
                dataLegend.Add(name);
                serie1.Name = name;
                name = "6-15";
                dataLegend.Add(name);
                serie2.Name = name;
                name = "+16";
                dataLegend.Add(name);
                serie3.Name = name;


                await this.WeekSetting(filter);
                (int weekFrom, int weekTo) = await this.WeeklyPeriod(filter);
                for (int i = weekFrom; i <= weekTo; i++)
                {

                    var statesEventsweek = statesEvents.Where(x => x.FechaFinEstado >= filter.DateFrom && x.FechaFinEstado < filter.DateFrom.AddDays(7));
                    if (statesEventsweek?.Count() > 0)
                    {

                        serie1.Data.Add(new Data() { Value = statesEventsweek.Where(x => x.MinutosAcumulados > 0 && x.MinutosAcumulados <= 5).Count() });
                        serie2.Data.Add(new Data() { Value = statesEventsweek.Where(x => x.MinutosAcumulados >= 6 && x.MinutosAcumulados <= 15).Count() });
                        serie3.Data.Add(new Data() { Value = statesEventsweek.Where(x => x.MinutosAcumulados >= 16).Count() });

                        serie4.Data.Add(new Data() { Value = statesEventsweek.Sum(x => x.MinutosAcumulados) });
                        serie5.Data.Add(new Data() { Value = statesEventsweek.Count() });

                    }
                    else
                    {
                        serie1.Data.Add(new Data() { Value = 0 });
                        serie2.Data.Add(new Data() { Value = 0 });
                        serie3.Data.Add(new Data() { Value = 0 });

                        serie4.Data.Add(new Data() { Value = 0 });
                        serie5.Data.Add(new Data() { Value = 0 });
                    }



                    dataAxis.Add("S" + i);
                    filter.DateFrom = filter.DateFrom.AddDays(7);
                }
                yAxis1.Interval = this.Interval(serie5.Data);
                yAxis1.Max = (int)serie5.Data.Max(x => x.Value) + yAxis1.Interval;

                yAxis2.Interval = this.Interval(serie4.Data);
                yAxis2.Max = (int)serie4.Data.Max(x => x.Value) + yAxis2.Interval;

                model.YAxis = new List<Axis>() { yAxis1, yAxis2 };
                model.XAxis = new Axis() { Data = dataAxis };
                model.DataLegend = dataLegend;
                model.Series = new List<Serie>() { serie1, serie2, serie3, serie4 };

                return model;
            }
            catch (Exception e)
            {

                throw;
            }


        }
        public async Task<List<Tuple<int, string>>> FilterFailureMode(FailureModeFilter filter)
        {
            var alertByOrdenesSap = await this.alertByOrdenesSapViewRepository.ListAsync(null, null, filter);
            var alertByOrdenesSapDis = alertByOrdenesSap.Select(x => new Tuple<int, string>(x.IdModoFallo, x.NombreTipoFallo)).Distinct().ToList();
            return alertByOrdenesSapDis;
        }
        private async Task WeekSetting(ReporteHistoricoFilter filter)
        {
            var dayOfWeekFrom = filter.DateFrom.DayOfWeek;
            var subDay = 0;
            var sumDay = 0;
            switch (dayOfWeekFrom)
            {
                case DayOfWeek.Sunday:
                    subDay = -6;
                    break;
                case DayOfWeek.Monday:
                    sumDay = 6;
                    break;
                case DayOfWeek.Tuesday:
                    subDay = -1;
                    sumDay = 5;
                    break;
                case DayOfWeek.Wednesday:
                    subDay = -2;
                    sumDay = 4;
                    break;
                case DayOfWeek.Thursday:
                    subDay = -3;
                    sumDay = 3;
                    break;
                case DayOfWeek.Friday:
                    subDay = -4;
                    sumDay = 2;
                    break;
                case DayOfWeek.Saturday:
                    subDay = -5;
                    sumDay = 1;
                    break;
                default:
                    break;
            }
            filter.DateFrom = filter.DateFrom.AddDays(subDay);
            filter.DateTo = filter.DateTo.AddDays(sumDay); 
        }
        private async Task<( int, int)> WeeklyPeriod(ReporteHistoricoFilter filter)
        {        
            CultureInfo myCI = new CultureInfo("es-ES");
            Calendar myCal = myCI.Calendar;
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
            var weekFrom = myCal.GetWeekOfYear(filter.DateFrom, myCWR, myFirstDOW);
            var weekTo = myCal.GetWeekOfYear(filter.DateTo, myCWR, myFirstDOW);
            return (weekFrom, weekTo);
        }
        private int Interval(List<Data> datas)
        {
            var datasOrder = datas.OrderByDescending(x => x.Value).ToArray();
            var min = 99999;
            var max = 0;
            for (int i = 0; i < datasOrder.Length; i++)
            {
                if (i < datasOrder.Length - 1)
                {
                    var value = datasOrder[i].Value - datasOrder[i + 1].Value;
                    if (value < min && value != 0)
                        min = Convert.ToInt32(value.Value);
                    if (value > max)
                        max = Convert.ToInt32(value.Value);
                }

            }

            return (min + max) / 2;
        }
    }
}
