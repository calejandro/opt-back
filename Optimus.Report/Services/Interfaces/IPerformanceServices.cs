﻿using Optimus.Report.Models;
using Optimus.Repositories.Filers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Services.Interfaces
{
    public interface IPerformanceServices
    {
        Task<PerformanceTableModel> GeneratePerformanceTableModel(ReporteHistoricoFilter filter);
        Task<BarModel> GenerateParettoModel(ParettoFilter filter);
        Task<BarModel> DifferenceBarsModel(ParettoFilter filter);

    }
}
