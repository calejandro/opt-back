﻿using Optimus.Report.Models;
using Optimus.Repositories.Filers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Services.Interfaces
{
    public interface IMachineService
    {
        Task<BarModel> GenerateFailureModeModel(FailureModeFilter filter);
        Task<StackedBarModel> GenerateFailureModeEvolutionModel(FailureModeFilter filter);
        Task<StackedBarModel> GenerateStateEvolutionModel(StateEvolutionFilter filter);
        Task<List<Tuple<int, string>>> FilterFailureMode(FailureModeFilter filter);
    }

}
