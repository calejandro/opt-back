﻿using Optimus.Report.Models;
using Optimus.Repositories.Filers;
using System.Threading.Tasks;

namespace Optimus.Report.Services.Implementations
{
    public interface IEfficiencyServices
    {
        Task<BarModel> GenerateEfficiencyModel(EfficiencyFilter filter);
        Task<PieModel> GenerateParticipationModel(ParticipationFilter filter);
        Task<CardModel> GenerateCardLineaEfficiencyModel(EfficiencyFilter filter);
        Task<CardModel> GenerateCardProducedTimeModel(ReporteHistoricoFilter filter);
        Task<CardModel> GenerateCardUnitCaseModel(ReporteHistoricoFilter filter);
    }
}
