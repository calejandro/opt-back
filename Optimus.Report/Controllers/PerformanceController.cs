﻿using System;
using System.Threading.Tasks;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using Optimus.Report.Services.Implementations;
using Optimus.Report.Services.Interfaces;
using Optimus.Repositories.Filers;

namespace Optimus.Report.Controllers
{
    [Route("reports/[controller]")]
    public class PerformanceController : Controller
    {
        private readonly IPerformanceServices performanceServices;
        public PerformanceController(IPerformanceServices performanceServices)
        {
           this.performanceServices = performanceServices;
        }
        [HttpGet("Table")]
        public async Task<IActionResult> PerformanceTableAsync([FromQuery] ReporteHistoricoFilter filter = null)
        {
            var model = await this.performanceServices.GeneratePerformanceTableModel(filter);
            return View("PerformanceTable", model);
        }
        [HttpGet("Paretto")]
        public async Task<IActionResult> ParettoAsync([FromQuery] ParettoFilter filter = null)
        {
            var model = await this.performanceServices.GenerateParettoModel(filter);
            return View("Paretto", model);
        }
        [HttpGet("DifferenceBars")]
        public async Task<IActionResult> DifferenceBarsAsync([FromQuery] ParettoFilter filter = null)
        {
            var model = await this.performanceServices.DifferenceBarsModel(filter);
            return View("DifferenceBars",model);
        }

    }
}
