﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Optimus.Report.Models;
using System.Text.Json;
using Optimus.Report.Services.Implementations;
using Optimus.Repositories.Filers;
using Optimus.Report.Services.Interfaces;
using System.Threading.Tasks;

namespace Optimus.Report
{
    [Route("reports/[controller]")]
    public class MachineController : Controller
    {
        private readonly IMachineService machineOpenedService;
        public MachineController(IMachineService machineOpenedService)
        {
            this.machineOpenedService = machineOpenedService;
        }

        [HttpGet("FailureMode/Paretto")]
        public async Task<IActionResult> FailureModeAsync([FromQuery] FailureModeFilter filter = null)
        {
            var model = await this.machineOpenedService.GenerateFailureModeModel(filter);
            return View("FailureMode",model);
        }

        [HttpGet("FailureModeEvolution")]
        public async Task<IActionResult> FailureModeEvolutionAsync([FromQuery] FailureModeFilter filter = null)
        {
            var model = await this.machineOpenedService.GenerateFailureModeEvolutionModel(filter);
            return View("FailureModeEvolution", model);
        }
        [HttpGet("StateEvolution")]
        public async Task<IActionResult> StateEvolutionAsync([FromQuery] StateEvolutionFilter filter = null)
        {
            var model = await this.machineOpenedService.GenerateStateEvolutionModel(filter);
            return View("StateEvolution", model);
        }
        [HttpGet("FailureMode/Filter")]
        public async Task<List<Tuple<int, string>>> FilterFailureModeAsync([FromQuery] FailureModeFilter filter = null)
        {
            return await this.machineOpenedService.FilterFailureMode(filter);

        }
    }
}
