﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Optimus.Report.Models;
using System.Text.Json;
using Optimus.Report.Services.Implementations;
using Optimus.Repositories.Filers;
using System.Threading.Tasks;

namespace Optimus.Report
{
    [Route("reports/[controller]")]
    public class ProductivityController : Controller
    {
        private readonly IEfficiencyServices efficiencySevices;
        public ProductivityController( IEfficiencyServices efficiencySevices)
        {
            this.efficiencySevices = efficiencySevices;
        }

        [HttpGet("Efficiency")]
        public async Task<IActionResult> EfficiencyAsync([FromQuery] EfficiencyFilter filter = null)
        {
            var model = await this.efficiencySevices.GenerateEfficiencyModel(filter);
            return View("Efficiency", model);
        }

        [HttpGet("Participation")]
        public async Task<IActionResult> ParticipationAsync([FromQuery] ParticipationFilter filter = null)
        {
            var model = await this.efficiencySevices.GenerateParticipationModel(filter);

            return View("Participation", model);
        }
        [HttpGet("CardLineaEfficiency")]
        public async Task<IActionResult> CardLineaEfficiencyAsync([FromQuery] EfficiencyFilter filter = null)
        {
            var model = await this.efficiencySevices.GenerateCardLineaEfficiencyModel(filter);

            return View("Cards", model);
        }
        [HttpGet("CardProducedTime")]
        public async Task<IActionResult> CardProducedTimeAsync([FromQuery] ReporteHistoricoFilter filter = null)
        {
            var model = await this.efficiencySevices.GenerateCardProducedTimeModel(filter);

            return View("Cards", model);
        }
        [HttpGet("CardUnitCase")]
        public async Task<IActionResult> CardUnitCaseAsync([FromQuery] ReporteHistoricoFilter filter = null)
        {
            var model = await this.efficiencySevices.GenerateCardUnitCaseModel(filter);

            return View("Cards", model);
        }
    }
}
