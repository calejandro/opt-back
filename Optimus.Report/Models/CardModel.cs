﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class CardModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string PrincipalValue { get; set; }
        public string PrincipalColor { get; set; }
        public string YearValue { get; set; }
        public string YearColor { get; set; }
        public string MonthValue { get; set; }
        public string MonthColor { get; set; }

        public string ObjValue { get; set; }
    }
}
