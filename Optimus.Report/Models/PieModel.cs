﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class PieModel
    {
        public double? Max { get; set; }
        public double? Min { get; set; }
        [JsonProperty("serie")]
        public Serie Serie { get; set; }
        [JsonProperty("data")]
        public List<Data> Data { get; set; }

    }
}
