﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class Serie
    {
        [JsonProperty("data")]
        public List<Data> Data { get; set; }
        [JsonProperty("type")]
        public string? Type { get; set; }
        [JsonProperty("label")]
        public Label Label { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public Serie()
        {
            Data = new List<Data>();
            Label = new Label();
        }

    }

}
