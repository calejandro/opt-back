﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class Axis
    {
        [JsonProperty("type")]
        public string? Type { get; set; }
        [JsonProperty("data")]
        public List<string>? Data { get; set; }
        [JsonProperty("max")]
        public int? Max { get; set; }
        [JsonProperty("interval")]
        public int? Interval { get; set; }

        public Axis()
        {
            this.Data = new List<string>();
        }

    }
}
