﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class Label
    {
        [JsonProperty("fontSize")]
        public int? FontSize { get; set; }
        [JsonProperty("color")]
        public string? Color { get; set; }
    }
}
