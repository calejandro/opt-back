﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Optimus.Models.Enum;
using Optimus.Models.Views;

namespace Optimus.Report.Models
{
    public class MachineData
    {
        public int IdEquipo { get; set; }
        public string DescEquipo { get; set; }
        public double? ND { get; set; }
        public double? PO { get; set; }
        public double? FP { get; set; }
        public double? PR{ get; set; }
        public double? PP { get; set; }
        public double? PA { get; set; }
        public double? PD { get; set; }
        public double? PF { get; set; }
        public double? SV { get; set; }
        public double? TMEF { get; set; }
        public double? TMPR { get; set; }

        public TipoAlertaParadaEnum? TMEFAler { get; set; }
        public TipoAlertaParadaEnum? TMPRAler { get; set; }
        public TipoAlertaParadaEnum? PPAler { get; set; }

    }
}
