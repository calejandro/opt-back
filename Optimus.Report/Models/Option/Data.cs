﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class Data 
    {
        [JsonProperty("value")]
        public double? Value { get; set; }
        [JsonProperty("name")]
        public string? Name { get; set; }
        [JsonProperty("itemStyle")]
        public ItemStyle ItemStyle { get; set; }
        [JsonProperty("label")]
        public Label Label { get; set; }
        
        [JsonProperty("children")]
        public List<Data> Children { get; set; }
    }
    public class ItemStyle
    {
        [JsonProperty("color")]
        public string? Color { get; set; }
    }

}
