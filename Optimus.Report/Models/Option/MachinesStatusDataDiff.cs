﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models.Option
{
    public class MachinesStatusDataDiff
    {
        public double Diff { get; set; }
        public int? IdEquipo { get; set; }
        public string? DesEquipo { get; set; }

    }
}
