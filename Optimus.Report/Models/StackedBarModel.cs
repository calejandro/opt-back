﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Optimus.Models.Views;

namespace Optimus.Report.Models
{
    public class StackedBarModel
    {
        [JsonProperty("xAxis")]
        public Axis XAxis { get; set; }
        [JsonProperty("yAxis")]
        public List<Axis> YAxis { get; set; }
        [JsonProperty("series")]
        public List<Serie> Series { get; set; }
        [JsonProperty("data")]
        public List<string> DataLegend { get; set; }
        public StackedBarModel()
        {
            this.XAxis = new Axis();
            this.YAxis = new List<Axis>();
            this.Series = new List<Serie>();
            this.DataLegend = new List<string>();
        }
    }
}
