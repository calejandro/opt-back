﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimus.Report.Models
{
    public class CauseFailureModel
    {
        public List<PieModel> PiesModel { get; set; }
    }
}
