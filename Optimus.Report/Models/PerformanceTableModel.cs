﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Optimus.Models.Views;

namespace Optimus.Report.Models
{
    public class PerformanceTableModel
    {
        public List<MachineData> MachineData { get; set; }
        
        public PerformanceTableModel()
        {
            MachineData = new List<MachineData>();
        }
    }
}
