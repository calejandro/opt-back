﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Optimus.Report.Models
{
    public class BarModel
    {
        [JsonProperty("xAxis")]
        public Axis XAxis { get; set; }
        [JsonProperty("yAxis")]
        public Axis YAxis { get; set; }
        [JsonProperty("series")]
        public List<Serie> Series { get; set; }
        
    }
}
