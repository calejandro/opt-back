﻿using System;
using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class LineaProduccionObjetivosConfigController : BaseApiMapperController<LineasProduccionObjetivosConfigViewModel, LineasProduccionObjetivosConfigViewModel, Guid, LineasProduccionObjetivosConfigFilter, ILineaProduccionObjetivosConfigService>
    {
        public LineaProduccionObjetivosConfigController(ILineaProduccionObjetivosConfigService service, IMapper mapper): base(service, mapper)
        {
        }
    }
}
