﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;
using System;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class SapOrdenesProduccionController : BaseApiMapperController<SapOrdenesProduccionViewModel, SapOrdenesProduccionCreateViewModel, int, SapOrdenesProduccionFilter, ISapOrdenesProduccionService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public SapOrdenesProduccionController(ISapOrdenesProduccionService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

        [HttpPut("linkTemporalOrder")]
        public async Task<SapOrdenesProduccionViewModel> LinkTemporalOrder([FromBody] LinkTemporalOrder linkTemporalOrder)
        {
            return await this.genericService.LinkTemporalOrder(linkTemporalOrder);
        }


    }
}