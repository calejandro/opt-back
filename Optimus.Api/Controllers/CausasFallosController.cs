﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class CausasFallosController : BaseApiMapperController<CausasFallosViewModel, CausasFallosCreateViewModel, int, TipoFallosFilter, ICausasFallosService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public CausasFallosController(ICausasFallosService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}