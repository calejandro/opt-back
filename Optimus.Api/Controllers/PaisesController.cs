﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class PaisesController : BaseApiMapperController<PaisesViewModel, PaisesCreateViewModel, int, BaseFilter, IPaisesService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public PaisesController(IPaisesService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}