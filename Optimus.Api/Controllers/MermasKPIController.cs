﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Services.Interfaces;

namespace Optimus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MermasKPIController : ControllerBase
    {
        private readonly IStoreProceduresMongoService storeProceduresServiceMongo;
        public MermasKPIController(IStoreProceduresMongoService storeProceduresServiceMongo)
        {
            this.storeProceduresServiceMongo = storeProceduresServiceMongo;
        }

        [HttpGet("ForBottles")]
        public async Task<List<KpiDepletion>> GetKpiDepletionForBottles([FromQuery]int idLinea, [FromQuery][Required] DateTime fechaHoraInicio, [FromQuery][Required] DateTime fechaHoraFin, [FromQuery] GroupingTypeEnum tipoAgrupacion)
        {
            return await this.storeProceduresServiceMongo.GetKpiDepletionForBottlesMongo(idLinea, fechaHoraInicio, fechaHoraFin, tipoAgrupacion);
        }
    }
}