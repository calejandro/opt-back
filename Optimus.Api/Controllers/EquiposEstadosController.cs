﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class EquiposEstadosController : BaseApiMapperController<EquiposEstadosViewModel, EquiposEstadosCreateViewModel, int, EquiposEstadosFilter, IEquiposEstadosService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public EquiposEstadosController(IEquiposEstadosService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}