﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Identity;
using System;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class UserController : BaseApiMapperController<UserViewModel, UserCreateViewModel, Guid, UserFilter, IUserService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public UserController(IUserService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

        [AllowAnonymous]
        public override async Task<ActionResult<UserViewModel>> Create([FromBody] UserCreateViewModel entity)
        {
            return await this.genericService.Register(entity);
        }
        [AllowAnonymous]
        [HttpPost("confirm")]
        public async Task ConfirmEmail([FromBody] UserEmailConfirmation userEmailConfirmation)
        {
            await this.genericService.ConfirmEmail(userEmailConfirmation);
        }
        [AllowAnonymous]
        [HttpPost("SendResetPassword")]
        public async Task SendResetPassword([FromBody] UserSendResetPassword userSendResetPassword)
        {
            await this.genericService.SendResetPassword(userSendResetPassword);
        }
        [AllowAnonymous]
        [HttpPost("ResetPassword")]
        public async Task ResetPassword([FromBody] UserResetPassword userResetPassword)
        {
            await this.genericService.ResetPassword(userResetPassword);
        }
        public async override Task<UserViewModel> Update([FromBody] UserCreateViewModel entity)
        {
            return await this.genericService.UpdateProfile(entity);

        }

        [HttpPut("Undelete")]
        public async Task<ActionResult<UserViewModel>> Undelete([FromBody] UserViewModel entity)
        {
            return await this.genericService.Undelete(entity);
        }

        [HttpGet("exisit-by-email")]
        public async Task<bool> ExistByEmail([FromQuery] string email)
        {
            return await this.genericService.ExistsByEmail(email, UserId.Value);
        }
    }
}