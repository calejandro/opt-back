﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Models.Views;
using Optimus.Services.Interfaces;

namespace Optimus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TurnosController : ControllerBase
    {
        private readonly IStoreProceduresService storeProceduresService;

        public TurnosController(IStoreProceduresService storeProceduresService)
        {
            this.storeProceduresService = storeProceduresService;
        }
        [HttpGet]
        public  List<Shift> GetShifts([FromQuery][Required] DateTime fecha)
        {
            return this.storeProceduresService.GetShifts(fecha);
        }

    }
}