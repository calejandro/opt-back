﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Api.Controllers
{
    public class KpiController : BaseApiMapperController<KpiViewModel, KpiViewModel, Guid, BaseFilter, IKpiService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public KpiController(IKpiService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}