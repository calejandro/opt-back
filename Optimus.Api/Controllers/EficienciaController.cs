﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Models.Enum;
using Optimus.Models.Views;
using Optimus.Services.Interfaces;
using Optimus.Services.Interfaces.Mongo;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EficienciaController : ControllerBase
    {
        private readonly IStoreProceduresMongoService storeProceduresServiceMongo;
        private readonly IAcumEficienciasService acumEficienciasService;
        public EficienciaController( IStoreProceduresMongoService storeProceduresServiceMongo, IAcumEficienciasService acumEficienciasService)
        {
            this.storeProceduresServiceMongo = storeProceduresServiceMongo;
            this.acumEficienciasService = acumEficienciasService;
        }

        [HttpGet]
        public async Task<List<ProduccionEficiencia>> GetEfficiencyProduction([FromQuery]int idLinea, [FromQuery] List<int> idLineas, [FromQuery][Required] DateTime fechaHoraInicio, [FromQuery][Required] DateTime fechaHoraFin)
        {
            return await this.storeProceduresServiceMongo.GetEfficiencyProductionMongo(idLinea, idLineas, fechaHoraInicio, fechaHoraFin);
        }
        [HttpGet("ByShift")]
        public async Task<List<ProduccionEficienciaByShift>> GetEfficiencyProductionByShift([FromQuery]int idLinea, [FromQuery] List<int> idLineas, [FromQuery][Required] DateTime fechaHoraInicio, [FromQuery][Required] DateTime fechaHoraFin)
        {
            return await this.storeProceduresServiceMongo.GetEfficiencyProductionByShiftMongo(idLinea, idLineas, fechaHoraInicio, fechaHoraFin);
        }
        [HttpGet("{idLinea}/Acum")]
        public async Task<AcumEficienciasViewModels> GetAcumEficienciaDiariaMensulAnual([FromRoute][Required] int idLinea,[FromQuery][Required] DateTime date)
        {
            return await this.acumEficienciasService.GetAcumEficienciaDiariaMensulAnual(idLinea, date);
        }
        [HttpGet("{idLinea}/AcumByPeriodo")]
        public async Task<AcumEficienciasPorPeriodoViewModels> GetAcumEficienciaPorPeriodo([FromRoute][Required] int idLinea, [FromQuery][Required] PeriodEfficiencyEnum periodo, [FromQuery][Required] DateTime date)
        {
            return await this.acumEficienciasService.GetAcumEficienciaPorPeriodo(idLinea, date, periodo);
        }

    }
}