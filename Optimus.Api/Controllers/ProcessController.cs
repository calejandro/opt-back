﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Api.Controllers
{
    public class ProcessController : BaseApiMapperController<ProcessViewModel, ProcessViewModel, Guid, BaseFilter, IProcessService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public ProcessController(IProcessService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}