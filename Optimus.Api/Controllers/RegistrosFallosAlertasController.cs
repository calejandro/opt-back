﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Api.Controllers
{
    public class RegistrosFallosAlertasController : BaseApiMapperController<RegistrosFallosAlertasViewModel, RegistrosFallosAlertasCreateViewModel, int, RegistrosFallosAlertasFilter, IRegistrosFallosAlertasService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public RegistrosFallosAlertasController(IRegistrosFallosAlertasService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}