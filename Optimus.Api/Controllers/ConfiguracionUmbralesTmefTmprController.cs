﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class ConfiguracionUmbralesTmefTmprController : BaseApiMapperController<ConfiguracionUmbralesTmefTmprViewModel, ConfiguracionUmbralesTmefTmprCreateViewModel, int, CommonFilter, IConfiguracionUmbralesTmefTmprService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public ConfiguracionUmbralesTmefTmprController(IConfiguracionUmbralesTmefTmprService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}