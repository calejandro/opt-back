﻿using AutoMapper;
using AutoWrapper.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class LineasProduccionAlertasController : BaseApiMapperController<LineasProduccionAlertasViewModel, LineasProduccionAlertasCreateViewModel, int, LineaProduccionAlertaFilter, ILineasProduccionAlertasService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public LineasProduccionAlertasController(ILineasProduccionAlertasService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }
        [HttpPost("{idAlert}/image")]
        public async Task<IActionResult> InsertImagLineasProduccionAlertas([FromRoute] int idAlert, [FromForm] InsertImagLineasProduccionAlertasViewModel form )
        {
            await this.genericService.InsertImagLineasProduccionAlertas(form.Images, idAlert);
            return new ObjectResult("OK");
        }
        [HttpGet("{idImg}/image")]
        [AutoWrapIgnore]
        public async Task<IActionResult> GetImage( Guid idImg)
        {
            var (str, name) = await this.genericService.GetImagLineasProduccionAlertas(idImg);
            return File(str, "image/jpeg", name);
        }
    }
}