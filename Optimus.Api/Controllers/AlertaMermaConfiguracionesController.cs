﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Api.Controllers
{
    public class AlertaMermaConfiguracionesController : BaseApiMapperController<AlertaMermaConfiguracionesViewModel, AlertaMermaConfiguracionesCreateViewModel, int, AlertaMermaFilter, IAlertaMermaConfiguracionesService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public AlertaMermaConfiguracionesController(IAlertaMermaConfiguracionesService alertaMermaConfiguracionesService, IMapper mapper) : base(alertaMermaConfiguracionesService, mapper)
        {
        }
    }
}