﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class TiposFallosController : BaseApiMapperController<TiposFallosViewModel, TiposFallosCreateViewModel, int, TipoFallosFilter, ITiposFallosService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public TiposFallosController(ITiposFallosService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}