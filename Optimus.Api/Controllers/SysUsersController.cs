﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class SysUsersController : BaseApiMapperController<SysUsersViewModel, SysUsersViewModel, int, UserFilter, ISysUsersService>
    {

        private readonly ILineasProduccionUsuarioService lineasProduccionUsuarioService;
        private readonly IUserService userService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public SysUsersController(ISysUsersService systemModuleService,
            ILineasProduccionUsuarioService lineasProduccionUsuarioService,
            IUserService userService,
            IMapper mapper) : base(systemModuleService, mapper)
        {
            this.lineasProduccionUsuarioService = lineasProduccionUsuarioService;
            this.userService = userService;
        }
        [HttpGet("addSysUser")]
        /// <summary>
        /// Agrega a la tabla sysUser el userIdentity logiado actualmente 
        /// </summary>
        public async Task AddSysUser()
        {
            await this.genericService.AddSysUser(UserId.Value);
        }
        [HttpPost("assignUsertoPlants")]
        public async Task<SysUsersViewModel> AssignUserToPlants([FromBody] SysUsersViewModel user)
        {
            return await this.genericService.AssignUserToPlants(user);
        }

        [HttpPut("{sysUserId}/add/{groupId}")]
        public async Task AddGroup(int sysUserId, int groupId)
        {
            await this.genericService.AddGroup(sysUserId, groupId);
        }

        [HttpDelete("{sysUserId}/{groupId}")]
        public async Task RemoveGroup(int sysUserId, int groupId)
        {
            await this.genericService.RemoveGroup(sysUserId, groupId);
        }

        [HttpGet("me")]
        public async Task<SysUsersViewModel> Me([FromQuery]string[] includes)
        {
            return await this.genericService.GetSysUserByUserCognito(UserId.Value, includes);
        }

        [HttpGet("me/lineas")]
        public async Task<List<LineasProduccionUsuarioViewModel>> MeLineas([FromQuery] string[] includes)
        {
            return await this.lineasProduccionUsuarioService.ListAsync(includes, new LineasProduccionUsuarioFilter
            {
                IsTake = true,
                UserId = (await this.genericService.GetSysUserByUserCognito(UserId.Value, null)).UserId                
            });
        }
    }
}