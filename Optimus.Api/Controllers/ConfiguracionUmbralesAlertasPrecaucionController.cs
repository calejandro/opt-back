﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class ConfiguracionUmbralesAlertasPrecaucionController : BaseApiMapperController<ConfiguracionUmbralesAlertasPrecaucionViewModel, ConfiguracionUmbralesAlertasPrecaucionCreateViewModel, int, CommonFilter, IConfiguracionUmbralesAlertasPrecaucionService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public ConfiguracionUmbralesAlertasPrecaucionController(IConfiguracionUmbralesAlertasPrecaucionService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}