﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Models.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class UserGroupController : BaseApiMapperController<UserGroupViewModel, UserGroupViewModel, int, BaseFilter, IUserGroupService>
    {
        public UserGroupController(IUserGroupService userGroupService, IMapper mapper): base(userGroupService, mapper)
        {
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public override Task DeleteAsync([FromRoute] int id)
        {
            throw new NotImplementedException();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public override Task<UserGroupViewModel> Update([FromBody] UserGroupViewModel entity)
        {
            throw new NotImplementedException();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public override Task<ActionResult<UserGroupViewModel>> Create([FromBody] UserGroupViewModel entity)
        {
            throw new NotImplementedException();
        }

    }
}
