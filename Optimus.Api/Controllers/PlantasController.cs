﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class PlantasController : BaseApiMapperController<PlantasViewModel, PlantasCreateViewModel, int, BaseFilter, IPlantasService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public PlantasController(IPlantasService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}