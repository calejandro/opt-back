﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Models.Views;
using Optimus.Services.Interfaces;

namespace Optimus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerformanceEquiposController : ControllerBase
    {
        private readonly IStoreProceduresMongoService storeProceduresServiceMongo;
        public PerformanceEquiposController(IStoreProceduresMongoService storeProceduresServiceMongo)
        {
            this.storeProceduresServiceMongo = storeProceduresServiceMongo;
        }

        [HttpGet("ActualStatus")]
        public async Task<List<MachineActualStatus>> GetMachineActualStatus([FromQuery]int idLinea)
        {
            return await this.storeProceduresServiceMongo.GetMachineActualStatusMongo(idLinea);
        }

        [HttpGet("Evolucion-ParadaPropia")]
        public async Task<List<EvolucionParadaPropiaHora>> GetEvolucionParadaPropia([FromQuery] int idLinea, [FromQuery] int idEquipo)
        {
            return await this.storeProceduresServiceMongo.GetEvolucionParadaPropiaHora(idLinea, idEquipo);
        }

        [HttpGet("ProductionOrders")]
        public async Task<List<MachineStatusProductionOrder>> GetMachineStatusProductionOrders([FromQuery]int idLinea, [FromQuery] DateTime fechaHoraInicio, [FromQuery] DateTime fechaHoraFin)
        {
            return await this.storeProceduresServiceMongo.GetMachineStatusProductionOrdersMongo(idLinea, fechaHoraInicio, fechaHoraFin);
        }
        [HttpGet("TimeStops")]
        public async Task<List<TimeStops>> GetTimeStops([FromQuery]int idLinea, [FromQuery] DateTime fechaHoraInicio, [FromQuery] DateTime fechaHoraFin, [FromQuery] int idEstado)
        {
            return await this.storeProceduresServiceMongo.GetTimeStopsMongo(idLinea, fechaHoraInicio, fechaHoraFin, idEstado);
        }
        [HttpGet("AverageTimeFailuresProduction")]
        public async Task<List<AverageTimeFailuresProduction>> AverageTimeFailuresProduction([FromQuery]int idLinea, [FromQuery] DateTime fechaHoraInicio, [FromQuery] DateTime fechaHoraFin)
        {
            return await this.storeProceduresServiceMongo.AverageTimeFailuresProductionMongo(idLinea, fechaHoraInicio, fechaHoraFin);
        }


    }
}