﻿using AutoMapper;
using AutoWrapper.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Services;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Optimus.Commons.Entities;

namespace Optimus.Api.Controllers
{
    public class OptimusTaskController : BaseApiMapperController<OptimusTaskViewModel, OptimusTaskCreateViewModel, Guid, OptimusTaskFilter, IOptimusTaskService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        /// 
        public OptimusTaskController(IOptimusTaskService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public override Task DeleteAsync([FromRoute] Guid id)
        {
            throw new NotImplementedException();
        }
        //[HttpPut("{idOptimusTask}/archive-unarchive")]
        //public async Task<OptimusTaskViewModel> ArchiveUnarchiveAsync([FromRoute] Guid idOptimusTask)
        //{
        //    return await this.genericService.ArchiveUnarchiveAsync(idOptimusTask);
        //}
        [HttpGet("myOptimusTaskList")]

        public  async Task<IEnumerable<OptimusTaskViewModel>> MyOptimusTaskList([FromQuery] TypeUserToOptimusTaskEnum typeUser,[FromQuery] string[] includes = null, [FromQuery] OptimusTaskFilter filter=null, [FromQuery] PaginatorRequestModel paginator = null,[FromQuery] string order = null)
        {
            return await this.genericService.MyOptimusTaskListAsync(UserId.Value, typeUser,includes,filter, paginator, order == null ? null : JsonConvert.DeserializeObject<List<Order>>(order));
        }
        public async override Task<ActionResult<OptimusTaskViewModel>> Create([FromBody] OptimusTaskCreateViewModel entity)
        {
            return  await this.genericService.InsertAsync(entity, UserId.Value);
        }
        [HttpGet("myOptimusTaskList/Count")]
        public async Task<int> MyOptimusTaskCount([FromQuery] TypeUserToOptimusTaskEnum typeUser, [FromQuery] OptimusTaskFilter filter = null, [FromQuery] string[] includes = null)
        {
            return await this.genericService.MyOptimusTaskCountAsync(UserId.Value,typeUser,filter, includes);
        }
        public override Task<OptimusTaskViewModel> Update([FromBody] OptimusTaskCreateViewModel entity)
        {
            return this.genericService.UpdateAsync(UserId.Value,entity);
        }
        

    }
}