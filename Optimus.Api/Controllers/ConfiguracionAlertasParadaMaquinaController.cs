﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class ConfiguracionAlertasParadaMaquinaController : BaseApiMapperController<ConfiguracionAlertasParadaMaquinaViewModel, ConfiguracionAlertasParadaMaquinaCreateViewModel, int, ConfigAlertasParadaMaqFilter, IConfiguracionAlertasParadaMaquinaService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public ConfiguracionAlertasParadaMaquinaController(IConfiguracionAlertasParadaMaquinaService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}