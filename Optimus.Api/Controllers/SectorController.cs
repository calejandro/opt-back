﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;

namespace Optimus.Api.Controllers
{
    public class SectorController : BaseApiMapperController<SectorViewModel, SectorViewModel, Guid, BaseFilter, ISectorService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public SectorController(ISectorService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}