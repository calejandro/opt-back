﻿using AutoMapper;
using AutoWrapper.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class FileUrlController : BaseApiMapperController<FileUrlViewModel, FileUrlViewModel, Guid, FileUrlFilter, IFileUrlService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileUrlService"></param>
        /// <param name="mapper"></param>
        /// 
        public FileUrlController(IFileUrlService fileUrlService, IMapper mapper) : base(fileUrlService, mapper)
        {
        }

        [HttpPost("{idEntity}/files")]

        public async Task<IActionResult> InsertFile([FromRoute] Guid idEntity, [FromForm] InsertFileUrlViewModel from)
        {
            await this.genericService.InsertFiles(from.Files, from.ToBelong, idEntity);
            return new ObjectResult("OK");
        }
        [HttpGet("{idFileUrl}/downloadFile")]
        [AutoWrapIgnore]
        public async Task<IActionResult> DownloadFile(Guid idFileUrl)
        {
            var (str, name,type) = await this.genericService.DownloadFile(idFileUrl);
            return File(str, type, name);
        }

    }
}