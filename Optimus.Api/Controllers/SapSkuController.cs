﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class SapSkuController : BaseApiMapperController<SapSkuViewModel, SapSkuCreateViewModel, int, SapSkuFilter, ISapSkuService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public SapSkuController(ISapSkuService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {

        }
        [HttpGet("ListBrand")]
        public async Task<List<string>> ListBrand([FromQuery] SkuByLineViewFilter filter )
        {
            return await this.genericService.ListBrand(filter);
        }
        [HttpGet("ListFlavor")]
        public async Task<List<string>> ListFlavor([FromQuery] SkuByLineViewFilter filter)
        {
            return await this.genericService.ListFlavor(filter);
        }
        [HttpGet("ListFormat")]
        public async Task<List<string>> ListFormat([FromQuery] SkuByLineViewFilter filter)
        {
            return await this.genericService.ListFormat(filter);
        }
        [HttpGet("ListSelectSKU")]
        public async Task<List<Tuple<int, string>>> ListSelectSKU([FromQuery] SkuByLineViewFilter filter)
        {
            return await this.genericService.ListSelectSKU(filter);
        }

    }
}