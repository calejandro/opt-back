﻿using AutoMapper;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;

namespace Optimus.Api.Controllers
{
    public class LineasProduccionEficienciaMesController : BaseApiMapperController<LineasProduccionEficienciaMesViewModel, LineasProduccionEficienciaMesCreateViewModel, int, LineaFilters, ILineasProduccionEficienciaMesService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public LineasProduccionEficienciaMesController(ILineasProduccionEficienciaMesService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

    }
}