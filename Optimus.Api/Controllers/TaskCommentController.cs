﻿using AutoMapper;
using AutoWrapper.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Models.Enum;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class TaskCommentController : BaseApiMapperController<TaskCommentViewModel, TaskCommentCreateViewModel, Guid, TaskCommentFilter, ITaskCommentService>
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public TaskCommentController(ITaskCommentService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }
        public async override Task<ActionResult<TaskCommentViewModel>> Create([FromBody] TaskCommentCreateViewModel entity)
        {
            return await this.genericService.InsertAsync(entity, UserId.Value);
        }
    }
}