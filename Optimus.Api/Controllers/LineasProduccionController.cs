﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Commons.Entities;
using Optimus.Commons.Services;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Optimus.Api.Controllers
{
    public class LineasProduccionController : BaseApiMapperController<LineasProduccionViewModel, LineasProduccionCreateViewModel, int, LineaFilters, ILineasProduccionService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public LineasProduccionController(ILineasProduccionService lineasProduccionService, IMapper mapper) : base(lineasProduccionService, mapper)
        {
        }

        [HttpGet("{id}/has-production-order")]
        public Task<bool> HasProductionOrders(int id)
        {
            return this.genericService.HasProductionOrders(id);
        }

        [HttpGet("{id}/orders")]
        public Task<List<SapOrdenesProduccionViewModel>> Orders(int id, [FromQuery, Required] List<DateTime> fechaInicio, [FromQuery] List<DateTime> fechaFin,
            [FromQuery] string[] includes)
        {
            return this.genericService.ListOrders(id, fechaInicio, fechaFin, includes);
        }



    }
}