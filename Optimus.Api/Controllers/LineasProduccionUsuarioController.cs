﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Optimus.Commons.Api.Controllers;
using Optimus.Repositories.Filers;
using Optimus.Services.Interfaces;
using Optimus.Services.ViewModels;
using Optimus.Services.ViewModels.Create;

namespace Optimus.Api.Controllers
{
    public class LineasProduccionUsuarioController : BaseApiMapperController<LineasProduccionUsuarioViewModel, LineasProduccionUsuarioCreateViewModel, int, LineasProduccionUsuarioFilter, ILineasProduccionUsuarioService>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="systemModuleService"></param>
        /// <param name="mapper"></param>
        public LineasProduccionUsuarioController(ILineasProduccionUsuarioService systemModuleService, IMapper mapper) : base(systemModuleService, mapper)
        {
        }

        [HttpPost("take")]
        public Task<List<LineasProduccionUsuarioTakeViewModel>> Take(List<LineasProduccionUsuarioTakeViewModel> lineasProduccionUsuarios)
        {
            return this.genericService.Take(lineasProduccionUsuarios, UserId.Value);
        }

        [HttpPost("clear")]
        public Task Clear()
        {
            return this.genericService.Clear(UserId.Value);
        }
        [HttpGet("{id}/is-valid-status-take")]
        public Task<bool> ValidateStatusTake(int id, [FromQuery] DateTime date )
        {
            return this.genericService.IsValidStatusTake(id,date);
        }
    }
}