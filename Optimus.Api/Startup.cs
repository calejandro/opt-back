using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Optimus.Commons.Extensions;
using Optimus.Models.Contexts;
using Optimus.Services.Extensions;
using Optimus.Repositories.Extensions;
using Optimus.Services.AutoMap;
using AutoMapper;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;
using Optimus.Models.Identity;
using Optimus.Commons.IdentityServer;
using Optimus.Services.Implementations;
using Optimus.Commons.Configurations;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using AutoWrapper;
using Microsoft.IdentityModel.Logging;
using Optimus.Services.Interfaces;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;
using Optimus.Commons.Services.Email;
using Optimus.Services.Support.Configuration;
using Optimus.Services.Support.Identity;
using Optimus.Commons.Ldap;
using Optimus.Commons.Services.Quartz;
using Optimus.Services.SignalR;
using MongoDB.Bson.Serialization;
using Optimus.Commons.Cognito;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Optimus.Commons.FileWriter.Interface;
using Optimus.Commons.FileWriter.Implementation;

namespace Optimus.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors((options) =>
            {
                options.AddPolicy("allcors", (policy) =>
                {
                    policy.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });
            services.Configure<AuthorizatorConfig>(Configuration.GetSection("AuthorizatorConfig"));
            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));
            services.Configure<EmailConfigOptions>(Configuration.GetSection("EmailAccount"));
            services.AddScoped<IEmailSender, EmailSender>();

            services.AddCommonsConfigurations(Configuration);

            services.AddCommonsServices();

            //services.AddInMemoryDbContext<ApiContext>("memory");

            //*TOKENISACION*
            services.AddIdentity<UserIdentity, RoleIdentity, Guid, IdentityContext, SpanishIdentityErrorDescriber>(Configuration);

            if(!Configuration["AuthorizatorConfig:AuthType"].Equals("Cognito"))
            {
                services.AddIdentityServer4<UserIdentity, Guid>(Configuration, "DefaultConnection", "Optimus.Models")
                    .AddResourceOwnerValidator<CustomResourcePasswordValidator>()
                    .AddProfileService<ProfileService>();
            }
            else
            {
                services.AddCognitoJwtAut(Configuration);
            }
            

            services.AddLdapServices<UserIdentity>(Configuration);

            services.Configure<LdapConfig>(Configuration.GetSection("LdapConfig"));

            services.AddSqlServerDbContext<OptimusContext>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<Optimus2Context>(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSqlServerDbContext<IdentityContext>(Configuration.GetConnectionString("DefaultConnection"));
            var mongoAuditDatabase = Configuration.GetSection("MongoDB:Database").Value;
            services.AddMongo<MongoContext>(Configuration.GetConnectionString("Mongo"), mongoAuditDatabase, (setting) =>
            {
                setting.UseTls = false;
            });
            BsonSerializer.RegisterSerializer(typeof(DateTime), new BsonUtcDateTimeSerializer());

            services.AddServices();

            services.AddJobsServices(Configuration);

            services.AddRepositories();

            services.AddQuartz();
            //services.AddSendGridConfigurations(Configuration);
            services.AddSignalR().AddNewtonsoftJsonProtocol(options =>
            {
                options.PayloadSerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.PayloadSerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            });

            services.Configure<RequestLocalizationOptions>(
            options =>
            {
              var supportedCultures = new List<CultureInfo>
               {
                        new CultureInfo("es-ES"),
               };

               options.DefaultRequestCulture = new RequestCulture(culture: "es-ES", uiCulture: "es-ES");
               options.SupportedCultures = supportedCultures;
               options.SupportedUICultures = supportedCultures;
            });

            services.AddAutoMapper(typeof(MapperProfile));

            services.AddSwagger("Optimus Api", "v1", (config) =>
            {                
                config.OperationFilter<AddRequiredHeaderParameter>(); // Add this here
            });

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
            });

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            });

            services.AddTransient<IFileWriter,FileWriter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            ILoggerFactory loggerfactory,
            ISeedService seedService,
            IOptions<AuthorizatorConfig> authConfig,
            IConfiguration configuration)
        {
            bool includeStackTraceInApiResponse = false;
            if(!authConfig.Value.AuthType.Equals("Cognito"))
            {
                IdentityServerInitialize.Initialize(app);
            }
            

            loggerfactory.AddSerilog();

            if (env.IsDevelopment() || env.IsEnvironment("Local"))
            {
                app.UseDeveloperExceptionPage();
                includeStackTraceInApiResponse = true;
                IdentityModelEventSource.ShowPII = true;
            }            

            app.UseHttpsRedirection();

            app.UseCors("allcors");

            Task.WaitAll(seedService.Seed());

            app.UseApiResponseAndExceptionWrapper(new AutoWrapperOptions
            {
                ShowApiVersion = true,
                ShowStatusCode = true,
                IsDebug = includeStackTraceInApiResponse,
                IsApiOnly = false,
                WrapWhenApiPathStartsWith = "/api"
            });

            if(!env.IsProduction())
            {
                app.UseHttpsRedirection();
            }             

            app.UseRouting();

            app.UseAuthorization();

            app.UseAuthentication();

            if (!authConfig.Value.AuthType.Equals("Cognito"))
            {
                app.UseIdentityServer();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Optimus API");
                c.DocExpansion(DocExpansion.None);
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSerilogRequestLogging();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<OptimusHub>("/optimus");
            });
        }
    }

    public class AddRequiredHeaderParameter : Swashbuckle.AspNetCore.SwaggerGen.IOperationFilter
    {

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();
            if (operation.Tags[0].Name == "AlertaMermaConfiguraciones"
                || operation.Tags[0].Name == "ConfiguracionAlertasParadaMaquina"
                || operation.Tags[0].Name == "ConfiguracionUmbralesTmefTmpr"
                || operation.Tags[0].Name == "Equipos"
                || operation.Tags[0].Name == "LineasProduccion"
                || operation.Tags[0].Name == "TiposFallos"
                || operation.Tags[0].Name == "LineasProduccionEficienciaMes"
                || operation.Tags[0].Name == "LineasProduccionUsuario"
                || operation.Tags[0].Name == "LineaProduccionObjetivosConfig")
            {
                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "plantaId",
                    In = ParameterLocation.Header,
                    Description = "Id Planta",
                    Required = true,
                    Schema = new OpenApiSchema { Type = "integer" }
                });
            }

        }
    }
}
