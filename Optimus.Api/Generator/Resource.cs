﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Microsoft.VisualStudio.Web.CodeGeneration.CommandLine;

namespace Optimus.Api.Generator
{
    [Alias("test")]
    public class Resource : ICodeGenerator
    {
        ILogger _logger;
        public Resource(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }
            _logger = logger;
        }
        public async Task GenerateCode(SampleGeneratorModel model)
        {
            _logger.LogMessage($"Model name: {model.Model}");
            await Task.CompletedTask;
        }
    }

    public class SampleGeneratorModel
    {
        [Option(Name = "model", ShortName = "m")]
        public string Model { get; set; }
    }
}
